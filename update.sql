### 创建用户对话消息表
# 2018-08-30
CREATE TABLE `chat_msg` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录ID',
  `from_id` int(10) unsigned NOT NULL COMMENT '会员ID',
  `from_name` varchar(50) NOT NULL COMMENT '会员名',
  `from_ip` varchar(15) NOT NULL COMMENT '发自IP',
  `to_id` int(10) unsigned NOT NULL COMMENT '接收会员ID',
  `to_name` varchar(50) NOT NULL COMMENT '接收会员名',
  `to_msg` varchar(300) DEFAULT NULL COMMENT '消息内容',
  `read_state` tinyint(1) unsigned DEFAULT '2' COMMENT '状态:1为已读,2为未读,默认为2',
  `from_delete` tinyint(1) DEFAULT '0' COMMENT '发送者是否删除：1是、0否。默认0',
  `to_delete` tinyint(1) DEFAULT '0' COMMENT '接收者是否删除：1是、0否。默认0',
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `user_id_index` (`from_id`,`to_id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COMMENT='消息表';

### 约展报名详情表
# 2018-08-30
ALTER TABLE `appointment_info`
ADD COLUMN `address` varchar(255) DEFAULT NULL COMMENT '地址信息',
ADD COLUMN `is_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '参与人是否删除：0未删，1已删除';

### 约展报名表
# 2018-08-30
ALTER TABLE `appointment_sign`
ADD COLUMN `address` varchar(255) DEFAULT NULL COMMENT '地址信息',
ADD COLUMN `is_read` tinyint(1) DEFAULT '0' COMMENT '是否阅读：1已阅读，0未阅读',
ADD COLUMN `is_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '发起人是否删除：0未删，1已删除';

### 创建感兴趣表
# 2018-08-30
CREATE TABLE `interested` (
  `initiator_user_id` int(11) unsigned NOT NULL COMMENT '发起约展用户ID',
  `participants_user_id` int(11) unsigned NOT NULL COMMENT '参与约展用户ID',
  `interested` tinyint(4) DEFAULT '1' COMMENT '是否感兴趣：0无操作、1感兴趣、2不感兴趣',
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='兴趣关联表';

### 约展报名表
# 2018-08-30
ALTER TABLE `article_comment`
ADD COLUMN `is_show` tinyint(2) DEFAULT '1' COMMENT '评论列表是否展示：1都显示，2仅评论人显示，3仅被评论人显示，3都不显示';


### ucenter库用户表增加新浪用户字段
# 2018-09-19
ALTER TABLE `users`
ADD COLUMN `sina_unionid` varchar(255) DEFAULT NULL COMMENT '新浪用户统一标识' AFTER `weixin_info`,
ADD COLUMN `sina_info` varchar(255) DEFAULT NULL COMMENT '新浪用户相关信息' AFTER `sina_unionid`,
ADD COLUMN `sina_unionid` varchar(255) DEFAULT NULL COMMENT '新浪用户统一标识' AFTER `weixin_info`,
ADD COLUMN `sina_info` varchar(255) DEFAULT NULL COMMENT '新浪用户相关信息' AFTER `sina_unionid`;


### common库用户表增加新浪用户字段
# 2018-09-19
ALTER TABLE `user_index`
ADD COLUMN `sina_unionid` varchar(255) DEFAULT NULL COMMENT '新浪用户统一标识' AFTER `weixin_info`,
ADD COLUMN `sina_info` varchar(255) DEFAULT NULL COMMENT '新浪用户相关信息' AFTER `sina_unionid`,
ADD COLUMN `sina_unionid` varchar(255) DEFAULT NULL COMMENT '新浪用户统一标识' AFTER `weixin_info`,
ADD COLUMN `sina_info` varchar(255) DEFAULT NULL COMMENT '新浪用户相关信息' AFTER `sina_unionid`;

### common库用户表增加新浪用户字段
# 2018-09-19
ALTER TABLE `users`
ADD COLUMN `sina_unionid` varchar(255) DEFAULT NULL COMMENT '新浪用户统一标识' AFTER `weixin_info`,
ADD COLUMN `sina_info` varchar(255) DEFAULT NULL COMMENT '新浪用户相关信息' AFTER `sina_unionid`,
ADD COLUMN `qq_unionid` varchar(255) DEFAULT NULL COMMENT 'QQ用户统一标识' AFTER `sina_info`,
ADD COLUMN `qq_info` varchar(255) DEFAULT NULL COMMENT 'QQ用户相关信息' AFTER `qq_unionid`;