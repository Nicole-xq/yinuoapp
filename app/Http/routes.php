<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to galaxy-client
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/test', "TestController@index");
Route::get('/', function () {
    return view('welcome');
});

Route::group(['namespace' => 'Api', 'middleware' => ['login.user']], function ($api) {
    // 获取版本号
    $api->any('/api/version', "VersionController@index");

    /**
     * 登录注册...
     * */
    $api->any('/api/login/{type?}', "LoginController@login");
    $api->any('/api/register/{type?}', "LoginController@register");
    $api->post('/api/password/reset', "LoginController@password");
    $api->post('/api/phone.captcha', "LoginController@sendCaptcha");

    /**
     * 首页
     * */
    $api->any('/api/index.banner', "BannerController@index");                                     // banner
    $api->any('/api/index.news', "ArticleTitleController@indexRecommend");                        // 资讯列表
    $api->any('/api/index.art.evaluation', "ArticleTitleController@indexArtEvaluation");          // 艺评列表

    /**
     * 新闻资讯
     * */
    $api->get('/api/news.nav', "ArticleTitleController@newsNav");                                 // 首页导航
    $api->get('/api/news.list/{nav_id?}', "ArticleTitleController@index");                        // 资讯列表
    $api->get('/api/news.info', "ArticleController@newsInfo");                                    // 资讯详情
    $api->get('/api/news.info.comment', "ArticleCommentController@index");                        // 文章详情评论列表
    $api->get('/api/news.recommend', "ArticleTitleController@infoRecommend")->middleware('verify.info.recommend'); // 详情底部推荐

    /**
     * 艺评
     * */
    $api->get('/api/art.evaluation/{state?}', "ArticleTitleController@artEvaluation");            // 艺评列表
    $api->post('/api/art.evaluation.vote', "ArticleController@articleVote");                      // 艺评投票

    /**
     * 点赞评论
     * */
    $api->post('/api/article.like', "ArticleController@articleLike");                             // 文章点赞
    $api->post('/api/article.comment.like', "ArticleController@articleCommentLike");              // 评论点赞
    $api->post('/api/article.comment', "ArticleCommentController@articleComment");                // 文章添加评论
    $api->post('/api/article/comments', "ArticleCommentController@comments");                     // 我的评论列表
    //个人中心--文章评论列表
    $api->post('/api/ArticleComment/personalCommentsList', "ArticleCommentController@personalCommentsList");
    $api->post('/api/article/be.comments', "ArticleCommentController@beComments");                // 评论我的列表


    /**
     * 银行卡
     * */
    $api->post('/api/bankcard', "BankcardController@index");                                      // 银行卡列表
    $api->get('/api/bankcard/list', "BankcardController@bankcardList");                                      // 支持银行列表
    $api->post('/api/bankcard/create', "BankcardController@create");                              // 添加银行卡
    $api->post('/api/bankcard/delete', "BankcardController@destroy");                             // 删除银行卡
    $api->post('/api/bankcard/member_info', "BankcardController@member_info");                    // 获取用户信息
    $api->post('/api/bankcard/default', "BankcardController@bankcard_default");                   // 设置默认银行卡
    $api->post('/api/bankcard/captcha', "BankcardController@captcha");                            // 发送验证码

    /**
     * 我的个人信息
     * */
    $api->post('/api/member', "MemberController@index");                                          // 个人中心信息
    $api->post('/api/member/homepage', "MemberController@homepage");                              // 个人主页,用户,艺术家,机构
    $api->post('/api/member/receiverAddress', "MemberController@receiverAddress");                // 商城用户收货地址
    $api->post('/api/member/receiverAddressCity', "MemberController@receiverAddressCity");        // 商城城市ID获取
    $api->post('/api/member/receiverAddressAdd', "MemberController@receiverAddressAdd");          // 商城用户收货地址新增
    $api->post('/api/member/receiverAddressDel', "MemberController@receiverAddressDel");          // 商城用户收货地址删除
    $api->get('/api/member/edit', "MemberController@edit");                                       // 编辑中心信息操作
    $api->post('/api/member/store', "MemberController@store");                                    // 修改信息
    $api->post('/api/member/store/avatar', "MemberController@avatar");                            // 修改头像
    $api->post('/api/member/show', "MemberController@show");                                      // 查看其他用户信息
    $api->post('/api/member/background', "FileOssController@backgroundEdit");                                      // 查看其他用户信息

    /**
     * 支付密码
     * */
    $api->post('/api/account/captcha', "MemberAccountController@captcha");                        // 发送验证码
    $api->post('/api/account/phone/check', "MemberAccountController@get_mobile_info");            // 检测是否绑定手机号
    $api->post('/api/account/paypassword/store', "MemberAccountController@store");                // 保存支付密码
    $api->post('/api/account/paypassword/check', "MemberAccountController@check_init_paypwd");    // 检测是否设置支付密码
    $api->post('/api/account/pd_cash', "MemberAccountController@pd_cash_add");                    // 申请余额提现

    /**
     * 消息
     * */
    $api->post('/api/member/msg/system', "MemberMsgController@system");                           // 系统消息
    $api->post('/api/member/msg/orders', "MemberMsgController@orders");                           // 订单消息
    $api->post('/api/member/msg/orders/delete', "MemberMsgController@destroy");                   // 删除消息
    $api->post('/api/member/msg/orders/show', "MemberMsgController@show");                        // 详细信息
    $api->post('/api/member/msg/unread', "MemberMsgController@unread");                           // 信息统计

    /*
     * @author 刘帅
     * @api
     * @return json
     */

    /*
     * 展览相关
     */
    $api->get('/api/exhibition.user.pic.list', "ExhibitionController@showExhibitions");            //展览首页  推荐列表
    $api->get('/api/exhibition.new.user.pic.list', "ExhibitionController@showNewestExhibitions");  //展览首页  最新列表
    $api->post('/api/exhibition.search.list', "ExhibitionController@searchExhibitions");           //筛选搜索 可以做同城
    $api->get('/api/exhibition.artist.pic.list', "ExhibitionController@revealExhibition");         //展览的详情页
    $api->get('/api/artist.artwork.list', "ArtistController@revealArtist");                        //艺术家信息及作品
    $api->post('/api/exhibition.give.like', "ExhibitionController@giveLike");                      //艺展的点赞


    /*
     * 约展相关
     */
    $api->get('/api/appointment.list', "AppointmentController@showAppointmentList");               // 当前会展发起列表
    $api->post('/api/appointment.add', "AppointmentController@addAppointment");                    // 发起约展
    $api->get('/api/appointment.initiator', "AppointmentController@showMyAppointmentList");        // 我的发起列表
    $api->get('/api/appointment.detail', "AppointmentController@showAppointment");                 // 我的发起详情(用于用户报名界面)
    $api->get('/api/appointment.info', "AppointmentController@showInfo");                          // 参与我的列表(2018/9/5以前)
    $api->get('/api/appointment.info/list', "AppointmentController@showInfo2");                    // 参与我的列表(2018/9/5新街口)
    $api->post('/api/appointment.info.add', "AppointmentController@addAppointmentInfo");           // 报名(已发布的)约展
    $api->get('/api/appointment.sign/list', "AppointmentSignController@index");                    // 我的参约列表
    $api->post('/api/appointment.sign/interested', "AppointmentSignController@interested");        // 操作是否感兴趣
    $api->post('/api/chat', "ChatMsgController@index");                                            // 对话消息列表
    $api->post('/api/chat/create', "ChatMsgController@create");                                    // 添加消息对话
    $api->post('/api/chat/delete', "ChatMsgController@delete");                                    // 删除对话消息


    /*
     * 作品相关
     */
    $api->get('/api/artwork/personalArticleList', "ArtworkController@personalArticleList");        //个人中心--作品列表
    $api->post('/api/artwork/personalCommentsList', "ArtworkController@personalCommentsList");     //个人中心--作品评论列表
    $api->post('/api/artwork/artworkPriceEdit', "ArtworkController@artworkPriceEdit");             //个人售卖作品价格修改
    $api->post('/api/artwork/commentsLike', "ArtworkController@commentsLike");                     //作品评论点赞
    $api->post('/api/artwork/commentsDelete', "ArtworkController@commentsDelete");                 //作品评论删除
    $api->post('/api/artwork/commentsDetail', "ArtworkController@commentsDetail");                 //作品评论详情
    $api->post('/api/artwork/editState', "ArtworkController@editState");                 		   //作品出售或者订单取消后状态更改
    //作品新增
    $api->post('/api/artwork/artworkInsert', "ArtworkController@artworkInsert");
    //作品点赞
    $api->post('/api/artwork/artworkLike', "ArtworkController@artworkLike");
    //用户作品列表
    $api->get('/api/artwork/memberArtworkList', "ArtworkController@memberArtworkList");
    $api->post('/api/artwork/searchArtworkByCategory', "ArtworkController@searchArtworkByCategory");//作品列表分类筛选
    $api->post('/api/artwork/searchArtworkByName', "ArtworkController@searchArtworkByName");        //作品名称筛选
    //作品详情
    $api->get('/api/artwork/artworkInfo', "ArtworkController@artworkInfo");
    $api->post('/api/artwork/pictureUpload', "ArtworkController@pictureUpload");              //作品图片上传
    //作品删除
    $api->post('/api/artwork/deleteInfo', "ArtworkController@deleteInfo");
    $api->post('/api/artwork/categoryList', "ArtworkController@categoryList");               //作品分类列表
    $api->post('/api/artwork/categoryChild', "ArtworkController@categoryChild");             //作品二级分类列表
    $api->post('/api/artwork/materialsList', "ArtworkController@materialsList");             //作品材质列表
    $api->post('/api/artwork/themeList', "ArtworkController@themeList");                     //作品题材列表


    /**
     * 艺术家模块
     */
    //搜索接口
    $api->post('/api/search', "SearchController@index");
    //最新作品更新
    $api->get('/api/artwork/newArtistArtworkList', "ArtworkController@newArtistArtworkList");
    //最新认证艺术家
    $api->get('/api/artist/newList', "ArtistController@newList");
    $api->post('/api/artist/searchArtistByArtistType', "ArtistController@searchArtistByArtistType");
    //关注艺术家
    $api->post('/api/artist/followArtist', "ArtistController@followArtist");
    //用户点赞列表
    $api->get('/api/member/memberLikeList', "MemberController@memberLikeList");
    //用户关注/粉丝列表
    $api->get('/api/member/memberFollowList', "MemberController@memberFollowList");
    //用户关注/粉丝量
    $api->get('/api/member/memberFollowNum', "MemberController@memberFollowNum");
    //用户主页信息
    $api->get('/api/member/memberDetail', "MemberController@memberDetail");
    //问题反馈
    $api->post('/api/member/feedback', "MemberController@feedback");

    /*
     * 没有分类的杂项
     */
    $api->get('/api/artist.rand.list', "ArtistController@getRandomArtistInfo");                    //艺评首页 顶部推荐用户随机
    $api->get('/api/get.address.info', "AddressController@getAddress");                            //地址获取--省列表编号
    $api->get('/api/address/fixArea', "AddressController@fixArea");                                //地址修复没有第三级省市区-城区
    $api->post('/api/get.city_id', "AddressController@getCityIdByCityName");                       //根据城市名获取城市ID
    $api->post('/api/file.upload', "FileOssController@fileUpload");                                //用户头像上传更改

    //展览报名领票
    $api->get('/api/registration.exhibition', "ExhibitionController@registrationExhibition");
    //用户约展感兴趣与发起人删除
    $api->post('/api/appointment.info.delete', "AppointmentController@deleteAppointmentInfo");

    /*
     * 数据库修复接口
     */
    $api->post('/api/fix/fixArtist', "FixController@fixArtist");                                //艺术家关联用户修复

    /*
     * 商城购物车
     */
    $api->post('/api/cart/getCartList', "CartController@getCartList");                               //获取用户购物车商品列表
    $api->post('/api/cart/cartCount', "CartController@cartCount");                                   //获取用户购物车商品数量
    $api->post('/api/cart/cartAdd', "CartController@cartAdd");                                       //用户购物车添加商品
    $api->post('/api/cart/cartDel', "CartController@cartDel");                                       //用户购物车删除商品
    $api->post('/api/cart/cartEdit', "CartController@cartEdit");                                     //修改用户购物车指定商品数量
});

/*
 * 认证通用接口
 * @auth MR.Liu
 */

Route::post('/ynh/authentication/fetchRoleType/{type}', "Ynh\AuthenticationController@fetchRoleType");              //获取认证类型(个人或机构)
Route::post('/ynh/authentication/fetchArtistType', "Ynh\AuthenticationController@fetchArtistType");                 //获取艺术家绘画类型
Route::post('/ynh/authentication/authenticationInsert/{type}', "Ynh\AuthenticationController@authenticationInsert");//个人认证or机构认证
Route::post('/ynh/authentication/fetchLast', "Ynh\AuthenticationController@fetchLast");                             //获取上一次认证信息
Route::post('/ynh/index/myCenter', "Ynh\IndexController@myCenter");                                                 //个人中心个人信息
Route::post('/ynh/article/articleList/{type}', "Ynh\ArticleController@articleList");                                //个人中心--文章列表显示
Route::post('/ynh/article/articleDelete', "Ynh\ArticleController@articleDelete");                                   //个人中心--文章删除
Route::post('/ynh/description/description', "Ynh\DescriptionController@description");                               //艺术家简介显示
Route::post('/ynh/description/descriptionUpdate', "Ynh\DescriptionController@descriptionUpdate");                   //艺术家简介编辑
Route::post('/ynh/description/textEditing', "Ynh\DescriptionController@textEditing");                               //艺术家简介文本文字编辑
Route::post('/ynh/article/personalArticleList', "Ynh\ArticleController@personalArticleList");                       //个人中心-艺术家主页文章
Route::post('/api/Artist/recommendArtistFetch', "Api\ArtistController@recommendArtistFetch");                       //艺术家列表--获取最新认证艺术家
Route::post('/api/artwork/hotComment', "Api\ArtworkController@hotComment");                                         //作品热门评论
Route::post('/api/artwork/commentInsert', "Api\ArtworkController@commentInsert");                                   //作品评论添加----回复评论
Route::post('/api/artwork/commentList', "Api\ArtworkController@commentList");                                       //获取作品评论列表
Route::get('/api/article/commentList', "Api\ArticleController@commentList");                                        //文章热门评论
Route::post('/api/MemberMsg/messageNotification', "Api\MemberMsgController@messageNotification");                   //个人中心消息通知


/*
 * 艺诺号
 */
Route::group(['namespace' => 'Ynh'], function ($api) {
    $api->post('/ynh/register', "LoginController@register");
    $api->post('/ynh/phone.captcha', "LoginController@sendCaptcha");
    $api->post('/ynh/login/{type?}', "LoginController@login");
    $api->post('/ynh/password.reset', "LoginController@resetPassword");

    //后台首页
    $api->post('/ynh/index', "IndexController@index");
    $api->post('/ynh/get.user.info', "IndexController@getUserInfo");

    //申请认证
    $api->post('/ynh/authentication.apply', "AuthenticationController@authenticationInsert");
    //获取认证的艺术家类型
    $api->post('/ynh/authentication.role_type', "AuthenticationController@fetchRoleType");
    //获取认证的创作分类
    $api->post('/ynh/authentication.artist_type', "AuthenticationController@fetchArtistType");
    $api->post('/ynh/get.artists', "AuthenticationController@fetchArtistInfoByName");

    //文章管理
    $api->post('/ynh/file.upload', "ArticleController@fileUpload");
    $api->post('/ynh/classify.list', "ArticleController@fetchClassify");
    $api->post('/ynh/article.list', "ArticleController@articleList");
    $api->post('/ynh/article.get', "ArticleController@getArticle");
    $api->post('/ynh/article.insert', "ArticleController@articleInsert");
    $api->post('/ynh/article.delete', "ArticleController@articleDelete");

    //简介管理
    $api->post('/ynh/description', "DescriptionController@description");
    $api->post('/ynh/description.update', "DescriptionController@descriptionUpdate");
    $api->post('/ynh/video.upload.token', "ArticleController@videoUpload");
    $api->get('/ynh/video.play.info', "ArticleController@getPlayInfo");
    $api->get('/ynh/delete.video', "ArticleController@deleteVideos");

    //通过艺术家id获得userId 并且跳转到wap艺术家页面
    $api->get('/ynh/getUserIdByArtistId', "ArticleController@getUserIdByArtistId");
});

/*
 * 小程序活动
 */
Route::group(['namespace' => 'Active'], function ($api) {
    //小程序首页
    $api->post('/active/phone.captcha', "HomeController@sendCaptcha");
    $api->post('/active/login', "HomeController@login");
    $api->post('/active/index', "HomeController@index");
    //小程序登陆商城账号 密码登陆
    $api->post('/active/app.login', "HomeController@appLogin");
    //获取城市列表
    $api->post('/active/get.address.info', "HomeController@getAddressInfo");
    //根据城市名获取城市ID
    $api->post('/active/get.city_id', "HomeController@getCityIdByCityName");
    //小程序注册用户接口
    $api->post('/active/register', "HomeController@register");
    //我的首页
    $api->post('/active/home', "HomeController@home");
    //用户获取分享二维码图片
    $api->post('/active/get.qr.code', "HomeController@qrCodeGet");
    //拍卖小程序获取分享二维码
    $api->post('/active/auctionMpPngGet', "HomeController@auctionMpPngGet");
    //活动金额转至余额
    $api->post('/active/amount.to.app', "HomeController@transferredAmount");
    //获取红包金额
    $api->post('/active/get.win_amount', "LotteryController@getWinAmount");
    //我的抽奖记录 --邀请人数
    $api->post('/active/get.my.invite', "LotteryController@myInviteListGet");
    //我的账单记录 --每天资金统计
    $api->post('/active/get.my.bill', "LotteryController@myBillListGet");

    //分享记录保存
    $api->post('/active/share.record', "HomeController@shareRecordSave");
});

//需要登录的接口
Route::group(['namespace' => 'Api', 'middleware' => 'verify.user.login', 'prefix'=> 'api'], function ($route) {
    //订单
    require __DIR__ . '/routes/order.php';
    require __DIR__ . '/routes/express.php';
    require __DIR__ . '/routes/artistOrder.php';
});


Route::options('/{all}', function(Request $request) {
    return response("", 200,
        [
            "Access-Control-Allow-Origin"=>"*",
            "Access-Control-Allow-Credentials"=>"true",
            "Access-Control-Allow-Methods"=>"POST, GET, OPTIONS, PUT, DELETE",
            "Access-Control-Allow-Headers" => "X-App-Ver, Origin, Access-Control-Request-Headers, SERVER_NAME, Access-Control-Allow-Headers, cache-control, token, X-Requested-With, Content-Type, Accept, Connection, User-Agent, Cookie",
        ]
    );
})->where(['all' => '([a-zA-Z0-9-]|/)+']);