<?php

namespace App\Http\Controllers\Active;

use App\Models\Active\MiniInvite;
use App\Models\Active\MiniLottery;
use App\Models\Active\MiniUser;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Validator;
use App\Http\Controllers\Controller;
use Mockery\Exception;

class LotteryController extends Controller
{
    protected $openid;

    //邀请人次获取抽奖机会 config('mini')['inviteNum']
    public function __construct()
    {
        $this->middleware('verify.user.login', ['only' => [
            //'getWinAmount'
        ]]);
    }

    public function getWinAmount(Request $request)
    {
        $underway = env('MINI_UNDERWAY');
        if (empty($underway)) {
            return response()->fail(200237);
        }
        $this->mini_code = $mini_code = $request->mini_code;
        if (empty($mini_code)) {
            return response()->fail(200224);
        }
        $where = ['mini_code' => $mini_code];
        $user_info = (new MiniUser())->where($where)->first();
        if (empty($user_info) || empty($user_info['user_id'])) {
            return response()->fail(300013);
        }
        if ($user_info['is_mini'] == 0) {
            //授权用户更改老用户
            $update = ['is_mini' => 2];
            (new MiniUser())->where($where)->update($update);
        }
        $nick_name = $user_info['nick_name'];
        //该账户历史获取的全部金额包含提现
        $amount_count = $user_info['amount_count'];
        $limit_num = env('MINI_LIMIT', 0);
        if ($amount_count > $limit_num) {
            $win_amount = 0;
        } else {
            //账户余额
            $amount = $user_info['amount'];
            //用户剩余抽奖次数
            $lottery_num = $user_info['lottery'];
            //用户已进行抽奖次数
            $remain_lottery = $user_info['remain_lottery'];
            if (empty($lottery_num)) {
                return response()->fail(200229);
            }
            $count_lottery = $lottery_num + $remain_lottery;
            $prize_arr = $this->getPrizeArray($count_lottery, $amount);
            foreach ($prize_arr as $val) {
                $arr[$val['id']] = $val['v'];
            }
            //根据概率获取奖项id
            $rid = $this->getRand($arr);

            //中奖项 中奖金额多少
            $win_amount = $prize_arr[$rid - 1]['prize'];
        }
        $lotteryInsert = [
            'mini_code' => $mini_code,
            'user_id' => $user_info['id'] ?? 0,
            'nick_name' => $nick_name ?? '',
            'win_amount' => $win_amount
        ];
        // 索引库开启事物
        DB::beginTransaction();
        try {
            $result1 = DB::connection()->table('mini_lottery')->insert($lotteryInsert);
            if (!$result1) {
                throw new Exception(300107);
            }

            $userWhere = ['mini_code' => $mini_code];
            //用户抽奖后抽奖次数修改
            $result2 = DB::connection()->table('mini_user')->where($userWhere)
                ->update(array(
                    'amount' => DB::raw('amount + ' . $win_amount),
                    'amount_count' => DB::raw('amount_count + ' . $win_amount),
                    'remain_lottery' => DB::raw('remain_lottery + 1'),
                    'lottery' => DB::raw('lottery - 1'),
                ));
            if (!$result2) {
                throw new Exception(300108);
            }
            if (!empty($win_amount)) {
                //更新奖金池金额
                $bonus_num = $win_amount * 99;
                $update = [
                    'initial_amount' => DB::raw('initial_amount + ' . $bonus_num),
                    'real_amount' => DB::raw('real_amount + ' . $win_amount)
                ];
                $result3 = DB::table('mini_amount')->update($update);
                if (!$result3) {
                    throw new Exception(300108);
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            Log::error('错误详情:获取token-' . $e->getMessage() . '.');
            return response()->fail($e->getMessage());
        }
        $return = [
            'win_amount' => $win_amount
        ];
        return response()->success($return);
    }

    public function getRand($proArr)
    {
        $result = '';

        //概率数组的总概率精度
        $proSum = array_sum($proArr);
        //概率数组循环
        foreach ($proArr as $key => $proCur) {
            $rand_num = mt_rand(1, $proSum);
            if ($rand_num <= $proCur) {
                $result = $key;
                break;
            } else {
                $proSum -= $proCur;
            }
        }
        if ($result == 1) {
            //TODO
            $max_id = DB::table('mini_lottery')->max('id');
            $id = $max_id - 1000;
            $sql = "SELECT * FROM `mini_lottery` WHERE type = 1 AND win_amount = 100 AND id > {$id}";
            $exit = DB::select($sql);
            if ($exit) {
                $this->getRand($proArr);
            }
        }
        return $result;
    }


    public function getPrizeArray($count_lottery, $amount)
    {
        //用户第一次抽奖 概率  红包金额 中概率
        if ($count_lottery == 1) {
            return config('mini.first');
        } else {
            //用户余额在 15 到 18元的抽奖概率
            if ($amount > 15) {
                return config('mini.section');
            } else {
                // 用户非第一次抽奖 的概率
                return config('mini.many');
            }
        }
    }

    public function myInviteListGet(Request $request)
    {
        $mini_code = $request->mini_code;
        if (empty($mini_code)) {
            $return = [
                'lottery_num' => 0,
                'invite_list' => []
            ];
            return response()->success($return);
        }
        $user_info = (new MiniUser())->where(['mini_code' => $mini_code])->first();
        if (empty($user_info)) {
            return response()->fail(200239);
        }
        $where = [
            'mini_code' => $mini_code
        ];
        $result = (new MiniInvite())->select(['*', DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d") as short_date')])
            ->where($where)
            ->orderBy('id', 'desc')->get()->toArray();
        if (empty($result)) {
            $return = [
                'lottery_num' => $user_info['lottery'],
                'invite_list' => []
            ];
            return response()->success($return);
        }
        $key = 'short_date';
        foreach ($result as &$value) {
            unset($value['mini_code'], $value['is_valid'], $value['created_at'], $value['updated_at']);
            if (isset($invite_list[$value[$key]])) {
                $invite_list[$value[$key]][] = $value;
            } else {
                $invite_list[$value[$key]][] = $value;
            }
        }

        $return = [
            'lottery_num' => $user_info['lottery'],
            'invite_list' => $result
        ];
        return response()->success($return);
    }

    /**
     * 获取我的资金记录
     * @param Request $request
     * @return mixed
     */
    public function myBillListGet(Request $request)
    {
        $mini_code = $request->mini_code;
        if (empty($mini_code)) {
            $return = [
                'lottery_count' => 0,
                'win_num' => 0,
                'none_num' => 0,
                'amount' => 0,
                'bill_count' => [],
            ];
            return response()->success($return);
        }
        $where = [
            'mini_code' => $mini_code
        ];
        $model = new MiniUser();
        $user_info = $model->where($where)->first();
        if (is_object($user_info)) {
            $user_info = $user_info->toArray();
        }
        if (empty($user_info)) {
            return response()->fail(200239);
        }
        $lotteryModel = new MiniLottery();
        $bill_list = $lotteryModel->select(['*', DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d") as short_date')])
            ->where($where)
            ->orderBy('short_date', 'desc')->get();
        if (empty($bill_list)) {
            return response()->fail(300109);
        }
        $bill_list = $bill_list->toArray();
        $result = [];
        $key = 'short_date';
        foreach ($bill_list as $value) {
            if (isset($result[$value[$key]])) {
                $result[$value[$key]][] = $value;
            } else {
                $result[$value[$key]][] = $value;
            }
        }
        foreach ($result as $key => &$item) {
            foreach ($item as &$val) {
                $val['time'] = $key;
            }
        }
        $bill_count = [];
        $tmp_arr1 = array(
            'type' => 1,
            'amount' => 0
        );
        $tmp_arr2 = array(
            'type' => 2,
            'amount' => 0
        );
        unset($val);
        foreach ($result as $key => $value) {
            foreach ($value as $val) {
                if ($val['type'] == 1) {
                    $tmp_arr1['amount'] += $val['win_amount'];
                } elseif ($val['type'] == 2) {
                    $tmp_arr2['amount'] += $val['win_amount'];
                }
            }
            if ($tmp_arr1['amount'] != 0) {
                $bill_count[$key][] = $tmp_arr1;
            }
            if ($tmp_arr2['amount'] != 0) {
                $bill_count[$key][] = $tmp_arr2;
            }
            $tmp_arr1['amount'] = 0;
            $tmp_arr2['amount'] = 0;
        }
        $bill_data = array_keys($bill_count);
        $bill_return = [];
        foreach ($bill_data as $key => $value) {
            $bill_return[$key]['time'] = $value;
            $bill_return[$key]['data'] = $bill_count[$value];
        }
        //抽奖总次数
        $lottery = $user_info['remain_lottery'];

        //中奖次数     //未中奖次数
        $where = [
            'type' => 1,
            'mini_code' => $mini_code,
        ];
        $lottery_win = $lotteryModel->where($where)->where('win_amount', '<>', 0)->count();
        $where['win_amount'] = 0;
        $lottery_none = $lotteryModel->where($where)->count();
        $return = [
            'lottery_count' => $lottery,
            'win_num' => $lottery_win,
            'none_num' => $lottery_none,
            'amount' => $user_info['amount'],
            'bill_count' => $bill_return,
        ];
        return response()->success($return);
    }
}
