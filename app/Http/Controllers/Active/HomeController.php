<?php

namespace App\Http\Controllers\Active;

use App\Libs\phpQuery\http;
use App\Libs\phpQuery\WXBizDataCrypt;
use App\Models\Active\MiniInvite;
use App\Models\Active\MiniRecord;
use App\Models\Active\MiniShareRecord;
use App\Models\Active\MiniUser;
use App\Models\Address;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Validator;
use App\Http\Controllers\Controller;
use Mockery\Exception;

class HomeController extends Controller
{
    protected $openid;
    protected $error = '';

    //邀请人次获取抽奖机会 config('mini')['inviteNum']
    public function __construct()
    {
        $this->middleware('verify.user.login', ['only' => [
            'auctionMpPngGet'
        ]]);
    }

    //首页自动获取奖金池金额
    public function index(Request $request)
    {
        $mini_code = $request->mini_code;
        if (isset($mini_code) && !empty($mini_code)) {
            $model = new MiniUser();
            $user_info = $model->where(['mini_code' => $mini_code])->first();
            if (!empty($user_info)) {
                $user_info = $user_info->toArray();
            }
            if (empty($user_info)) {
                return response()->fail(200239);
            }
            $return['lottery_num'] = $user_info['lottery'];
            $return['status'] = 0;
            if (!empty($user_info['user_id'])) {
                $return['status'] = 1;
            }
        } else {
            $return['status'] = 0;
            $return['lottery_num'] = 0;
        }
        $bonus_pool = DB::table('mini_amount')->get();
        if (!empty($bonus_pool)) {
            $bonus_pool = $bonus_pool[0];
        }
        $bonus_num = intval($bonus_pool['initial_amount']) ?? 2800000 + rand(1, 1600000);
        $lottery_list = $this->lotteryListGet();
        $bonus_num = str_split($bonus_num, 1);
        $return['bonus_num'] = $bonus_num;
        $return['lottery_list'] = $lottery_list;
        return response()->success($return);
    }

    public function login(Request $request)
    {
        $mini_code = $request->mini_code;
        $model = new MiniUser();
        //判断用户是否需要登录  1不需要 0需要
        $status = 0;
        if (!empty($mini_code)) {
            $user_info = $model->where(['mini_code' => $mini_code])->first();
            if (!empty($user_info)) {
                $user_info = $user_info->toArray();
            }
            if (empty($user_info)) {
                return response()->fail(200239);
            }
            if (!empty($user_info['user_id'])) {
                $status = 1;
            }
            $return = [
                'mini_code' => $user_info['mini_code'],
                'lottery_num' => $user_info['lottery'],
                'status' => $status
            ];
            return response()->success($return);
        }
        $validator = $this->loginCheck($request);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->fail($errors->all()[0]);
        }
        //获取微信小程序授权用户信息
        $data = $this->getWxUserInfo($request);
        if (!empty($this->error)) {
            return response()->fail($this->error);
        }
        //保存用户访问记录
        $this->visitRecordInsert($data['nickName']);
        //判断用户是否第一次登陆
        $user_info = $model->where(['openid' => $this->openid])->first();
        if (!empty($user_info)) {
            $user_info = $user_info->toArray();
        }
        if (!$user_info) {
            //用户第一次登陆小程序 -----//用户保存信息数组
            $mini_code = md5('mini' . $this->openid);
            $params = [
                'openid' => $this->openid,
                'nick_name' => $data['nickName'],
                'avatar_url' => $data['avatarUrl'],
                'gender' => $data['gender'],
                'city' => $data['city'],
                'province' => $data['province'],
                'mini_code' => $mini_code,
                'language' => $data['language'],
            ];
            if (empty($params)) {
                return response()->fail(200224);
            }
            $params['last_login_time'] = date('Y-m-d H:i:s');
            $mini_uid = $model->insertGetId($params);
            if (!$mini_uid) {
                return response()->fail(300002);
            }
            $return = [
                'mini_code' => $mini_code,
                'lottery_num' => 1,
                'status' => $status
            ];
            return response()->success($return);
        } else {
            if (!empty($user_info['user_id'])) {
                $status = 1;
            }
            $update['last_login_time'] = date('Y-m-d H:i:s');
            $res = $model->where(['openid' => $this->openid])
                ->update($update);
            if (empty($res)) {
                return response()->fail(200005);
            }

            //用户剩余抽奖次数
            $lottery_num = $user_info['lottery'];
            //不是第一次登陆的 , 获取抽奖新增次数
            $new_lottery_num = $this->getNewLotteryNum($user_info['mini_code']);
            $return = [
                'mini_code' => $user_info['mini_code'],
                'lottery_num' => $lottery_num + $new_lottery_num,
                'status' => $status
            ];
            return response()->success($return);
        }
    }

    /**
     * 登陆App账号
     * @param Request $request
     * @return mixed
     */
    public function appLogin(Request $request)
    {
        $username = $request->username;
        $password = $request->password;
        $mini_code = $request->mini_code;
        if (empty($username) || empty($password) || empty($mini_code)) {
            return response()->fail(200224);
        }
        $base_url = env('BASE_URL', '');
        if (empty($base_url)) {
            return response()->fail(200227);
        }
        $url = $base_url . '/api/login';
        $data = [
            'username' => $username,
            'password' => $password,
            'client' => 'mini'
        ];
        $result = http::post($url, $data);
        $user_data = json_decode($result, true);
        if (isset($user_data['status_code']) && $user_data['status_code'] == 200) {
            $shop_url = env('SHOP_URL', '');
            if (empty($shop_url)) {
                return response()->fail(200227);
            }
            $url = $shop_url . '/mobile/index.php?act=login';
            $post_data = [
                'username' => $username,
                'password' => $password,
                'client' => 'ios'
            ];
            $shop_result = http::post($url, $post_data);
            if (!empty($shop_result)) {
                $shop_result = json_decode($shop_result);
                if (json_last_error() != 0) {
                    return response()->fail(200222);
                }
            } else {
                return response()->fail(200223);
            }
            if ($shop_result->code != 200) {
                return response()->fail($shop_result->code, $shop_result->datas->error);
            }
            $key = $shop_result->datas->key;
            //小程序保存的商城的user_id
            $user_id = $shop_result->datas->userid;
            $model = new MiniUser();
            $limit_where = ['user_id' => $user_id];
            $is_exit = $model->where($limit_where)->first();
            if (is_object($is_exit)) {
                $is_exit = $is_exit->toArray();
            }
            if ($is_exit) {
                if ($is_exit['mini_code'] != $mini_code) {
                    return response()->fail(200238);
                }
            }
            $where = ['mini_code' => $mini_code];
            $user_info = $model->where($where)->first();
            if (is_object($user_info)) {
                $user_info = $user_info->toArray();
            }
            if (empty($user_info)) {
                return response()->fail(200239);
            }
            if (!empty($user_info['user_id'])) {
                return response()->fail(200238);
                //echo $result;
                //die;
            }
            $shop_url = env('SHOP_URL', '');
            if (empty($shop_url)) {
                return response()->fail(200227);
            }
            $invite_url = $shop_url . '/mobile/index.php?act=member_qrcode&key=' . $key;
            $invite = http::post($invite_url);
            if (!empty($invite)) {
                $result_invite = json_decode($invite);
                if (json_last_error() != 0) {
                    return response()->fail(200222);
                }
            } else {
                return response()->fail(200223);
            }
            if ($result_invite->code != 200) {
                return response()->fail($result_invite->code, $result_invite->datas->error);
            }
            //记录 该用户是通过小程序注册 ----  商城获取的小程序邀请码
            $invite_code = $result_invite->datas->invite_code;
            $update = [
                'key' => $key,
                'user_id' => $user_id,
                'invite_code' => $invite_code
            ];
            $model->where($where)->update($update);
        }
        echo $result;
        die;
    }

    public function loginCheck($request)
    {
        $rule = [
            'iv' => 'required',
            'code' => 'required',
            'encryptedData' => 'required',
        ];
        $message = [
            '*.required' => 200224
        ];
        return $validator = Validator::make($request->all(), $rule, $message);
    }

    public function visitRecordInsert($nick_name)
    {
        //用户登录访问记录
        $recordModel = new MiniRecord();
        $record = [
            'openid' => $this->openid,
            'nick_name' => $nick_name
        ];
        $recordModel::insert($record);
    }

    /**
     * 根据用户openid 获取邀请注册人数
     * 更新用户抽奖次数
     * @param $invite_id
     * @return integer
     */
    public function getNewLotteryNum($invite_id)
    {
        $where = [
            'mini_code' => $invite_id,
            'is_valid' => 1,
        ];
        $model = new MiniInvite();
        //获取邀请人数列表
        $valid_ids = $model->where($where)->lists('id')->toArray();
        //邀请人数不满足抽奖条件
        if (empty($valid_ids) || count($valid_ids) < config('mini')['inviteNum']) {
            return 0;
        }
        $arr = array_chunk($valid_ids, config('mini')['inviteNum']);
        //邀请计算状态修改Ids
        $disable = [];
        //抽奖次数更新标识数组
        $act = [];
        $new_lottery_num = 0;
        foreach ($arr as $value) {
            if (count($value) == config('mini')['inviteNum']) {
                $new_lottery_num = $new_lottery_num + 1;
                $disable = array_merge($disable, $value);
                $act[] = end($value);
            }
        }
        if (!empty($new_lottery_num)) {
            DB::begintransaction();
            try {
                $update = ['is_valid' => 0];
                $result1 = DB::connection()->table('mini_invite')->whereIn('id', $disable)->update($update);
                if (!$result1) {
                    throw new Exception(300102);
                }
                $updateAct = ['is_act' => 1];
                $resultAct = DB::connection()->table('mini_invite')->whereIn('id', $act)->update($updateAct);
                if (!$resultAct) {
                    throw new Exception(300103);
                }
                $user_where = ['mini_code' => $invite_id];
                $result2 = DB::connection()->table('mini_user')
                    ->where($user_where)->increment('lottery', $new_lottery_num);
                if (!$result2) {
                    throw new Exception(300104);
                }
                DB::commit();
            } catch (Exception $e) {
                DB::rollback();
                Log::error('错误详情:获取token-' . $e->getMessage() . '.');
                return response()->fail($e->getMessage());
            }
        }
        return $new_lottery_num;
    }

    public function home(Request $request)
    {
        $mini_code = $request->mini_code;
        if (empty($mini_code)) {
            return response()->fail(200224);
        }
        $user_info = (new MiniUser())->where(['mini_code' => $mini_code])->first();
        if (empty($user_info)) {
            return response()->fail(200239);
        }
        $return['lottery_num'] = $user_info['lottery'];
        $return['amount'] = $user_info['amount'];
        return response()->success($return);
    }


    public function lotteryListGet()
    {
        $names = $appId = config('mini')['names'];
        $names_arr = explode(' ', $names);
        $return = [];
        $num = 20;
        $bonus_arr = [20 => 1, 30 => 1, 50 => 1, 100 => 1];
        $begin = rand(3, 7);
        $hour = 1;
        for ($i = 0; $i <= $num; $i++) {
            $begin += rand(1, 5);
            if ($begin > 60) {
                $rand = rand(0, 100);
                if ($rand < 30) {
                    $hour += rand(0, 1);
                }
                $create = $hour . '小时前';
            } else {
                $create = $begin . '分钟前';
            }
            $return[$i]['info'] = $names_arr[array_rand($names_arr)] . '**' . ' 抽中' . array_rand($bonus_arr) .
                '元现金红包';
            $return[$i]['time'] = $create;
        }
        return $return;
    }

    public function shareRecordSave(Request $request)
    {
        $mini_code = $request->mini_code;
        if (empty($mini_code)) {
            return response()->fail(200224);
        }
        $user_info = (new MiniUser())->where(['mini_code' => $mini_code])->first();
        $insert = [
            'mini_code' => $mini_code,
            'nick_name' => $user_info['nick_name'] ?? '',
            'is_mini' => $user_info['is_mini'] ?? 0,
        ];
        $insert['register_time'] = $user_info['register_time'] ?? null;
        $result = (new MiniShareRecord())::insert($insert);
        if ($result) {
            return response()->success();
        }
        return response()->fail(200107);
    }

    /**
     * 活动金额转至账户余额
     * @param Request $request
     * @return mixed
     */
    public function transferredAmount(Request $request)
    {
        $amount = $request->amount;
        $mini_code = $request->mini_code;
        if (empty($mini_code) || empty($amount)) {
            return response()->fail(200224);
        }
        if ($amount < 18) {
            return response()->fail(200221);
        }
        $model = new MiniUser();
        $userWhere = ['mini_code' => $mini_code];
        $user_info = $model->where($userWhere)->first();
        if (empty($user_info) || empty($user_info['key'])) {
            return response()->fail(300013);
        }
        $nick_name = $user_info['nick_name'];
        $user_amount = $user_info['amount'];
        $key = $user_info['key'];
        if ($amount > $user_amount) {
            return response()->fail(200240);
        }
        DB::beginTransaction();
        try {
            //保存提现记录
            $lotteryInsert = [
                'mini_code' => $mini_code,
                'user_id' => $user_info['id'] ?? 0,
                'nick_name' => $nick_name ?? '',
                'win_amount' => $amount,
                'type' => 2
            ];
            $result1 = DB::connection()->table('mini_lottery')->insert($lotteryInsert);
            if (!$result1) {
                throw new Exception(300107);
            }
            //用户转余额后,账户变更
            $result2 = DB::connection()->table('mini_user')->where($userWhere)
                ->update(array(
                    'amount' => DB::raw('amount - ' . $amount),
                ));
            if (!$result2) {
                throw new Exception(300108);
            }
            $postData = [
                'pdr_amount' => $amount,
                'key' => $key,
                'msg' => 'mini_app',
            ];
            $base_url = env('SHOP_URL', '');
            $url = $base_url . '/mobile/index.php?act=activity_bonus&op=activity_bonus';
            $result3 = http::post($url, $postData);
            if (empty($result3)) {
                throw new Exception(300112);
            }
            $result3 = json_decode($result3);
            if ($result3->code != 200) {
                throw new Exception(300111);
            }
            //库数据提交
            DB::commit();
        } catch (\Exception $e) {
            //库数据回滚
            DB::rollback();
            return response()->fail($e->getMessage());
        }
        return response()->success();
    }

    /**
     * 通过小程序注册商城账号
     * @param Request $request
     * @return mixed
     */
    public function register(Request $request)
    {
        $validator = $this->registerCheck($request);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->fail($errors->all()[0]);
        }
        $phone = $request->phone;
        //商城 账号注册
        $register = [
            'phone' => $phone,
            'captcha' => $request->captcha,
            'password' => $request->password,
            'member_areaid' => $request->areaid,
            'member_cityid' => $request->cityid,
            'member_provinceid' => $request->provinceid,
            'member_areainfo' => $request->areainfo ?? '未知',
            'client' => 'wap'
        ];
        $mini_code = $request->mini_code;
        $invite_id = $request->invite_id ?? 0;
        $shop_url = env('SHOP_URL', '');
        if (empty($shop_url)) {
            return response()->fail(200227);
        }
        $url = $shop_url . '/mobile/index.php?act=login&op=sms_register';
        if (empty($invite_id)) {
            $result = http::post($url, $register);
        } else {
            $user_info = (new MiniUser())->where(['mini_code' => $invite_id])->first();
            if (empty($user_info)) {
                return response()->fail(200235);
            }
            $invite_user_id = $user_info['id'];
            $result = http::post($url, $register);
        }
        if (!empty($result)) {
            $result = json_decode($result);
            if (json_last_error() != 0) {
                return response()->fail(200222);
            }
        } else {
            return response()->fail(200223);
        }
        if ($result->code != 200) {
            return response()->fail($result->code, $result->datas->error);
        }
        //记录 该用户是通过小程序注册
        $key = $result->datas->key;
        $user_id = $result->datas->userid;
        $update = [
            'key' => $key,
            'user_id' => $user_id,
            'is_mini' => 1
        ];
        (new MiniUser())->where(['mini_code' => $mini_code])->update($update);
        //小程序当前用户信息
        $user_info = (new MiniUser())->where(['mini_code' => $mini_code])->first();
        if (empty($user_info)) {
            return response()->fail(200235);
        }
        $mini_id = $user_info['id'];
        //判断是否 为邀请用户   如果是则保存邀请记录
        if (!empty($invite_id) && !empty($invite_user_id)) {
            $inviteParams['invite_id'] = $invite_user_id;
            $inviteParams['register_id'] = $mini_id;
            $inviteParams['mini_code'] = $invite_id;
            $inviteParams['register_name'] = $user_info['nick_name'] ?? $phone;

            //邀请人code TODO
            $userWhere['mini_code'] = $invite_id;
            // 索引库开启事物
            DB::beginTransaction();
            try {
                $result1 = DB::connection()->table('mini_invite')->insertGetId($inviteParams);
                if (!$result1) {
                    throw new Exception(200225);
                }
                //判断邀请人是否满足新增抽奖次数
                $user_where = ['mini_code' => $invite_id];
                $result2 = DB::connection()->table('mini_user')
                    ->where($user_where)->increment('lottery');
                if (!$result2) {
                    throw new Exception(200226);
                }
                DB::commit();
            } catch (Exception $e) {
                DB::rollback();
                Log::error('错误详情:获取token-' . $e->getMessage() . '.');
                return response()->fail($e->getMessage());
            }
            $this->getNewLotteryNum($invite_id);
        }
        return response()->success($result->datas);
    }

    /**
     * 注册参数效验
     * @param $request
     * @return mixed
     */
    public function registerCheck($request)
    {
        $rules = [
            'mini_code' => 'required',
            'phone' => 'required|regex:/^1[34578][0-9]{9}$/',
            'captcha' => 'required|regex:/^[0-9]+$/',
            'password' => 'required|min:6|max:80',
            'areaid' => 'required|regex:/^[0-9]+$/',
            'cityid' => 'required|regex:/^[0-9]+$/',
            'provinceid' => 'required|regex:/^[0-9]+$/',
            //'areainfo' => 'required',                 //这个暂时小程序没有填写注册的详细地址
        ];
        $message = [
            'mini_code.required' => 200001,
            'phone.required' => 200001,
            'phone.regex' => 300007,
            'captcha.required' => 200001,
            'captcha.regex' => 200100,
            'password.required' => 200001,
            'password.min' => 200051,
            'password.max' => 200051,
            'areaid.required' => 200050,
            'areaid.regex' => 200002,
            'cityid.required' => 200050,
            'cityid.regex' => 200002,
            'provinceid.required' => 200050,
            'provinceid.regex' => 200002,
            //'areainfo.required' => 200050,
        ];
        return $validator = Validator::make($request->all(), $rules, $message);
    }

    public function getWxUserInfo($request)
    {
        $iv = $request->iv;
        $code = $request->code;
        $encryptedData = $request->encryptedData;
        $appId = env('APP_ID', '');
        $appSecret = env('APP_SECRET', '');
        $url = 'https://api.weixin.qq.com/sns/jscode2session?appid=';
        $url .= $appId . '&secret=' . $appSecret . '&js_code=' . $code . '&grant_type=authorization_code';
        //获取session_key 用于解密数据
        $grantInfo = file_get_contents($url);
        $grantInfo = json_decode($grantInfo, true);
        if (empty($grantInfo['session_key'])) {
            $this->error = 200228;
            return false;
        }
        $sessionKey = $grantInfo['session_key'];
        $this->openid = $grantInfo['openid'];

        $pc = new WXBizDataCrypt($appId, $sessionKey);
        $errCode = $pc->decryptData($encryptedData, $iv, $data);

        if ($errCode != 0) {
            //解密后的用户信息
            $this->error = $errCode;
            return false;
        }
        if (!is_array($data)) {
            $data = json_decode($data, true);
        }
        $data['session_key'] = $sessionKey;
        return $data;
    }

    public function getAddressInfo(Request $req)
    {
        $pid = $req->get('pid');
        if (!isset($pid)) {
            return response()->fail(200001);
        }
        $model = new Address();
        $res = $model->getAddress($pid);
        return response()->success($res);
    }

    /**
     * 小程序发送短信 走的商城短发送
     * @param Request $request
     * @return mixed
     */
    public function sendCaptcha(Request $request)
    {
        if (empty($request->phone)) {
            return response()->fail(200224);
        }
        $phone = $request->phone;
        $shop_url = env('SHOP_URL', '');
        if (empty($shop_url)) {
            return response()->fail(200227);
        }
        $sign = md5($phone . 'ynys');
        $url = $shop_url . '/mobile/index.php?act=login&op=get_sms_captcha&phone=' . $phone . '&type=1&sign=' . $sign;
        $response = http::get($url);
        if (empty($response)) {
            return response()->fail(200233);
        }
        $result = json_decode($response, true);
        if ($result['code'] != 200) {
            return response()->fail($result['code'], $result['datas']['error']);
        }
        return response()->success();
    }

    public function getCityIdByCityName(Request $request)
    {
        $city_name = $request->input('city_name');
        if (empty($city_name)) {
            return response()->fail(200001);
        }
        $model = new Address();
        $city_id = $model->get_cityId_byCityName($city_name);
        if (is_object($city_id)) {
            $city_id = json_decode(json_encode($city_id), true);
        }
        if (!empty($city_id) && is_array($city_id)) {
            return response()->success(['city_id' => $city_id[0]]);
        }
        return response()->success(['city_id' => 0]);
    }

    /**
     * 用户获取分享二维码图片
     * @param Request $request
     * @return mixed
     */
    public function qrCodeGet(Request $request)
    {
        $mini_code = $request->mini_code;
        if (empty($mini_code)) {
            return response()->fail(200224);
        }
        $appId = env('APP_ID', '');
        $appSecret = env('APP_SECRET', '');
        $url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=' . $appId;
        $url .= '&secret=' . $appSecret;
        $result = http::get($url);
        $res = json_decode($result, true);
        if (!isset($res['access_token']) || empty($res)) {
            return response()->fail(200230);
        }
        $access_token = $res['access_token'];
        // $scene  用户注册的邀请码 -- 商城上下级
        //http://www.yinuovip.com/mobile/index.php?act=member_qrcode&key=bb52aa45c9d495f3e8eaea809a6b312b
        $mini_where = ['mini_code' => $mini_code];
        $user_info = (new MiniUser())->where($mini_where)->first();
        if (empty($user_info)) {
            return response()->fail(200235);
        }
        $key = $user_info['key'];
        if (empty($key)) {
            return response()->fail(200236);
        }
        if (empty($user_info['invite_code'])) {
            $shop_url = env('SHOP_URL', '');
            if (empty($shop_url)) {
                return response()->fail(200227);
            }
            $invite_url = $shop_url . '/mobile/index.php?act=member_qrcode&key=' . $key;
            $invite = http::post($invite_url);
            if (!empty($invite)) {
                $result = json_decode($invite);
                if (json_last_error() != 0) {
                    return response()->fail(200222);
                }
            } else {
                return response()->fail(200223);
            }
            if ($result->code != 200) {
                return response()->fail($result->code, $result->datas->error);
            }
            //记录 该用户是通过小程序注册 ----  商城获取的小程序邀请码
            $invite_code = $result->datas->invite_code;
            //$usertype = $result->datas->usertype;
            //$bind_uri = $result->datas->bind_uri;

            $invite_update = [
                'invite_code' => $invite_code
            ];
            (new MiniUser())->where($mini_where)->update($invite_update);
        }
        $result = $this->createPngByInviteCode($access_token, $invite_code);
        if (isset($result['error'])) {
            return response()->fail($result['error']);
        }
        return response()->success($result);
    }


    /**
     * 用户获取分享二维码图片
     * @param Request $request
     * @return mixed
     */
    public function auctionMpPngGet(Request $request)
    {
        $userInfo = $request->user;
        $key = $request->api_token;
        $appId = env('AUCTION_APP_ID', '');
        $appSecret = env('AUCTION_APP_SECRET', '');
        $url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=' . $appId;
        $url .= '&secret=' . $appSecret;
        $result = http::get($url);
        $res = json_decode($result, true);
        if (!isset($res['access_token']) || empty($res)) {
            return response()->fail(200230);
        }
        $access_token = $res['access_token'];
        // $scene  用户注册的邀请码 -- 商城上下级
        //http://www.yinuovip.com/mobile/index.php?act=member_qrcode&key=bb52aa45c9d495f3e8eaea809a6b312b
        if (empty($key)) {
            return response()->fail(200236);
        }
        if (empty($userInfo['invite_code'])) {
            $shop_url = env('SHOP_URL', '');
            if (empty($shop_url)) {
                return response()->fail(200227);
            }
            $invite_url = $shop_url . '/mobile/index.php?act=member_qrcode&key=' . $key;
            $result = http::post($invite_url);
            if (!empty($result)) {
                $result = json_decode($result);
                if (json_last_error() != 0) {
                    return response()->fail(200222);
                }
            } else {
                return response()->fail(200223);
            }
            if ($result->code != 200) {
                return response()->fail($result->code, $result->datas->error);
            }
            //记录 该用户是通过小程序注册 ----  商城获取的小程序邀请码
            $invite_code = $result->datas->invite_code;
        } else {
            $invite_code = $userInfo['invite_code'];
        }
        $result = $this->createPngByInviteCode($access_token, $invite_code);
        if (isset($result['error'])) {
            return response()->fail($result['error']);
        }
        return response()->success($result);
    }

    public function createPngByInviteCode($accessToken, $inviteCode)
    {
        //小程序识别后的跳转页面
        //用户邀请的邀请码 使用小程序的 mini_code 然后通过mini_code 进行保存
        $qr_url = 'https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=' . $accessToken;
        $data = [
            'scene' => $inviteCode,
        ];
        $data = json_encode($data);
        $response = http::post($qr_url, $data);
        if (empty($response)) {
            return ['error' => 200231];
        }
        $base_path = base_path() . '/public/upload/';
        if (!is_dir($base_path)) {
            mkdir($base_path, 0777, true);
        }
        //$base_path = str_replace('/', '\\', $base_path);
        $base_path = str_replace('\\', '/', $base_path);
        $file_name = time() . $inviteCode. rand(1000, 9999) . '.jpg';
        $file_path = $base_path . $file_name;
        $res = file_put_contents($file_path, $response);
        if (empty($res)) {
            return ['error' => 200232];
        }
        $base_url = env('BASE_URL', '');
        if (empty($base_url)) {
            return ['error' => 200227];
        }
        $pic_url = $base_url . '/upload/' . $file_name;
        $return = [
            'pic' => $pic_url
        ];
        return $return;
    }
}