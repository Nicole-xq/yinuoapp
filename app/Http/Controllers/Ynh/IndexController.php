<?php

namespace App\Http\Controllers\Ynh;

use App\Logic\Member;
use App\Models\AppBroadcasts;
use App\Models\AppointmentSign;
use App\Models\Article;
use App\Models\ArticleComment;
use App\Models\ArticleLike;
use App\Models\ArticleTitle;
use App\Models\ArtistOrganization;
use App\Models\ArtistPersonal;
use App\Models\Artwork;
use App\Models\ArtworkComment;
use App\Models\ArtworkLike;
use App\Models\UserFollow;
use App\Models\Ynh\YnhAuthenticationApply;
use App\Models\Ynh\YnhUser;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Swagger\Annotations as SWG;

class IndexController extends Controller
{
    //文章统计大分类
    protected $classify = [
        1 => [
            3, 4, 5
        ],
        2 => [6],
    ];
    protected $type = [
        1 => 'news',
        2 => 'artReview',
    ];

    private $authentication = 0;
    private $role_id = 0;

    public function __construct()
    {
        $this->middleware('verify.user.login', ['only' => [
            'index',
            'getUserInfo',
            'myCenter',
        ]]);
    }

    /*
     * 艺诺号后台首页
     */
    public function index(Request $request)
    {
        $user_id = $request->user['user_id'];
        if (empty($user_id)) {
            return response()->fail(200224);
        }
        //显示文章数量 作品数量 评论总条数

        $return['article_count'] = (new ArticleTitle())->articleCountGet($user_id) ?: 0;
        //$artwork_count = (new Artwork())->artworkCountGet($user_id) ?: 0;
        //$article_count = (new ArticleTitle())->countNumGet($user_id);
        //$article_comment_num = $article_count['comment_num'] ?? 0;
        //$article_accepter__num = (new ArticleComment())->countNumGet($user_id);
        //$artwork_accepter__num = (new ArtworkComment())->countNumGet($user_id);
        //$artwork_count = (new Artwork())->countNumGet($user_id);
        //$artwork_comment_num = $artwork_count['comment_num'] ?? 0;
        //$return['comment_count'] = $article_comment_num + $artwork_comment_num + $article_accepter__num + $artwork_accepter__num;
        $countNum = [];
        foreach ($this->classify as $key => $value) {
            if (is_array($value) && count($value) > 1) {
                $classify_ids = implode("','", $value);
            } else {
                $classify_ids = $value[0] ?? '';
            }
            $countData = $this->articleCount($user_id, $classify_ids);
            if (empty($countData)) {
                return response()->success();
            }
            $typeName = $this->type[$key];

            $countNum[$typeName]['articleData'] = $this->fetchArticles($user_id, $value) ?? [];
            $countNum[$typeName]['article_num'] = $countData[0]['article_num'] ?? 0;
            $countNum[$typeName]['vote_num'] = $countData[0]['vote_num'] ?? 0;
            $countNum[$typeName]['click_num'] = $countData[0]['click_num'] ?? 0;
            $countNum[$typeName]['like_num'] = $countData[0]['like_num'] ?? 0;
            $countNum[$typeName]['comment_num'] = $countData[0]['comment_num'] ?? 0;
        }
//        $fields = ['id', 'artwork_name', 'like_num', 'click_num', 'comment_num'];
//        $artwork_data = (new Artwork())->artworkFetchByUserId($user_id, $fields, 3);
//        $countArtwork = $this->artworkCount($user_id);
//        $countNum['artwork']['article_num'] = $countArtwork[0]['article_num'] ?? 0;
//        $countNum['artwork']['click_num'] = $countArtwork[0]['click_num'] ?? 0;
//        $countNum['artwork']['like_num'] = $countArtwork[0]['like_num'] ?? 0;
//        $countNum['artwork']['comment_num'] = $countArtwork[0]['comment_num'] ?? 0;
//        $countNum['artwork']['artwork_count'] = $artwork_data;

        return response()->success($countNum);
    }


    /**
     * 获取不同类型下最新的三篇文章
     * @param $user_id
     * @param $classify_ids
     * @return mixed
     */
    public function fetchArticles($user_id, $classify_ids)
    {
        $model = new ArticleTitle();
        $where = [
            'author_id' => $user_id,
            'is_delete' => 0,
            'status' => 1,
        ];
        $fields = [
            'id', 'title', 'like_num', 'click_num', 'comment_num', 'created_at'
        ];
        return $model::where($where)->whereIn('classify_id', $classify_ids)->select($fields)->limit(3)
            ->orderBy('updated_at', 'desc')->get()->toArray();
    }

    /**
     * 获取文章总数, 点赞总数, 于都总数
     * @param $user_id
     * @param $classify_ids
     * @return mixed
     */
    public function articleCount($user_id, $classify_ids)
    {
        $sql = "SELECT `author_id`, COUNT(*) as article_num, SUM(`click_num`) as click_num,";
        $sql .= " SUM(`like_num`) as like_num,";
        $sql .= " SUM(`comment_num`) as comment_num, (SUM(`vote_agree_num`)+SUM(`vote_disagree_num`)) as vote_num";
        $sql .= " FROM `article_title`";
        $sql .= " WHERE";
        $sql .= " `is_delete` = '0' AND `classify_id` in ('" . $classify_ids . "') AND";
        $sql .= " `author_id` = " . $user_id . " AND";
        $sql .= " status = 1";
        return DB::select($sql);
    }


    public function artworkCount($user_id)
    {
        $sql = "SELECT `artist_id`, COUNT(*) as article_num, SUM(`click_num`) as click_num,";
        $sql .= " SUM(`like_num`) as like_num,";
        $sql .= " SUM(`comment_num`) as comment_num";
        $sql .= " FROM `artwork`";
        $sql .= " WHERE";
        $sql .= " `is_delete` = '0' AND";
        $sql .= " `user_id` = " . $user_id . " AND";
        $sql .= " status = 1";
        return DB::select($sql);
    }

    public function getUserInfo(Request $request)
    {
        $userInfo = $request->user;
        $user_status['user_id'] = $userInfo['user_id'] ?? 0;
        $user_status['user_name'] = $userInfo['user_name'] ?? '';
        $user_status['user_nickname'] = $userInfo['user_nickname'] ?? '';
        $user_status['user_avatar'] = $userInfo['user_avatar'] ?? '';
        $user_status['user_sex'] = $userInfo['user_sex'] ?? 0;
        $user_status['user_mobile'] = $userInfo['user_mobile'] ?? 0;
        $user_status['authentication_type'] = $userInfo['authentication_type'] ?? 0;
        $user_status['authentication_id'] = $userInfo['authentication_id'] ?? 0;
        $statusInfo = (new YnhUser())::where(['user_id' => $userInfo['user_id']])->first();
        if (empty($statusInfo)) {
            $statusInfo['status'] = 3;
        } else {
            $statusInfo = $statusInfo->toArray();
        }
        $status = $statusInfo['status'];
        if ($status == 2) {
            $failInfo = (new YnhAuthenticationApply())::where(['user_id' => $userInfo['user_id']])
                ->orderBy('created_at', 'desc')->first();
            if (isset($failInfo['fail_cause'])) {
                $userInfo['fail_cause'] = $failInfo['fail_cause'];
            } else {
                $userInfo['fail_cause'] = '没有错误原因的呢 !';
            }
        }
        $user_status['status'] = $status;
        return response()->success($user_status);
    }

    public function getUserStatus($user_id)
    {
        $statusInfo = (new YnhUser())::where(['user_id' => $user_id])->first();
        if (empty($statusInfo)) {
            return 3;
        } else {
            $statusInfo = $statusInfo->toArray();
            return $statusInfo['status'];
        }
    }

    /**
     * @SWG\POST(
     *     path="/ynh/index/myCenter",
     *     tags={"member"},
     *     operationId="ynh_index_myCenter",
     *     summary="个人中心",
     *     description="个人中心接口",
     *     produces={"application/json"},
     *     @SWG\Response(response="default", description=""),
     *     @SWG\Response(response="200", description="successful operation", @SWG\Schema(ref="#/definitions/ApiMyCenterResponse"))
     * )
     */
    /*
     * @param Request $request
     * @return mixed
     */
    public function myCenter(Request $request)
    {
//        个人中心个人信息
//        我的主页昵称头像
//        关注 粉丝
//        个人所属文章作品
//        出售中作品和已售作品展示
        $return = [];
        $userInfo = $request->user;
        $api_token = $request->api_token;
        $bankcardLogic = new Member($api_token);
        $result = $bankcardLogic->member_index();
        $description = '';
        if ($result) {
            if ($result->code == 200) {
                if (!empty($userInfo['authentication_type'])) {
                    $artistInfo = $this->getArtistInfo(
                        $userInfo['authentication_type'],
                        $userInfo['authentication_id']
                    );
                    $role_id = $artistInfo['role_id'] ?? '';
                    $description = $artistInfo['description'] ?? '';
                    $artist_type = $this->getRoleName($role_id);
                }
                $data = $result->datas->member_info;
            }
        }
        $invite_code = $result->datas->top_member ?? '';
        //$count_price = sprintf("%.2f", $data->count_price);
        //$advance_deposit = sprintf("%.2f", $data->available_predeposit);
        //$freeze_margin = sprintf("%.2f", $data->freeze_margin);
        //$available_commission = sprintf("%.2f", $data->available_commis);
        $return['bankcardCount'] = empty($userInfo['user_id'])?0:application()->shopUserService->getBankcardCount($api_token);
        $user_id = $userInfo['user_id'] ?? 42;
        $user_info['user_id'] = $userInfo['user_id'] ?? '';
        $user_info['authentication_type'] = $userInfo['authentication_type'] ?? 0;          // 认证类型
        $user_info['artist_type'] = $artist_type ?? '申请认证';                              // 角色名称
        $user_info['description'] = $description;                           // 艺术家简介
        $user_info['user_nickname'] = $userInfo['user_nickname']
            ?: tellHide($userInfo['user_name']);                                            // 昵称
        $user_info['user_avatar'] = getMemberAvatarByID($user_id) . '?' . time();           // 头像
        $user_info['type_logo'] = $data->type_logo ?? '';                                   // 成员等级logo
        $user_info['level'] = $data->level ?? '';                                           // 会员等级
        $user_info['level_name'] = $data->level_name ?? '';                                 // 会员等级名称
        $user_info['member_type_name'] = $data->member_type_name ?? '';                     // 会员类型名称
        //$user_info['order_no_pay_count'] = $data->order_nopay_count;                        // 待支付订单数量
        //$user_info['order_no_receipt_count'] = $data->order_noreceipt_count;                // 待收货订单数量
        //$user_info['order_no_takes_count'] = $data->order_notakes_count;                    // 待自提订单数量
        //$user_info['order_no_eval_count'] = $data->order_noeval_count;                      // 待评价订单数量
        //$user_info['count_price'] = $count_price;                                           // 总资产
        //$user_info['advance_deposit'] = $advance_deposit;                                   // 预存款余额
        //$user_info['freeze_margin'] = $freeze_margin;                                       // 冻结保证金金额
        //$user_info['point'] = $data->member_points;                                         // 诺币
        $user_info['count_partner'] = $data->count_partner ?? 0;                                 // 一二级合伙人总数
        //$user_info['available_commission'] = $available_commission;                         // 奖励金总额
        //$user_info['rpt_num'] = $data->rpt_num;                                             // 红包数量
        $user_info['invite_code'] = $invite_code->dis_code ?? '';                                 // 邀请码

        $user_info['user_sex'] = $userInfo['user_sex'] ?? 0;                               // 用户性别
        $user_info['authentication_id'] = $userInfo['authentication_id'] ?? 0;              // 用户认证艺术家或机构ID
        //我的关注
        $follows_num = (new UserFollow())->followsNumGet($user_id) ?: 0;
        $fans_data = (new UserFollow())->fansGet($user_id) ?: 0;
        if (empty($fans_data)) {
            $fans['count'] = 0;
        } else {
            $fans['count'] = count($fans_data);
            if (empty($userInfo['fans_num'])) {
                $fans['is_show'] = 0;
            } else {
                $fans['is_show'] = 1;
            }
        }

        //评论数量等于 文章评论数 加作品评论数量  和点赞数量一起查询
        //$article_count = (new ArticleTitle())->countNumGet($user_id);
        //$article_comment_num = $article_count['comment_num'] ?: 0;
        $article_accepter__num = (new ArticleComment())->countNumGet($user_id);
        $artwork_accepter__num = (new ArtworkComment())->countNumGet($user_id);
        $article_like_num = (new ArticleLike())->setConnection($userInfo['db'])->countLikeNum($user_id);
        //$artwork_count = (new Artwork())->countNumGet($user_id);
        //$artwork_comment_num = $artwork_count['comment_num'] ?: 0;
        $artwork_like_num = (new ArtworkLike())->setConnection($userInfo['db'])->countLikeNum($user_id);


        if (empty($userInfo['comment_num'])) {
            $comment['is_show'] = 0;
        } else {
            $comment['is_show'] = 1;
        }
        //$article_comment_num + $artwork_comment_num +
        $comment['count'] = $article_accepter__num + $artwork_accepter__num;

        $like_num = $article_like_num + $artwork_like_num;
        $appointment['count'] = (new AppointmentSign())->appointmentNumGet($user_id) ?: 0;
        //获取未阅读的约展信息
        if (empty($userInfo['appointment_num'])) {
            $appointment['is_show'] = 0;
        } else {
            $appointment['is_show'] = 1;
        }
        $read_time = $userInfo['read_time'];

        //获取未读系统通知消息列表
        //$notification['count'] = (new AppBroadcasts())->notificationCountGet() ?: 0;
        $notification['count'] = $notificationRead = (new AppBroadcasts())->notificationNoRead($read_time) ?: 0;
        if (empty($notificationRead)) {
            $notification['is_show'] = 0;
        } else {
            $notification['is_show'] = 1;
        }

        $return['status'] = $this->getUserStatus($user_id);                         // 用户认证状态
        $return['user_info'] = $user_info;                                          // 用户信息
        $return['follows_num'] = $follows_num;                                      // 关注数目
        $return['fans'] = $fans;                                                    // 粉丝数目
        $return['comment'] = $comment;                                              // 评论数量
        $return['like_num'] = $like_num;                                            // 点赞数目
        $return['appointment'] = $appointment;                                      // 约展信息数目
        $return['notification'] = $notification;                                    // 系统消息数目
        return response()->success($return);
    }


    private function getRoleName($role_id)
    {
        $role = DB::table('artist_role')->where('role_id', $role_id)->first();
        return $role['role_name'];
    }

    public function getArtistInfo($artist_type, $artist_id)
    {
        if ($artist_type == 1) {
            $model = new ArtistPersonal();
        } else {
            $model = new ArtistOrganization();
        }
        return $model->where('id', $artist_id)->first();
    }
}
