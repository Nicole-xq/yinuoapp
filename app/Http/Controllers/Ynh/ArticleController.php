<?php

namespace App\Http\Controllers\Ynh;

use App\Libs\phpQuery\OSS;
use App\Models\ArticleClassify;
use App\Models\ArticleLike;
use App\Models\ArticleTitle;
use App\Models\ArtistPersonal;
use App\Models\UserIndex;
use App\Models\UserToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;
use Validator;
use App\Http\Controllers\Controller;
use vod\Request\V20170321\CreateUploadVideoRequest;
use vod\Request\V20170321\DeleteVideoRequest;
use vod\Request\V20170321\GetPlayInfoRequest;

class ArticleController extends Controller
{
    // 新闻ID
    const NEWS = 1;

    public function __construct()
    {
        $this->middleware('verify.user.login', ['only' => [
            'articleList',
            'articleInsert',
            'articleDelete',
            'fileUpload',
            'getArticle'
        ]]);
    }


    public function getUserIdByArtistId(Request $request){
        $artistId = $request->input("artistId", null);
        $apiToken = $request->input("api_token", "");
        if(!$artistId){
            return response()->fail(200014);
        }
        $artistP = ArtistPersonal::where("id", $artistId)->first(["user_id"]);
        if(!$artistP){
            return response()->success(0);
        }
        //跳转
        return response()->redirectTo(env('SHARE_URL', '') . "/personalHomePage?user_id={$artistP['user_id']}&api_token={$apiToken}");
    }

    /**
     * 获取文章列表
     * @param Request $request
     * @return mixed
     */
    public function articleList(Request $request)
    {
        //新闻分类下的都是文章   新闻分类ID 1
        $type = $request->type;
        $user_id = $request->user['user_id'];
        $authentication_id = $request->user['authentication_id'];
        $table = [
            'news' => 1,
            'artReview' => 2,
        ];
        if (empty($authentication_id)) {
            return response()->fail(200200);
        }
        if (empty($type) || !array_key_exists($type, $table) || empty($user_id)) {
            return response()->fail(200224);
        }
        $condition = [
            'author_id' => $user_id,
            'is_delete' => 0
        ];
        $fields = [
            'id', 'title', 'like_num', 'click_num', 'picture',
            'comment_num', 'status', 'is_recommend',
            'is_top', 'created_at', 'classify_id'
        ];
        $articleList = (new ArticleTitle(['admin' => 'admin']))->articleList($condition, $table[$type], $fields);
        if (!empty($articleList) && is_object($articleList)) {
            $articleList = json_encode($articleList);
            $articleList = json_decode($articleList, true);
        }
        if ($articleList && is_array($articleList['data'])) {
            foreach ($articleList['data'] as &$value) {
                $img_list = json_decode($value['picture']) ?: [];
                if ($img_list) {
                    $picture = getPicturePath($img_list[0]);
                    $value['picture'] = $picture ? $picture . '?x-oss-process=image/resize,h_300' : '';
                } else {
                    $value['picture'] = '';
                }
            }
        }
        return response()->success($articleList);
    }


    /**
     * 个人中心- 艺术家主页文章列表
     * @param Request $request
     * @return mixed
     */
    public function personalArticleList(Request $request)
    {
        //新闻分类下的都是文章   新闻分类ID 1
        $user_id = $request->user_id;
        $api_token = $request->api_token;
        if (!empty($api_token)) {
            $login_info = (new UserIndex())->getUserInfoByToken($api_token);
            if (!is_array($login_info)) {
                return response()->fail($login_info);
            }
        }
        if (empty($user_id)) {
            return response()->fail(200224);
        }
        $condition = [
            'author_id' => $user_id,
            'is_delete' => 0,
            'status' => 1       //前台展示 显示审核通过文章
        ];
        $fields = [
            'id', 'title', 'picture', 'is_top', 'classify_id', 'like_num', 'click_num', 'comment_num'
        ];
        $article_list = (new ArticleTitle())->articleList($condition, 1, $fields);
        if (empty($article_list) || !isset($article_list['data'])) {
            return response()->success([]);
        }
        if (isset($login_info) && $login_info) {
            $article_ids = array_column($article_list['data'], 'id');
            $article_like_ids = (new ArticleLike())
                ->doesLikeJudgeByIds($article_ids, $login_info['user_id'], $login_info['db']);
        }
        foreach ($article_list['data'] as &$item) {
            $is_like = 0;
            if (isset($article_like_ids)) {
                $is_like = in_array($item['id'], $article_like_ids) ? 1 : 0;
            }
            $item['is_like'] = $is_like;

            $array = [];
            if (!empty($item['picture'])) {
                $picture = json_decode($item['picture'], true);
                foreach ($picture as $k => $v) {
                    $end = empty($item['is_top']) ? 3 : 1;
                    if ($k == $end) {
                        break;
                    }
                    $array[] = getPicturePath($v);
                }
            }
            $item['picture'] = $array;
        }
        return response()->success($article_list);
    }

    /**
     * 文章新增
     * @param Request $request
     * @throws \OSS\Core\OssException
     * @return mixed
     */
    public function articleInsert(Request $request)
    {
        //判断是修改文章还是新增文章 是否存在article_id
        $article_id = $request->article_id;
        $db_name = $request->user['db'];
        $user_id = $request->user['user_id'];

        $result = $this->articleListCheck($request);
        if ($result['code'] !== 200) {
            return response()->fail($result['data']);
        }
        $surface = $request->surface;
        if (!empty($surface)) {
            //base64 接收封面图//封面图片处理
            $surfaceUrl = $this->surfaceDispose($surface);
            $article_title_insert['picture'] = $surfaceUrl ?: [];
            $article_insert['pic'] = $surfaceUrl ?: [];
            $article_insert['share_pic'] = (json_decode($surfaceUrl))[0] ?? '';
        }
        $params['title'] = trim($request->title);
        $params['description'] = $request->description;
        $params['classify_id'] = $request->classify_id;
        $params['author_name'] = $request->author_name;
        $params['city_id'] = $request->city_id;
        $params['author_id'] = $request->author_id ?: $user_id;
        $params['created_at'] = date("Y-m-d H:i:s");

        $article_title_insert['city_id'] = $request->city_id;
        $article_title_insert = array_merge($params, $article_title_insert);
        //分库数据表插入数据
        $article_insert['article_id'] = $request->article_id;
        $article_insert['share_description'] = $request->description;
        $article_insert['content'] = $request->contents;
        $article_insert['province_id'] = $request->province_id;
        $article_insert['addr'] = $request->addr;
        $article_insert['city_id'] = $request->city_id;
        $article_insert = array_merge($params, $article_insert);

        // 索引库开启事物
        DB::beginTransaction();
        DB::connection($db_name)->beginTransaction();
        try {
            if (empty($article_id)) {
                $result1 = DB::connection()->table('article_title')->insertGetId($article_title_insert);
                if (!$result1) {
                    throw new Exception(503004);
                }
                $article_insert['article_id'] = $result1;
                $result2 = DB::connection($db_name)->table('article')->insert($article_insert);
                if (!$result2) {
                    throw new Exception(503004);
                }
            } else {
                $article_title_insert['status'] = 0;
                $result1 = DB::connection()->table('article_title')
                    ->where(['id' => $article_id])->update($article_title_insert);
                if (!$result1) {
                    throw new Exception(200108);
                }
                $article_insert['status'] = 0;
                $result2 = DB::connection($db_name)->table('article')
                    ->where(['article_id' => $article_id])->update($article_insert);
                if (!$result2) {
                    throw new Exception(200108);
                }
            }
            DB::commit();
            DB::connection($db_name)->commit();
        } catch (Exception $e) {
            DB::rollback();
            DB::connection($db_name)->rollback();
            Log::error('错误详情:获取token-' . $e->getMessage() . '.');
            return response()->fail($e->getMessage());
        }
        return response()->success();
    }

    /**
     * @param $surface
     * @return array|mixed
     * @throws \OSS\Core\OssException
     */
    public function surfaceDispose($surface)
    {
        $surface = json_decode($surface);
        if (!is_array($surface)) {
            return response()->fail(200002);
        }
        return json_encode($this->pictureProcessing($surface));
    }

    /**
     * 图片上传返回路径
     * @param $share_pic
     * @return array|mixed
     * @throws \OSS\Core\OssException
     */
    public function pictureProcessing($share_pic)
    {
        $shareUrl = [];
        foreach ($share_pic as $value) {
            if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $value, $result)) {
                $type = $result[2];
                if (in_array($type, array('pjpeg', 'jpeg', 'jpg', 'gif', 'bmp', 'png'))) {
                    $oss = new OSS();
                    $object = 'head_img/article_' . rand(1000, 9999) . time() . '.' . $type;
                    $content = base64_decode(str_replace($result[1], '', $value));
                    $oss->putObject($object, $content);
                    $shareUrl[] = $object;
                } else {
                    return response()->fail(503001);
                }
            } else {
                //判断图片是否存在oss
                if (!empty($value)) {
                    $shareUrl[] = $value;
                }
            }
        }
        return $shareUrl;
    }

    /**
     * 文章参数检查
     * @param $request
     * @return mixed
     */
    public function articleListCheck($request)
    {
        $rule = [
            'title' => 'required|max:50',
            'description' => 'required',
            'classify_id' => 'required',
            'author_name' => 'required',
            'contents' => 'required',
            'province_id' => 'required',
            'city_id' => 'required',
            'surface' => 'required',
        ];
        $message = [
            '*.required' => 200224,
            'title.max' => '标题长度过长 !'
        ];
        $validator = Validator::make($request->all(), $rule, $message);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return getReturn($errors->all()[0], false);
            //return response()->fail($errors->all()[0]);
        }
        return getReturn();
    }

    /**
     * 文章删除-- 软删除
     * @param Request $request
     * @return mixed
     */
    public function articleDelete(Request $request)
    {
        $db_name = $request->user['db'];
        $user_id = $request->user['user_id'];
        $article_id = $request->article_id;
        if (empty($article_id)) {
            return response()->fail(200224);
        }
        // 索引库开启事物
        DB::beginTransaction();
        DB::connection($db_name)->beginTransaction();
        try {
            $update = ['is_delete' => 1];
            $result1 = DB::connection()->table('article_title')
                ->where(['id' => $article_id, 'author_id' => $user_id])
                ->update($update);
            if (!$result1) {
                throw new Exception(200109);
            }
            $result2 = DB::connection($db_name)->table('article')
                ->where(['article_id' => $article_id, 'author_id' => $user_id])
                ->update($update);
            if (!$result2) {
                throw new Exception(200109);
            }
            DB::commit();
            DB::connection($db_name)->commit();
        } catch (Exception $e) {
            DB::rollback();
            DB::connection($db_name)->rollback();
            Log::error('错误详情:获取token-' . $e->getMessage() . '.');
            return response()->fail($e->getMessage());
        }
        return response()->success();
    }


    public function fetchClassify(Request $request)
    {
        $table = [
            'news' => 1,
            'artReview' => 2,
        ];
        $type = $request->type;
        if (empty($type) || !array_key_exists($type, $table)) {
            return response()->fail(200224);
        }

        $condition = ['parent_id' => $table[$type]];
        $classifyList = (new ArticleClassify())->getList($condition);
        return response()->success($classifyList);
    }

    public function fileUpload()
    {
        $data = $this->pictureProcessing1($_FILES);
        if (empty($data)) {
            $return = [
                'errno' => 1,
                'msg' => '文件上传错误'
            ];
            echo json_encode($return);
            die;
        }
        $return = [
            'errno' => 0,
            'data' => $data
        ];
        echo json_encode($return);
        die;
    }

    public function videoUpload(Request $request)
    {
        try {
            $client = $this->init_vod_client('LTAI10eqxhsCDmz3', 'QjlZInK8htuVie2nOkoSMgvorjnbZd');
            $uploadInfo = $this->create_upload_video($client, $request->video_name);
            $info = json_decode(json_encode($uploadInfo, true));
            return response()->success($info);
            var_dump($uploadInfo);
        } catch (Exception $e) {
            print $e->getMessage() . "\n";
        }
    }

    function init_vod_client($accessKeyId, $accessKeySecret)
    {
        require 'vendor/aliyun-php-sdk/aliyun-php-sdk-core/Config.php';
        $regionId = 'cn-shanghai';  // 点播服务所在的Region，国内请填cn-shanghai，不要填写别的区域
        $profile = \DefaultProfile::getProfile($regionId, $accessKeyId, $accessKeySecret);
        return new \DefaultAcsClient($profile);
    }

    /**
     * 获取视频上传3要素
     * @param $client
     * @param $video_name
     * @return mixed
     */
    public function create_upload_video($client, $video_name)
    {
        $request = new CreateUploadVideoRequest();
        $request->setTitle($video_name);        // 视频标题(必填参数)
        $request->setFileName("{$video_name}"); // 视频源文件名称，必须包含扩展名(必填参数)
//        $request->setDescription("视频描述");  // 视频源文件描述(可选)
//        $request->setCoverURL("http://img.alicdn.com/tps/TB1qnJ1PVXXXXXCXXXXXXXXXXXX-700-700.png"); // 自定义视频封面(可选)
//        $request->setTags("标签1,标签2"); // 视频标签，多个用逗号分隔(可选)
        $request->setAcceptFormat('JSON');
        return $client->getAcsResponse($request);
    }

    function getPlayInfo()
    {
        try {
            $client = $this->init_vod_client('LTAI10eqxhsCDmz3', 'QjlZInK8htuVie2nOkoSMgvorjnbZd');
            $playInfo = $this->get_play_info($client, $_GET['video_id']);
            $playInfo = json_decode(json_encode($playInfo, true), true);
            $info = array(
                'base_info' => $playInfo['VideoBase'],
                'play_info' => $playInfo['PlayInfoList']['PlayInfo']
            );
            return response()->success($info);
        } catch (Exception $e) {
            print $e->getMessage() . "\n";
        }
    }

    /**
     * 获取视频播放信息
     * @param $client
     * @param $videoId
     * @return mixed
     */
    function get_play_info($client, $videoId)
    {
        $request = new GetPlayInfoRequest();
        $request->setVideoId($videoId);
        $request->setFormats('mp4');
        $request->setAuthTimeout(3600 * 24);    // 播放地址过期时间（只有开启了URL鉴权才生效），默认为3600秒，支持设置最小值为3600秒
        $request->setAcceptFormat('JSON');
        return $client->getAcsResponse($request);
    }

    function deleteVideos()
    {
        $video_ids = $_GET['ids'];
        try {
            $client = $this->init_vod_client('LTAI10eqxhsCDmz3', 'QjlZInK8htuVie2nOkoSMgvorjnbZd');
            $delInfo = $this->delete_videos($client, $video_ids);
            var_dump($delInfo);
        } catch (Exception $e) {
            print $e->getMessage() . "\n";
        }
    }

    /**
     * 删除视频
     * @param $client
     * @param $videoIds
     * @return mixed
     */
    function delete_videos($client, $videoIds)
    {
        $request = new DeleteVideoRequest();
        $request->setVideoIds($videoIds);   // 支持批量删除视频；videoIds为传入的视频ID列表，多个用逗号分隔
        $request->setAcceptFormat('JSON');
        return $client->getAcsResponse($request);
    }

    public function pictureProcessing1($share_pic)
    {
        if (empty($share_pic)) {
            return [];
        }
        if (is_array($share_pic)) {
            $pictures = [];
            foreach ($share_pic as $value) {
                $ext = 'jpg';     // 扩展名
                $filePath = $value['tmp_name'];
                $oss = new OSS();
                //目标文件名
                $object = 'head_img/article_' . rand(1000, 9999) . time() . '.' . $ext;
                $result = $oss->upload($filePath, $object);
                if (!$result) {
                    return response()->fail(200104);
                }
                $pictures[] = $result;
            }
            return $pictures;
        }
        if (!$share_pic->isValid()) {
            return response()->fail(503001);
        }
        $filePath = $share_pic->getRealPath();
        $ext = $share_pic->getClientOriginalExtension();     //
        $oss = new OSS();
        //目标文件名
        $object = 'head_img/article_' . rand(1000, 9999) . time() . '.' . $ext;
        $result = $oss->upload($filePath, $object);
        if (!$result) {
            return response()->fail(503001);
        }
        return $result;
    }

    public function getArticle(Request $request)
    {
        $article_id = $request->article_id;
        if (empty($article_id)) {
            return response()->fail(200224);
        }
        $db_name = $request->user['db'];
        $condition['article_id'] = $article_id;
        $article_info = DB::connection($db_name)->table('article')->where($condition)->first();
        if (empty($article_info)) {
            return response()->fail(200009);
        }
        if ($article_info['status'] == 1) {
            return response()->fail(200105);
        }
        $pic = $article_info['pic'];
        $pic = json_decode($pic);
        if (is_array($pic)) {
            foreach ($pic as &$value) {
                $value = getPicturePath($value);
            }
        } else {
            $pic = getPicturePath($pic);
        }
        $article_info['pic'] = $pic;
        return response()->success($article_info);
    }
}