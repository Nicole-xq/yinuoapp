<?php

namespace App\Http\Controllers\Ynh;

use App\Libs\phpQuery\OSS;
use App\Models\Address;
use App\Models\ArtistOrganization;
use App\Models\ArtistPersonal;
use App\Models\Ynh\ArtistRole;
use App\Models\Ynh\ArtistType;
use App\Models\Ynh\YnhAuthenticationApply;
use App\Models\Ynh\YnhUser;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Mockery\Exception;
use Validator;
use App\Http\Controllers\Controller;

class AuthenticationController extends Controller
{
    //用户认证上传图片大小限制 单位 M
    protected $pic_max = 2048 * 1024;


    /**
     * AuthenticationController constructor.
     */
    public function __construct()
    {
        $this->middleware('verify.user.login', ['only' => [
            'authenticationInsert', 'fetchLast',
        ]]);
    }

    /**
     * 发起认证申请
     * @param Request $request
     * @param $type
     * @throws \OSS\Core\OssException
     * @return mixed
     */
    public function authenticationInsert(Request $request, $type = '')
    {
        $user_id = $request->user['user_id'];
        $user_key = $request->user['user_key'];
        $type = $type ?: $request->type;
        if (empty($type)) {
            return response()->fail(200224);
        }
        $redis = new Redis();
        $cacheKey = 'authentication_form' . $user_id;
        if ($redis::exists($cacheKey)) {
            return response()->fail(223002);
        }
        $redis::setex($cacheKey, 5, $user_id);
        $result = (new YnhUser())::where(['user_key' => $user_key])->first();
        if (empty($result)) {
            //这里用户没有通过艺诺号登录  没有艺诺号用户信息  通过App用户信息进行添加
            //艺诺号用户数据插入
            $ynh_insert['user_id'] = $user_id;
            $ynh_insert['user_key'] = $user_key;
            $ynh_insert['status'] = 3;
            $result_ynh = (new YnhUser())->insert($ynh_insert);
            if (!$result_ynh) {
                return response()->fail(300002);
            }
        } else {
            if ($result['status'] == 0) {
                return response()->fail(200201);
            }
            if ($result['status'] == 1) {
                return response()->fail(200202);
            }
        }
        $oss = new OSS();
        $oss_url = env('PICTURE_UPLOAD_URL', 'https://testyinuo2.oss-cn-beijing.aliyuncs.com');
        $pose = '.com';
        switch ($type) {
            case 'personal':
                $nation = $request->nation;
                switch ($nation) {
                    case '1':
                        $identification_num = $request->identification_num;
                        $user_sex = $this->getSex($identification_num);
                        $params['province_id'] = $request->province_id ?? '';
                        $params['city_id'] = $request->city_id ?? '';
                        $params['area_id'] = $request->area_id ?? '';
                        $params['identification_num'] = $request->identification_num ?? '';
                        $picture['identification_pic']['ID_pic_front'] = $request->ID_pic_front ?? '';
                        $picture['identification_pic']['ID_pic_verso'] = $request->ID_pic_verso ?? '';
                        if (empty($params['province_id'])
                            || empty($params['city_id'])
                            || empty($params['identification_num']) || empty($picture['identification_pic'])) {
                            return response()->fail(200224);
                        }
                        break;
                    case '2':
                        $user_sex = $request->user_sex ?? 1;
                        $params['identification_num'] = $request->passport_num ?? '';
                        $picture['identification_pic']['passport_pic'] = $request->passport_pic ?? '';
                        if (empty($params['identification_num'])
                            || empty($picture['identification_pic']['passport_pic'])) {
                            return response()->fail(200224);
                        }
                        break;
                    default:
                        return response()->fail(200224);
                }
                $basePath = 'personal_auth/' . $user_id;
                $authentication_type = 1;
                $result = $this->personalInsertCheck($request);
                if ($result !== true) {
                    return response()->fail($result);
                }
                $picture['real_avatar'] = $request->real_avatar;
                $picture['artwork_pic'] = $request->artwork_pic;
                $artworkUrl = $request->input('artwork_pic');
                if (isset($artworkUrl) && $artworkUrl) {
                    if (!is_array($artworkUrl)) {
                        return response()->fail(200224);
                    }
                    $picture['artwork_pic'] = array_merge($picture['artwork_pic'], $artworkUrl);
                    $picture['artwork_pic'] = array_unique($picture['artwork_pic']);
                }
                if ($request->document_pic) {
                    $picture['document_pic'] = $request->document_pic;
                    $documentUrl = $request->input('document_pic');
                    if (isset($documentUrl) && $documentUrl) {
                        if (!is_array($documentUrl)) {
                            return response()->fail(200224);
                        }
                        $picture['document_pic'] = array_merge($picture['document_pic'], $documentUrl);
                        $picture['document_pic'] = array_unique($picture['document_pic']);
                    }
                }
                $picture = array_filter($picture);
                foreach ($picture as $key => &$value) {
                    if (empty($value)) {
                        continue;
                    }
                    if (is_array($value)) {
                        $pic_path = [];
                        foreach ($value as $k => $item) {
                            if (is_string($item)) {
                                if (strpos($item, $pose) !== false) {
                                    $pic_url = substr(strstr($item, $pose), strlen($pose) + 1);
                                    $pic_path[] = $pic_url;
                                } else {
                                    return response()->fail(200204);
                                }
                            } else {
                                $filename = $item->getClientOriginalName();
                                if (empty($filename)) {
                                    return response()->fail(503001);
                                }
                                $size = $item->getClientSize();
                                if ($size > $this->pic_max) {
                                    return response()->fail(503006);
                                }
                                $extension = strrchr($filename, '.');
                                if (empty($extension)) {
                                    return response()->fail(223001);
                                }
                                $PicObject = $basePath . '/' . $k . rand(1000, 9999) . $extension;
                                $pic_url = $oss->upload($item->getRealPath(), $PicObject);
                                if ($pic_url) {
                                    $pic_url = substr(strstr($pic_url, $pose), strlen($pose) + 1);
                                    $pic_path[] = $pic_url;
                                } else {
                                    return response()->fail(200104);
                                }
                            }
                        }
                    } else {
                        if (is_string($value)) {
                            if (strpos($value, $pose) !== false) {
                                $pic_url = substr(strstr($value, $pose), strlen($pose) + 1);
                                $pic_path = $pic_url;
                            } else {
                                return response()->fail(200204);
                            }
                        } else {
                            $filename = $value->getClientOriginalName();
                            if (empty($filename)) {
                                return response()->fail(503001);
                            }
                            $size = $value->getClientSize();
                            if ($size > $this->pic_max) {
                                return response()->fail(503006);
                            }
                            $extension = strrchr($filename, '.');
                            if (empty($extension)) {
                                return response()->fail(223001);
                            }
                            $PicObject = $basePath . '/' . $key . rand(1000, 9999) . $extension;
                            $pic_url = $oss->upload($value->getRealPath(), $PicObject);
                            if ($pic_url) {
                                $pic_url = substr(strstr($pic_url, $pose), strlen($pose) + 1);
                                $pic_path = $pic_url;
                            } else {
                                return response()->fail(200104);
                            }
                        }
                    }
                    //个别图片不需要json_encode()
                    //$value = json_encode($pic_path);
                    $value = $pic_path;
                }
                //将数组转化
                extract($picture);
                $params['real_name'] = $request->real_name;
                $params['nick_name'] = $request->nick_name;
                $params['phone_num'] = $request->phone_num;
                $params['user_sex'] = $user_sex;
                $params['nation'] = $request->nation;
                $params['role_id'] = $request->role_id;
                if ($request->role_id == 2) {
                    $params['artist_type'] = $request->artist_type;
                }
                $params['description'] = $request->description;
                $params['real_avatar'] = $real_avatar ?? '';
                $params['identification_pic'] = isset($identification_pic) ? json_encode($identification_pic) : '';
                $params['artwork_pic'] = isset($artwork_pic) ? json_encode($artwork_pic) : '';   //多张图片数组 json入库
                $params['document_pic'] = isset($document_pic) ? json_encode($document_pic) : '';//多张图片数组 json入库
                break;
            case 'organization':
                $authentication_type = 2;
                $result = $this->organizationInsertCheck($request);
                if ($result !== true) {
                    return response()->fail($result);
                }
                $basePath = 'organization_auth/' . $user_id;
                $picture['logo'] = $request->logo;
                $picture['document_pic'] = $request->document_pic;
                $documentUrl = $request->input('document_pic');
                if (isset($documentUrl) && $documentUrl) {
                    if (!is_array($documentUrl)) {
                        return response()->fail(200224);
                    }
                    $picture['document_pic'] = array_merge($picture['document_pic'], $documentUrl);
                    $picture['document_pic'] = array_unique($picture['document_pic']);
                }
                $picture['identification_pic']['ID_pic_front'] = $request->ID_pic_front;
                $picture['identification_pic']['ID_pic_verso'] = $request->ID_pic_verso;
                if (!empty($request->brand_pic)) {
                    $picture['brand_pic'] = $request->brand_pic;
                    $brandUrl = $request->input('brand_pic');
                    if (isset($brandUrl) && $brandUrl) {
                        if (!is_array($brandUrl)) {
                            return response()->fail(200224);
                        }
                        $picture['brand_pic'] = array_merge($picture['brand_pic'], $brandUrl);
                        $picture['brand_pic'] = array_unique($picture['brand_pic']);
                    }
                }
                foreach ($picture as $key => &$value) {
                    if (is_array($value)) {
                        $pic_path = [];
                        foreach ($value as $k => $item) {
                            if (is_string($item)) {
                                if (strpos($item, $pose) !== false) {
                                    $pic_url = substr(strstr($item, $pose), strlen($pose) + 1);
                                    $pic_path[] = $pic_url;
                                } else {
                                    return response()->fail(200204);
                                }
                            } else {
                                $filename = $item->getClientOriginalName();
                                if (empty($filename)) {
                                    return response()->fail(503001);
                                }
                                $size = $item->getClientSize();
                                if ($size > $this->pic_max) {
                                    return response()->fail(503006);
                                }
                                $extension = strrchr($filename, '.');
                                if (empty($extension)) {
                                    return response()->fail(223001);
                                }
                                $PicObject = $basePath . '/' . $k . rand(1000, 9999) . $extension;
                                $pic_url = $oss->upload($item->getRealPath(), $PicObject);
                                if ($pic_url) {
                                    $pic_url = substr(strstr($pic_url, $pose), strlen($pose) + 1);
                                    $pic_path[] = $pic_url;
                                } else {
                                    return response()->fail(200104);
                                }
                            }
                        }
                    } else {
                        if (is_string($value)) {
                            if (strpos($value, $pose) !== false) {
                                $pic_url = substr(strstr($value, $pose), strlen($pose) + 1);
                                $pic_path = $pic_url;
                            } else {
                                return response()->fail(200204);
                            }
                        } else {
                            $filename = $value->getClientOriginalName();
                            if (empty($filename)) {
                                return response()->fail(503001);
                            }
                            $size = $value->getClientSize();
                            if ($size > $this->pic_max) {
                                return response()->fail(503006);
                            }
                            $extension = strrchr($filename, '.');
                            if (empty($extension)) {
                                return response()->fail(223001);
                            }
                            $PicObject = $basePath . '/' . $key . rand(1000, 9999) . $extension;
                            $pic_url = $oss->upload($value->getRealPath(), $PicObject);
                            if (!empty($pic_url)) {
                                $pic_url = substr(strstr($pic_url, $pose), strlen($pose) + 1);
                                $pic_path = $pic_url;
                            } else {
                                return response()->fail(200104);
                            }
                        }
                    }
                    //个别图片不需要json_encode()
                    //$value = json_encode($pic_path);
                    $value = $pic_path;
                }
                //将数组转化
                extract($picture);
                $params = [
                    'real_name' => $request->real_name,
                    'nick_name' => $request->nick_name,
                    'role_id' => $request->role_id,
                    'province_id' => $request->province_id,
                    'city_id' => $request->city_id,
                    'area_id' => $request->area_id,
                    'address' => $request->address,
                    'phone_num' => $request->phone_num,
                    'description' => $request->description,
                    'name' => $request->name,
                    'identification_num' => $request->identification_num,
                    'trinity_num' => $request->trinity_num,
                    'logo' => $logo ?? '',
                    'document_pic' => isset($document_pic) ? json_encode($document_pic) : '',
                    'identification_pic' => isset($identification_pic) ? json_encode($identification_pic) : '',
                    'brand_pic' => isset($brand_pic) ? json_encode($brand_pic) : '',              //多张图片数组 json入库
                ];
                break;
            default:
                return response()->fail(200224);
        }
        // 索引库开启事物
        DB::beginTransaction();
        try {
            $model = new YnhAuthenticationApply();
            $params['user_id'] = $user_id;
            $params['authentication_type'] = $authentication_type;
            $result1 = $model::insert($params);
            if (!$result1) {
                throw new Exception(200107);
            }
            $result3 = (new YnhUser())::where('user_key', $user_key)
                ->update(['status' => 0]);
            if (!$result3) {
                throw new Exception(200106);
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            Log::error('错误详情:获取token-' . $e->getMessage() . '.');
            return response()->fail($e->getMessage());
        }
        $redis::del($cacheKey);
        return response()->success();
    }

    /**
     * 通过姓名获取艺术家信息
     * @param Request $request
     * @return mixed
     */
    public function fetchArtistInfoByName(Request $request)
    {
        $name = $request->name;
        $type = $request->type;
        if (empty($name) || empty($type)) {
            return response()->success();
        }
        switch ($type) {
            case 'personal':
                $model = new ArtistPersonal();
                $fields = [
                    'id', 'name', 'real_name', 'logo',
                ];
                $artists = $model::select($fields)->where(['user_id' => 0])
                    ->where('name', 'like', $name . '%')->get()->toArray();
                foreach ($artists as &$value) {
                    $value['real_name'] = $value['real_name'] ?: '';
                    $value['logo'] = getPicturePath(json_decode($value['logo'])) ?: '';
                }
                break;
            case 'organization':
                $model = new ArtistOrganization();
                $fields = [
                    'id', 'name', 'real_name', 'logo',
                ];
                $artists = $model::select($fields)->where(['user_id' => 0])
                    ->where('name', 'like', $name . '%')->get()->toArray();
                foreach ($artists as &$value) {
                    $value['real_name'] = $value['real_name'] ?: '';
                    $value['logo'] = getPicturePath(json_encode($value['logo'])) ?: '';
                }
                break;
            default:
                $artists = '';
                break;
        }
        return response()->success($artists);
    }

    /**
     * 个人绑定用户认证参数检查
     * @param $request
     * @return mixed
     */
    public function personalBingCheck($request)
    {
        $rule = [
            'identification_num' => 'required',
            'identification_pic_front' => 'required',
            'identification_pic_verso' => 'required',
        ];
        $message = [
            '*.required' => 200224
        ];
        $validator = Validator::make($request->all(), $rule, $message);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->fail($errors->all()[0]);
        }
        $params['authentication_id'] = $request->authentication_id;
        $params['identification_num'] = $request->identification_num;
        return $params;
    }

    /**
     * 个人新增参数检查
     * @param $request
     * @return mixed
     */
    public function personalInsertCheck($request)
    {
        $rule = [
            'real_name' => 'required',          //真实姓名
            'nick_name' => 'required',          //认证昵称
            'phone_num' => 'required',          //手机号
            //'user_sex' => 'required',           //性别  性别客户端不传输
            'nation' => 'required|regex:/^(?!0+$)^\d*$$/',             //国家 -- 1中国 or 2海外
            //'province_id' => 'required',        //省
            //'city_id' => 'required',            //市
            //'area_id' => 'required',            //区
            'role_id' => 'required|regex:/^(?!0+$)^\d*$$/',            //个人认证类型
            //'artist_type' => 'required',        //个人绘画类型
            'real_avatar' => "required",  //个人照片
            //'identification_num' => 'required', //身份证号
            //'ID_pic_front' => "required|image|max:$this->pic_max", //身份证正面照片
            //'ID_pic_verso' => "required|image|max:$this->pic_max", //身份证反面照片
            //'passport_num' => 'required',
            //'passport_pic' => "required|image|max:$this->pic_max",
            //'artwork_pic' => "required",  //作品图片
            //'document_pic' => "required|image|max:$this->pic_max", //奖项证书图片 x选填字段
            'description' => 'required',        //个人简介
        ];
        $message = [
            '*.required' => 200224,
            '*.image' => 503001,
            '*.regex' => 200002,
            //'*.min' => 503006,
            //'*.max' => 503006,
        ];
        $validator = Validator::make($request->all(), $rule, $message);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $errors->all()[0];
        }
        return true;
    }

    /**
     * 机构认证参数检查
     * @param $request
     * @return mixed
     */
    public function organizationInsertCheck($request)
    {
        $rule = [
            'real_name' => "required",          //公司名称
            'nick_name' => 'required',          //公司简称
            'role_id' => 'required|regex:/^(?!0+$)^\d*$$/',            //机构类型
            'logo' => "required",         //LOGO图片
            'province_id' => 'required|regex:/^(?!0+$)^\d*$$/',        //省
            'city_id' => 'required|regex:/^(?!0+$)^\d*$$/',            //市
            //'area_id' => 'required|regex:/^(?!0+$)^\d*$$/',            //区
            'address' => 'required',            //详细地址
            'phone_num' => 'required',          //联系电话
            'description' => 'required',        //机构简介
            'name' => 'required',               //法人姓名
            'identification_num' => 'required', //法人身份证号
            'trinity_num' => 'required',  //营业执照号
            'document_pic' => "required", //营业执照照片
            'ID_pic_front' => "required", //身份证正面照片
            'ID_pic_verso' => "required", //身份证反面照片
            //'brand_pic' => 'required|image',    //商标专利图片
        ];
        $message = [
            '*.required' => 200224,
            '*.regex' => 200002,
            //'*.image' => 503001,
            //'*.max' => 503006,
        ];
        $validator = Validator::make($request->all(), $rule, $message);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $errors->all()[0];
        }
        return true;
    }


    /**
     * 获取艺术家角色类型
     * @param Request $request
     * @param  $type //个人 : personal 机构 : organization
     * @return mixed
     */
    public function fetchRoleType(Request $request, $type = '')
    {
        if (empty($type)) {
            $type = $request->type;
        }
        if (empty($type)) {
            return response()->success();
        }
        switch ($type) {
            case 'personal':
                $role_type = 1;
                break;
            case 'organization':
                $role_type = 2;
                break;
            default:
                return response()->success();
                break;
        }
        $where = ['role_type' => $role_type];
        $artistRoleList = (new ArtistRole())::where($where)->get();
        if (!is_object($artistRoleList) || empty($artistRoleList)) {
            return response()->success([]);
        }
        $artistRoleList = $artistRoleList->toArray();
        $return = [];
        if (is_array($artistRoleList)) {
            $key = 0;
            foreach ($artistRoleList as $value) {
                if ($value['role_id'] != 1) {
                    $return[$key]['role_id'] = $value['role_id'];
                    $return[$key]['role_name'] = $value['role_name'];
                    $key += 1;
                }
            }
        }
        return response()->success($return);
    }


    /**
     * 用户获取上一次认证信息进行重新认证
     * @param Request $request
     * @return mixed
     */
    public function fetchLast(Request $request)
    {
        $user_id = $request->user['user_id'];
        $model = new YnhAuthenticationApply();
        $where = ['user_id' => $user_id];
        $authenticationInfo = $model->where($where)->orderBy('created_at', 'desc')->first();
        $authenticationInfo['real_avatar'] = getPicturePath($authenticationInfo['real_avatar']) ?? '';
        $authenticationInfo['document_pic'] = getPicturePath(json_decode($authenticationInfo['document_pic'])) ?? [];
        $identification_pic
            = getPicturePath(json_decode($authenticationInfo['identification_pic'])) ?? [];
        unset($authenticationInfo['identification_pic']);
        $authenticationInfo['ID_pic_front'] = isset($identification_pic[0]) ? $identification_pic[0] : '';
        $authenticationInfo['ID_pic_verso'] = isset($identification_pic[1]) ? $identification_pic[1] : '';
        $authenticationInfo['logo'] = getPicturePath($authenticationInfo['logo']) ?? '';
        $authenticationInfo['artwork_pic'] = getPicturePath(json_decode($authenticationInfo['artwork_pic'])) ?? [];
        $authenticationInfo['brand_pic'] = getPicturePath(json_decode($authenticationInfo['brand_pic'])) ?? [];
        $address = new Address();
        $province_info = $address->getInfoById($authenticationInfo['province_id'])['area_name'] ?? '上海';
        $city_info = $address->getInfoById($authenticationInfo['city_id'])['area_name'] ?? '上海市';
        $area_info = $address->getInfoById($authenticationInfo['area_id'])['area_name'] ?? '闵行区';
        $authenticationInfo['area_info'] = $province_info . $city_info . $area_info;
        return response()->success($authenticationInfo);
    }

    /**
     *  艺术类型列表
     */
    public function fetchArtistType()
    {
        $artistTypeList = (new ArtistType())::get()->toArray();
        return response()->success($artistTypeList);
    }


    public function getSex($identification_num)
    {
        $sexInt = (int)substr($identification_num, 16, 1);
        return $sexInt % 2 === 0 ? 2 : 1; //返回用户性别
    }
}
