<?php

namespace App\Http\Controllers\Ynh;

use App\Libs\phpQuery\OSS;
use App\Models\Address;
use App\Models\ArtistOrganization;
use App\Models\ArtistPersonal;
use Illuminate\Http\Request;

use Validator;
use App\Http\Controllers\Controller;

class DescriptionController extends Controller
{
    public function __construct()
    {
        $this->middleware('verify.user.login', ['only' => [
            'description',
            'descriptionUpdate',
        ]]);
    }

    /**
     * 简介显示
     * @param Request $request
     */
    public function description(Request $request)
    {
        $type = $request->user['authentication_type'];
        $artist_id = $request->user['authentication_id'];
        if (empty($type)) {
            return response()->fail(200200);
        }
        switch ($type) {
            case '1':
                $description = $this->getPersonalArtistInfo($artist_id);
                break;
            case '2':
                $description = $this->getOrganizationArtistInfo($artist_id);
                break;
            default:
                $description = [];
                break;
        }
        return response()->success($description);
    }


    /**
     * 获取个人艺术家信息
     * @param $artist_id
     * @return mixed
     */
    public function getPersonalArtistInfo($artist_id)
    {
        $model = new ArtistPersonal();
        $artist_info = $model->getArtistInfo($artist_id);
        $description['id'] = $artist_info['id'];
        $description['logo'] = is_array(json_decode($artist_info['logo']))
            ? getPicturePath(json_decode($artist_info['logo']))
            : getPicturePath($artist_info['logo']);
        $description['description'] = $artist_info['description'];
        $description['address'] = $this->getAddress($artist_info['province_id']);
        $description['address'] .= $this->getAddress($artist_info['city_id']);
        $description['address'] .= $this->getAddress($artist_info['area_id']);
        $description['address'] .= $artist_info['address'];
        return $description;
    }

    /**
     * 获取机构艺术家信息
     * @param $artist_id
     * @return mixed
     */
    public function getOrganizationArtistInfo($artist_id)
    {
        $model = new ArtistOrganization();
        $artist_info = $model::where(['id' => $artist_id])->first();
        $description['id'] = $artist_info['id'];
        $description['logo'] = is_array(json_decode($artist_info['logo']))
            ? getPicturePath(json_decode($artist_info['logo']))
            : getPicturePath($artist_info['logo']);
        $description['description'] = $artist_info['description'];
        $description['address'] = $this->getAddress($artist_info['province_id']);
        $description['address'] .= $this->getAddress($artist_info['city_id']);
        $description['address'] .= $this->getAddress($artist_info['area_id']);
        $description['address'] .= $artist_info['address'];
        $description['addr'] = $artist_info['addr'] ?: '1,0';
        return $description;
    }

    /**
     * 获取省市区ID
     * @param $address_id
     * @return mixed
     */
    public function getAddress($address_id)
    {
        $address_info = (new Address())->getInfoById($address_id);
        return $address_info['area_name'] ?: '';
    }

    /**
     * 个人简介编辑更新
     * @param $request
     * @return mixed
     */
    public function descriptionUpdate(Request $request)
    {
        $type = $request->user['authentication_type'];
        $artist_id = $request->user['authentication_id'];
        if (empty($type) || empty($artist_id)) {
            return response()->fail(200200);
        }
        $oss = new OSS();
        $model = '';
        if (!contentFilter($request->description)) {
            return response()->fail(221005);
        }
        switch ($type) {
            case '1':
                $model = new ArtistPersonal();
                $update = $this->personalUpdateCheck($request);
                if (!empty($request->logo)) {
                    $logo = $request->logo;
                    $documentPath = $logo->getRealPath();
                    //目标文件名
                    $documentPicObject = 'head_img/artist_' . rand(1000, 9999) . time() . '.jpg';
                    $result = $oss->upload($documentPath, $documentPicObject);
                    $update['logo'] = $result;
                }
                break;
            case '2':
                $model = new ArtistOrganization();
                $update = $this->organizationUpdateCheck($request);
                if (!empty($request->logo)) {
                    $logo = $request->logo;
                    $documentPath = $logo->getRealPath();
                    //目标文件名
                    $documentPicObject = 'head_img/artist_' . rand(1000, 9999) . time() . '.jpg';
                    $result = $oss->upload($documentPath, $documentPicObject);
                    $update['logo'] = $result;
                }
                break;
            default:
                $update = [];
                break;
        }
        $where = ['id' => $artist_id];
        $update['updated_at'] = date('Y-m-d H:i:s');
        if (empty($update)) {
            return response()->success([]);
        }
        $result = $model::where($where)->update($update);
        if (!$result && $result !== 0) {
            return response()->fail(200005);
        }
        return response()->success();
    }

    /**
     * 个人简介更新参数检查
     * @param $request
     * @return mixed
     */
    public function personalUpdateCheck($request)
    {
        if (!empty($request->logo)) {
            $params['logo'] = $request->logo;
        }
        if (!empty($request->description)) {
            $params['description'] = $request->description;
        }
        if (!empty($request->province_id)) {
            $params['province_id'] = $request->province_id;
        }
        if (!empty($request->city_id)) {
            $params['city_id'] = $request->city_id;
        }
        if (!empty($request->area_id)) {
            $params['area_id'] = $request->area_id;
        }
        if (!empty($request->address)) {
            $params['address'] = $request->address;
        }
        if (empty($params)) {
            return [];
        }
        return $params;
    }

    /**
     * 机构简介更新参数检查
     * @param $request
     * @return mixed
     */
    public function organizationUpdateCheck($request)
    {
        if (!empty($request->description)) {
            $params['description'] = $request->description;
        }
        if (!empty($request->province_id)) {
            $params['province_id'] = $request->province_id;
        }
        if (!empty($request->city_id)) {
            $params['city_id'] = $request->city_id;
        }
        if (!empty($request->area_id)) {
            $params['area_id'] = $request->area_id;
        }
        if (!empty($request->address)) {
            $params['address'] = $request->address;
        }
        if (!empty($request->addr)) {
            $params['addr'] = $request->addr;
        }
        if (empty($params)) {
            return [];
        }
        return $params;
    }

    public function textEditing(Request $request)
    {
        $description = $request->description;
        if (empty($description)) {
            return response()->fail(200224);
        }
        $authentication_id = $request->user['authentication_id'];
        $artist_type = $request->user['authentication_type'];
        if (empty($authentication_id)) {
            return response()->fail(200200);
        }
        switch ($artist_type) {
            case '1':
                $model = new ArtistPersonal();
                break;
            case '2':
                $model = new ArtistOrganization();
                break;
            default:
                return response()->fail(200200);
        }
        $where = ['id' => $authentication_id];
        $update = ['description' => $description];
        $result = $model->where($where)->update($update);
        if (!$result && $result !== 0) {
            return response()->fail(200005);
        }
        return response()->success([]);
    }
}
