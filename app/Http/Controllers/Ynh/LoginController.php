<?php

namespace App\Http\Controllers\Ynh;

use App\Libs\phpQuery\http;
use App\Models\UserIndex;
use App\Models\Ynh\YnhLoginLog;
use App\Models\Ynh\YnhUser;
use Illuminate\Http\Request;

use Validator;
use App\Http\Controllers\Controller;
use App\Libs\phpQuery\sms as phoneSms;

class LoginController extends Controller
{
    protected $db = array();
    protected $user_db = '';
    protected $db_num = 0;

    public function __construct()
    {
        $this->middleware('verify.ynh.user', ['only' => [
            'register',
            'login',
            'resetPassword',
        ]]);
        $db = env('DB_NEW_USER', '');
        if (!empty($db)) {
            $db = json_decode($db);
            $this->db = $db;
            $this->db_num = count($db);
        }
    }

    public function register(Request $request)
    {
        $this->registerCheck($request);
        $user_name = $request->user_name;
        $phone = $request->phone;
        $captcha = $request->captcha;
        $password = $request->password;

        //注册信息默认上海市
        $register_data = [
            'phone' => $phone,
            'captcha' => $captcha,
            'password' => $password,
            'provinceid' => 9,
            'cityid' => 9,
            'areaid' => 39,
            'areainfo' => '上海市区',
            'client' => 'wap'
        ];

        $base_url = env('BASE_URL', 'yinuo.com');
        $url = $base_url . '/api/register';
        $response = http::post($url, $register_data);
        $return = json_decode($response, true);
        if (!is_array($return) || $return['status_code'] != 200) {
            echo $response;
            die;
        }
        $user_id = $return['data']['user_id'];
        $datetime = date('Y-m-d H:i:s', time());
        $member = array();
        $member['user_name'] = $phone;
        $member['user_passwd'] = md5(trim($password));
        $member['user_mobile'] = $phone;
        $member['user_mobile_bind'] = 1;
        $member['user_nickname'] = $user_name;
        $member['created_at'] = $datetime;
        $member['updated_at'] = $datetime;

        $condition = [
            'user_id' => $user_id
        ];
        $model = new UserIndex();
        $user_info = $model->where($condition)->first();
        if (is_object($user_info)) {
            $user_info = $user_info->toArray();
        }
        $update = [
            'user_name' => $user_name
        ];
        $model->where($condition)->update($update);
        $user_key = $user_info['user_key'];

        //艺诺号用户数据插入
        $ynh_insert['user_id'] = $user_id;
        $ynh_insert['user_key'] = $user_key;
        $ynh_insert['status'] = 3;
        $result_ynh = (new YnhUser())->insert($ynh_insert);
        if (!$result_ynh) {
            return response()->fail(300002);
        }
        return response()->success($return['data']);
    }

    /**
     * 调用ucenter登录接口
     * @param
     * @return mixed
     */
    public function loginUCenter($param)
    {
        $postUrl = 'login';
        $url = env('USER_CENTER', '');
        if (empty($url)) {
            return null;
        }
        $postUrl = $url . $postUrl;
        $result = http::post($postUrl, $param);
        if ($result) {
            return json_decode($result);
        }
        return null;
    }

    /**
     * 调用ucenter注册接口
     *
     * @param
     * @return mixed
     */
    public function registerUCenter($param)
    {
        $postUrl = 'register';
        $url = env('USER_CENTER', '');
        if (empty($url)) {
            return null;
        }
        $postUrl = $url . $postUrl;
        $result = http::post($postUrl, $param);
        if ($result) {
            return json_decode($result);
        }
        return null;
    }

    /**
     * 调用ucenter修改密码接口
     *
     * @param
     * @return mixed
     */
    public function passwordUCenter($param)
    {
        $url = env('USER_CENTER', '');
        $postUrl = 'password';
        $postUrl = $url . $postUrl;
        $result = http::post($postUrl, $param);
        if ($result) {
            return json_decode($result);
        }
        return null;
    }

    /**
     * 艺诺号登录
     * @param Request $request
     * @param string $type
     * @return mixed
     */
    public function login(Request $request, $type = '')
    {
        $base_url = env('BASE_URL', 'yinuo.com');
        switch ($type) {
            case 'wx':
                $url = $base_url . '/api/login/wx';
                $login_data = [
                    'unionid' => $request->union_id,
                ];
                $response = http::post($url, $login_data);
                break;
            case 'sina':
                $url = $base_url . '/api/login/sina';
                $login_data = [
                    'unionid' => $request->union_id,
                ];
                $response = http::post($url, $login_data);
                break;
            case 'qq':
                $url = $base_url . '/api/login/qq';
                $login_data = [
                    'uid' => $request->union_id,
                ];
                $response = http::post($url, $login_data);
                break;
            case 'captcha':
                $url = $base_url . '/api/login/captcha';
                $login_data = [
                    'username' => $request->username,
                    'captcha' => $request->captcha,
                    'client' => $request->client
                ];
                $response = http::post($url, $login_data);
                break;
            default:
                $url = $base_url . '/api/login';
                $login_data = [
                    'username' => $request->username,
                    'password' => $request->password,
                    'client' => $request->client
                ];
                $response = http::post($url, $login_data);
        }
        $return = json_decode($response, true);
        if (!is_array($return) || $return['status_code'] != 200) {
            echo $response;
            die;
        }
        $user_id = $return['data']['user_id'];
        $model = new YnhUser();
        $condition = [
            'user_id' => $user_id
        ];
        $authentication_info = $model->getAuthenticationInfo($condition);
        if (is_object($authentication_info) && !empty($authentication_info)) {
            $authentication_info = $authentication_info->toArray();
        }
        $user_info = (new UserIndex())->where($condition)->first();
        if (is_object($user_info)) {
            $user_info = $user_info->toArray();
        }
        $user_key = $user_info['user_key'];
        if (empty($authentication_info)) {
            $insert = [
                'user_key' => $user_key,
                'user_id' => $user_id,
                'status' => 3,          //认证审核状态: 未申请
                'is_disable' => 0,      //认证账号是否封禁: 未封禁
            ];
            $result = $model->authenticationInsert($insert);
            if (!$result) {
                return response()->fail(300010);
            }
        }
        $status = $authentication_info['status'] ?? 3;
        //插入登录日志
        $ip = getIp();
        $logInsert = [
            'user_id' => $user_id,
            'ip' => $ip,
        ];
        $logModel = new YnhLoginLog();
        $logModel->loginLogInsert($logInsert);
        $return['data']['status'] = $status;
        return response()->success($return['data']);
    }

    /**
     * 修改密码
     * @param Request $request
     * @return mixed
     */
    public function resetPassword(Request $request)
    {
        //TODO 最新需求密码重置
        $phone = $request->phone;
        $captcha = $request->captcha;
        $password = $request->password;

        $base_url = env('BASE_URL', 'yinuo.com');
        $url = $base_url . '/api/password/reset';
        $reset_data = [
            'phone' => $phone,
            'password' => $password,
            'captcha' => $captcha,
            'client' => 'wap'
        ];
        $response = http::post($url, $reset_data);
        $return = json_decode($response, true);
        if (!is_array($return) || $return['status_code'] != 200) {
            echo $response;
            die;
        }
        return response()->success();
    }


    public function sendCaptcha(Request $request)
    {
        $type = [
            'register' => 1,
            'login' => 2,
            'reset_pwd' => 3,
        ];
        if (empty($request->phone) || empty($request->type)) {
            return response()->fail(200224);
        }
        $state = (new phoneSms())->sendCaptcha($request->phone, $type[$request->type]);

        if ($state === true) {
            return response()->success(null);
        }

        if (!is_int($state)) {
            $state = 200005;
        }
        return response()->fail($state);
    }


    public function registerCheck($request)
    {
        $rule = [
            'user_name' => 'required',
            'phone' => 'required',
            'captcha' => 'required',
            'password' => 'required',
        ];
        $message = [
            '*.required' => 200224
        ];
        $validator = Validator::make($request->all(), $rule, $message);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->fail($errors->all()[0]);
        }
    }
}
