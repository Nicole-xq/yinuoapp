<?php

namespace App\Http\Controllers;

use App\Entity\Constant\ApiResponseCode;
use App\Exceptions\ApiResponseException;
use App\Lib\Helpers\ResponseHelper;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Swagger\Annotations as SWG;

/**
 * @SWG\Swagger(
 *     host="",
 *     basePath="/",
 *     swagger="2.0",
 *     @SWG\Info(
 *     version="2.0",
 *     title="艺诺艺术艺术家API",
 *     description="
 *     App请求加入:header X-App-Ver=app版本
 *     响应结构:{status_code:200, message:'成功', data: {}}, 接口中如果没有特殊status_code,只说明data
 *     在api_key输入框可以输入token
 * <details><summary>status_code</summary>200:成功; 200001:缺少必要的参数; 200002:参数格式不正确; 200004:没有找到内容，或已被删除; 200005:操作失败; 200009:没有这篇文章; 200033:该文章没有所属作者 !; 200034:被评论人信息查询出错 !; 200032:没有这篇作品; 200031:该文章不属于你,无权操作; 200010:没有这个评论了; 200011:您已经投过票了; 200012:评论失败了; 200013:评论成功，审核中; 200014:未通过审核的评论，暂不能操作; 200020:服务器内部错误，操作失败！; 200021:您输入的手机号格式错误，请重新输入！; 200022:请输入账号！; 200023:请输入手机号！; 200024:您输入的验证码！; 200025:您输入的验证码错误，请重新输入！; 200026:您输入的验证码已经失效，请重新输入！; 200027:间隔时间倒计时期间不可以重复获取验证码！; 200028:请输入密码！; 200029:密码格式错误，请重新出入！; 200030:您输入的手机号或密码错误，请重新登录！; 200050:请选择地区; 200051:密码长度6-50; 200052:登录失败; 200053:修改密码不能与最近密码相同; 300001:数据格式错误，操作失败！; 300002:会员信息保存失败！; 300003:用户名或密码错误,请重新输入！; 300004:用户名或手机号错误,请重新输入！; 300005:验证码错误或已过期，请重新输入！; 300006:当前手机号已被注册，请更换其他号码。; 300007:请填写正确手机号; 300008:获取token失败; 300010:登录失败; 300011:帐号被限制; 300012:手机号尚未注册，无需操作！; 300013:请先登录！; 300014:用户信息已过期，请重新登录！; 300015:没有此账号！; 300016:头像上传失败！; 300017:当前手机号已被绑定，请更换其他号码。; 300018:没有找到此用户！; 300019:头像保存失败！; 300020:信息保存失败！; 200100:验证码格式错误; 200101:验证码已失效，请重新获取; 200102:验证码发送失败; 200103:60秒内不可频繁发送; 200104:图片上传失败 !; 200105:该文章审核已通过,不能修改 !; 200106:信息提交状态修改失败 !; 200107:信息提交保存失败 !; 200108:文章修改保存失败 !; 200109:文章删除失败 !; 200110:您不能关注自己 !; 200111:评论删除失败 !; 200112:该评论已不存在 !; 200200:您还没有认证艺术家 !; 200201:您的提交正在审核中 !; 200202:您已认证成功,请勿重复认证 !; 200203:艺术家信息查询出错 !; 200204:图片地址错误 !; 503001:上传文件的格式不正确; 503006:上传文件的超出大小限制; 503002:同步成功-记录保存失败; 503003:权限错误; 503004:文章保存失败; 403017:临近定时时间不能取消发送任务; 403018:临近定时时间不能修改发送任务; 403019:超过发送时间不能发送; 403020:缺少发表记录ID参数; 403021:该文章已删除, 无法查看 !; 416001:添加成功,审核中,请耐心等待; 416002:签名添加失败; 503005:查询出错了，请联系管理员; 57001:约展添加失败 !; 57002:查询出错 !; 57003:你已经发布过该约展了 !; 57004:你已经报名该约展了 !; 57010:没有这个参约啊 ！; 57011:您不能再次操作了 ！; 205001:没有内容 !; 200237:本次活动已结束 !; 200221:转余额不能少于18 !; 200222:json格式,解析错误 !; 200223:服务器出错 !; 200224:请补全用户信息 !; 200225:用户记录保存失败 !; 200226:用户抽奖次数新增失败 !; 200227:请求地址有误 !; 200228:session_key获取失败 !; 200229:抽奖次数已用完 !; 200230:access_token获取失败 !; 200231:获取二维码失败 !; 200232:二维码保存失败 !; 200233:验证码发送失败 !; 200234:商城用户注册失败 !; 200235:用户信息获取失败 !; 200236:您的账号还未进行注册 !; 200238:您的微信已绑定账号 !; 200239:微信信息查询出错 !; 200240:提现金额不能大于账户余额 !; 220001:用户性别填写错误 !; 221001:该作品已删除,无法查看 !; 221003:该作品不存在 !; 221004:该作品不属于你 !; 221002:该评论已被删除 !; 221006:二级评论删除失败 !; 221005:您发的内容包含敏感文字，请修改后再发布 !; 223001:文件扩展名获取失败 !; 223002:表单请勿重复提交 !</details>",
 * )
 * )
 * @SWG\SecurityScheme(
 *   securityDefinition="apiKey",
 *   type="apiKey",
 *   in="query",
 *   description = "认证api_token shopnc_member表 token",
 *   name="api_token"
 * ),
 */
class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;
    /**
     * @param Request|array $validateSource
     * @param array $rules
     * @param array $messages
     * @param array $customAttributes
     * @example         $rules = [
    'amount' => 'required|in:' . implode(',', $products['amount']),
    'days' => 'required|in:' . implode(',', $products['deadline_day']),
    'usage' => 'required|in:' . implode(',', array_keys($usages)),
    'member_id' => 'required|integer|min:1',
    ];
    $this->nValidate($request, $rules,
    [
    'title.required' => 'A title is required',
    'title.*' => 'A title is required',
    'required' => 'The :attribute field is required.',
    'same'    => 'The :attribute and :other must match.',
    'size'    => 'The :attribute must be exactly :size.',
    'between' => 'The :attribute must be between :min - :max.',
    'in'      => 'The :attribute must be one of the following types: :values',
    ],
    [
    'amount' => '金额',
    'days' => '天数',
    'usage' => '借款用途',
    ]);
     */
    public function nValidate($validateSource, array $rules, array $messages = [], array $customAttributes = [])
    {
        $validator = $this->getValidationFactory()->make((is_array($validateSource)) ? $validateSource : $validateSource->all(), $rules, $messages, $customAttributes);

        if ($validator->fails()) {
            //todo 后期增加字段 到data里
            ApiResponseException::throwException(ApiResponseCode::ARG_INVALID, $validator->errors()->first());
        }
    }
    protected function _json($data = [], $code = NULL, $message = ''){
        if($code === NULL){
            $code = ApiResponseCode::SUCCESS;
        }
        return ResponseHelper::json($code, $data, $message);
    }
    protected function _jsonError($code = ApiResponseCode::OPERATE_FAIL, $message = '', $data = NULL){
        return ResponseHelper::json($code, $data, $message);
    }
}
