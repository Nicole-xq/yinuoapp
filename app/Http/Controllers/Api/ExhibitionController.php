<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\ArtistOrganization;
use App\Models\ArtistPersonal;
use App\Models\Artwork;
use App\Models\ExhibitionArtistJoint;
use App\Models\ExhibitionArtworkJoint;
use App\Models\ExhibitionArtworkList as Exhibition;
use App\Models\ExhibitionCurator;
use App\Models\ExhibitionLikeLog;
use App\Models\registrationExhibition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ExhibitionController extends Controller
{
    /**
     * 艺展相关接口开发
     * @author ls 17612157796@163.com
     * @api
     * @var
     * @return
     */
    protected $user_db = '';

    protected $Period = 10;            //查询艺展  距离开始  距离结束的时间区间  单位:day

    protected $Model;

    public function __construct()
    {
        $this->Model = new Exhibition();
        //only 数组中是需要登录的方法
        $this->middleware('verify.user.login', ['only' => [
            'giveLike','registrationExhibition'
        ]]);
    }


    public function giveLike(Request $req)
    {
        $user_id = $req->user['user_id'];
        $db_name = $req->user['db'];
        $id = Input::get('id');
        if (empty($id) || empty($user_id) || empty($db_name)) {
            return response()->fail(200001);
        }
        //获取当前会员所属的是一库 还是 二库
        //开启所属数据库  与主数据库的事务

        DB::beginTransaction();
        DB::connection($db_name)->beginTransaction();
        try {
            $where = ['id' => $id];

            $where2 = [
                'exhibition_id' => $id,
                'user_id' => $user_id,
            ];
            $res = DB::connection($db_name)->table('exhibition_like_log')->where($where2)->first();

            if ($res) {
                if (is_object($res)) {
                    $res = $res->toArray();
                }
                if ($res['is_delete'] == 1) {
                    $update = ['is_delete' => 0];
                    $result2 = DB::table('exhibition_artwork_list')->where($where)->increment('like_num', 1);
                } else {
                    $update = ['is_delete' => 1];
                    $result2 = DB::table('exhibition_artwork_list')->where($where)->decrement('like_num', 1);
                }
                $result1 = DB::connection($db_name)->table('exhibition_like_log')->where($where2)->update($update);
            } else {
                $insert = [
                    'exhibition_id' => $id,
                    'user_id' => $user_id,
                    'is_delete' => 0,       //这个条件也可以不写  默认就0
                    'created_at' => date('Y-m-d H:i:s', time())
                ];
                $update['is_delete'] = 0;
                $result1 = DB::connection($db_name)->table('exhibition_like_log')->insert($insert);
                $result2 = DB::table('exhibition_artwork_list')->where($where)->increment('like_num', 1);
            }

            if (!($result1 && $result2)) {
                throw new \Exception(200005);
            }

            //库数据提交
            DB::commit();
            DB::connection($db_name)->commit();

        } catch (\Exception $e) {
            //库数据回滚
            DB::rollback();
            DB::connection($db_name)->rollback();
            return response()->fail($e->getMessage());
        }

        return response()->success($update['is_delete']);
    }

    /**
     * 展览首页--艺展列表-默认显示收费--艺展列表,艺展图片,参展艺术家信息
     * @return mixed
     */
    public function showExhibitions(Request $req)
    {
        $user_id = $req->user['user_id'];
        $db_name = $req->user['db'];
        //首页显示条件
        $where = [
            'is_delete' => 0,
            'is_recommend' => 1
        ];
        $exhibition_lists = $this->Model->fetchExhibitions($where);
        if (!$exhibition_lists) {
            return response()->fail(205001);
        }
        if ($exhibition_lists && is_array($exhibition_lists['data'])) {
            $organization_lists = $this->getOrganizationList($exhibition_lists['data']);
            //展馆信息处理
            if ($organization_lists) {
                $organization_list = $this->disposeExhibitionLists($organization_lists);
            }
            $avatar = $this->getUsersAvatar($exhibition_lists['data']);
            $exhibition_ids = array();
            foreach ($exhibition_lists['data'] as &$value) {
                foreach ($value['artwork_appoint'] as &$val) {
                    $val['user_avatar'] = $avatar[$val['user_id']] . '?' . time();
                    $val['appoint_id'] = $val['id'];
                    unset($val['id']);
                }

                //获取展馆信息
                $value['organization_name'] = $organization_list[$value['organization_id']]['name'] ?? '';
                $value['addr'] = $organization_list[$value['organization_id']]['addr'] ?? '0,0';
                $img_list = json_decode($value['img_list']) ?: [];
                unset($value['img_list']);
                if ($img_list) {
                    $value['min_pic'] = getPicturePath($img_list[0]) . '?x-oss-process=image/resize,h_300';
                } else {
                    $value['min_pic'] = '';
                }
                $value['is_like'] = 0;
                $value['user_num'] = count($value['artwork_appoint']) ?: 0;
                $exhibition_ids[] = $value['id'];
            }
            if ($user_id && $db_name && $exhibition_lists) {
                $exhibition_lists = $this->getLikeStatus($db_name, $user_id, $exhibition_lists);
            }
            return response()->success($exhibition_lists);
        }
        return response()->fail(205001);
    }

    /*
     * 根据展览信息获取该会展点赞状态
     */
    public function getLikeStatus($db_name, $user_id, $lists)
    {
        $exhibition_ids = $this->fetchExhibitionIds($lists);
        $model = new ExhibitionLikeLog();
        $result = $model->get_like_status($db_name, $user_id, $exhibition_ids);
        if (!$result) {
            return $lists;
        }
        $likes = array_map(function ($val) {
            return $val['exhibition_id'];
        }, $result);
        if (isset($lists['data']) && is_array($lists['data'])) {
            foreach ($lists['data'] as &$value) {
                if (in_array($value['id'], $likes)) {
                    $value['is_like'] = 1;
                } else {
                    $value['is_like'] = 0;
                }
            }
            return $lists;
        }
        if (isset($lists['id']) && in_array($lists['id'], $likes)) {
            $lists['is_like'] = 1;
        }
        return $lists;
    }


    /**
     *
     * 展览首页--最新列表--艺展列表,艺展图片,参展艺术家信息
     * @return mixed
     * @param Request $request
     */
    public function showNewestExhibitions(Request $request)
    {
        $user_id = $request->user['user_id'];
        $db_name = $request->user['db'];
        $where = [
            'is_delete' => 0,
            'is_recommend' => 0,
        ];
        $exhibition_lists = $this->Model->fetchExhibitions($where);
        if (!$exhibition_lists) {
            return response()->fail(205001);
        }

        $exhibition_ids = [];
        if (isset($exhibition_lists['data']) && is_array($exhibition_lists['data']) && $exhibition_lists['data']) {
            $organization_lists = $this->getOrganizationList($exhibition_lists['data']);
            //展馆信息处理
            if ($organization_lists) {
                $organization_list = $this->disposeExhibitionLists($organization_lists);
            }
            $avatar = $this->getUsersAvatar($exhibition_lists['data']);
            foreach ($exhibition_lists['data'] as &$value) {
                if (is_array($value['artwork_appoint'])) {
                    foreach ($value['artwork_appoint'] as &$val) {
                        $val['appoint_id'] = $val['id'];
                        $val['user_avatar'] = $avatar[$val['user_id']] . '?' . time();
                        unset($val['id']);
                    }
                }
                //获取展馆信息
                $value['organization_name'] = $organization_list[$value['organization_id']]['name'] ?? '';
                $value['addr'] = $organization_list[$value['organization_id']]['addr'] ?? '0,0';
                $img_list = json_decode($value['img_list']) ?: [];
                unset($value['img_list']);
                if (!empty($img_list)) {
                    $value['min_pic'] = getPicturePath($img_list[0]) . '?x-oss-process=image/resize,h_300';
                } else {
                    $value['min_pic'] = '';
                }
                $value['is_like'] = 0;
                $value['user_num'] = count($value['artwork_appoint']) ?: 0;
                $exhibition_ids[] = $value['id'];
            }

        }
        if ($user_id && $db_name && $exhibition_lists) {
            $exhibition_lists = $this->getLikeStatus($db_name, $user_id, $exhibition_lists);
        }
        return response()->success($exhibition_lists);
    }


    public function fetchExhibitionIds($exhibition_lists)
    {
        $exhibition_ids = [];
        if (isset($exhibition_lists['id'])) {
            $exhibition_ids[] = $exhibition_lists['id'];
            return $exhibition_ids;
        }
        foreach ($exhibition_lists['data'] as $value) {
            $exhibition_ids[] = $value['id'] ?: '';
        }
        return $exhibition_ids;
    }

    public function disposeExhibitionLists($organization_lists)
    {
        $newOrganization = [];
        foreach ($organization_lists as $value) {
            $newOrganization[$value['id']]['name'] = $value['name'];
            $newOrganization[$value['id']]['addr'] = $value['addr'];
        }
        return $newOrganization;
    }


    /**
     * 艺展的搜索展示列表    与同城艺展显示 统一做一个接口
     * @var int 'is_charge' 1:(收费), 0:(不收费); 即将开始和即将结束的艺展 艺展的地区限定
     */
    public function searchExhibitions(Request $request)
    {
        $user_id = $request->user['user_id'];
        $db_name = $request->user['db'];

        $city = $request->input('city');
        $is_charge = $request->input('is_charge');
        $status = $request->input('status');
        if (!$city && !isset($is_charge) && !$status) {
            return response()->fail(200004);
        }
        if (!empty($city)) {
            $where['city_id'] = $city;
        }
        switch ($status) {
            case 'begin':
                $field = 'start_time';
                break;
            case 'end':
                $field = 'end_time';
                break;
            default:
                $field = '';
                break;
        }
        $where['is_delete'] = 0;
        if (isset($is_charge) && strlen($is_charge) != 0) {
            $where['is_charge'] = $is_charge;
        }
        $exhibition_lists = $this->Model->searchExhibitions($where, $field);
        if (is_object($exhibition_lists)) {
            $exhibition_lists = $exhibition_lists->toArray();
        }
        if (isset($exhibition_lists['data']) && is_array($exhibition_lists['data'])) {
            $avatar = $this->getUsersAvatar($exhibition_lists['data']);
            foreach ($exhibition_lists['data'] as &$value) {
                if (is_array($value['artwork_appoint']) && !empty($value['artwork_appoint'])) {
                    foreach ($value['artwork_appoint'] as &$val) {
                        $val['appoint_id'] = $val['id'];
                        $val['user_avatar'] = $avatar[$val['user_id']] . '?' . time();
                        unset($val['id']);
                    }
                }
                $organization_lists = $this->getOrganizationList($exhibition_lists['data']);
                //展馆信息处理
                if ($organization_lists) {
                    $organization_list = $this->disposeExhibitionLists($organization_lists);
                }
                //获取展馆信息
                $value['organization_name'] = $organization_list[$value['organization_id']]['name'] ?? '';
                $value['addr'] = $organization_list[$value['organization_id']]['addr'] ?? '0,0';
                $img_list = json_decode($value['img_list']) ?: [];
                unset($value['img_list']);
                if (!empty($img_list)) {
                    $value['min_pic'] = getPicturePath($img_list[0], 300);
                } else {
                    $value['min_pic'] = '';
                }
                $value['is_like'] = 0;
                $value['user_num'] = count($value['artwork_appoint']) ?: 0;
            }
        }
        if ($user_id && $db_name && $exhibition_lists) {
            $exhibition_lists = $this->getLikeStatus($db_name, $user_id, $exhibition_lists);
        }
        return response()->success($exhibition_lists);
    }


    /**
     * 展览的详情页   艺展  约展用户 艺展图片 艺术家
     * @return mixed
     */
    public function revealExhibition(Request $req)
    {

        $user_id = $req->user['user_id'];
        $db_name = $req->user['db'];
        $exhibition_id = $req->get('exhibition_id');
        if (empty($exhibition_id)) {
            return response()->fail(200001);
        }

        $exhibition_info = $this->Model->getExhibition($exhibition_id);
        if (is_object($exhibition_info)) {
            $exhibition_info = $exhibition_info->toArray();
        }
        if (empty($exhibition_info)) {
            return response()->fail(200004);
        }

        //先进行 浏览 +1
        $this->Model->increment('click_num');

        //获取展览策展人信息
        $exhibition_curator = new ExhibitionCurator();
        $curator_list = $exhibition_curator
            ->where('exhibition_id', $exhibition_id)
            ->get();
        if (is_object($curator_list)) {
            $curator_list = $curator_list->toArray();
        }
        if ($curator_list && is_array($curator_list)) {
            $tmp = array();
            $artist_personal = new ArtistPersonal();
            $fields = ['id', 'name'];
            foreach ($curator_list as $v) {
                $curator_info = $artist_personal::select($fields)
                    ->where('id', $v['curator_id'])
                    ->first();
                if (!empty($curator_info)) {
                    $tmp[] = $curator_info['name'];
                }
            }
            $exhibition_info['curator'] = implode(',', $tmp);
        } else {
            $exhibition_info['curator'] = '';
        }

        //获取展馆信息
        $organization_info = $this->getOrganizationInfo($exhibition_info['organization_id']);
        $exhibition_info['organization_name'] = $organization_info['organization_name'];
        $exhibition_info['addr'] = $organization_info['addr'] ?: '0,0';
        $exhibition_info['address'] = $organization_info['address'];
        //获取一共有多少人约展
        $exhibition_info['user_num'] = count($exhibition_info['artwork_users']) ?: 0;

        //艺术家作品的ID 集合
        $artist_ids = (new ExhibitionArtistJoint())->get_artist_ids_byExhibitionId($exhibition_id);
        $artwork_artists = (new ArtistPersonal())->getArtistListByIds($artist_ids);
        if (is_object($artwork_artists)) {
            $artwork_artists = $artwork_artists->toArray();
        }
        $artwork_info = (new ExhibitionArtworkJoint())->get_artworkIds_byExhibitionId($exhibition_id, $artist_ids);
        $artwork_ids = array();
        foreach ($artwork_artists as &$value) {
            $value['artwork_num'] = 0;
            foreach ($artwork_info as $val) {
                if ($val['artist_id'] == $value['id']) {
                    $value['artwork_num'] += 1;
                    $artwork_ids[] = $val['artwork_id'];
                }
            }
        }
        $exhibition_info['artwork_artists'] = $artwork_artists;
        $exhibition_info['pic_list'] = json_decode($exhibition_info['img_list']) ?: [];
        unset($exhibition_info['img_list']);
        $exhibition_info['pic_num'] = count($exhibition_info['pic_list']);
        //获取艺术家作品图片
        $exhibition_info['artwork_pics'] = (new Artwork())->getArtworkByIds($artwork_ids);
        $exhibition_info['min_pic'] = $exhibition_info['pic_list'] ? getPicturePath($exhibition_info['pic_list'][0], 300) : '';
        $exhibition_info['is_like'] = 0;

        if (is_array($exhibition_info['artwork_users']) && count($exhibition_info['artwork_users']) > 0) {
            $user_ids = array_column($exhibition_info['artwork_users'], 'user_id');
            $avatar = getMemberAvatarByID($user_ids);
            foreach ($exhibition_info['artwork_users'] as &$val) {
                $val['user_avatar'] = $avatar[$val['user_id']] . '?' . time();
            }
        }

        if (is_array($exhibition_info['artwork_artists']) && count($exhibition_info['artwork_artists']) > 0) {
            foreach ($exhibition_info['artwork_artists'] as &$val) {
                $val['logo'] = getPicturePath($val['logo']);
            }
        }

        if (is_array($exhibition_info['artwork_pics']) && count($exhibition_info['artwork_pics']) > 0) {
            foreach ($exhibition_info['artwork_pics'] as &$val) {
                $val['artwork_img'] = getPicturePath($val['artwork_img']);
            }
        }

        if (is_array($exhibition_info['pic_list']) && count($exhibition_info['pic_list']) > 0) {
            foreach ($exhibition_info['pic_list'] as &$val) {
                $val = getPicturePath($val);
            }
        }

        //分享相关
        $exhibition_info['share_description'] = '刚刚发现一个好看的展览，实在忍不住就跟大家分享了，一起来看展览吧！';
        $exhibition_info['share_pic'] = $exhibition_info['pic_list'][0] ?? env('YN_LOGO');
        $exhibition_info['share_url'] = env('SHARE_URL', '');
        $exhibition_info['share_url'] .= "/exhibitionDetailsCharge?exhibition_id={$exhibition_id}";
        if ($user_id && $db_name && $exhibition_info) {
            $exhibition_info = $this->getLikeStatus($db_name, $user_id, $exhibition_info);
        }
        unset($exhibition_info['data']);
        return response()->success($exhibition_info);
    }

    /**
     * 艺展的展示列表中的参展的艺术家
     * @return mixed
     */
    public function showArtist(Request $req)
    {
        $id = $req->get('id');
        $lists = $this->Model->showArtist($id);
        return response()->success($lists);
    }

    public function getOrganizationInfo($id)
    {
        $info = array();
        //获取展馆信息
        $artist_organization = new ArtistOrganization();
        $organization_info = $artist_organization
            ->where('id', $id)
            ->first();
        $info['organization_name'] = $organization_info['name'] ?? '';
        $info['addr'] = $organization_info['addr'] ?? '0,0';
        $address = new Address();
        $province_info = $address->getInfoById($organization_info['province_id']);
        $city_info = $address->getInfoById($organization_info['city_id']);
        $area_info = $address->getInfoById($organization_info['area_id']);
        $info['address'] = '';
        if (!empty($organization_info)) {
            $info['address'] = $province_info['area_name'];
            $info['address'] .= $city_info['area_name'];
            $info['address'] .= $area_info['area_name'];
            $info['address'] .= $organization_info['address'];
        }
        return $info;
    }

    /*
     * 根据艺展数据列表 获取展馆信息
     */
    public function getOrganizationList($exhibition_lists)
    {
        $organization_ids = [];
        foreach ($exhibition_lists as $val) {
            if (isset($val['organization_id'])) {
                $organization_ids[] = $val['organization_id'];
            } else {
                return false;
            }
        }
        $organization_ids = array_unique($organization_ids);
        $organization_model = new ArtistOrganization();
        return $organization_model->getOrganizationList($organization_ids);
    }

    private function getUsersAvatar($data)
    {
        $user_ids = array();
        foreach ($data as $item) {
            $array_column = array_column($item['artwork_appoint'], 'user_id');
            $user_ids = array_merge($user_ids, $array_column);
        }
        return getMemberAvatarByID($user_ids);
    }

    function registrationExhibition(Request $param){
        $mobile = $param->user['user_mobile'];
        $user_id = $param->user['user_id'];
        $model = new registrationExhibition();
        $info = $model->getInfo(['user_id'=>$user_id]);
        if($info){
            return response()->fail(200005,'已经报过名了!');
        }
        $arr = array(
            'user_id'=>$user_id,
            'mobile'=>$mobile,
            'created_at'=>date('Y-m-d H:i:s')
        );
        $id = $model->addInfo($arr);
        return response()->success();
        if($id){
            return response()->success();
        }
        return response()->fail(200005,'报名失败!');

    }
}
