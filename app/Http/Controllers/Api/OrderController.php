<?php

namespace App\Http\Controllers\Api;

use App\Entity\Constant\ApiResponseCode;
use App\Exceptions\ApiResponseException;
use App\Lib\Helpers\ArrayHelper;
use App\Logic\Order;
use App\Models\ArtworkCategory;
use App\Models\Shop\OrderLog;
use App\Models\Shop\Orders;
use App\Models\Shop\Store;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    /**
     * @SWG\Post(path="/api/order/getBuyInfo",
     *   tags={"order"},
     *   operationId="/api/order/getBuyInfo",
     *   summary="下单第一步 获得商品信息",
     *   description="下单第一步 获得商品信息",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="cartId",
     *     in="formData",
     *     description="ifCart=1:购物车id|个数...  ifCart=0:商品id|个数(688|1,689|1)",
     *     required=true,
     *     type="string",
     *     default="",
     *   ),
     *   @SWG\Parameter(
     *     name="ifCart",
     *     in="formData",
     *     description="是否来自购物车 1.来自购物车 0.直接购买",
     *     required=false,
     *     type="integer",
     *   ),
     *   @SWG\Parameter(
     *     name="addressId",
     *     in="formData",
     *     description="地址id",
     *     required=false,
     *     type="integer",
     *   ),
     *  @SWG\Response(response="200", description="successful operation",@SWG\Schema(ref="#/definitions/MyBuyInfoResponse"))
     * )
     *
     */
    /**
     * @param Request $request
     * @return mixed
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public function getBuyInfo(Request $request)
    {
        $rules = [
            'cartId' => 'required',
            'addressId' => 'int',
        ];
        $this->nValidate($request, $rules);

        $cartId = $request->input("cartId");
        $ifCart = $request->input("ifCart", 0);
        $addressId = $request->input("addressId", 0);
        $buyInfo = application()->shopService->getBuyInfo($request->api_token, $cartId, $ifCart, $addressId, null);
        $buyInfo = json_decode($buyInfo, true);
        if(!empty($buyInfo['store_cart_list'])){
            $buyInfo['store_cart_list'] = ArrayHelper::mapToList($buyInfo['store_cart_list'], "store_id");
        }
        return $this->_json($buyInfo);
    }

    /**
     * @SWG\Post(path="/api/order/createOrder",
     *   tags={"order"},
     *   operationId="/api/order/createOrder",
     *   summary="下单第二步 生成订单",
     *   description="下单第二步 生成订单",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="addressId",
     *     in="formData",
     *     description="地址id",
     *     required=true,
     *     type="integer",
     *     default="",
     *   ),
     *   @SWG\Parameter(
     *     name="cartId",
     *     in="formData",
     *     description="ifCart=1:购物车id|个数...  ifCart=0:商品id|个数(688|1,689|1)",
     *     required=true,
     *     type="string",
     *     default="",
     *   ),
     *   @SWG\Parameter(
     *     name="ifCart",
     *     in="formData",
     *     description="是否来自购物车 1.来自购物车 0.直接购买",
     *     required=true,
     *     type="integer",
     *   ),
     *   @SWG\Parameter(
     *     name="rpt",
     *     in="formData",
     *     description="红包id集合",
     *     required=false,
     *     type="string",
     *     default="",
     *   ),
     *   @SWG\Parameter(
     *     name="fcode",
     *     in="formData",
     *     description="本次下单的F码",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="invoiceId",
     *     in="formData",
     *     description="发票id",
     *     required=false,
     *     type="integer",
     *   ),
     *   @SWG\Parameter(
     *     name="offPayHash",
     *     in="formData",
     *     description="回传 货到付款hash",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="offPayHashBatch",
     *     in="formData",
     *     description="回传 货到付款hash具体到各个店铺",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="password",
     *     in="formData",
     *     description="支付密码 可选如果需要使用 充值卡支付 预存款支付 诺币支付则为必填",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="payMessage",
     *     in="formData",
     *     description="71|msg, store_id|store_msg, store_id2|store_msg2",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="payName",
     *     in="formData",
     *     description="online 付款方式:'online','offline','chain','underline'",
     *     default="online",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="pdPay",
     *     in="formData",
     *     default="0",
     *     description="是否预存款支付 1.使用",
     *     required=false,
     *     type="integer",
     *   ),
     *   @SWG\Parameter(
     *     name="pointsPay",
     *     in="formData",
     *     default="",
     *     description="是否诺币支付 1.使用",
     *     required=false,
     *     type="integer",
     *   ),
     *   @SWG\Parameter(
     *     name="rcbPay",
     *     in="formData",
     *     description="是否使用充值卡支付 1.使用",
     *     required=false,
     *     type="integer",
     *   ),
     *   @SWG\Parameter(
     *     name="vatHash",
     *     in="formData",
     *     description="增值税发票hash",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="voucher",
     *     in="formData",
     *     description="代金券信息 voucher_t_id|store_id|voucher_price,voucher_t_id|store_id|voucher_price",
     *     required=false,
     *     type="string",
     *   ),
     *  @SWG\Response(response="200", description="successful operation",
     *      @SWG\Schema(
     *          @SWG\Property(
     *              type="string",
     *              property="pay_sn",
     *              description="订单号"
     *          ),
     *          @SWG\Property(
     *              type="string",
     *              property="payment_code",
     *              description="支付类型",
     *              default="online"
     *          )
     *      )
     * ))
     *
     */
    /**
     * @param Request $request
     * @return mixed
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public function createOrder(Request $request)
    {
        $rules = [
            'ifCart' => 'required',
            'addressId' => 'required',
            'cartId' => 'required',
            'offPayHash' => 'required',
            'offPayHashBatch' => 'required',
            'payName' => 'required',
            'vatHash' => 'required',
        ];
        $this->nValidate($request, $rules);
//        return $this->_json(json_decode('{"pay_sn":"350606257193943434","payment_code":"online"}'));
        //是否是来自购物车
        $ifCart = $request->input("ifCart");
        //购物车商品id
        $cartId = $request->input("cartId");
        //地址id
        $addressId = $request->input("addressId");
        //发票hash
        $vatHash = $request->input("vatHash");
        //运费相关hash
        $offPayHash = $request->input("offPayHash");
        //运费相关hash
        $offPayHashBatch = $request->input("offPayHashBatch");
        //支付方式
        $payName = $request->input("payName");
        //发票id
        $invoiceId = $request->input("invoiceId", null);
        //红包id
        $rpt = $request->input("rpt", null);

        //是否预存款支付
        $pdPay = $request->input("pdPay", 0);
        //是否使用充值卡支付
        $rcbPay = $request->input("rcbPay", 0);
        //是否诺币支付
        $pointsPay = $request->input("pointsPay", 0);

        //支付密码
        $password = $request->input("password", '');
        //f码
        $fcode = $request->input("fcode", '');

        //代金券
        $voucher = $request->input("voucher", '');
        //备注信息
        $payMessage = $request->input("payMessage", '');




        $buyInfo = application()->shopService->createOrder(
            $request->api_token,$ifCart, $cartId, $addressId, $vatHash,
            $offPayHash, $offPayHashBatch, $payName, $invoiceId, $rpt,
            $pdPay, $rcbPay, $pointsPay, $password, $fcode, $voucher,
            $payMessage, 3
        );
        return $this->_json(json_decode($buyInfo, true));
    }

    /**
     * @SWG\Post(path="/api/order/payOrderInfo",
     *   tags={"order"},
     *   operationId="/api/order/payOrderInfo",
     *   summary="获取支付信息 下单第三步 ",
     *   description="获取支付信息 下单第三步",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="paySn",
     *     in="formData",
     *     description="支付单号",
     *     required=true,
     *     type="string",
     *     default="",
     *   ),
     *  @SWG\Response(response="200", description="successful operation",@SWG\Schema(ref="#/definitions/PayOrderInfoResponse"))
     * )
     *
     */
    /**
     * @param Request $request
     * @return mixed
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public function payOrderInfo(Request $request)
    {
        $rules = [
            'paySn' => 'required',
        ];
        $this->nValidate($request, $rules);

        $paySn = $request->input("paySn");
        $buyInfo = application()->shopService->getPayInfo($request->api_token, $paySn);
        return $this->_json(json_decode($buyInfo));
    }

    /**
     * @SWG\Post(path="/api/order/orderPay",
     *   tags={"order"},
     *   operationId="/api/order/orderPay",
     *   summary="订单支付 下单最后一步(第四步)",
     *   description="订单支付 下单最后一步(第四步)",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="paySn",
     *     in="formData",
     *     description="支付单号",
     *     required=true,
     *     type="string",
     *     default="",
     *   ),
     *   @SWG\Parameter(
     *     name="payType",
     *     in="formData",
     *     description="第三方支付方法 alipay wxPay",
     *     required=false,
     *     default="alipay",
     *     type="integer",
     *   ),
     *   @SWG\Parameter(
     *     name="pdPay",
     *     in="formData",
     *     default="0",
     *     description="是否预存款支付 1.使用",
     *     required=false,
     *     type="integer",
     *   ),
     *   @SWG\Parameter(
     *     name="pointsPay",
     *     in="formData",
     *     default="",
     *     description="是否诺币支付 1.使用",
     *     required=false,
     *     type="integer",
     *   ),
     *   @SWG\Parameter(
     *     name="rcbPay",
     *     in="formData",
     *     description="是否使用充值卡支付 1.使用",
     *     required=false,
     *     type="integer",
     *   ),
     *   @SWG\Parameter(
     *     name="password",
     *     in="formData",
     *     description="支付密码 可选如果需要使用 充值卡支付 预存款支付 诺币支付则为必填",
     *     required=false,
     *     type="string",
     *   ),
     *  @SWG\Response(response="200", description="successful operation",@SWG\Schema(ref="#/definitions/MyOrderPayResponse"))
     * )
     *
     */
    /**
     * @param Request $request
     * @return mixed
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public function orderPay(Request $request)
    {
        $rules = [
            'paySn' => 'required',
            'payType' => 'in:alipay,wxPay'
        ];
        $this->nValidate($request, $rules);
        //支付单号
        $paySn = $request->input("paySn");
        //是否预存款支付
        $pdPay = $request->input("pdPay", 0);
        //是否使用充值卡支付
        $rcbPay = $request->input("rcbPay", 0);
        //是否诺币支付
        $pointsPay = $request->input("pointsPay", 0);
        //支付密码
        $password = $request->input("password", '');

        $payType = $request->input("payType", "alipay");
        if($payType == 'alipay'){
            $buyInfo = application()->shopService->alipayNativePay($request->api_token, $paySn, $pdPay, $rcbPay, $pointsPay, $password);
        }else{
            $buyInfo = application()->shopService->wxAppPay($request->api_token, $paySn, $pdPay, $rcbPay, $pointsPay, $password);
        }
        return $this->_json(json_decode($buyInfo));
    }
    /**
     * @SWG\Post(path="/api/order/getNewestOrderPayedInfo",
     *   tags={"order"},
     *   operationId="/api/order/getNewestOrderPayedInfo",
     *   summary="获得最新的支付完成订单",
     *   description="获得最新的支付完成订单",
     *   produces={"application/json"},
     *  @SWG\Response(response="200", description="successful operation"))
     * )
     *
     */
    public function getNewestOrderPayedInfo(Request $request){
        $orderInfo = application()->shopService->getNewestOrderPayedInfo($request->api_token);
        return $this->_json(json_decode($orderInfo));
    }

    /**
     * @SWG\Post(path="/api/order/getList",
     *   tags={"order"},
     *   operationId="/api/order/getList",
     *   summary="用户 获得订单列表",
     *   description="用户 获得订单列表",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="page",
     *     in="formData",
     *     description="当前页",
     *     required=true,
     *     type="integer",
     *     default="1",
     *   ),
     *   @SWG\Parameter(
     *     name="pageSize",
     *     in="formData",
     *     description="页面大小 默认10",
     *     required=false,
     *     type="integer",
     *     default="",
     *   ),
     *   @SWG\Parameter(
     *     name="status",
     *     in="formData",
     *     description="状态 默认全部 -1.所有 10.待付款 30.待收货(20待发货 归到 待收货 ---- 加一个提示正在发货) 40.已完成 0.已取消",
     *     required=false,
     *     type="integer",
     *     default="",
     *   ),
     *  @SWG\Response(response="200", description="successful operation",@SWG\Schema(ref="#/definitions/MyOrderListResponse"))
     * )
     *
     */
    /**
     * @param Request $request
     * @return mixed
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public function getList(Request $request)
    {
        $rules = [
            'page' => 'required',
            'pageSize' => 'int',
            'status' => 'int',
        ];
        $this->nValidate($request, $rules);

        $page = $request->input("page");
        $pageSize = $request->input("pageSize", 10);
        $status = $request->input("status", -1);
        $orderList = application()->shopService->getUserOrderList($request->api_token, $page, $pageSize, $status);
        $orderList = json_decode($orderList,true);
        if(isset($orderList["list"])){
            foreach ($orderList["list"] as &$item) {
                //处理和实际支付价  订单总价格(不包括红包,包括诺币的实价)
                if(isset($item["order_amount"]) && $item["is_points"] == 1 && $item["points_amount"] > 0 ){
                    $item["order_amount"] = bcsub($item["order_amount"], $item["points_amount"] , 2);
                }
            }
        }
        return $this->_json($orderList);
    }

    /**
     * SWG\Post(path="/api/order/getAuctionOrderList",
     *   tags={"order"},
     *   operationId="/api/order/getAuctionOrderList",
     *   summary="用户 获得拍卖订单列表",
     *   description="用户 获得拍卖订单列表",
     *   produces={"application/json"},
     *   SWG\Parameter(
     *     name="page",
     *     in="formData",
     *     description="当前页",
     *     required=true,
     *     type="integer",
     *     default="1",
     *   ),
     *   SWG\Parameter(
     *     name="pageSize",
     *     in="formData",
     *     description="页面大小 默认10",
     *     required=false,
     *     type="integer",
     *     default="",
     *   ),
     *   SWG\Parameter(
     *     name="stateType",
     *     in="formData",
     *     description="状态类型 默认list_all  list_all:所有保证金订单, list_have:已拍下, list_die:未拍中",
     *     required=false,
     *     type="integer",
     *     default="",
     *   ),
     *  SWG\Response(response="200", description="successful operation",SWG\Schema(ref="#/definitions/MyAuctionOrderListResponse"))
     * )
     *
     *
     */
    /**
     * @param Request $request
     * @return mixed
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    /*public function getAuctionOrderList(Request $request)
    {
        $rules = [
            'page' => 'required',
            'pageSize' => 'int',
            'stateType' => 'in:list_all,list_have,list_die',
        ];
        $this->nValidate($request, $rules);

        $page = $request->input("page");
        $pageSize = $request->input("pageSize", 10);
        $stateType = $request->input("stateType", "list_all");
        $buyInfo = application()->shopService->auctionOrderListOp($request->api_token, $page, $pageSize, $stateType);
        $buyInfo = json_decode($buyInfo, true);
        if(isset($buyInfo['margin_order_list'])){
            $buyInfo['list'] = $buyInfo['margin_order_list'];
            foreach ($buyInfo['list'] as &$v){
                $v['goods_num'] = 1;
            }
            unset($buyInfo['margin_order_list']);
        }
        if(isset($buyInfo['page_count'])){
            $buyInfo['count'] = intval($buyInfo['page_count']);
            unset($buyInfo['page_count']);
        }
        return $this->_json($buyInfo);
    }*/

    /**
     * @SWG\Post(path="/api/order/getOrderInfo",
     *   tags={"order"},
     *   operationId="/api/order/getOrderInfo",
     *   summary="获取订单详情",
     *   description="获取订单详情",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="order_id",
     *     in="formData",
     *     description="订单ID",
     *     required=true,
     *     type="integer",
     *     default=""
     *   ),
     *  @SWG\Response(response="default", description="10000:token过期,10001:无效的token,10002:无效的数据格式"),
     *  @SWG\Response(response="200", description="successful operation",@SWG\Schema(ref="#/definitions/OrderInfoResponse"))
     * )
     */
    public function getOrderInfo(Request $request)
    {
        $orderId = $request->order_id;
        $api_token = $request->api_token;
        if (!$orderId) {
            return $this->_jsonError(200001);
        }
        $orderId = intval($orderId);
        $orderLogic = new Order($api_token);
        $orderInfo = $orderLogic->getOrderInfo($orderId);
        if ($orderInfo && isset($orderInfo['code'])) {
            if ($orderInfo['code'] == 200) {
                $returnInfo = $orderInfo['datas']['order_info'];
                //判断是否卖家查看订单, 屏蔽修改地址显示
                if ($returnInfo['if_seller']) {
                    $transshipmentInfo = config('shop-info.transshipment');
                    $returnInfo['reciver_phone'] = $transshipmentInfo['receive_phone'];
                    $returnInfo['reciver_name'] = $transshipmentInfo['receive_name'];
                    $returnInfo['reciver_addr'] = $transshipmentInfo['receive_addr'];
                }
                $order_amount = $returnInfo["order_amount"];
                //处理总价 订单总价格(不包括红包,包括诺币的实价)
                if(isset($returnInfo["order_amount"]) && $returnInfo["rpt_amount"] > 0 ){
                    $returnInfo["order_amount"] = bcadd($order_amount, $returnInfo["rpt_amount"] , 2);
                }
                //处理和实际支付价  订单总价格(不包括红包,包括诺币的实价)
                if(isset($returnInfo["order_amount"]) && $returnInfo["is_points"] == 1 && $returnInfo["points_amount"] > 0 ){
                    $returnInfo["real_pay_amount"] = bcsub($order_amount, $returnInfo["points_amount"] , 2);
                }
                return $this->_json($returnInfo);
            } else {
                return $this->_jsonError($orderInfo['code'], $orderInfo['datas']['error']);
            }
        }
        return response()->fail(503005);
    }

    /**
     * @SWG\Post(path="/api/order/getExpressInfo",
     *   tags={"order"},
     *   operationId="/api/order/getExpressInfo",
     *   summary="获取订单物流详情",
     *   description="获取订单物流详情",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="order_id",
     *     in="formData",
     *     description="订单ID",
     *     required=true,
     *     type="integer",
     *     default=""
     *   ),
     *  @SWG\Response(response="default", description="10000:token过期,10001:无效的token,10002:无效的数据格式"),
     *  @SWG\Response(response="200", description="successful operation",@SWG\Schema(ref="#/definitions/ExpressInfoResponse"))
     * )
     */
    public function getExpressInfo(Request $request)
    {
        $orderId = $request->order_id;
        $api_token = $request->api_token;
        if (!$orderId) {
            return $this->_jsonError(200001);
        }
        $orderId = intval($orderId);
        $orderLogic = new Order($api_token);
        $postData = ['order_id' => $orderId];
        $orderInfo = $orderLogic->getExpressInfo($postData);
        if ($orderInfo && isset($orderInfo['code'])) {
            if ($orderInfo['code'] == 200) {
                return $this->_json($orderInfo['datas']);
            } else {
                return $this->_jsonError($orderInfo['code'], $orderInfo['datas']['error']);
            }
        }
        return response()->fail(503005);
    }

    /**
     * @SWG\Post(path="/api/order/orderCancelOp",
     *   tags={"order"},
     *   operationId="/api/order/orderCancelOp",
     *   summary="用户 取消订单",
     *   description="用户 取消订单",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="orderId",
     *     in="formData",
     *     description="订单id",
     *     required=true,
     *     type="integer",
     *   ),
     *  @SWG\Response(response="200", description="successful operation")
     * )
     *
     */
    /**
     * @param Request $request
     * @return mixed
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public function orderCancelOp(Request $request)
    {
        $rules = [
            'orderId' => 'required|int',
        ];
        $this->nValidate($request, $rules);

        $orderId = $request->input("orderId");
        application()->shopService->orderCancelOp($request->api_token, $orderId);
        return $this->_json();
    }

    /**
     * @SWG\Post(path="/api/order/orderReceiveOp",
     *   tags={"order"},
     *   operationId="/api/order/orderReceiveOp",
     *   summary="用户 确认收货",
     *   description="用户 确认收货",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="orderId",
     *     in="formData",
     *     description="订单id",
     *     required=true,
     *     type="integer",
     *   ),
     *  @SWG\Response(response="200", description="successful operation")
     * )
     *
     */
    /**
     * @param Request $request
     * @return mixed
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public function orderReceiveOp(Request $request)
    {
        $rules = [
            'orderId' => 'required|int',
        ];
        $this->nValidate($request, $rules);

        $orderId = $request->input("orderId");
        application()->shopService->orderReceiveOp($request->api_token, $orderId);
        return $this->_json();
    }
    /**
     * @SWG\Post(path="/api/order/orderDeleteOp",
     *   tags={"order"},
     *   operationId="/api/order/orderDeleteOp",
     *   summary="用户 移除订单",
     *   description="用户 移除订单",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="orderId",
     *     in="formData",
     *     description="订单id",
     *     required=true,
     *     type="integer",
     *   ),
     *  @SWG\Response(response="200", description="successful operation")
     * )
     *
     */
    /**
     * @param Request $request
     * @return mixed
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public function orderDeleteOp(Request $request)
    {
        $rules = [
            'orderId' => 'required|int',
        ];
        $this->nValidate($request, $rules);

        $orderId = $request->input("orderId");
        application()->shopService->orderDeleteOp($request->api_token, $orderId);
        return $this->_json();
    }
}
