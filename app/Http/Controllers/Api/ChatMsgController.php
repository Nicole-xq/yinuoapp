<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Interested;
use Illuminate\Http\Request;
use App\Models\ChatMsg;
use App\Models\UserIndex;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class ChatMsgController extends Controller
{
    public function __construct()
    {
        $this->middleware('verify.user.login');
        $this->middleware('verify.chat');
    }

    /**
     * 列表
     *
     * @return array
     */
    public function index(Request $request)
    {
        $res_arr = array(
            'total' => 0,
            'per_page' => 0,
            'current_page' => 1,
            'last_page' => 0,
            'next_page_url' => null,
            'prev_page_url' => null,
            'from' => null,
            'to' => null,
            'data' => [],
        );
        $self_id = (int)$request->user['user_id'];
        $user_id = (int)$request->user_id;
        $UserIndex = new UserIndex();
        $to_user = $UserIndex->getUserInfoById($user_id);
        if (empty($to_user)) {
            return response()->success($res_arr);
        }
        $model = new ChatMsg();
        // 查询列表
        $model->user_id = $user_id;
        $model->self_id = $self_id;
        $result = $model->getList([]);
        if (empty($result->toArray()['data'])) {
            return response()->success($result);
        }
        // 标记已读状态
        $model->modify(['to_id' => $self_id, 'from_id' => $user_id], ['read_state' => 1]);

        $data = $result->toArray();
        $where = ['initiator_user_id' => $user_id, 'participants_user_id' => $self_id];
        $orWhere = ['participants_user_id' => $user_id, 'initiator_user_id' => $self_id];
        $interested = (new Interested())->where($where)->orWhere($orWhere)->first();
        if ($interested) {
            if ($interested->initiator_user_id == $self_id) {
                $interested->type = 'initiator';
                $interested->initiator_nickname = $request->user['user_nickname'] ?: '';
                $interested->participants_nickname = $to_user['user_nickname'] ?: '';
            } else {
                $interested->type = 'participants';
                $interested->initiator_nickname = $to_user['user_nickname'] ?: '';
                $interested->participants_nickname = $request->user['user_nickname'] ?: '';
            }
        }
        $data['interested'] = $interested;
        if (empty($data['data'])) {
            return response()->success($data);
        }

        $avatar = getMemberAvatarByID([$user_id, $self_id]);
        $tmp = [];
        for ($i = (count($data['data']) - 1); $i >= 0; $i--) {
            $values = $data['data'][$i];
            $values['from_avatar'] = $avatar[$values['from_id']] . '?' . time();
            $values['to_avatar'] = $avatar[$values['to_id']] . '?' . time();
            $tmp[] = $values;
        }
        $data['data'] = $tmp;

        return response()->success($data);
    }

    /**
     * 添加信息
     *
     * @return array
     */
    public function create(Request $request)
    {
        $user_id = $request->user_id;
        $UserIndex = new UserIndex();
        $to_user = $UserIndex->getUserInfoById($user_id);
        if (empty($to_user)) {
            return response()->fail(300018);
        }

        $insert = [
            'from_id' => $request->user['user_id'],
            'from_name' => $request->user['user_name'],
            'from_ip' => getIp(),
            'to_id' => $to_user['user_id'],
            'to_name' => $to_user['user_name'],
            'to_msg' => $request->msg,
            'updated_at' => date('Y-m-d H:i:s', time()),
            'created_at' => date('Y-m-d H:i:s', time()),
        ];

        $chat_msg = new ChatMsg();
        $result = $chat_msg->add($insert);
        if ($result) {
            $insert['from_avatar'] = getMemberAvatarByID($request->user['user_id']);
            return response()->success($insert);
        }
        return response()->fail(200005);
    }

    /**
     * 删除聊天信息
     *
     * @return array
     */
    public function delete(Request $request)
    {
        $self_id = $request->user['user_id'];
        $user_id = $request->user_id;

        DB::beginTransaction();
        try {
            $model = new ChatMsg();
            $condition = array(
                'from_id' => $self_id,
                'to_id' => $user_id,
            );
            $count = $model->where($condition)->count();
            if ($count) {
                $result = $model->where($condition)->update(['from_delete' => 1]);
                if (!$result) {
                    throw new Exception('删除失败_1 ' . json_decode($condition));
                }
            }
            $condition = array(
                'from_id' => $user_id,
                'to_id' => $self_id,
            );
            $count2 = $model->where($condition)->count();
            if ($count2) {
                $result = $model->where($condition)->update(['to_delete' => 1]);
                if (!$result) {
                    throw new Exception('删除失败_2 ' . json_decode($condition));
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            Log::error('错误详情:' . $e->getMessage() . '.');
            return response()->fail(200020);
        }
        return response()->success();
    }

}
