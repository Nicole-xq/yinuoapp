<?php
/**
 * 我的参约
 * */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\AppointmentSign;
use App\Models\Interested;
use App\Models\ChatMsg;
use Illuminate\Http\Request;
use Validator;

class AppointmentSignController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('verify.user.login');
    }

    /**
     * 我参与的约展列表
     * @return mixed
     */
    public function index(Request $request)
    {
        $self_id = (int)$request->user['user_id'];
        $AppointmentSign = new AppointmentSign();
        $AppointmentSign->apListField = ['id', 'user_id', 'user_nickname', 'user_avatar', 'address'];
        $condition = ['user_id' => $self_id];
        $appointment_sign_list = $AppointmentSign->getList($condition);

        $lists = $appointment_sign_list->toarray();
        $data = [];
        foreach ($lists['data'] as $key => $value) {
            $user_id = $value['appointment_list']['user_id'];
            $value['user_id'] = $user_id;
            $value['user_nickname'] = $value['appointment_list']['user_nickname'];
            $value['address'] = $value['appointment_list']['address'];
            $value['user_avatar'] = getMemberAvatarByID($value['appointment_list']['user_id']) . '?' . time();
            $value['interested'] = $value['participants_interested']['interested'];
            unset($value['participants_interested']);
            unset($value['appointment_list']);
            unset($value['updated_at']);

            // 对话消息统计
            $condition = array('to_id' => $self_id, 'read_state' => 2, 'from_id' => $user_id);
            $chat_sum = (ChatMsg::where($condition)->count()) ?: 0;
            $value['chat_sum'] = $chat_sum;
            $data[] = $value;
        }
        $lists['data'] = $data;
        return response()->success($lists);
    }


    /**
     * 感兴趣操作
     * @return mixed
     */
    public function interested(Request $request)
    {
        // 验证
        $rules = [
            'appointment_sign_id' => 'required|regex:/^[0-9]+$/',
            'interested' => 'required|regex:/^[1-2]$/',
        ];
        $message = [
            '*.required' => 200001,
            '*.regex' => 200002,
        ];
        $validator = Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {
            $error = $validator->errors();
            return response()->fail($error->all()[0], array_keys($error->toArray())[0]);
        }

        $id = $request->appointment_sign_id;
        $AppointmentSign = new AppointmentSign();
        $appointment_sign = $AppointmentSign->getOne(['appointment_id' => $id]);
        if (empty($appointment_sign)) {
            return response()->fail(57010);
        }
        $user_id = $request->user['user_id'];
        $user_id2 = $appointment_sign->user_id;
        $interested = (int)$request->interested;

        $model = new Interested();
        $condition = ['initiator_user_id' => $user_id, 'participants_user_id' => $user_id2];
        $result = $model->where($condition)->first();
        if ($result) {
            if ($result->interested == $interested) {
                $result = true;
            } else {
                $update = array('interested' => $interested);
                $result = $model->where($condition)->update($update);
            }
            if ($result) {
                return response()->success();
            }
        }
        return response()->fail(200005);
    }

}