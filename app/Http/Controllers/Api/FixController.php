<?php
/**
 * Created by PhpStorm.
 * User: Mr.Liu
 * Date: 2018/12/12 0012
 * Time: 17:22
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\ArtistPersonal;
use App\Models\UserIndex;

class FixController extends Controller
{

    public function fixArtist()
    {
        $model = new ArtistPersonal();
        $artist_list = $model->where('user_id', '<>', '0')->lists('id', 'user_id');
        if (empty($artist_list)) {
            return response()->success([]);
        }
        $user_index = new UserIndex();
        foreach ($artist_list as $key => $value) {
            $update = [
                'authentication_type' => 1,
                'authentication_id' => $value,
            ];
            $where = ['user_id' => $key];
            if (empty($where)) {
                continue;
            }
            $user_index->where($where)->update($update);
            $user_index->cacheDel($key);
        }
        pp('done');
    }
}