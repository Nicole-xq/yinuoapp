<?php

namespace App\Http\Controllers\Api;

use App\Models\Address;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class AddressController extends Controller
{
    public function getAddress(Request $req)
    {
        $pid = $req->get('pid');
        if (!isset($pid)) {
            return response()->fail(200001);
        }
        $model = new Address();
        $res = $model->getAddress($pid);
        return response()->success($res);
    }


    /*
     * 根据接收的地址名 返回数据库与之对应的城市ID
     */
    public function getCityIdByCityName(Request $req)
    {
        $city_name = $req->input('city_name');
        if (empty($city_name)) {
            return response()->fail(200001);
        }
        $model = new Address();
        $city_id = $model->get_cityId_byCityName($city_name);
        if (is_object($city_id)) {
            $city_id = json_decode(json_encode($city_id), true);
        }
        if (!empty($city_id) && is_array($city_id)) {
            return response()->success(['city_id'=>$city_id[0]]);
        }
        return response()->success(['city_id'=>0]);
    }

    public function fixArea()
    {
        $area = new Address();
        $list = $area->select(['area_id', 'area_name'])->where(['area_deep' => 2])->get()->toArray();
        $ids = array_column($list, 'area_id');
        $noAreaList = $area->select(['area_id', 'area_name', 'area_parent_id'])
            ->whereIn('area_parent_id', $ids)->get()->toArray();
        $ids = array_flip($ids);
        foreach ($noAreaList as $value) {
            if (array_key_exists($value['area_parent_id'], $ids)) {
                unset($ids[$value['area_parent_id']]);
            }
        }
        if (!empty($ids)) {
            $ids = array_flip($ids);
            foreach ($ids as $val) {
                $insert = [
                    'area_name' => '城区',
                    'area_parent_id' => $val,
                    'area_deep' => 3,
                ];
                $area->insert($insert);
            }
        }
        $return = [
            '待修复二级城市ID' => $ids
        ];
        return response()->success($return);
    }
}
