<?php

namespace App\Http\Controllers\Api;

use App\Models\Banner;
use App\Http\Controllers\Controller;
use App\Models\SlideShowInfo;

class BannerController extends Controller
{
    /**
     * 获取首页banner图
     *
     * @return array
     */
    public function index()
    {
        $model_banner = new SlideShowInfo();

        $field = ['img','url'];
        $banner = $model_banner->getList(['slideshow_id'=>4], $field);
        $banner = json_decode(json_encode($banner), true);
        foreach ($banner as &$value) {
            $value['img'] = getPicturePath($value['img']);
        }
        return response()->success(['banner'=>$banner]);
    }
}
