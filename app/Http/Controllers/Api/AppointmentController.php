<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\AppointmentList;
use App\Models\AppointmentInfo;
use App\Models\AppointmentSign;
use App\Models\ArtistOrganization;
use App\Models\ExhibitionArtworkList;
use App\Models\UserIndex;
use Illuminate\Http\Request;
use App\Models\AppointmentList as Appoint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;
use Validator;

class AppointmentController extends Controller
{
    protected $Model;

    protected $table = [];


    public function __construct()
    {
        $this->Model = new Appoint();
        $this->middleware('verify.user.login', [
            'only' => [
                'showMyAppointmentList',
                'addAppointmentInfo',
                'showInfo',
                'showInfo2',
                'addAppointment',
                'deleteAppointmentInfo'
            ]
        ]);
    }

    /**
     * 约展列表,用户信息,艺展信息,展览图片,展示美术馆名称
     * @param Request $request
     * @return mixed
     */
    public function showAppointmentList(Request $request)
    {
        $exhibition_id = $request->input('exhibition_id');
        if (empty($exhibition_id)) {
            return response()->fail(200001);
        }

        $appointmentLists = $this->Model->showAppointmentList($exhibition_id);
        if (empty($appointmentLists)) {
            return response()->fail(200004);
        }

        if (is_object($appointmentLists)) {
            $appointmentLists = $appointmentLists->toArray();
        }
        if (isset($appointmentLists['data']) && is_array($appointmentLists['data'])) {
            $user_ids = [];
            $exhibition_ids = [];
            $organization_ids = [];
            foreach ($appointmentLists['data'] as $val) {
                $user_ids[] = $val['user_id'] ?? '';
                $user_ids = array_unique($user_ids);
                $exhibition_ids[] = $val['exhibition_id'] ?? '';
                $exhibition_ids = array_unique($exhibition_ids);
                $organization_ids[] = $val['appoint_exhibition']['organization_id'] ?? '';
                $organization_ids = array_unique($organization_ids);
            }
            if (!empty($user_ids)) {
                $user_list = $this->fetchUserList($user_ids);
            }
            if (!empty($exhibition_ids)) {
                $exhibition_list = $this->fetchExhibitionList($exhibition_ids);
            }
            if (!empty($organization_ids)) {
                $organization_list = $this->fetchOrganizationList($organization_ids);
            }
            $user_ids = array_column($appointmentLists['data'], 'user_id');
            $avatar = getMemberAvatarByID($user_ids);
            foreach ($appointmentLists['data'] as &$val) {
                $val['user_sex'] = $user_list[$val['user_id']]['user_sex'] ?? 0;
                $val['user_name'] = $user_list[$val['user_id']]['user_name'] ?? '';
                $val['title'] = $val['appoint_exhibition']['title'];
                $val['begin_time'] = $val['appoint_exhibition']['begin_time'];
                $tmp_img = json_decode($exhibition_list[$val['exhibition_id']]['img_list']) ?: [''];
                $val['min_pic'] = getPicturePath($tmp_img[0]);
                $organization_id = $val['appoint_exhibition']['organization_id'];
                $val['organization_name'] = $organization_list[$organization_id]['organization_name'] ?? '';
                unset($val['appoint_exhibition']);
                $val['appoint_id'] = $val['id'];
                $val['user_avatar'] = $avatar[$val['user_id']] . '?' . time();
                $val['user_nickname'] = $val['user_nickname'] ?: tellHide($val['user_name']);
                $val['created_at'] = date("Y-m-d", strtotime($val['created_at']));
                unset($val['id']);
            }
        }
        return response()->success($appointmentLists);
    }


    public function fetchUserList($user_ids)
    {
        $user_model = new UserIndex();
        $user_lists = $user_model->getUserInfoByIds($user_ids);
        $user_lists = $user_lists->toArray();
        $user_list = [];
        foreach ($user_lists as $value) {
            $user_list[$value['user_id']]['user_sex'] = $value['user_sex'];
            $user_list[$value['user_id']]['user_name'] = $value['user_name'];
        }
        return $user_list;
    }

    public function fetchExhibitionList($exhibition_ids)
    {
        $exhibition_model = new ExhibitionArtworkList();
        $exhibition_lists = $exhibition_model->getExhibitionInfo($exhibition_ids);
        $exhibition_list = [];
        foreach ($exhibition_lists as $value) {
            $exhibition_list[$value['id']]['img_list'] = $value['img_list'];
        }
        return $exhibition_list;
    }

    public function fetchOrganizationList($organization_ids)
    {
        $organization_model = new ArtistOrganization();
        $organization_lists = $organization_model->getOrganizationList($organization_ids);
        $organization_list = [];
        foreach ($organization_lists as $value) {
            $organization_list[$value['id']]['organization_name'] = $value['name'];
            $organization_list[$value['id']]['organization_address'] = $value['address'];
        }
        return $organization_list;
    }


    /**
     * 我的约展 我发起的约展的列表
     * @param Request $req
     * @return mixed
     */
    public function showMyAppointmentList(Request $req)
    {
        $user_id = $req->user['user_id'];
        $user_sex = $req->user['user_sex'];
        $appointmentLists = $this->Model->showMyAppointmentList($user_id);

        if (is_object($appointmentLists)) {
            $appointmentLists = $appointmentLists->toArray();
        }
        if (empty($appointmentLists)) {
            return response()->fail(200004);
        }
        $organization_ids = [];
        foreach ($appointmentLists['data'] as $value) {
            $organization_ids[] = $value['appoint_exhibition']['organization_id'] ?? '';
            $organization_ids = array_unique($organization_ids);
        }
        if ($organization_ids) {
            $organization_list = $this->fetchOrganizationList($organization_ids);
        }
        if (isset($appointmentLists['data']) && is_array($appointmentLists['data'])) {
            $user_ids = array_column($appointmentLists['data'], 'user_id');
            $avatar = getMemberAvatarByID($user_ids);
            foreach ($appointmentLists['data'] as &$val) {
                $val['user_avatar'] = $avatar[$val['user_id']] . '?' . time();
                $val['user_sex'] = $user_sex ?: 0;
                $val['appoint_id'] = $val['id'];
                $val['title'] = $val['appoint_exhibition']['title'];
                $val['begin_time'] = $val['appoint_exhibition']['begin_time'];
                $tmp_img = json_decode($val['appoint_exhibition']['img_list']);
                if (!empty($tmp_img) && is_array($tmp_img)) {
                    $val['min_pic'] = getPicturePath($tmp_img[0]);
                } else {
                    $val['min_pic'] = '';
                }
                $organization_id = $val['appoint_exhibition']['organization_id'];
                $val['organization_name'] = $organization_list[$organization_id]['organization_name'] ?? '';
                $val['organization_address'] = $organization_list[$organization_id]['organization_address'] ?? '';
                unset($val['appoint_exhibition']);
            }
        }
        $model = new UserIndex();
        $model->where(['user_id' => $user_id])->update(['appointment_num' => 0]);
        $model->cacheDel($user_id);
        return response()->success($appointmentLists);
    }


    /**
     * 发布约展
     * @param Request $request
     * @return mixed
     */
    public function addAppointment(Request $request)
    {
        $params = $this->addAppointmentCheck($request);

        //判断是否已经发起 防止重复发起
        $result = $this->Model->getAppointment($params['exhibition_id'], $params['user_id']);
        if (is_object($result)) {
            $result = $result->toArray();
        }

        if (!empty($result)) {
            return response()->fail(57003);
        }

        //获取用户信息
        $userInfo = (new UserIndex())->getUserInfoById($params['user_id']);

        if (empty($userInfo)) {
            return response()->fail(200001);
        }

        $params['user_nickname'] = $userInfo['user_nickname'] ?: tellHide($userInfo['user_name']);
        $params['user_avatar'] = getMemberAvatarByID($userInfo['user_id']);

        //获取艺展信息
        $exhibitionInfo = (new ExhibitionArtworkList())->getArtworkInfo($params['exhibition_id']);

        if (empty($exhibitionInfo)) {
            return response()->fail(200004);
        }

        $params['exhibition_name'] = $exhibitionInfo['title'];

        $params['go_date'] = strtotime($params['go_date']);

        $params['created_at'] = date('Y-m-d H:i:s', time());
        $result = $this->Model->addAppointment($params);
        if (!$result) {
            return response()->fail(57001);
        }
        return response()->success();
    }


    public function addAppointmentCheck($request)
    {
        $rule = [
            'go_date' => 'required',
            'time_node' => 'required',
            'sex' => 'required',
            'exhibition_id' => 'required',
            'address' => 'required',
        ];

        $message = [
            '*.required' => 200001
        ];
        $validator = Validator::make($request->all(), $rule, $message);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->fail($errors->all()[0]);
        }
        $params['user_id'] = $request->user['user_id'];
        $params['go_date'] = $request->go_date;
        $params['time_node'] = $request->time_node;
        $params['sex'] = $request->sex;
        $params['exhibition_id'] = $request->exhibition_id;
        $params['address'] = $request->address;
        return $params;
    }

    /**
     * 约展详情,用户信息,艺展信息,展览图片,展示美术馆名称
     * @param Request $req
     * @return mixed
     */
    public function showAppointment(Request $request)
    {
        $appoint_id = $request->input('appoint_id');
        if (empty($appoint_id)) {
            return response()->fail(200001);
        }
        $result = $this->Model->showAppointment($appoint_id);
        if (is_object($result)) {
            $result = $result->toArray();
        }
        if (empty($result)) {
            return response()->fail(205001);
        }
        $result['user_avatar'] = getMemberAvatarByID($result['user_id']) . '?' . time();
        if (!empty($result['appoint_exhibition'])) {
            $result['exhibition_id'] = $result['appoint_exhibition']['id'];
            //$user_sex = (new UserIndex())->getUserInfo(['user_id' => $result['user_id']]);
            $user_sex = (new UserIndex())->getUserInfoById($result['user_id']);
            $result['user_sex'] = $user_sex['user_sex'] ?: 0;
            $result['user_nickname'] = $result['user_nickname'] ?: tellHide($user_sex['user_name']);
            $result['exhibition_title'] = $result['appoint_exhibition']['title'];
            if (!empty($result['appoint_exhibition']['img_list'])) {
                $tmp_img = json_decode($result['appoint_exhibition']['img_list'], true) ?: [];
                if (!empty($tmp_img) && isset($tmp_img[0])) {
                    $result['exhibition_pic'] = getPicturePath($tmp_img[0], 300);
                } else {
                    $result['exhibition_pic'] = '';
                }
            } else {
                $result['exhibition_pic'] = '';
            }
            $result['exhibition_begin_time'] = $result['appoint_exhibition']['begin_time'];
            $result['exhibition_end_time'] = $result['appoint_exhibition']['end_time'];
            $organization_info = $this->getOrganizationInfo($result['appoint_exhibition']['organization_id']);
            $result['exhibition_addr'] = $organization_info['addr'] ?? '';
            $result['exhibition_address'] = $organization_info['address'] ?? '';
            $result['exhibition_organization'] = $organization_info['organization_name'] ?? '';
            unset($result['appoint_exhibition']);
        }
        if (!$result) {
            return response()->fail(57002);
        }
        return response()->success($result);
    }


    /**
     * 和TA 约, 添加至约展详情
     * @param Request $request
     * @return mixed
     */
    public function addAppointmentInfo(Request $request)
    {
        $params = $this->addAppointmentInfoCheck($request);
        $address = $request->get('address', '0,1');
        //首先判断 该用户属于db1  还是 db2
        $db_name = $request->user['db'];

        //接着判断该 用户是否已经和 ta 约了这一场会展
        $result = (new AppointmentInfo())->getAppointment($params, $db_name);

        if (is_object($result)) {
            $result = json_decode(json_encode($result), true);
        }
        if ($result) {
            return response()->fail(57004);
        }
        $result = AppointmentList::where('id', $request->appointment_id)->first();
        if (empty($result)) {
            return response()->fail(57010);
        }

        //开启事务  约展报名表 和   约展报名详情表  一起进行添加

        //获取当前会员所属的是一库 还是 二库
        //开启所属数据库  与主数据库的事务
        DB::beginTransaction();
        DB::connection($db_name)->beginTransaction();
        try {
            $insert = [
                'initiator_user_id' => $result->user_id,
                'participants_user_id' => $request->user['user_id'],
            ];
            $orWhere = [
                'initiator_user_id' => $result->user_id,
                'participants_user_id' => $request->user['user_id'],
            ];

            $res = DB::table('interested')->where($insert)->orWhere($orWhere)->first();
            if ($res) {
                $res = json_decode(json_encode($res), true);
                if ($res['interested'] == 1) {
                    $result3 = true;
                } else {
                    $result3 = DB::table('interested')->where($insert)->orWhere($orWhere)->update(['interested' => 1]);
                }
            } else {
                $insert['interested'] = 1;
                $result3 = DB::table('interested')->insert($insert);
            }
            if (!$result3) {
                throw new Exception('interested 兴趣表添加失败');
            }

            $insert = [
                'exhibition_id' => $params['exhibition_id'],
                'appointment_id' => $params['appointment_id'],
                'user_id' => $params['user_id'],
                'status' => 1                           //审核状态  默认0  未审核
            ];

            unset($params['exhibition_id']);
            $params['created_at'] = date('Y-m-d H:i:s', time());
            $params['address'] = $address;
            $result2 = DB::table('appointment_sign')->insertGetId($insert);
            if (!$result2) {
                throw new Exception('appointment_sign 报名表添加失败');
            }
            $params['appointment_sign_id'] = $result2;
            $result1 = DB::connection($db_name)->table('appointment_info')->insert($params);
            if (!$result1) {
                throw new Exception('appointment_info 报名详情表添加失败');
            }
            if (!empty($insert['status']) && $insert['status'] == 1) {
                $result2 = AppointmentList::where('id', $request->appointment_id)->increment('count_num');
                if (!$result2) {
                    throw new Exception('appointment_list 约展表自增失败！');
                }
            }

            //库数据提交
            DB::commit();
            DB::connection($db_name)->commit();
        } catch (\Exception $e) {
            //库数据回滚
            DB::rollback();
            DB::connection($db_name)->rollback();
            Log::error('创建参约：' . $e->getMessage());
            return response()->fail(57001);
        }
        return response()->success();
    }


    public function addAppointmentInfoCheck($request)
    {
        $rule = [
            'appointment_id' => 'required',
            'name' => 'required',
            'mobile' => 'required',
            'wx_number' => 'required',
            'msg' => 'required',
            'exhibition_id' => 'required'
        ];
        $message = [
            '*.required' => 200001
        ];
        $validator = Validator::make($request->all(), $rule, $message);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->fail($errors->all()[0]);
        }

        $initiator = (new AppointmentList())
            ->where('id', $request->appointment_id)
            ->select('user_id')
            ->first()->toarray();
        $params['initiator_id'] = $initiator['user_id'];
        $params['appointment_id'] = $request->appointment_id;
        $params['name'] = $request->name;
        $params['mobile'] = $request->mobile;
        $params['wx_number'] = $request->wx_number;
        $params['msg'] = $request->msg;
        $params['exhibition_id'] = $request->exhibition_id;
        $params['user_id'] = $request->user['user_id'];
        if ($params['initiator_id'] == $params['user_id']) {
            return response()->fail(57005);
        }
        return $params;
    }

    /**
     * 和TA约 约展报名记录  查询所有的报名记录
     * @return mixed
     */
    public function showInfoList()
    {
        $lists = (new AppointmentInfo())->showInfoList();
        return response()->success($lists);
    }

    /**
     * 和TA约 约展报名记录  查询所有的报名记录
     * @param Request $req
     * @return mixed
     */
    public function showInfo(Request $req)
    {
        $appointment_id = $req->get('appointment_id');
        if (empty($appointment_id)) {
            return response()->fail(200001);
        }

        $list = (new AppointmentSign())->showInfo($appointment_id);
        $list = ($list->toarray())['data'];
        if (empty($list)) {
            return response()->success();
        }
        $db = [];
        $user_ids = [];
        $appointment_ids = [];
        foreach ($list as $val) {
            $appoint_users = $val['appoint_users'];
            $user_ids[] = $appoint_users['user_id'];
            $db[$appoint_users['db']][] = $appoint_users['user_id'];
            $user_sex[$appoint_users['user_id']] = $appoint_users['user_sex'];
            $appointment_ids[] = $val['id'];
        }
        $model = new AppointmentInfo();
        $result = array();
        foreach ($db as $key => $value) {
            $model->setConnection($key);
            $res = $model->showInfo($appointment_ids, $value);
            if ($result) {
                $result = array_merge($result, $res);
            } else {
                $result = $res;
            }
        }
        $avatar = getMemberAvatarByID($user_ids);
        foreach ($result as &$val) {
            $val['user_avatar'] = $avatar[$val['user_id']] . '?' . time();
            $val['user_sex'] = $user_sex[$val['user_id']] ?? 0;
        }
        return response()->success($result);
    }

    /**
     * 和TA约 约展报名记录  查询所有的报名记录
     * @param Request $req
     * @return mixed
     */
    public function showInfo2(Request $req)
    {
        $appointment_id = $req->get('appointment_id');
        if (empty($appointment_id)) {
            return response()->fail(200001);
        }

        $list = (new AppointmentSign())->showInfo2($appointment_id);
        $list = $list->toArray();
        if (empty($list['data'])) {
            return response()->success($list);
        }
        $user_ids = [];
        foreach ($list['data'] as $val) {
            $user_ids[] = $val['user_id'];
        }
        $avatar = getMemberAvatarByID($user_ids);
        foreach ($list['data'] as &$val) {
            $val['user_nickname'] = $val['appoint_users']['user_nickname'] ?: '';
            $val['user_avatar'] = $avatar[$val['user_id']] . '?' . time();
            if ($val['initiator_interested']) {
                $val['interested'] = $val['initiator_interested']['interested'];
            } else {
                $val['interested'] = 0;
            }
            unset($val['status']);
            unset($val['is_delete']);
            unset($val['appoint_users']);
            unset($val['initiator_interested']);
        }
        AppointmentSign::where('appointment_id', $appointment_id)->where('is_read', 0)->update(['is_read' => 1]);
        return response()->success($list);
    }

    /**
     * @param $id
     * @return array
     */
    public function getOrganizationInfo($id)
    {
        $info = array();
        //获取展馆信息
        $artist_organization = new ArtistOrganization();
        $organization_info = $artist_organization->where('id', $id)->first();
        $info['organization_name'] = empty($organization_info) ? '' : $organization_info['name'];
        $info['addr'] = !$organization_info || !$organization_info['addr'] ? '0,0' : $organization_info['addr'];
        $address = new Address();
        $province_info = $address->getInfoById($organization_info['province_id']);
        $city_info = $address->getInfoById($organization_info['city_id']);
        $area_info = $address->getInfoById($organization_info['area_id']);
        $area = $province_info['area_name'] . $city_info['area_name'];
        $area .= $area_info['area_name'] . $organization_info['address'];
        $info['address'] = !$organization_info ? '' : $area;
        return $info;
    }

    public function deleteAppointmentInfo(Request $request)
    {
        $user_id = $request->user['user_id'];
        $appointment_id = $request->get('appointment_id', null);
        if (empty($appointment_id)) {
            return response()->fail(200224);
        }
        $initiator_info = (new AppointmentList())->select('user_id')->where('id', $appointment_id)->first();
        if (empty($initiator_info) || !isset($initiator_info['user_id'])) {
            return response()->fail(200004);
        }
        if ($user_id == $initiator_info['user_id']) {
            $model = new AppointmentSign();
        } else {
            $model = new AppointmentInfo();
        }
        $where = [
            'appointment_id' => $appointment_id,
            'user_id' => $user_id,
        ];
        $update = [
            'is_delete' => 1,
        ];
        $result = $model->where($where)->update($update);
        if (!$result) {
            return response()->fail(200014);
        }
        return response()->success();
    }
}