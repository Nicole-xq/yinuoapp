<?php

namespace App\Http\Controllers\Api;

use App\Logic\MemberAccount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class MemberAccountController extends Controller
{
    public function __construct(Request $request)
    {
//        $this->middleware('verify.user.login', ['except' => 'index']);
        $this->middleware('verify.user.login');
    }

    /**
     * 检测是否绑定手机号
     * */
    public function get_mobile_info(Request $request)
    {
        $bankcardLogic = new MemberAccount($request->api_token);
        $result = $bankcardLogic->get_mobile_info();
        if ($result->code == 200) {
            $phone = $result->datas->mobile;
            if ($phone) {
                $phone = '********'.substr($phone,-3);
            }
            return response()->success(['phone' => $phone]);
        }
        return response()->fail(503005);
    }

    /**
     * 点击获取验证码
     * */
    public function captcha(Request $request)
    {
        $bankcardLogic = new MemberAccount($request->api_token);
        $result = $bankcardLogic->modify_paypwd_step2();
        if (isset($result->code)) {
            if ($result->code == 200) {
                return response()->success($result->datas);
            } else {
                return response()->failError($result->datas->error);
            }
        } else {
            return response()->fail(200102);
        }
    }

    /**
     * 点击获取验证码
     * */
    public function store(Request $request)
    {
        $bankcardLogic = new MemberAccount($request->api_token);
        $param = [
            'auth_code' => $request->captcha,
            'password' => $request->password,
        ];
        $result = $bankcardLogic->new_modify_paypwd($param);
        if (isset($result->code)) {
            if ($result->code == 200) {
                return response()->success([]);
            } else {
                return response()->failError($result->datas->error);
            }
        } else {
            return response()->fail(200102);
        }
    }

    // 检测是否设置支付密码
    public function check_init_paypwd(Request $request)
    {
        $bankcardLogic = new MemberAccount($request->api_token);
        $result = $bankcardLogic->check_init_paypwd();
        if ($result->code == 200) {
            $data = ($result->datas == 1) ?  true : false;
            return response()->success($data);
        }
        return response()->fail(503005);
    }

    // 申请余额提现
    public function pd_cash_add(Request $request)
    {
        $rules = [
            'money' => 'required',
            'card_number' => 'required',
            'password' => 'required',
        ];
        $message = [
            '*.required' => 200001,
        ];
        $validator = Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {
            $error = $validator->errors();
            return response()->fail($error->all()[0], array_keys($error->toArray())[0]);
        }

        $bankcardLogic = new MemberAccount($request->api_token);
        $param = [
            'money' => $request->money,
            'card_number' => $request->card_number,
            'password' => $request->password,
        ];
        $result = $bankcardLogic->pd_cash_add($param);
        if ($result->code == 200) {
            $data = ($result->datas == 1) ?  true : false;
            return response()->success($data);
        }elseif ($result->code == 400) {
            return response()->failError($result->datas->error);
        }
        return response()->fail(503005);
    }

}