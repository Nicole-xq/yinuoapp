<?php

namespace App\Http\Controllers\Api;

use App\Libs\phpQuery\OSS;
use App\Models\UserIndex;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use OSS\OssClient;
use OSS\Core\OssException;

class FileOssController extends Controller
{

    private $ossClient;

    private $table = 'user_index';

    /**
     * FileOssController constructor.
     */
    public function __construct()
    {
        try {
            $this->ossClient = new OssClient(
                config('oss.AccessKeyId'),
                config('oss.AccessKeySecret'),
                config('oss.BucketName')
            );
        } catch (OssException $e) {
            print $e->getMessage();
        }

        $this->Model = new UserIndex();
        //only 数组中是需要登录的方法
        $this->middleware('verify.user.login', ['only' => [
            'fileUpload', 'backgroundEdit'
        ]]);
    }


    public function fileUpload(Request $req)
    {
        $user_id = $req->user['user_id'];
        $db_name = $req->user['db'];
        //图片接收
        $pic = $req->file('user_avatar');
        //图片验证
        if (empty($pic)) {
            return response()->fail(200001);
        }
        if (!$pic->isValid()) {
            return response()->fail(503001);
        }
        $filePath = $pic->getRealPath();
        $oss = new OSS();
        //目标文件名
        $object = $oss->get_object_name($user_id);
        $result = $oss->upload($filePath, $object);
        if (!$result) {
            return response()->fail(200104);
        }
        $update = [
            'user_avatar' => $result
        ];
        $where = [
            'user_id' => $user_id
        ];
        //开启所属数据库  与主数据库的事务
        DB::beginTransaction();
        DB::connection($db_name)->beginTransaction();
        try {
            $result1 = DB::table($this->table)->where($where)->update($update);
            $result2 = DB::connection($db_name)->table('users')->where($where)->update($update);
            if ((!$result1 || !$result2) && $result2 !== 0 && $result1 !== 0) {
                throw new \Exception(200104);
            }
            //库数据提交
            DB::commit();
            DB::connection($db_name)->commit();
        } catch (\Exception $e) {
            //库数据回滚
            DB::rollback();
            DB::connection($db_name)->rollback();
            return response()->fail($e->getMessage());
        }
        return response()->success($update);
    }


    /**
     * @SWG\Post(path="/api/member/background",
     *   tags={"artwork"},
     *   operationId="/api/member/background",
     *   summary="个人主页背景图修改",
     *   description="个人主页背景图修改",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="background_img",
     *     in="formData",
     *     description="背景图片",
     *     required=true,
     *     type="file",
     *     default=""
     *   ),
     *   @SWG\Response(response="200", description="successful operation")
     * )
     */
    public function backgroundEdit(Request $request)
    {
        $user_id = $request->user['user_id'];
        $db_name = $request->user['db'];
        //图片接收
        $pic = $request->file('background_img');
        //图片验证
        if (empty($pic)) {
            return response()->fail(200001);
        }
        if (!$pic->isValid()) {
            return response()->fail(503001);
        }
        $filePath = $pic->getRealPath();
        $oss = new OSS();
        //目标文件名
        $object = $oss->get_background_name($user_id);
        $result = $oss->upload($filePath, $object);
        if (!$result) {
            return response()->fail(200104);
        }
        $update = [
            'background_img' => $result
        ];
        $where = [
            'user_id' => $user_id
        ];
        //开启所属数据库  与主数据库的事务
        DB::beginTransaction();
        DB::connection($db_name)->beginTransaction();
        try {
            $result1 = DB::table($this->table)->where($where)->update($update);
            $result2 = DB::connection($db_name)->table('users')->where($where)->update($update);
            if ((!$result1 || !$result2) && $result2 !== 0 && $result1 !== 0) {
                throw new \Exception(200104);
            }
            //库数据提交
            DB::commit();
            DB::connection($db_name)->commit();
        } catch (\Exception $e) {
            //库数据回滚
            DB::rollback();
            DB::connection($db_name)->rollback();
            return response()->fail($e->getMessage());
        }
        (new UserIndex())->cacheDel($user_id);
        return response()->success($update);
    }


    public function index()
    {
        echo 'hello';
        die;
    }
}
