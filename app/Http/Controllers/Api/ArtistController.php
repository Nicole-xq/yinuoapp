<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ArticleTitle;
use App\Models\ArtistPersonal;
use App\Models\Artwork;
use App\Models\ExhibitionArtworkJoint;
use App\Models\UserFollow;
use App\Models\UserIndex;
use App\Models\Ynh\YnhAuthenticationApply;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class ArtistController extends Controller
{

    protected $Model;

    public function __construct()
    {
        $this->Model = new ArtistPersonal();
        $this->middleware('verify.user.login', ['only' => [
            //'recommendArtistFetch'
        ]]);
    }


    /*
     * 艺评首页 顶部推荐用户随机 获取$this->limit 条数
     * 获取艺评人列表 role_id = 3
     */
    public function getRandomArtistInfo()
    {
        $user_lists = (new ArtistPersonal())->getRandomArtistInfo();
        if (!$user_lists) {
            return response()->fail(200004);
        }
        $user_lists = json_decode(json_encode($user_lists), true);
        $user_ids = [];
        foreach ($user_lists as $val) {
            if ($val['user_id'] != 0) {
                $user_ids[] = $val['user_id'];
            }
        }
        if (empty($user_ids)) {
            foreach ($user_lists as &$val) {
                $val['articleNum'] = 0;
            }
            return response()->success($user_lists);
        }
        $user_ids = array_filter($user_ids);
        $author_ids = implode(",", $user_ids);
        $artNum = (new ArticleTitle())->getArtistNum($author_ids);
        $artNum = json_decode(json_encode($artNum), true);
        if (empty($artNum)) {
            foreach ($user_lists as &$val) {
                $val['articleNum'] = 0;
                $val['logo'] = getPicturePath($val['logo']);
            }
            return response()->success($user_lists);
        }
        foreach ($user_lists as &$value) {
            foreach ($artNum as $val) {
                if ($val['author_id'] == $value['user_id']) {
                    $value['article'][] = $val;
                }
            }
            $value['logo'] = getPicturePath($value['logo']);
        }
        foreach ($user_lists as &$val) {
            if (isset($val['article'])) {
                $val['articleNum'] = count($val['article']);
            } else {
                $val['articleNum'] = 0;
            }
            unset($val['article']);
        }
        return response()->success($user_lists);
    }

    /*
     * 根据艺术家ID 获取艺术家 头像 名称 及其作品名称 作品图片
     */
    public function revealArtist(Request $req)
    {
        $artist_id = $req->get('artist_id');
        $exhibition_id = $req->get('exhibition_id');
        if (empty($artist_id) || empty($exhibition_id)) {
            return response()->fail(200001);
        }
        $artwork_list = [];
        $artwork_ids = (new ExhibitionArtworkJoint())->get_artists_artworks_byExhibitionId($exhibition_id, $artist_id);
        if (!empty($artwork_ids)) {
            $artwork_list = (new Artwork())->getArtworkByIds($artwork_ids);
            if (is_object($artwork_list)) {
                $artwork_list = $artwork_list->toArray();
            }
        }
        $artist_info = (new ArtistPersonal())->getArtistInfo($artist_id);
        if (is_object($artist_info)) {
            $artist_info = $artist_info->toArray();
        }
        $artist_info['logo'] = getPicturePath($artist_info['logo']);
        foreach ($artwork_list as &$val) {
            $val['artwork_img'] = getPicturePath($val['artwork_img']);
        }
        $artist_info['artworks'] = $artwork_list;
        return response()->success($artist_info);
    }

    /*
     * 根据艺术家ID 获取艺术家信息
     */
    public function getArtistInfo(Request $req)
    {
        $artist_id = $req->get('artist_id');
        if (empty($artist_id)) {
            return response()->fail(200001);
        }
        $list = (new ArtistPersonal())->getArtistInfo($artist_id);
        if (empty($list) || !is_object($list)) {
            return response()->success();
        }
        if (isset($list->logo)) {
            $list->logo = getPicturePath($list->logo);
        }
        return response()->success($list);
    }

    /**
     * 最新认证艺术家
     * @return mixed
     */
    public function newList()
    {
        //Redis::del('recommend_three_artist');
        $recommend_three = Redis::get('recommend_three_artist');
        if (empty($recommend_three)) {
            $where = ['is_recommend' => 1, 'role_id' => '2'];
            $fields = ['user_id', 'logo as avatar', 'name', 'real_name'];
            $recommend_artist = (new ArtistPersonal())
                ->get_artist_list(
                    $where,
                    $fields,
                    'authentication_time desc',
                    3
                );
            foreach ($recommend_artist as &$value) {
                $value['avatar'] = $value['avatar']
                    ? getPicturePath($value['avatar']) . '?' . date('z', time())
                    : getMemberAvatarByID($value['user_id']) . '?' . date('z', time());
                $value['user_nickname'] = $value['name'] ?? $value['real_name'];
                unset($value['name'], $value['real_name']);
            }
            $recommend_three = json_encode($recommend_artist);
            if (count($recommend_artist) == 3) {
                Redis::set('recommend_three_artist', $recommend_three);
            }
        }
        $list = json_decode($recommend_three, true);
        $exclude = [];
        if (!empty($list)) {
            $exclude = array_map(function ($value) {
                return $value['user_id'] ?? '';
            }, $list);
        }
        $condition = [
            'role_id' => '2',
        ];
        $fields = [
            'user_id', 'logo as real_avatar', 'name', 'real_name', 'logo'
        ];
        $newest_artist = (new ArtistPersonal())
            ->get_artist_list(
                $condition,
                $fields,
                'authentication_time desc',
                10
            );
        if (!empty($newest_artist)) {
            foreach ($newest_artist as $key => $value) {
                if (in_array($value['user_id'], $exclude)) {
                    continue;
                }
                if (count($list) == 10) {
                    continue;
                }
                if (empty($value['user_id'])) {
                    unset($newest_artist[$key]);
                    continue;
                }
                $list[] = array(
                    'user_id' => $value['user_id'] ?? 0,
                    'avatar' => $value['real_avatar']
                        ? getPicturePath($value['real_avatar']) . '?' . date('z', time())
                        : getMemberAvatarByID($value['user_id']) . '?' . date('z', time()),
                    'user_nickname' => $value['name'] ?? $value['real_name']
                );
            }
        }
        return response()->success($list);
    }

    /**
     * 关注/取关艺术家
     * @param Request $request
     * @return mixed
     */
    public function followArtist(Request $request)
    {
        $follow_user_id = $request->follow_user_id;
        $user_id = $request->user['user_id'];
        if (empty($user_id) || empty($follow_user_id)) {
            return response()->fail(200001);
        }
        if ($user_id == $follow_user_id) {
            return response()->fail(200110);
        }
        $user_followModel = new UserFollow();
        $model = new UserIndex();
        $res = $user_followModel->getInfo(['user_id' => $user_id, 'follow_user_id' => $follow_user_id]);
        if ($res) {
            if ($res['is_delete'] == 1) {
                $status = 1;
                $re = $user_followModel->updateInfo(['is_delete' => 0], ['id' => $res['id']]);
                $model->where(['user_id' => $follow_user_id])->increment('fans_num', 1);
                $model->cacheDel($follow_user_id);
            } else {
                $status = 0;
                $re = $user_followModel->updateInfo(['is_delete' => 1], ['id' => $res['id']]);
            }
            if ($re != 1) {
                return response()->fail(200005);
            }
        } else {
            $param = array(
                'user_id' => $user_id,
                'follow_user_id' => $follow_user_id,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            );
            $re = $user_followModel->addInfo($param);
            if (!$re) {
                return response()->fail(200005);
            }
            $status = 1;
            $model->where(['user_id' => $follow_user_id])->increment('fans_num', 1);
            $model->cacheDel($follow_user_id);
        }
        return response()->success($status);
    }

    public function recommendArtistFetch(Request $request)
    {
        $type_id = $request->type_id ?? '';
        $condition['is_valid'] = 1;
        if (!empty($type_id)) {
            $condition['artist_type'] = $type_id;
        }
        $model = new YnhAuthenticationApply();
        $artist_list = $model->recommendArtistFetch($condition);
        if (empty($artist_list)) {
            return response()->success($artist_list);
        }
        $artist_list = $artist_list->toArray();
        $artists = [];
        if (isset($artist_list['data']) && is_array($artist_list['data'])) {
            foreach ($artist_list['data'] as $value) {
                $artist['user_id'] = $value['user_id'];
                if (strlen($value['nick_name']) > 24) {
                    $artist['nick_name'] = mb_substr($value['nick_name'], 0, 24, 'utf-8') . '...';
                } else {
                    $artist['nick_name'] = $value['nick_name'];
                }
                $user_flag = (new UserIndex())->getUserInfoById($value['user_id'])['user_flag'] ?? '';
                $user_flag = $user_flag ?: '这个人很懒，没有留下签名~';
                $artist['user_flag'] = (strlen($user_flag) > 48)
                    ?
                    mb_substr($user_flag, 0, 48, 'utf-8') . '...' : $user_flag;
                if ($value['authentication_type'] == 1) {
                    //显示认证艺术家信息
                    $artist['real_avatar'] = $value['logo']
                        ?
                        getPicturePath(json_decode($value['logo'])) : '';
                    unset($value['address'], $value['brand_pic']);
                    unset($value['name'], $value['trinity_num'], $value['logo']);
                } else {
                    //显示认证机构信息
                    unset($value['real_avatar'], $value['artist_type']);
                    unset($value['user_sex'], $value['artwork_pic']);
                    $artist['logo'] = $value['logo']
                        ?
                        getPicturePath(json_decode($value['logo'])) : '';
                }
                //认证用户下,作品展示
                $artwork_pic = (new Artwork())->artworkFetchByUserId($value['user_id']);
                if (!empty($artwork_pic)) {
                    foreach ($artwork_pic as $k => $value) {
                        $pic_path = getPicturePath($value);
                        $artist['artwork'][] = is_array($pic_path) ? $pic_path[0] : $pic_path;
                    }
                } else {
                    $artist['artwork'] = [];
                }

                $artists[] = $artist;
                //删除不必要字段
                unset($value['addr'], $value['is_valid'], $value['fail_cause']);
                unset($value['authentication_id']);
            }
        }
        $artist_list['data'] = $artists;
        return response()->success($artist_list);
    }

    public function searchArtistByArtistType(Request $request)
    {
        $artist_type = $request->artist_type;
        $api_token = $request->api_token;
        if (!empty($api_token)) {
            $login_info = (new UserIndex())->getUserInfoByToken($api_token);
            if (!is_array($login_info)) {
                return response()->fail($login_info);
            }
            $user_id = $login_info['user_id'];
        }
        $where = ['role_id' => 2];
        if (!empty($artist_type)) {
            $where['artist_type'] = $artist_type;
        }
        $files = ['id', 'user_id', 'name', 'logo'];
        //$NotNull = 'artwork_time';
        //$artist_list = (new ArtistPersonal())->get_artist_list($where, $files, 'authentication_time desc', 0, $NotNull);
        $artist_list = (new ArtistPersonal())->get_artist_list($where, $files, 'authentication_time desc', 0);
        $this->Model = new Artwork();
        $artwork_fields = ['id', 'artist_id', 'artwork_name', 'artwork_img', 'created_at'];
        $artwork_order = 'created_at desc';
        $artwork_condition = [
            'artist_type' => 1,         //只查询获取艺术家
            'is_delete' => 0,
            'status' => 1
        ];
        $take = 4;
        $user_model = new UserIndex();
        foreach ($artist_list['data'] as $key => &$value) {
            if (isset($user_id) && $user_id) {
                if ($user_id == $value['user_id']) {
                    $value['is_follow'] = 2;//用户自己
                } else {
                    $user_followModel = new UserFollow();
                    $tmp = $user_followModel->getInfo([
                        'user_id' => $user_id,
                        'follow_user_id' => $value['user_id'],
                        'is_delete' => 0
                    ]);
                    $value['is_follow'] = $tmp ? 1 : 0;
                }
            } else {
                $value['is_follow'] = 0;
            }
            $value['name'] = strlen($value['name']) > 24 ? mb_substr($value['name'], 0, 24) . '...' : $value['name'];
            $value['logo'] = $value['logo'] ? getPicturePath($value['logo']) . '?' . date('z', time()) : '';
            $user_info = $user_model->getUserInfoById($value['user_id']);
            if (empty($user_info)) {
                unset($artist_list['data'][$key]);
                continue;
            }
            $value['user_flag'] = $user_info['user_flag'] ?? '';
            $artwork_condition['artist_id'] = $value['id'];
            $value['artwork'] = $this->Model
                ->getArtworkList($artwork_condition, $artwork_fields, $artwork_order, $take);
            if (!empty($value['artwork']) && is_array($value['artwork'])) {
                foreach ($value['artwork'] as &$item) {
                    $item['artwork_img'] = $item['artwork_img'] ? getPicturePath($item['artwork_img']) : '';
                }
            }
        }
        $artist_list['data'] = array_values($artist_list['data']);
        return response()->success($artist_list);
    }
}
