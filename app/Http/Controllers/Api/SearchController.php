<?php
/**
 * Created by Test, 2018/09/28 16:56.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ArtistOrganization;
use App\Models\ArtistPersonal;
use App\Models\Artwork;
use App\Models\UserFollow;
use App\Models\UserIndex;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{

    /**
     * 搜索接口(艺术家/作品)
     * @param $type 搜索类型(0:全部 1:艺术家 2:作品) 默认0
     * @param Request $request
     * @todo 全能接口
     */
    public function index(Request $request)
    {
        $list = [];
        $type = in_array($request->type, [0, 1, 2]) ? $request->type : 0;
        $text = $request->text;
        $c_id = $request->c_id;
        $api_token = $request->api_token;
        if (!empty($api_token)) {
            $login_info = (new UserIndex())->getUserInfoByToken($api_token);
            if (!is_array($login_info)) {
                return response()->fail($login_info);
            }
            $u_id = $login_info['user_id'];
        }
        $user_followModel = new UserFollow();
        switch ($type) {
            case 0://全部搜索
                $artistModel = new ArtistPersonal();
                if ($text != '') {
                    $artist_condition[] = ['name', 'like', "%$text%"];
                    $fields = ['id', 'user_id', 'role_id', 'name'];
                    $artist_list = $artistModel->get_artist_list($artist_condition, $fields);
                } else {
                    $artist_list = $artistModel->get_artist_list();
                }
                $list['artist'] = $artist_list ?: [];
                $organizationModel = new ArtistOrganization();
                if ($text != '') {
                    $organization_condition[] = ['nick_name', 'like', "%$text%"];
                    $fields = ['id', 'user_id', 'role_id', 'nick_name'];
                    $organization_list = $organizationModel->get_organization_list($organization_condition, $fields);
                } else {
                    $organization_list = $organizationModel->get_organization_list();
                }
                $list['organization'] = $organization_list ?: [];
                $artworkModel = new Artwork();
                if ($text != '') {
                    $artwork_condition[] = ['artwork_name', 'like', "%$text%"];
                    $artwork_condition['is_delete'] = 0;
                    $artwork_list = $artworkModel->getArtworkList($artwork_condition);
                } else {
                    $artwork_list = $artworkModel->getArtworkList();
                }
                $list['artwork'] = $artwork_list ?: [];
                break;
            case 1://艺术家
                $artistModel = new ArtistPersonal();
                if ($text != '') {
                    $like_condition[] = ['name', 'like', "%$text%"];
                    $query = $artistModel->where($like_condition);
                } else {
                    $query = $artistModel;
                }
                if ($c_id != '') {
                    $artist_condition[] = ['artist_type' => $c_id];
                    $query = $query->where($artist_condition);
                }
                $artist_list = $query->orderByRaw('id desc')->paginate();
                $artist_list = $artist_list ? $artist_list->toArray() : $artist_list;
                $list['artist'] = $artist_list;
                break;
            case 2://作品
                $artworkModel = new Artwork();
                if ($text != '') {
                    $like_condition[] = ['artwork_name', 'like', "%$text%"];
                    $like_condition['is_delete'] = 0;
                    $query = $artworkModel->where($like_condition);
                } else {
                    $query = $artworkModel;
                }
                if ($c_id != '') {
                    $artwork_condition[] = ['category_id' => $c_id];
                    $query = $query->where($artwork_condition);
                }
                $artwork_list = $query->orderByRaw('id desc')->paginate();
                $artwork_list = $artwork_list ? $artwork_list->toArray() : $artwork_list;
                $list['artwork'] = $artwork_list;
                break;
            default;
                break;
        }
        $user_arr = [];
        if (isset($list['artist']) && !empty($list['artist']['data'])) {
            foreach ($list['artist']['data'] as $key => &$artist_info) {
                if (!isset($user_arr[$artist_info['user_id']])) {
                    $user_indexModel = new UserIndex();
                    $user_info = $user_indexModel->getUserInfoById($artist_info['user_id']);
                    if (!empty($user_info)) {
                        $user_arr[$artist_info['user_id']] = array(
                            'avatar' => getPicturePath($user_info['user_avatar']),
                            'user_nickname' => $artist_info['name'] ?? '认证坏数据',
                            'user_flag' => $user_info['user_flag'],
                            'authentication_id' => $user_info['authentication_id']
                        );
                    } else {
                        unset($list['artist']['data'][$key]);
                        continue;
                    }
                }
                $artist_info['avatar'] = $user_arr[$artist_info['user_id']]['avatar']
                     ? $user_arr[$artist_info['user_id']]['avatar'] . '?' . date('z', time()) : env('YN_LOGO', '');
                $artist_info['real_name'] = $user_arr[$artist_info['user_id']]['user_nickname'];
                $artist_info['user_flag'] = $user_arr[$artist_info['user_id']]['user_flag'];
                $role_id = $artist_info['role_id'] ?? 0;
                $artist_info['artist_type'] = $this->getRoleName($role_id);
                $artist_info['authentication_id'] = $user_arr[$artist_info['user_id']]['authentication_id'];

                if (isset($u_id) && $u_id) {
                    if ($u_id == $artist_info['user_id']) {
                        $artist_info['is_follow'] = 2;//用户自己
                    } else {
                        $tmp = $user_followModel->getInfo([
                            'user_id' => $u_id,
                            'follow_user_id' => $artist_info['user_id'],
                            'is_delete' => 0
                        ]);
                        $artist_info['is_follow'] = $tmp ? 1 : 0;
                    }
                } else {
                    $artist_info['is_follow'] = 0;
                }
            }
            $list['artist']['data'] = array_values($list['artist']['data']);
            unset($artist_info);
        }
        $organization_arr = [];
        if (isset($list['organization']) && !empty($list['organization']['data'])) {
            foreach ($list['organization']['data'] as $key => &$organization_info) {
                if (!isset($organization_arr[$organization_info['user_id']])) {
                    $user_indexModel = new UserIndex();
                    $user_info = $user_indexModel->getUserInfoById($organization_info['user_id']);
                    if (!empty($user_info)) {
                        $organization_arr[$organization_info['user_id']] = array(
                            'avatar' => getPicturePath($user_info['user_avatar']),
                            'user_nickname' => $organization_info['nick_name'],
                            'user_flag' => $user_info['user_flag'],
                            'authentication_id' => $user_info['authentication_id']
                        );
                    } else {
                        unset($list['organization']['data'][$key]);
                        continue;
                    }
                }
                $organization_info['avatar'] = $organization_arr[$organization_info['user_id']]['avatar']
                    ? $organization_arr[$organization_info['user_id']]['avatar'] . '?' . date('z', time())
                    : env('YN_LOGO', '');
                $organization_info['real_name'] = $organization_arr[$organization_info['user_id']]['user_nickname'];
                $organization_info['user_flag'] = $organization_arr[$organization_info['user_id']]['user_flag'];
                $role_id = $organization_info['role_id'] ?? 0;
                $organization_info['artist_type'] = $this->getRoleName($role_id);
                $organization_info['authentication_id'] =
                    $organization_arr[$organization_info['user_id']]['authentication_id'];

                if (isset($u_id) && $u_id) {
                    if ($u_id == $organization_info['user_id']) {
                        $organization_info['is_follow'] = 2;//用户自己
                    } else {
                        $tmp = $user_followModel->getInfo([
                            'user_id' => $u_id,
                            'follow_user_id' => $organization_info['user_id'],
                            'is_delete' => 0
                        ]);
                        $organization_info['is_follow'] = $tmp ? 1 : 0;
                    }
                } else {
                    $organization_info['is_follow'] = 0;
                }
            }
            unset($organization_info);
        }
        $artist_arr = [];
        if (isset($list['artwork']) && !empty($list['artwork']['data'])) {
            foreach ($list['artwork']['data'] as $key => &$artwork_info) {
                if (!isset($artist_arr[$artwork_info['artist_id']])) {
                    if ($artwork_info['artist_type'] == 1) {
                        $artist_personalModel = new ArtistPersonal();
                    } else {
                        $artist_personalModel = new ArtistOrganization();
                    }
                    $artist_info = $artist_personalModel->getArtistInfo($artwork_info['artist_id']);
                    if (!empty($artist_info)) {
                        $artist_arr[$artwork_info['artist_id']] = array(
                            'artist_name' => $artist_info['real_name'],
                        );
                    } else {
                        unset($list['artwork']['data'][$key]);
                        continue;
                    }
                }
                $artwork_info['artist_name'] = $artist_arr[$artwork_info['artist_id']]['artist_name'] ?? '匿名用户';
                $artwork_info['type_info'] = $artwork_info['type_info']
                    ? json_decode($artwork_info['type_info'], true)
                    : [
                        'l' => 0,
                        'w' => 0,
                        'h' => 0,
                    ];
                $artwork_info['artwork_img'] = $artwork_info['artwork_img']
                    ? getPicturePath($artwork_info['artwork_img']) : '';
            }
            unset($artwork_info);
        }
        if (is_array($list)) {
            foreach ($list as &$value) {
                if (isset($value['data'])) {
                    $value['data'] = array_values($value['data']);
                }
            }
        }
        return response()->success($list);
    }

    private function getRoleName($role_id = 0)
    {
        if (!empty($role_id) && is_int($role_id)) {
            $role = DB::table('artist_role')->where('role_id', $role_id)->first();
            $artist_type = $role['role_name'];
        } else {
            $artist_type = '';
        }
        return $artist_type;
    }
}