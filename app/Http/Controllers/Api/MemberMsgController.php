<?php

namespace App\Http\Controllers\Api;

use App\Models\AppBroadcasts;
use App\Models\AppointmentList;
use App\Models\AppointmentSign;
use App\Models\ArticleComment;
use App\Models\ChatMsg;
use App\Models\UserIndex;
use App\Providers\ApiServiceProvider;
use Illuminate\Support\Facades\Redis;
use Validator;
use App\Logic\MemberMsg;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MemberMsgController extends Controller
{
    public $total;
    public $current_page = 1;
    public $per_page = 10;
    public $last_page;
    public $next_page_url = null;
    public $prev_page_url = null;
    public $from;
    public $to;

    public function __construct(Request $request)
    {
        $this->current_page = (int)$request->get('page', 1);
        $this->middleware('verify.user.login', ['only' => [
            'messageNotification',
        ]]);
    }

    public function system()
    {
        $model = new AppBroadcasts();
        $field = ['id', 'title', 'subtitle', 'description', 'goto_type', 'param', 'updated_at', 'created_at',];
        $list = $model->getList([], $field);
        return response()->success($list);
    }

    public function orders(Request $request)
    {
        $MemberMsgLogic = new MemberMsg($request->api_token);
        $param = ['curpage' => $this->current_page];
        $result = $MemberMsgLogic->message_list($param);
        if ($result->code == 200) {
            $MemberMsgLogic->setReadState();
            $data_list = $result->datas->message_list;
            $total = $result->page_total ?? 0;
            $this->from = $this->current_page * $this->per_page - $this->per_page;
            $this->to = count($data_list) + $this->from++;
            $data = array(
                'total' => $total,
                'per_page' => $this->per_page,
                'current_page' => $this->current_page,
                'last_page' => ceil($total / $this->per_page),
                'next_page_url' => $this->next_page_url,
                'prev_page_url' => $this->prev_page_url,
                'from' => $this->from,
                'to' => $this->to,
            );

            if ((int)$this->current_page > (int)$result->page_total) {
                $data['from'] = null;
                $data['to'] = null;
                $data['data'] = [];
                return response()->success($data);
            }
            $data['data'] = [];
            $data_list = json_decode(json_encode($data_list), true);
            foreach ($data_list as $key => $value) {
                $pattern = '/<a(.*)<\/a>/i';
                $replace = '';
                $message_body = preg_replace($pattern, $replace, $value['message_body']);
                $data['data'][$key]['message_id'] = $value['message_id'];
                $data['data'][$key]['message_time'] = $value['message_time'];
                $data['data'][$key]['message_title'] = $value['message_title'];
                $data['data'][$key]['message_body'] = $message_body;
                $data['data'][$key]['message_open'] = $value['message_open'];
            }
            return response()->success($data);
        }
        return response()->fail(503005);
    }

    public function show(Request $request)
    {
        $message_id = $request->get('message_id', 0);
        if (empty($message_id)) {
            return response()->fail(200001);
        }
        $MemberMsgLogic = new MemberMsg($request->api_token);
        $result = $MemberMsgLogic->message_detail($request->message_id);
        if (isset($result->code) && $result->code == 200) {
            return response()->success($result->datas);
        } else {
            return response()->fail(200005);
        }
    }

    /**
     * 消息删除操作
     * */
    public function destroy(Request $request)
    {
        $message_id = $request->get('message_id', 0);
        if (empty($message_id)) {
            return response()->fail(200001);
        }
        $MemberMsgLogic = new MemberMsg($request->api_token);
        $result = $MemberMsgLogic->message_delete($request->message_id);
        if (isset($result->code) && $result->code == 200) {
            return response()->success();
        } else {
            return response()->fail(200005);
        }
    }


    /**
     * 未读消息统计
     *
     * */
    public function unread(Request $request)
    {
        $orders_sum = 0;
        $system_sum = 0;
        $appointment_sum = 0;
        $chat_sum = 0;
        $article_comment_sum = 0;
        $article_be_comment_sum = 0;
        $user_id = $request->user['user_id'];

        // 订单信息统计
        $MemberMsgLogic = new MemberMsg($request->api_token);
        $result = $MemberMsgLogic->get_msg_count();
        if (isset($result->code) && $result->code == 200) {
            $orders_sum = (int)$result->datas;
        }

        // 评论消息统计
        $condition = array(
            'accepter_id' => $user_id,
            'state' => 0,
            'is_delete' => 0,
            'is_examine' => 1,
        );
        // 查询回复评论
        $article_be_comment_sum = ArticleComment::where($condition)->where(function ($query) {
            $query->where('is_show', 1)->orWhere('is_show', 3);
        })->count();

        // 约展人数统计
        $list = AppointmentList::where('user_id', $user_id)->lists('id');
        if (!empty($list)) {
            $list = $list->toArray();
            $appointment_sum = AppointmentSign::where('is_read', 0)->whereIn('appointment_id', $list)->count();
        }

        // 对话消息统计
        $condition = array(
            'to_id' => $user_id,
            'read_state' => 2,
        );
        if (!empty($request->get('from_id', null))) {
            $condition['from_id'] = $request->from_id;
        }
//        $chat_sum = ChatMsg::where($condition)->count();

        $count = array(
            'orders' => $orders_sum,
            'system' => $system_sum,
            'appointment' => $appointment_sum,
            'chat' => $chat_sum,
            'article_comment' => 0,
            'article_be_comment' => $article_be_comment_sum,
            'count' => ($orders_sum + $system_sum + $chat_sum + $article_be_comment_sum),
        );
        return response()->success($count);
    }


    /**
     * 个人中心 消息 系统通知
     * @param Request $request
     * @return mixed
     */
    public function messageNotification(Request $request)
    {
        $user_id = $request->user['user_id'];
        //更新访问系统推送消息时间
        $time_now = date('Y-m-d H:i:s');
        $update = ['read_time' => $time_now];
        $where = ['user_id' => $user_id];
        $read_time = $request->user['read_time'];
        $model = new AppBroadcasts();
        $field = ['id', 'title', 'subtitle', 'description', 'goto_type', 'param', 'updated_at', 'created_at',];
        $list = $model->getList([], $field);
        if (empty($list) || !isset($list['data'])) {
            return response()->success([]);
        }
        if (!empty($where)) {
            $model = new UserIndex();
            $model->where($where)->update($update);
            $redis = new Redis();
            //  修改用户信息之后这里要删除用户保存的缓存信息防止数据读取错误
            if ($redis::hexists($model::CACHE_KEY . $user_id, $model::USER_INFO)) {
                $user_info = json_decode($redis::hget($model::CACHE_KEY . $user_id, $model::USER_INFO), true);
                $user_info['read_time'] = $time_now;
                $redis::hset($model::CACHE_KEY . $user_id, $model::USER_INFO, json_encode($user_info));
            }
            //$redis::del($model::CACHE_KEY . $user_id, $model::USER_INFO);
            $model->cacheDel($user_id);
        }
        foreach ($list['data'] as &$value) {
            if ($value['created_at'] > $read_time) {
                $value['is_read'] = 0;
            } else {
                $value['is_read'] = 1;
            }
        }
        return response()->success($list);
    }
}