<?php

namespace App\Http\Controllers\Api;

use App\Entity\Constant\ApiResponseCode;
use App\Exceptions\ApiResponseException;
use App\Lib\Helpers\LogHelper;
use App\Logic\Member;
use App\Models\Address;
use App\Models\ArticleLike;
use App\Models\ArticleTitle;
use App\Models\Artwork;
use App\Models\ArtworkLike;
use App\Models\UserFollow;
use App\Models\UserIndex;
use App\Models\ArtistPersonal;
use App\Models\ArtistOrganization;
use App\Models\Users;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Libs\phpQuery\OSS;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Mockery\Exception;
use Swagger\Annotations as SWG;
use Validator;

class MemberController extends Controller
{
    private $authentication = 0;
    private $role_id = 0;

    public function __construct(Request $request)
    {
        $this->middleware('verify.user.login', [
            'except' => [
                'index', 'memberFollowList', 'memberFollowNum', 'memberDetail',
                'receiverAddressCity', 'homepage'
            ]
        ]);
        $authentication_type = $request->user['authentication_type'];
        $authentication_id = $request->user['authentication_id'];
        if ($authentication_type == 1) {
            $result = ArtistPersonal::where('id', $authentication_id)->first();
            $this->role_id = $result['role_id'];
            $this->authentication = 1;
        }
        if ($authentication_type == 2) {
            $result = ArtistOrganization::where('id', $authentication_id)->first();
            $this->role_id = $result['role_id'];
            $this->authentication = 2;
        }
    }

    /**
     * 个人中心详情信息
     * */
    public function index(Request $request)
    {
        if (!$request->get('api_token', false)) {
            return response()->success();
        }
        $bankcardLogic = new Member($request->api_token);
        $result = $bankcardLogic->member_index();
        if ($result->code == 200) {
            $artist_type = $this->getRoleName();
            $data = $result->datas->member_info;
            $invite_code = $result->datas->top_member;
            $user_id = $request->user['user_id'] ?: '';
            $user_nickname = $request->user['user_nickname'] ?: tellHide($request->user['user_name']);
            $count_price = sprintf("%.2f", $data->available_predeposit);
            $available_predeposit = sprintf("%.2f", $data->available_predeposit);
            $freeze_margin = sprintf("%.2f", $data->freeze_margin);
            $available_commis = sprintf("%.2f", $data->available_commis);
            $user_info = [
                'authentication_type' => $this->authentication,         // 认证状态
                'artist_type' => $artist_type ?: '申请认证',             // 角色名称
                'user_nickname' => $user_nickname,                      // 昵称
                'user_avatar' => getMemberAvatarByID($user_id) . '?' . time(),  // 头像
                'type_logo' => $data->type_logo,                        // 成员等级logo
                'level' => $data->level,                                // 会员等级
                'level_name' => $data->level_name,                      // 会员等级名称
                'member_type_name' => $data->member_type_name,          // 会员类型名称
                'order_nopay_count' => $data->order_nopay_count,        // 待支付订单数量
                'order_noreceipt_count' => $data->order_noreceipt_count,// 待收货订单数量
                'order_notakes_count' => $data->order_notakes_count,    // 待自提订单数量
                'order_noeval_count' => $data->order_noeval_count,      // 待评价订单数量
                'count_price' => $count_price,                          // 总资产
                'available_predeposit' => $available_predeposit,        // 预存款余额
                'freeze_margin' => $freeze_margin,                      // 冻结保证金金额
                'point' => (string)($data->member_points / 100),        // 诺币
                'count_partner' => $data->count_partner,                // 一二级合伙人总数
                'available_commis' => $available_commis,                // 奖励金总额
                'rpt_num' => $data->rpt_num,                            // 红包数量
                'invite_code' => $invite_code->dis_code,                // 邀请码
            ];
            return response()->success($user_info);
        }
        return response()->fail(503005);
    }

    public function edit(Request $request)
    {
        $dara = array(
            'authentication' => $this->authentication,
            'user_nickname' => $request->user['user_nickname'] ?: tellHide($request->user['user_name']),
            'user_avatar' => getMemberAvatarByID($request->user['user_id']) . '?' . time(),
            'user_flag' => $request->user['user_flag'],
            'user_sex' => $request->user['user_sex'],
            'user_real_name' => $request->user['user_real_name'],
            'user_birthday' => $request->user['user_birthday'],
            'user_areainfo' => $request->user['user_areainfo']                     // 会员地区
        );
        if (empty($dara['user_areainfo']) && !empty($request->user['user_mobile'])){
            //去查shopNcMember表
            $memberInfo = application()->memberApi->getShopNcMemberInfoByPhone($request->user['user_mobile'], ["member_areainfo"]);
            if($memberInfo){
                $dara['user_areainfo'] = $memberInfo->member_areainfo;
            }
        }
        return response()->success($dara);
    }


    /**
     * 约展个人信息
     * */
    public function show(Request $request)
    {
        $model = new UserIndex();
        //$condition = ['user_id' => $request->user_id];
        $user_info = $model->getUserInfoById($request->user_id);
        if (empty($user_info)) {
            return response()->success([]);
        }
        $data = [
            'user_nickname' => $user_info['user_nickname'] ?: '',                   // 昵称
            'user_avatar' => getMemberAvatarByID($user_info['user_id']) . '?' . time(),      // 昵称
            'user_areainfo' => $user_info['user_areainfo'] ?: '',             // 地址
            'user_flag' => $user_info['user_flag'],                           // 个性简介，全部显示
            'user_age' => birthday($user_info['user_birthday']) ?: '',        // 年龄
            'artist_type' => $this->getRoleName(),                          // 角色名称
        ];

        return response()->success($data);
    }

    private function getRoleName()
    {
        if ($this->role_id) {
            $role = DB::table('artist_role')->where('role_id', $this->role_id)->first();
            $artist_type = $role['role_name'];
        } else {
            $artist_type = '';
        }
        return $artist_type;
    }

    /**
     * 修改用户信息
     *
     * @param $request ->nickname string 用户昵称
     * @param $request ->flag string 用户简介
     * */
    public function store(Request $request)
    {
        $user_id = $request->user['user_id'];
        $update = [];
        isset($request->flag) and $update['user_flag'] = $request->flag;
        isset($request->nickname) and $update['user_nickname'] = $request->nickname;
        isset($request->real_name) and $update['user_real_name'] = $request->real_name;
        isset($request->birthday) and $update['user_birthday'] = $request->birthday;
        isset($request->sex) and $update['user_sex'] = $request->sex;
        if (empty($update)) {
            return response()->success();
        }

        DB::beginTransaction();
        $user_index = new UserIndex();
        $user_info = $user_index->find($user_id);
        try {
            foreach ($update as $key => $value) {
                $user_info->$key = $value;
            }
            $result = $user_info->save();
            if (empty($result)) {
                throw new Exception('user_index 保存失败');
            }
            if ($user_info->db) {
                DB::connection($user_info->db)->beginTransaction();
                $users = new Users();
                $users->setConnection($user_info->db);
                $user = $users->where('user_id', $user_id)->first();
                foreach ($update as $key => $value) {
                    $user->$key = $value;
                }
                $result = $user->save();
                if (empty($result)) {
                    throw new Exception('users 保存失败，connection->' . $user_info->db);
                }
            }
            if ($request->get('flag', '') || $request->get('nickname', '')) {
                $param = $request->all();
                $param['key'] = $user_info['user_key'];
                $result = $user_index->updateUCenter($param);
                if (!(isset($result->status_code) && $result->status_code == 200)) {
                    throw new Exception('ucenter 保存失败');
                }
            }
            DB::commit();
            if ($user_info->db) {
                DB::connection($user_info->db)->commit();
            }
            $user_index->cacheDel($user_id);
        } catch (Exception $e) {
            DB::rollback();
            if ($user_info->db) {
                DB::connection($user_info->db)->rollback();
            }
            Log::error('修改个人信息错误详情：' . $e->getMessage());
            return response()->fail(300020);
        }
        return response()->success();
    }

    // 修改头像
    public function avatar(Request $request)
    {
        $user_id = $request->user['user_id'];
        $pic = $request->file('avatar');
        //图片验证
        if (empty($pic)) {
            return response()->fail(200001);
        }
        if (!$pic->isValid()) {
            return response()->fail(503001);
        }
        try{
            $files = $pic->getRealPath();
        }catch (\Exception $e){
            \Log::error("avatar_upload_error", [LogHelper::getEInfo($e), $pic]);
            ApiResponseException::throwException(ApiResponseCode::OPERATE_FAIL, "头像没有上传成功");
        }
        $update = [];
        if ($files) {
            //头像上传
            $oss = new OSS();
            //目标文件名
            $object = $oss->get_object_name($user_id);
            $user_avatar = $oss->upload($files, $object);
            if ($user_avatar == false) {
                return response()->fail(300016);
            }
            $update['avatar'] = $user_avatar;
            $update['key'] = $request->user['user_key'];
        }
        if ($update) {
            $user_index = new UserIndex();
            $user_index->where(['user_id' => $user_id])->update(['user_avatar' => $user_avatar]);
            $user_index->cacheDel($user_id);
            $result = $user_index->updateUCenter($update);
            if ($result && isset($result->status_code)) {
                if ($result->status_code == 200) {
                    return response()->success();
                } else {
                    return response()->fail($result->status_code);
                }
            }
        }
        return response()->success();
    }

    /**
     * 点赞列表
     * @param type 1:文章 2:作品
     * @param Request $request
     * @return mixed
     */
    public function memberLikeList(Request $request)
    {
        $user_id = $request->user['user_id'];
        $type = $request->type;
        $db_name = $request->user['db'];
        if (empty($type) || empty($user_id) || empty($db_name)) {
            return response()->fail(200001);
        }
        switch ($type) {
            case 1://文章
                $article_likeModel = new ArticleLike();
                $article_likeModel->setConnection($db_name);
                $list = $article_likeModel->getList(['user_id' => $user_id, 'is_delete' => 0]);
                if (!empty($list['data'])) {
                    foreach ($list['data'] as &$v) {
                        $article_info = (new ArticleTitle())->getInfoById($v['article_id']);
                        if (!empty($article_info)) {
                            $v['author_id'] = $article_info['author_id'];
                            $v['title'] = $article_info['title'];
                            $picture = !empty($article_info['picture']) ?
                                getPicturePath(json_decode($article_info['picture'])) : '';
                            $v['picture'] = isset($picture[0]) ? $picture[0] : '';
                            $v['author_name'] = $article_info['author_name'];
                            $v['like_num'] = $article_info['like_num'];
                            $v['click_num'] = $article_info['click_num'];
                            $v['comment_num'] = $article_info['comment_num'];
                            unset($v['is_delete']);
                        }
                    }
                }
                break;
            case 2://作品
                $artwork_likeModel = new ArtworkLike();
                $artwork_likeModel->setConnection($db_name);
                $list = $artwork_likeModel->getList(['user_id' => $user_id, 'is_delete' => 0]);
                if (!empty($list['data'])) {
                    foreach ($list['data'] as $k => &$v) {
                        $artwork_info = (new Artwork())->getInfoById($v['artwork_id']);
                        if (!empty($artwork_info)) {
                            $tmp_user_info = (new UserIndex())->getUserInfoById($artwork_info['user_id']);
                            if (empty($tmp_user_info)) {
                                unset($list['data'][$k]);
                                continue;
                            }
                            $v['author_id'] = $artwork_info['user_id'];
                            $v['title'] = $artwork_info['artwork_name'];
                            $v['picture'] = !empty($artwork_info['artwork_img']) ?
                                getPicturePath($artwork_info['artwork_img']) : '';
                            $v['author_name'] = $tmp_user_info['user_nickname'];
                            $v['like_num'] = $artwork_info['like_num'];
                            $v['click_num'] = $artwork_info['click_num'];
                            $v['comment_num'] = $artwork_info['comment_num'];
                            unset($v['is_delete']);
                        }
                    }
                    $list['data'] = array_values($list['data']);
                }
                break;
            default :
                $artwork_likeModel = new ArtworkLike();
                $artwork_likeModel->setConnection($db_name);
                $list = $artwork_likeModel->getList(['user_id' => $user_id, 'is_delete' => 0]);
                if (!empty($list['data'])) {
                    foreach ($list['data'] as $k => &$v) {
                        $artwork_info = (new Artwork())->getInfoById($v['artwork_id']);
                        if (!empty($artwork_info)) {
                            $tmp_user_info = (new UserIndex())->getUserInfoById($artwork_info['user_id']);
                            if (empty($tmp_user_info)) {
                                unset($list['data'][$k]);
                                continue;
                            }
                            $v['author_id'] = $artwork_info['user_id'];
                            $v['title'] = $artwork_info['artwork_name'];
                            $v['picture'] = !empty($artwork_info['artwork_img']) ?
                                getPicturePath($artwork_info['artwork_img']) : '';
                            $v['author_name'] = $tmp_user_info['user_nickname'];
                            $v['like_num'] = $artwork_info['like_num'];
                            $v['click_num'] = $artwork_info['click_num'];
                            $v['comment_num'] = $artwork_info['comment_num'];
                            unset($v['is_delete']);
                        }
                    }
                    $list['data'] = array_values($list['data']);
                }
                break;
        }
        return response()->success($list);
    }

    /**
     * 用户关注/粉丝列表
     * @param $user_id 查询用户id
     * @param $type 列表类型 1:关注列表 2:粉丝列表
     * @param Request $request
     */
    public function memberFollowList(Request $request)
    {
        $user_id = $request->user_id;
        $type = $request->type;
        if (empty($user_id) || empty($type)) {
            return response()->fail(200001);
        }
        $api_token = $request->api_token;
        if (!empty($api_token)) {
            $user_index = new UserIndex();
            $login_info = $user_index->getUserInfoByToken($api_token);
            if (!is_array($login_info)) {
                return response()->fail($login_info);
            }
            $u_id = $login_info['user_id'];
            if ($user_id == $u_id) {
                $update = ['fans_num' => 0];
                $where = ['user_id' => $u_id];
                $user_index->where($where)->update($update);
                $user_index->cacheDel($u_id);
            }
        }
        $user_followModel = new UserFollow();
        $userModel = new UserIndex();
        switch ($type) {
            case 1://关注列表
                $list = $user_followModel->getList(['user_id' => $user_id, 'is_delete' => 0]);
                if (!empty($list['data'])) {
                    foreach ($list['data'] as $k => &$v) {
                        if (isset($u_id) && $u_id) {
                            if ($u_id == $v['follow_user_id']) {
                                $v['is_follow'] = 2;//用户自己
                            } else {
                                $tmp = $user_followModel->getInfo([
                                    'user_id' => $u_id,
                                    'follow_user_id' => $v['follow_user_id'],
                                    'is_delete' => 0
                                ]);
                                $v['is_follow'] = $tmp ? 1 : 0;
                            }
                        } else {
                            $v['is_follow'] = 0;
                        }
                        $v['user_id'] = $v['follow_user_id'];
                        $tmp_user_info = $userModel->getUserInfoById($v['user_id']);
                        if (!empty($tmp_user_info)) {
                            $v['user_nickname'] = $tmp_user_info['user_nickname'] ?: '';
                            $v['user_avatar'] = $tmp_user_info['user_avatar'] ?:
                                getMemberAvatarByID($tmp_user_info['user_id']);
                            $v['user_flag'] = $tmp_user_info['user_flag'] ?: '';
                        } else {
                            $v['user_nickname'] = '';
                            $v['user_avatar'] = '';
                            $v['user_flag'] = '';
                        }
                        unset($list['data'][$k]['id'], $list['data'][$k]['follow_user_id']);
                        unset($list['data'][$k]['updated_at'], $list['data'][$k]['created_at']);
                        unset($list['data'][$k]['is_delete']);
                    }
                    unset($v);
                }
                break;
            case 2://粉丝列表
                $list = $user_followModel->getList(['follow_user_id' => $user_id, 'is_delete' => 0]);
                if (!empty($list['data'])) {
                    foreach ($list['data'] as $k => &$v) {
                        if (isset($u_id) && $u_id) {
                            if ($u_id == $user_id) {
                                $model = new UserIndex();
                                $model->where(['user_id' => $user_id])->update(['fans_num' => 0]);
                                $model->cacheDel($user_id);
                            }
                            if ($u_id == $v['user_id']) {
                                $v['is_follow'] = 2;//用户自己
                            } else {
                                $tmp = $user_followModel->getInfo([
                                    'follow_user_id' => $v['user_id'],
                                    'user_id' => $u_id,
                                    'is_delete' => 0
                                ]);
                                $v['is_follow'] = $tmp ? 1 : 0;
                            }
                        } else {
                            $v['is_follow'] = 0;
                        }
                        $tmp_user_info = $userModel->getUserInfoById($v['user_id']);
                        if (!empty($tmp_user_info)) {
                            $v['user_nickname'] = $tmp_user_info['user_nickname'] ?: '';
                            $v['user_avatar'] = $tmp_user_info['user_avatar'] ?:
                                getMemberAvatarByID($tmp_user_info['user_id']);
                            $v['user_flag'] = $tmp_user_info['user_flag'] ?: '';
                        } else {
                            $v['user_nickname'] = '';
                            $v['user_avatar'] = '';
                            $v['user_flag'] = '';
                        }
                        unset($list['data'][$k]['id']);
                        unset($list['data'][$k]['follow_user_id'], $list['data'][$k]['updated_at']);
                        unset($list['data'][$k]['created_at'], $list['data'][$k]['is_delete']);
                    }
                    unset($v);
                }
                break;
            default:
                $list = $user_followModel->getList(['user_id' => $user_id, 'is_delete' => 0]);
                if (!empty($list['data'])) {
                    foreach ($list['data'] as $k => &$v) {
                        if (isset($u_id) && $u_id) {
                            if ($u_id == $v['follow_user_id']) {
                                $v['is_follow'] = 2;//用户自己
                            } else {
                                $tmp = $user_followModel->getInfo([
                                    'user_id' => $u_id,
                                    'follow_user_id' => $v['follow_user_id'],
                                    'is_delete' => 0
                                ]);
                                $v['is_follow'] = $tmp ? 1 : 0;
                            }
                        } else {
                            $v['is_follow'] = 0;
                        }
                        $v['user_id'] = $v['follow_user_id'];
                        $tmp_user_info = $userModel->getUserInfoById($v['user_id']);
                        if (!empty($tmp_user_info)) {
                            $v['user_nickname'] = $tmp_user_info['user_nickname'] ?: '';
                            $v['user_avatar'] = $tmp_user_info['user_avatar'] ?:
                                getMemberAvatarByID($tmp_user_info['user_id']);
                            $v['user_flag'] = $tmp_user_info['user_flag'] ?: '';
                        } else {
                            $v['user_nickname'] = '';
                            $v['user_avatar'] = '';
                            $v['user_flag'] = '';
                        }
                        unset($list['data'][$k]['id'], $list['data'][$k]['follow_user_id']);
                        unset($list['data'][$k]['updated_at'], $list['data'][$k]['created_at']);
                        unset($list['data'][$k]['is_delete']);
                    }
                    unset($v);
                }
                break;
        }

        return response()->success($list);
    }

    public function memberFollowNum(Request $request)
    {
        $type = $request->type;
        $list = array(
            'follow_num' => 12138,
            'fans_num' => 8888888
        );
        return response()->success($list);
    }

    /**
     * 用户主页信息
     * @param Request $request
     * @return mixed
     */
    public function memberDetail(Request $request)
    {
        $user_id = $request->user_id;
        if (empty($user_id)) {
            return response()->fail(200001);
        }
        $api_token = $request->api_token;
        if (!empty($api_token)) {
            $login_info = (new UserIndex())->getUserInfoByToken($api_token);
            if (!is_array($login_info)) {
                return response()->fail($login_info);
            }
        }
        $userModel = new UserIndex();
        $user_followModel = new UserFollow();
        $user_info = $userModel->getUserInfoById($user_id);
        if (!$user_info) {
            return response()->fail(200005);
        }
        $follow_num = $user_followModel->getCount(['user_id' => $user_id, 'is_delete' => 0]);
        $fans_num = $user_followModel->getCount(['follow_user_id' => $user_id, 'is_delete' => 0]);
        if (isset($login_info) && $login_info && $login_info['user_id'] != $user_id) {
            $is_follow = $user_followModel->getInfo([
                'user_id' => $login_info['user_id'],
                'follow_user_id' => $user_id,
                'is_delete' => 0,
            ]) ? 1 : 0;
        } else {
            $is_follow = 0;
        }
        $info = array(
            'user_id' => $user_info['user_id'],
            'user_nickname' => $user_info['user_nickname'],
            'user_avatar' => $user_info['user_avatar'],
            'user_flag' => $user_info['user_flag'],
            'follow_num' => $follow_num,
            'fans_num' => $fans_num,
            'artist_description' => '',
            'authentication_type' => $user_info['authentication_type'],
            'is_follow' => $is_follow
        );
        if ($user_info['authentication_type'] == 1) {
            $artistModel = new ArtistPersonal();
            $artist_info = $artistModel->getArtistInfo($user_info['authentication_id']);
            $info['artist_description'] = $artist_info['description'];
            $user_info['user_avatar'] = $user_info['user_avatar']
                ? getPicturePath($user_info['user_avatar'])
                : getMemberAvatarByID($user_info['user_id']);
            $info['user_avatar'] = getPicturePath($artist_info['logo']) ?: $user_info['user_avatar'];
            $this->role_id = $artist_info['role_id'] ?? '';
            $info['artist_type'] = $this->getRoleName();
        } elseif ($user_info['authentication_type'] == 2) {
            $organizationModel = new ArtistOrganization();
            $organization_info = $organizationModel->getArtistInfo($user_info['authentication_id']);
            $info['artist_description'] = $organization_info['description'];
            $user_info['user_avatar'] = $user_info['user_avatar']
                ? getPicturePath($user_info['user_avatar'])
                : getMemberAvatarByID($user_info['user_id']);
            $info['user_avatar'] = getPicturePath($organization_info['logo']) ?: $user_info['user_avatar'];
            $this->role_id = $organization_info['role_id'] ?? '';
            $info['artist_type'] = $this->getRoleName();
        }
        return response()->success($info);
    }

    /**
     * @SWG\Post(path="/api/member/receiverAddress",
     *   tags={"member"},
     *   operationId="/api/member/receiverAddress",
     *   summary="获取地址列表",
     *   description="获取地址列表",
     *   produces={"application/json"},
     *   @SWG\Response(response="200", description="", @SWG\Schema(ref="#/definitions/ReceiverAddressResponse")),
     * )
     * )
     */
    public function receiverAddress(Request $request)
    {
        $api_token = $request->api_token;
        $bankcardLogic = new Member($api_token);
        $return = [];
        $result = $bankcardLogic->receiverAddress();
        if ($result) {
            if ($result->code == 200) {
                $return = $result->datas->address_list;
            }
        }
        return response()->success($return);
    }

    /**
     * @SWG\Post(path="/api/member/receiverAddressAdd",
     *   tags={"member"},
     *   operationId="api_member_receiverAddressAdd",
     *   summary="添加编辑地址",
     *   description="添加编辑地址",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="true_name",
     *     in="formData",
     *     description="会员姓名",
     *     required=true,
     *     type="string",
     *     default=""
     *   ),
     *   @SWG\Parameter(
     *     name="mob_phone",
     *     in="formData",
     *     description="手机电话",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="city_id",
     *     in="formData",
     *     description="市级ID",
     *     required=true,
     *     type="integer",
     *   ),
     *   @SWG\Parameter(
     *     name="area_id",
     *     in="formData",
     *     description="地区ID",
     *     required=true,
     *     type="integer",
     *   ),
     *   @SWG\Parameter(
     *     name="address",
     *     in="formData",
     *     description="地址",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="area_info",
     *     in="formData",
     *     description="地区内容",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="is_default",
     *     in="formData",
     *     description="1 默认收货地址 ",
     *     required=false,
     *     type="integer",
     *   ),
     *   @SWG\Parameter(
     *     name="address_id",
     *     in="formData",
     *     description="编辑地址填入地址id",
     *     required=false,
     *     type="integer",
     *   ),
     *   @SWG\Response(response="200", description="successful operation"))
     * )
     * )
     */
    /**
     * @param Request $request
     * @return mixed
     */
    public function receiverAddressAdd(Request $request)
    {
        $api_token = $request->api_token;
        $bankcardLogic = new Member($api_token);
        $data['true_name'] = $request->true_name ?? '';
        $data['mob_phone'] = $request->mob_phone ?? '';
        $data['city_id'] = $request->city_id ?? '';
        $data['area_id'] = $request->area_id ?? '';
        $data['address'] = $request->address ?? '';
        $data['area_info'] = $request->area_info ?? '';
        $data['is_default'] = $request->is_default ?? '';
        if (empty($data['true_name']) || empty($data['city_id']) || empty($data['area_info'])) {
            return response()->fail(200001);
        }
        if (empty($data['mob_phone']) || empty($data['area_id']) || empty($data['address'])) {
            return response()->fail(200001);
        }
        $address_id = $request->address_id ?? '';
        if (empty($address_id)) {
            $result = $bankcardLogic->receiverAddressAdd($data);
        } else {
            $data['address_id'] = $address_id;
            $result = $bankcardLogic->receiverAddressEdit($data);
        }
        if ($result) {
            if ($result->code == 200) {
                return response()->success([]);
            }
        }
        return response()->fail(200005);
    }


    public function receiverAddressDel(Request $request)
    {
        $api_token = $request->api_token;
        $address_id = $request->address_id ?? '';
        if (empty($address_id)) {
            return response()->fail(200001);
        }
        $bankcardLogic = new Member($api_token);
        $data['address_id'] = $address_id;
        $result = $bankcardLogic->receiverAddressDel($data);
        if ($result) {
            if ($result->code == 200) {
                return response()->success([]);
            }
        }
        return response()->fail(200005);
    }

    public function receiverAddressCity(Request $request)
    {
        $pid = $request->pid ?: 0;
        $bankcardLogic = new Member();
        $result = $bankcardLogic->receiverAddressCity($pid);
        $return = [];
        if ($result) {
            if ($result->code == 200) {
                $return = $result->datas->area_list;
            }
        }
        return response()->success($return);
    }

    public function homepage(Request $request)
    {
        $user_id = $request->user_id;
        if (empty($user_id) || !is_numeric($user_id)) {
            return response()->fail(200224);
        }
        $api_token = $request->api_token;
        $login_info = [];
        if (!empty($api_token)) {
            $login_info = (new UserIndex())->getUserInfoByToken($api_token);
            if (!is_array($login_info)) {
                return response()->fail($login_info);
            }
        }
        $model = new UserIndex();
        $user_info = $model->getUserInfoById($user_id);
        if (empty($user_info)) {
            return response()->fail(300018);
        }
        $artist_type = $user_info['authentication_type'];
        $user_info['artist_type'] = $artist_type ?? '申请认证';                              // 角色名称
        switch ($user_info['authentication_type']) {
            case 0:
                $return['user_avatar'] = getMemberAvatarByID($user_id) . '?' . date('z', time());                           // 头像
                $return['user_nickname'] = $user_info['user_nickname']
                    ?: tellHide($user_info['user_name']);                                            // 昵称
                $return['user_flag'] = $user_info['user_flag'] ?? '';                               //个性签名
                $return['artist_type'] = '未认证';                              // 角色名称
                $return['share_url'] = env('SHARE_URL', '') . "/personalHomePage?user_id={$user_id}"; //分享地址
                break;
            case 1:
                $model = new ArtistPersonal();
                $artist_info = $model->getArtistInfo($user_info['authentication_id']);
                $this->role_id = $artist_info['role_id'] ?? '';
                $return['artist_type'] = $this->getRoleName();
                $return['user_avatar'] = $artist_info['logo']
                    ? getPicturePath($artist_info['logo']) . '?' . date('z', time())
                    : getMemberAvatarByID($user_id);
                $return['user_nickname'] = $user_info['user_nickname'];
                $return['user_flag'] = $user_info['user_flag'] ?? '';                               //个性签名
                $return['description'] = $artist_info['description'] ? removeTheLabel($artist_info['description']) : '';                         //个人简介
                $return['share_url'] = env('SHARE_URL', '') . "/personalHomePage?user_id={$user_id}"; //分享地址
                $return['shop_url'] = $artist_info['shop_url'] ?: ''; //商店的链接地址
                break;
            case 2:
                $model = new ArtistOrganization();
                $artist_info = $model->getArtistInfo($user_info['authentication_id']);
                $this->role_id = $artist_info['role_id'] ?? '';
                $return['artist_type'] = $this->getRoleName();
                $return['user_avatar'] = $artist_info['logo']
                    ? getPicturePath($artist_info['logo']) . '?' . date('z', time())
                    : getMemberAvatarByID($user_id);
                $return['user_nickname'] = $user_info['user_nickname'];
                $return['user_flag'] = $user_info['user_flag'] ?? '';                               //个性签名
                $return['description'] = $artist_info['description'] ? removeTheLabel($artist_info['description']) : '';                         //个人简介
                $return['phone_num'] = $artist_info['phone_num'] ?: '';
                $address = new Address();
                $province_id = ($address->getInfoById($artist_info['province_id']))['area_name'] ?? '';
                $city_id = ($address->getInfoById($artist_info['city_id']))['area_name'] ?? '';
                $area_id = ($address->getInfoById($artist_info['area_id']))['area_name'] ?? '';
                $return['address'] = $province_id . $city_id . $area_id . $artist_info['address'];
                $return['addr'] = $artist_info['addr'] ?: '31.16768,121.42922';
                $return['share_url'] = env('SHARE_URL', '') . "/institutionalHomePage?user_id={$user_id}"; //分享地址
                break;
            default:
                break;
        }
        $return['user_id'] = (int)$user_id;
        $return['is_follow'] = 0;
        if ($login_info) {
            if ($user_id == $login_info['user_id']) {
                $return['is_follow'] = 2;
            } else {
                $return['is_follow'] = (new UserFollow())->doesLikeJudgeByIds($user_id, $login_info['user_id']) ? 1 : 0;
            }
        }
        $return['authentication_type'] = $user_info['authentication_type'] ?? 0;          // 认证类型
        $return['background_img'] = !empty($user_info['background_img']) ? $user_info['background_img'] . '?' . time() : '';                         // 个人主页背景图
        $follows_num = (new UserFollow())->followsNumGet($user_id) ?: 0;
        $fans_num = (new UserFollow())->fansNumGet($user_id) ?: 0;
        $return['follows_num'] = $follows_num;                                      // 关注数目
        $return['fans_num'] = $fans_num;                                            // 粉丝数目
        return response()->success($return);
    }
    /**
     * @SWG\Post(path="/api/member/feedback",
     *   tags={"member"},
     *   operationId="/api/member/feedback",
     *   summary="用户反馈",
     *   description="用户反馈",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="content",
     *     in="formData",
     *     description="反馈内容",
     *     required=true,
     *     type="string",
     *     default=""
     *   ),
     *   @SWG\Parameter(
     *     name="client",
     *     in="formData",
     *     description="客户端 android | ios",
     *     required=false,
     *     type="string",
     *     default="android"
     *   ),
     *   @SWG\Parameter(
     *     name="module",
     *     in="formData",
     *     description="模块",
     *     required=false,
     *     type="string",
     *     default="app_system"
     *   ),
     *   @SWG\Response(response="200", description="successful operation"))
     * )
     * )
     */
    /**
     * 意见反馈
     * @param Request $request
     * @return bool
     */
    public function feedback(Request $request)
    {
        if(!application()->feedbackApi->feedback($request->user['user_id'], $request->input("content"),
            $request->input("module", "app_system"), $request->input("client", "unknown"))){
            ApiResponseException::throwException(ApiResponseCode::FAIL_ERROR);
        }
        return $this->_json();
    }
}