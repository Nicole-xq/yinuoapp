<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VersionController extends Controller
{
    /**
     * 获取版本号
     *
     * @return array
     */
    public function index(Request $request)
    {
        $data = array();
        $data['version'] = env('VERSION','');
        $data['version_url'] = env('VERSION_URL','');
        $data['ios']['new'] = env('VERSION_IOS_NEW','');
        $data['ios']['force'] = env('VERSION_IOS_FORCE','');
        $data['ios']['auction'] = env('AUCTION_IOS_SWITCH', 0);
        $data['android']['new'] = env('VERSION_ANDROID_NEW','');
        $data['android']['force'] = env('VERSION_ANDROID_FORCE','');
        $data['android']['auction'] = env('AUCTION_ANDROID_SWITCH', 0);
        //todo 这里做个特殊处理 关于拍卖开关的 以后审核了拍卖资质后取消
        if($data['ios']['auction'] == 1){
            $appVer = $request->header("X-App-Ver");
            $userAgent = $request->header("User-Agent");
            if($appVer && $userAgent){
                if(stripos($userAgent, "Android") === false){
                    //ios 这里比较版本,如果版本大于当前版本(ios审核版本),隐藏拍卖
                    if(version_compare($appVer, $data['ios']['new']) == 1){
                        $data['ios']['auction'] = 0;
                    }
                }

            }
        }

        return response()->success($data);
    }
}
