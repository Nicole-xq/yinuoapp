<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ArticleClassify;
use App\Models\ArticleComment;
use App\Models\ArticleCommentLike;
use App\Models\ArticleLike;
use App\Models\ArticleTitle;
use App\Models\ArticleVote;
use App\Models\UserIndex;
use Illuminate\Http\Request;

class ArticleTitleController extends Controller
{
    // 新闻ID
    const NEWS = 1;
    // 艺评 art evaluation
    const ART_E = 2;
    // 获取热评
    private $is_comment = true;
    private $is_vote = false;
    private $user = null;

    public function __construct(Request $request)
    {
        if (!empty($request->user) && isset($request->user['user_id'])) {
            $this->user = $request->user;
        }
    }

    /**
     * 首页模块
     */

    /**
     * 首页推荐列表
     *
     * @return mixed
     */
    public function indexRecommend()
    {
        $field = ['id', 'classify_id', 'author_id', 'author_name',
            'title', 'picture', 'description', 'like_num', 'click_num', 'is_top',];
        $condition = ['is_recommend' => 1];
        $news = (new ArticleTitle())->indexRecommend($condition, self::NEWS, $field);
        $news['data'] = $this->_dataFactory($news);

        return response()->success($news);
    }

    /**
     * 首页艺评列表
     *
     * @return array
     */
    public function indexArtEvaluation()
    {
        $this->is_vote = true;
        $model_article_title = new ArticleTitle();

        $model_article_title->is_page = false;

        $field = ['id', 'classify_id', 'author_id', 'author_name', 'title', 'picture', 'description',
            'click_num', 'like_num', 'vote_agree_num', 'vote_disagree_num', 'is_top', 'created_at'];
        $condition = ['is_recommend' => 1];

        $result = $model_article_title->articleList($condition, self::ART_E, $field);
        $result = $this->_dataFactory($result);

        return response()->success($result);
    }


    /**
     * 新闻资讯模块
     */

    /**
     * 新闻资讯导航
     *
     * @return mixed
     */
    public function newsNav()
    {
        $nav = (new ArticleClassify())->getList(['parent_id' => self::NEWS]);

        $path = 'api/news.list/';
        $data = [['nav_name' => '推荐', 'url' => $path]];
        foreach ($nav as $item) {
            $data[] = [
                'nav_name' => $item->type_name,
                'url' => $path . $item->id,
            ];
        }

        return response()->success($data);
    }

    /**
     * 新闻资讯列表
     *
     * @return mixed
     */
    public function index(Request $request, $nav_id = self::NEWS)
    {
        // 默认查询所有推荐
        if ($nav_id == self::NEWS) {
            $condition = ['is_recommend' => 1];
        } else {
            $condition = [];
        }

        if (!empty($request->author_id)) {
            $condition['author_id'] = $request->author_id;
        }
        $news = (new ArticleTitle)->articleList($condition, $nav_id);
        $news['data'] = $this->_dataFactory($news);

        return response()->success($news);
    }

    /**
     * 详情底部推荐
     * @param int $classify_id
     * @return mixed
     */
    public function infoRecommend(Request $request)
    {
        $model_article_title = new ArticleTitle();
        $this->is_comment = false;                // 不要评论
        $class_id = [$request->classify_id];
        $condition = [];
        if (!empty($request->article_id)) {
            $condition['id'] = ['<>', $request->article_id];
            $article_title = $model_article_title->where('id', $request->article_id)->first();
            if (empty($article_title)) {
                return response()->fail(200004);
            }
            $class_id = [$article_title->classify_id];
        }
        $model_article_title->perPage(3);               // 每页3条数据
        $field = ['id', 'title', 'description', 'picture', 'author_id', 'author_name', 'is_top', 'created_at',];
        $news = $model_article_title->articleList($condition, $class_id, $field);
        $news['data'] = $this->_dataFactory($news);

        return response()->success($news);
    }

    /**
     * 艺评模块
     */

    /**
     *  艺评列表
     *
     * @return mixed
     */
    public function artEvaluation(Request $request, $state = 'recommend')
    {
        $this->is_vote = true;
        $model_article_title = new ArticleTitle();

        $field = ['id', 'classify_id', 'author_id', 'author_name', 'title', 'picture', 'description',
            'click_num', 'like_num', 'vote_agree_num', 'vote_disagree_num', 'is_top', 'created_at'];
        $condition = [];
        $order = '';
        switch ($state) {
            case 'recommend' :
                $condition = ['is_recommend' => 1];
                $order = 'created_at desc';
                break;
            case 'hot' :
                $order = 'click_num desc';
                break;
            case 'now' :
                $order = 'created_at desc';
                break;
        }
        if (isset($request->author_id)) {
            if (empty($request->author_id)) {
                return response()->success([]);
            }
            $condition = ['author_id' => $request->author_id];
        }

        $result = $model_article_title->articleList($condition, [self::ART_E], $field, $order);
        $result['data'] = $this->_dataFactory($result);

        return response()->success($result);
    }

    private function _dataFactory($result)
    {
        if (count($result) == 0) {
            return $result;
        }
        if (is_object($result)) {
            $result = $result->toArray();
        }
        if (isset($result['data'])) {
            $result = $result['data'];
        }
        if ($this->user) {
            //$data = [];
            //if (is_object($result)) {
            //  $data = $result->toArray();
            //}
            //if (isset($data['data'])) {
              //  $data = $data['data'];
            //}

            $article_ids = array_column($result, 'id');
            $article_like = (new ArticleLike())->setConnection($this->user['db']);
            $like_list = $article_like->where('user_id', $this->user['user_id'])
                ->whereIn('article_id', $article_ids)
                ->lists('is_delete', 'article_id')
                ->toArray();
            if ($this->is_vote) {
                $article_vote = new ArticleVote();
                $article_vote->setConnection($this->user['db']);
                $vote_list = $article_vote->where('user_id', $this->user['user_id'])
                    ->whereIn('article_id', $article_ids)
                    ->lists('is_vote', 'article_id')
                    ->toArray();
            }
        }
        // 获取头像
        $author_ids = array_column($result, 'author_id');
        $author_ids = array_unique($author_ids);
        $avatar = getMemberAvatarByID($author_ids);

        $article_comment = new ArticleComment();
        foreach ($result as &$item) {
            $is_like = 0;
            if (isset($like_list)) {
                $is_like = !isset($like_list[$item['id']]) || $like_list[$item['id']] == 1 ? 0 : 1;
            }
            $user_index = new UserIndex();
            $author_info = $user_index->getUserInfoById($item['author_id']);
            $nike_name = $author_info['user_nickname'] ?? '';
            $item['author_nickname'] = $nike_name ?? '已被删除';
            $item['author_avatar'] = $avatar[$item['author_id']] . '?' . time();
            $item['is_like'] = $is_like;

            if ($this->is_vote) {
                $item['is_vote'] = $vote_list[$item['id']] ?? 0;
            }

            $array = [];
            if (!empty($item['picture'])) {
                $picture = json_decode($item['picture'], true);
                foreach ($picture as $k => $v) {
                    $end = empty($item['is_top']) ? 3 : 1;
                    if ($k == $end) {
                        break;
                    }
                    $array[] = getPicturePath($v);
                }
            }
            $item['picture'] = $array;
            // 是否追加热评
            if ($this->is_comment) {
                $condition = [
                    'parent_id' => 0,
                    'article_id' => $item['id'],
                ];
                $field = ['id', 'commentator_id', 'commentator_nickname', 'msg', 'like_num'];
                $order = 'like_num desc ,id desc';

                $comment = $article_comment->getInfo($condition, $field, $order);
                if (empty($comment->commentator_nickname) && !empty($comment->commentator_id)) {
                    $commentator_info = UserIndex::find($comment->commentator_id);
                    if ($commentator_info) {
                        $comment->commentator_nickname = isset($commentator_info->user_name)
                            ? tellHide($commentator_info->user_name)
                            : '';
                    } else {
                        $comment->commentator_nickname = '已注销';
                    }
                }

                $item['comment'] = $comment;
                if (!empty($item['comment'])) {
                    $item['comment']['is_like'] = 0;
                    if ($this->user) {
                        $article_comment_like = new ArticleCommentLike();
                        $condition = array(
                            'article_comment_id' => $item['comment']['id'],
                            'user_id' => $this->user['user_id']
                        );
                        $user_like_info = $article_comment_like->setConnection($this->user['db'])->getInfo($condition);
                        if (empty($user_like_info) || $user_like_info['is_delete'] == 1) {
                            $item['comment']['is_like'] = 0;
                        } else {
                            $item['comment']['is_like'] = 1;
                        }
                    }
                }
            }
        }
        return $result;
    }

}
