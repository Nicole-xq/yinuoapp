<?php

namespace App\Http\Controllers\Api;

use App\Models\Article;
use App\Models\Artwork;
use Illuminate\Http\Request;
use App\Models\UserIndex;
use App\Models\ArticleTitle;
use App\Models\ArticleComment;
use App\Models\ArticleCommentLike;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class ArticleCommentController extends Controller
{

    private $model;
    private $user = null;

    public function __construct(Request $request)
    {
        if (!empty($request->user) && isset($request->user['user_id'])) {
            $this->user = $request->user;
        }
        $this->model = new ArticleComment();
        $this->middleware('verify.user.login', ['only' => [
            'articleComment', 'comments', 'beComments', 'personalCommentsList'
            ]
        ]);
    }

    /**
     * 评论列表
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if ((int)$request->article_id < 1) {
            return response()->fail(200004);
        }

        $condition = [
            'article_id' => $request->article_id,
            'parent_id' => 0,
        ];
        $field = ['id', 'article_id', 'author_id', 'parent_id', 'commentator_id', 'commentator_nickname', 'accepter_id',
            'accepter_nickname', 'msg', 'like_num', 'created_at'];
        $result = $this->model->getList($condition, $field);
        $result = $this->_articleCommentFactory($result);

        return response()->success($result);
    }

    /**
     * 我发起的评论列表
     *
     */
    public function comments()
    {
        $condition = [
            'commentator_id' => $this->user['user_id'],
        ];
        $field = ['id', 'article_id', 'author_id', 'parent_id', 'commentator_id', 'commentator_nickname', 'accepter_id',
            'accepter_nickname', 'msg', 'like_num', 'is_examine', 'created_at'];
        $result = $this->model->getMyComment($condition, $field);
        $result = $this->_commentFactory($result, 'myComments');

        return response()->success($result);
    }

    /**
     * 被评论列表
     *
     **/
    public function beComments()
    {
        $condition = [
            'accepter_id' => $this->user['user_id'],
        ];
        $field = ['id', 'article_id', 'author_id', 'parent_id', 'commentator_id', 'commentator_nickname', 'accepter_id',
            'accepter_nickname', 'msg', 'like_num', 'created_at'];
        $result = $this->model->getBeComment($condition, $field);
        $result = $this->_commentFactory($result, 'beComments');

        ArticleComment::where($condition)->update(['state' => 1]);

        return response()->success($result);
    }

    /**
     * 文章评论 (需登录)
     *
     * @param int $id
     * @param string $msg
     * @param int $comment_id
     * @return mixed
     */
    public function articleComment(Request $request)
    {
        $article_id = $request->id;
        $comment_msg = $request->msg;
        $comment_id = $request->comment_id ?: 0;

        $user_id = $request->user['user_id'];
        $article_title = ArticleTitle::find($article_id);
        $author_id = $article_title->author_id;
        if (empty($article_title)) {
            return response()->fail(200009);
        }

        if (!contentFilter($comment_msg)) {
            return response()->fail(221005);
        }
        $parent_id = 0;
        $accepter_id = $author_id;
        if ($comment_id) {
            $result = ArticleComment::find($comment_id);
            if (empty($result) && $result->parent > 0) { // 不能评论非1级评论
                return response()->fail(200010);
            }
            $parent_id = $comment_id;
            $accepter_id = $result->commentator_id;
        }
        if (!empty($accepter_id)) {
            $accepter_info = (new UserIndex())->getUserInfoById($accepter_id);
        }

        $datetime = date('Y-m-d H:i:s', time());
        $insert = [
            'article_id' => $article_id,
            'author_id' => $author_id,
            'commentator_id' => $request->user['user_id'],
            'commentator_nickname' => $request->user['user_nickname'] ?: tellHide($request->user['user_name']),
            'msg' => $comment_msg,
            'is_examine' => 1,      //现在评论默认审核通过
            'parent_id' => $parent_id,
            'accepter_id' => $accepter_id,
            'accepter_nickname' => $accepter_info['user_nickname'] ?? '',
            'updated_at' => $datetime,
            'created_at' => $datetime,
        ];
        // 以后不需要审核后，会用到此代码
        $db = UserIndex::where('user_id', $article_title->author_id)->lists('db')->toArray();
        $db = $db[0];
        // 评论量自增
        DB::beginTransaction();
        DB::connection($db)->beginTransaction();
        try {
            $article_comment_id = (new ArticleComment())->insertGetId($insert);
            if (empty($article_comment_id)) {
                throw new \Exception('评论添加失败');
            }

            $result1 = DB::table('article_title')->where('id', $article_id)->increment('comment_num', 1);
            $result2 = DB::connection($db)->table('article')
                ->where('article_id', $article_id)
                ->increment('comment_num', 1);

            if (!($result1 && $result2)) {
                throw new \Exception(json_encode(['result1' => $result1, 'result2' => $result2]));
            }

            DB::commit();
            DB::connection($db)->commit();

        } catch (\Exception $e) {
            // 3个库数据回滚
            DB::rollback();
            DB::connection($db)->rollback();
            Log::error('错误详情:添加评论操作---' . $e->getMessage() . '.');
            return response()->fail(200012);
        }

        $insert['like_num'] = 0;
        $insert['id'] = $article_comment_id;
        $insert['commentator_avatar'] = $request->user['user_avatar']
            ? getPicturePath($request->user['user_avatar'])
            : getMemberAvatarByID($user_id);
        if (!empty($accepter_id)) {
            $model = new UserIndex();
            $model->where(['user_id' => $accepter_id])->increment('comment_num', 1);
            $model->cacheDel($accepter_id);
        }
        return response()->success($insert);
    }

    private function _commentFactory($result, $type)
    {
        if (count($result) == 0) {
            return $result;
        }
        switch ($type) {
            case 'myComments':
                // 追加作者头像
                $author_ids = array_unique($result->pluck('author_id')->toArray());
                $author_avatar = getMemberAvatarByID($author_ids);
                break;
            case 'beComments':
                // 追加评论人头像
                $accepter_ids = array_unique($result->pluck('accepter_id')->toArray());
                $accepter_avatar = getMemberAvatarByID($accepter_ids);
                break;
        }
        // 追加文章title
        $article_ids = array_unique($result->pluck('article_id')->toArray());
        $article = (new ArticleTitle())->getByIds($article_ids);
        $article = $article->lists('title', 'id')->toArray();
        // 追加点赞状态
        $article_comment_id = $result->pluck('id')->toArray();
        $article_comment_like = new ArticleCommentLike();
        $article_comment_like->setConnection($this->user['db']);
        $condition = [
            'user_id' => $this->user['user_id'],
            'in' => [['article_comment_id', $article_comment_id]],
        ];
        $like_list = $article_comment_like->getList($condition);
        $like_list = $like_list->lists('is_delete', 'article_comment_id')->toArray();

        // 追加数据
        $result = $result->toArray();
        foreach ($result['data'] as &$item) {
            switch ($type) {
                case 'myComments':
                    // 追加作者头像
                    $item['author_avatar'] = $author_avatar[$item['author_id']] . '?' . time();
                    break;
                case 'beComments':
                    // 追加作者头像
                    $item['accepter_avatar'] = $accepter_avatar[$item['accepter_id']] . '?' . time();
                    break;
            }
            if (isset($article[$item['article_id']])) {
                $article_title = '原文：' . $article[$item['article_id']];
            } else {
                $article_title = '原文被删除！';
            }
            $item['article_title'] = $article_title;
            $is_like = 0;
            if (isset($like_list[$item['id']]) && $like_list[$item['id']] == 0) {
                $is_like = 1;
            }

            $item['is_like'] = $is_like;
            $item['is_reply'] = false;
            if ($item['parent_id'] > 0) {
                $item['is_reply'] = true;
            }
        }
        return $result;
    }

    private function _articleCommentFactory($result)
    {
        if (count($result) == 0) {
            return $result;
        }

        if ($this->user) {
            $article_comment_like = new ArticleCommentLike();
            $article_comment_like->setConnection($this->user['db']);
            $data = [];
            if (is_object($result)) {
                $data = $result->toArray();
            }
            if (isset($data['data'])) {
                $data = $data['data'];
            }
            $article_comment_id = array_column($data, 'id');
            $like_list = $article_comment_like->where('user_id', $this->user['user_id'])
                ->whereIn('article_comment_id', $article_comment_id)
                ->lists('is_delete', 'article_comment_id')
                ->toArray();
        }

        $author_ids = $result->pluck('commentator_id')->toArray();
        $avatar = getMemberAvatarByID($author_ids);

        $field = ['id', 'parent_id', 'commentator_id', 'commentator_nickname',
            'accepter_id', 'accepter_nickname', 'msg', 'created_at'];
        foreach ($result as $item) {
            $is_like = 0;
            if (isset($like_list[$item['id']])) {
                $is_like = $like_list[$item['id']] == 1 ? 0 : 1;
            }
            $item->commentator_avatar = $avatar[$item['commentator_id']] . '?' . time();
            $item->is_like = $is_like;
            $item->child = $this->model->getChildListById($item->id, $field);   // 获取2级评论
        }
        return $result;
    }



    public function personalCommentsList(Request $request)
    {
        $user_info = $request->user;
        $user_id = $user_info['user_id'];
        $CommentsList = (new ArticleComment())->where(['accepter_id' => $user_id])
            //->orWhere(['commentator_id' => $user_id])
            ->where([
                'is_delete' => 0,
                'is_examine' => 1,
            ])
            ->orderBy('created_at', 'desc')
            ->paginate();
        if (isset($CommentsList['data']) && empty($CommentsList['data'])) {
            return response()->success($CommentsList);
        }
        $CommentsList = $CommentsList->toArray();
        $this->model = new ArticleTitle();
        foreach ($CommentsList['data'] as &$value) {
            $article_info = $this->model->getInfoById($value['article_id']);
            $article_img = '';
            if (isset($article_info['picture']) && !empty($article_info['picture'])) {
                $article_img = json_decode($article_info['picture'], true);
            }
            $value['article_img'] = $article_img ? getPicturePath($article_img)[0] : '';
            $value['title'] = $article_info['title'] ?? '';
            unset($value['like_num'], $value['updated_at'], $value['is_delete']);
            unset($value['is_examine'], $value['is_show'], $value['admin_id']);
        }
        $model = new UserIndex();
        $model->where(['user_id' => $user_id])->update(['comment_num' => 0]);
        $model->cacheDel($user_id);
        return response()->success($CommentsList);
    }
}
