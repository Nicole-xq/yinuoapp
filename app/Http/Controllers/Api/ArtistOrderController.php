<?php

namespace App\Http\Controllers\Api;

use App\Entity\Constant\ApiResponseCode;
use App\Entity\Constant\RedisKeyConstant;
use App\Exceptions\ApiResponseException;
use App\Lib\Redis\RedisLock;
use App\Models\Shop\Member;
use App\Models\Shop\OrderLog;
use App\Models\Shop\Orders;
use App\Models\Shop\Store;
use Illuminate\Cache\RateLimiter;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

/**
 * Class ArtistOrderController
 * @package App\Http\Controllers\Api
 * 艺术家订单中心
 */
class ArtistOrderController extends Controller
{
    /**
     * @SWG\Post(path="/api/artistOrder/getOrderList",
     *   tags={"artistOrder"},
     *   operationId="/api/artistOrder/getOrderList",
     *   summary="艺术家后台 获得订单列表",
     *   description="艺术家后台 获得订单列表",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="page",
     *     in="formData",
     *     description="当前页",
     *     required=true,
     *     type="integer",
     *     default="1",
     *   ),
     *   @SWG\Parameter(
     *     name="pageSize",
     *     in="formData",
     *     description="页面大小 默认10",
     *     required=false,
     *     type="integer",
     *     default="",
     *   ),
     *   @SWG\Parameter(
     *     name="status",
     *     in="formData",
     *     description="状态 默认全部 -1.所有 10.待付款 20.待发货 30.待收货 40.已完成 0.已取消",
     *     required=false,
     *     type="integer",
     *     default="",
     *   ),
     *  @SWG\Response(response="200", description="successful operation",@SWG\Schema(ref="#/definitions/MyOrderListResponse"))
     * )
     *
     */
    /**
     * @param Request $request
     * @return mixed
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public function getOrderList(Request $request)
    {
        $rules = [
            'page' => 'required',
            'pageSize' => 'int',
            'status' => 'int',
        ];
        $this->nValidate($request, $rules);

        $page = $request->input("page");
        $pageSize = $request->input("pageSize", 10);
        $status = $request->input("status", -1);
        $artistInfo = application()->artistApi->getArtistPersonalInfoByUserId($request->user['user_id']);
        if(!$artistInfo){
            ApiResponseException::throwException(ApiResponseCode::NOT_PERMISSION, "您还未认证艺术家身份");
        }
        $list = application()->shopService->getOrderListByArtistId($artistInfo->id, $page, $pageSize, $status);
        return $this->_json(json_decode($list));
    }

    /**
     * @SWG\Post(path="/api/artistOrder/getClosingOrderList",
     *   tags={"artistOrder"},
     *   operationId="/api/artistOrder/getClosingOrderList",
     *   summary="艺术家后台 获得结算订单列表",
     *   description="艺术家后台 获得结算订单列表",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="page",
     *     in="formData",
     *     description="当前页",
     *     required=true,
     *     type="integer",
     *     default="1",
     *   ),
     *   @SWG\Parameter(
     *     name="pageSize",
     *     in="formData",
     *     description="页面大小 默认10",
     *     required=false,
     *     type="integer",
     *     default="",
     *   ),
     *   @SWG\Parameter(
     *     name="type",
     *     in="formData",
     *     description="状态 默认全部 -1.所有 1.待结算 2.已结算",
     *     required=false,
     *     type="integer",
     *     default="",
     *   ),
     *  @SWG\Response(response="200", description="successful operation",@SWG\Schema(ref="#/definitions/MyClosingOrderListResponse"))
     * )
     *
     */
    /**
     * @param Request $request
     * @return mixed
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public function getClosingOrderList(Request $request)
    {
        $rules = [
            'page' => 'required',
            'pageSize' => 'int',
            'type' => 'in:-1,1,2'
        ];
        $this->nValidate($request, $rules);

        $page = $request->input("page");
        $pageSize = $request->input("pageSize", 10);
        $type = $request->input("type", -1);
        $artistInfo = application()->artistApi->getArtistPersonalInfoByUserId($request->user['user_id']);
        if(!$artistInfo){
            ApiResponseException::throwException(ApiResponseCode::NOT_PERMISSION, "您还未认证艺术家身份");
        }
        $list = application()->shopService->getArtistClosingOrderList($artistInfo->id, $page, $pageSize, $type);
        return $this->_json(json_decode($list));
    }

    /**
     * @SWG\Post(path="/api/artistOrder/orderToTransitShipment",
     *   tags={"artistOrder"},
     *   operationId="/api/artistOrder/orderToTransitShipment",
     *   summary="发往中转仓",
     *   description="发往中转仓",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="orderId",
     *     in="formData",
     *     description="订单id",
     *     required=true,
     *     type="integer",
     *   ),
     *   @SWG\Parameter(
     *     name="expressCompany",
     *     in="formData",
     *     description="物流公司",
     *     required=true,
     *     type="string",
     *     default="",
     *   ),
     *   @SWG\Parameter(
     *     name="expressNumber",
     *     in="formData",
     *     description="物流单号",
     *     required=true,
     *     type="string",
     *     default="",
     *   ),
     *  @SWG\Response(response="200", description="successful operation"))
     * )
     *
     */
    /**
     * @param Request $request
     * @return mixed
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public function orderToTransitShipment(Request $request)
    {
        $rules = [
            'orderId' => 'required',
            'expressCompany' => 'required',
            'expressNumber' => 'required',
        ];
        $this->nValidate($request, $rules);

        $orderId = $request->input("orderId");
        $expressCompany = $request->input("expressCompany");
        $expressNumber = $request->input("expressNumber");
        $artistInfo = application()->artistApi->getArtistPersonalInfoByUserId($request->user['user_id']);
        if(!$artistInfo){
            ApiResponseException::throwException(ApiResponseCode::NOT_PERMISSION, "您还未认证艺术家身份");
        }
        if(!application()->shopService->artistOrderToTransitShipment($artistInfo->id, $orderId, $expressCompany, $expressNumber)){
            ApiResponseException::throwException(ApiResponseCode::OPERATE_FAIL);
        }
        return $this->_json();
    }
    /**
     * @SWG\Post(path="/api/artistOrder/getArtistClosingWaitSum",
     *   tags={"artistOrder"},
     *   operationId="/api/artistOrder/getArtistClosingWaitSum",
     *   summary="获得待结算总金额",
     *   description="获得待结算总金额",
     *   produces={"application/json"},
     *  @SWG\Response(response="200", description="successful operation"))
     * )
     *
     */
    /**
     * @param Request $request
     * @return mixed
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public function getArtistClosingWaitSum(Request $request)
    {
        $artistInfo = application()->artistApi->getArtistPersonalInfoByUserId($request->user['user_id']);
        if(!$artistInfo){
            ApiResponseException::throwException(ApiResponseCode::NOT_PERMISSION, "您还未认证艺术家身份");
        }
        return $this->_json(application()->shopService->getArtistClosingWaitSum($artistInfo->id));
    }
    /**
     * @SWG\Post(path="/api/artistOrder/artistToClosing",
     *   tags={"artistOrder"},
     *   operationId="/api/artistOrder/artistToClosing",
     *   summary="结算总金额",
     *   description="结算总金额",
     *   produces={"application/json"},
     *  @SWG\Response(response="200", description="successful operation"))
     * )
     *
     */
    /**
     * @param Request $request
     * @return mixed
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public function artistToClosing(Request $request)
    {
        $artistInfo = application()->artistApi->getArtistPersonalInfoByUserId($request->user['user_id']);
        if(!$artistInfo){
            ApiResponseException::throwException(ApiResponseCode::NOT_PERMISSION, "您还未认证艺术家身份");
        }
        $lockValue = application()->redisLock->lock(RedisKeyConstant::LOCK_ARTIST_TO_CLOSING.$artistInfo->id);
        if(!$lockValue){
            ApiResponseException::throwException(ApiResponseCode::LIMIT_ACTION);
        }
        $sum = $this->_json(application()->shopService->artistToClosing($request->api_token, $artistInfo->id));
        application()->redisLock->unLock(RedisKeyConstant::LOCK_ARTIST_TO_CLOSING.$artistInfo->id, $lockValue);
        return $sum;
    }

    /**
     * @SWG\Post(path="/api/artistOrder/getArtistClosingLog",
     *   tags={"artistOrder"},
     *   operationId="/api/artistOrder/getArtistClosingLog",
     *   summary="艺术家后台 获得结算明细列表",
     *   description="艺术家后台 获得结算明细列表",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="page",
     *     in="formData",
     *     description="当前页",
     *     required=true,
     *     type="integer",
     *     default="1",
     *   ),
     *   @SWG\Parameter(
     *     name="pageSize",
     *     in="formData",
     *     description="页面大小 默认10",
     *     required=false,
     *     type="integer",
     *     default="",
     *   ),
     *  @SWG\Response(response="200", description="successful operation",@SWG\Schema(ref="#/definitions/MyArtistClosingLogResponse"))
     * )
     *
     */
    /**
     * @param Request $request
     * @return mixed
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public function getArtistClosingLog(Request $request)
    {
        $rules = [
            'page' => 'required',
            'pageSize' => 'int',
        ];
        $this->nValidate($request, $rules);

        $artistInfo = application()->artistApi->getArtistPersonalInfoByUserId($request->user['user_id']);
        if(!$artistInfo){
            ApiResponseException::throwException(ApiResponseCode::NOT_PERMISSION, "您还未认证艺术家身份");
        }
        $page = $request->input("page");
        $pageSize = $request->input("pageSize", 10);
        $list = application()->shopService->getArtistClosingLog($artistInfo->id, $page, $pageSize);
        $list = json_decode($list, true);
        if($list && !empty($list['list'])){
            foreach ($list['list'] as &$item) {
                $item['lg_id'] = $item['id'];
                unset($item['id']);
                $item['lg_av_amount'] = $item['change_wait_amount'];
                unset($item['change_wait_amount']);
                $item['lg_available_amount'] = $item['later_wait_amount'];
                unset($item['later_wait_amount']);
                $item['lg_add_time'] = strtotime($item['created_at']);
                unset($item['created_at']);
                $item['lg_desc'] = $item['note'];
                unset($item['note']);
                $item['lg_source'] = $item['transaction_source'];
                unset($item['transaction_source']);
            }
        }
        return $this->_json($list);
    }

    /**
     * @SWG\Post(path="/api/artistOrder/editOrderPrice",
     *   tags={"artistOrder"},
     *   operationId="/api/artistOrder/editOrderPrice",
     *   summary="修改订单金额",
     *   description="修改订单金额",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="order_id",
     *     in="formData",
     *     description="订单ID",
     *     required=true,
     *     type="integer",
     *     default=""
     *   ),
     *   @SWG\Parameter(
     *     name="new_price",
     *     in="formData",
     *     description="最新订单金额",
     *     required=true,
     *     type="integer",
     *     default=""
     *   ),
     *   @SWG\Response(response="200", description="successful operation")
     * )
     */
    /*
     * 订单价格修改
     */
    public function editOrderPrice(Request $request)
    {
        $orderId = $request->order_id ?? '';
        $newPrice = $request->new_price ?? '';
        $artistId = $request->user['authentication_id'];
        $userId = $request->user['user_id'];
        $key = $request->user['user_key'];
        if (empty($orderId) || empty($newPrice)) {
            return response()->fail(200002);
        }
        $orderModel = new Orders();
        $where = ['order_id' => $orderId];
        $orderInfo = $orderModel->where($where)->first();
        if (empty($orderInfo)) {
            return response()->fail(224004);
        }
        if ($orderInfo['order_amount'] == $newPrice) {
            return response()->fail(224005);
        }
        $newPrice = trim($newPrice);
        $storeModel = new Store();
        $memberId = (new Member())->getInfoByKey($key);
        $storeInfo = $storeModel->createStoreByUserId($artistId, $memberId);
        if ($orderInfo['store_id'] != $storeInfo['store_id']) {
            return response()->fail(221004);
        }
        $orderUpdate = ['order_amount' => $newPrice];
        $result = $orderModel->where($where)->update($orderUpdate);
        if (!$result) {
            return response()->fail(224006);
        }
        $orderLogInsert = [
            'order_id' => $orderId,
            'log_msg' => '订单改价',
            'log_time' => time(),
            'log_role' => '卖家(art)',
            'log_user' => $userId,
            'log_orderstate' => $orderInfo['order_state'],
            'act_detail' => '初始金额: ' . $orderInfo['order_amount'] . '修改为: ' . $newPrice
        ];
        $orderLogModel = new OrderLog();
        $orderLogModel->insert($orderLogInsert);
        return response()->success([]);
    }
}
