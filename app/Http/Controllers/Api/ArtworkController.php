<?php

namespace App\Http\Controllers\Api;

use App\Libs\phpQuery\OSS;
use App\Models\ArtistOrganization;
use App\Models\ArtistPersonal;
use App\Models\Artwork;
use App\Models\ArtworkCategory;
use App\Models\ArtworkComment;
use App\Models\ArtworkCommentLike;
use App\Models\ArtworkMaterials;
use App\Models\ArtworkTheme;
use App\Models\Shop\Cart;
use App\Models\Shop\Goods;
use App\Models\Shop\Member;
use App\Models\Shop\Store;
use App\Models\UserFollow;
use App\Models\UserIndex;
use App\Models\Ynh\ArtistType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ArtworkController extends Controller
{
    protected $Model;

    //用户认证上传图片大小限制 单位 M
    protected $pic_max = 2048 * 1024;

    protected $table = [];

    private $user = null;

    public function __construct(Request $request)
    {
        $this->middleware('verify.user.login', ['only' => [
            'artworkLike', 'commentInsert',
            'artworkInsert', 'personalCommentsList', 'commentsDelete',
            'commentsLike', 'pictureUpload', 'artworkPriceEdit'
        ]]);
    }

    public function hotComment(Request $request)
    {
        $artwork_id = $request->artwork_id;
        if (empty($artwork_id)) {
            return response()->fail(200224);
        }
        $api_token = $request->api_token;
        if (!empty($api_token)) {
            $this->user = $login_info = (new UserIndex())->getUserInfoByToken($api_token);
            if (!is_array($login_info)) {
                return response()->fail($login_info);
            }
        }
        $condition = [
            'parent_id' => 0,
            'is_delete' => 0,
            'artwork_id' => $artwork_id,
            'is_examine' => 1
        ];
        $field = ['id', 'artwork_id', 'author_id', 'parent_id', 'commentator_id', 'commentator_nickname', 'accepter_id',
            'accepter_nickname', 'comment_msg', 'like_num', 'created_at'];
        $order = 'like_num desc ,id desc';
        $return = (new ArtworkComment())->select($field)->where($condition)->orderByRaw($order)->paginate(10);
        if (empty($return)) {
            $return = [];
        }
        $return = $return->toArray();
        $return['data'] = $this->artworkCommentFactory($return, 3);
        return response()->success($return);
    }




    /**
     * @SWG\Post(path="/api/artwork/artworkPriceEdit",
     *   tags={"artwork"},
     *   operationId="/api/artwork/artworkPriceEdit",
     *   summary="修改售卖作品价格",
     *   description="修改售卖作品价格",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="artwork_id",
     *     in="formData",
     *     description="作品ID",
     *     required=true,
     *     type="integer",
     *     default=""
     *   ),
     *   @SWG\Parameter(
     *     name="is_sell",
     *     in="formData",
     *     description="出售或者展示",
     *     required=false,
     *     type="integer",
     *     default=""
     *   ),
     *   @SWG\Parameter(
     *     name="price",
     *     in="formData",
     *     description="作品新价格",
     *     required=false,
     *     type="integer",
     *     default=""
     *   ),
     *   @SWG\Response(response="200", description="successful operation")
     * )
     */
    public function artworkPriceEdit(Request $request)
    {
        $user_id = $request->user['user_id'];
        $key = $request->user['user_key'];
        $authentication_id = $request->user['authentication_id'];
        $artwork_id = $request->artwork_id ?? '';
        $is_sell = $request->is_sell ?? null;
        $price = $request->price ?? '';
        if (empty($artwork_id)) {
            return response()->fail(200002);
        }
        if (is_null($is_sell) && empty($price)) {
            return response()->fail(200002);
        }
        $artworkModel = new Artwork();
        $where = ['id' => $artwork_id];
        $artworkInfo = $artworkModel->where($where)->first();
        //pp($artworkInfo);
        if (empty($artworkInfo)) {
            return response()->fail(221003);
        }
        if ($artworkInfo['user_id'] != $user_id) {
            return response()->fail(221004);
        }
        //作品ID转换商品ID
        $goodModel = new Goods();
        $goodInfo = $goodModel->getGoodsIdByArtworkId($artwork_id);
        if (empty($goodInfo)) {
            $storeModel = new Store();
            $memberId = (new Member())->getInfoByKey($key);
            $storeInfo = $storeModel->createStoreByUserId($authentication_id, $memberId);
            $model = new Artwork();
            $goods_model = new Goods();
            $artwork_info = $model->where($where)->first();
            $good_insert = $goods_model->getInsertData($artwork_info);
            $good_insert['artwork_id'] = $artwork_id;
            $good_insert['artist_id'] = $authentication_id;
            $good_insert['goods_state'] = $is_sell ?: $artwork_info['is_sell'];
            $good_insert['goods_price'] = $price ?: $artwork_info['price'];
            $good_insert['store_id'] = $storeInfo['store_id'];
            $good_insert['store_name'] = $storeInfo['store_name'];
            $goodId = $goods_model->insertGetId($good_insert);
        } else {
            $goodId = $goodInfo['goods_id'];
        }
        $artworkUpdate = [];
        if (!is_null($is_sell)) {
            $artworkUpdate['is_sell'] = $is_sell;
        }
        if (!empty($price)) {
            $artworkUpdate['price'] = $price;
        }
        $result1 = $artworkModel->where($where)->update($artworkUpdate);
        $update = [];
        if (!is_null($is_sell)) {
            $update['goods_state'] = $is_sell;
        }
        if (!empty($price)) {
            $update['goods_price'] = $price;
        }
        $result2 = $goodModel->where(['artwork_id' => $artwork_id])->update($update);
        if ($result1 === false || $result2 === false) {
            return response()->fail(400, '修改失败');
        }
        //修改购物车价格
        $cartModel = new Cart();
        $cartModel->editCartPriceByGoodId($goodId, $price);
        return response()->success([]);
    }


    /**
     * @SWG\Post(path="/api/artwork/artworkInsert",
     *   tags={"artwork"},
     *   operationId="/api/artwork/artworkInsert",
     *   summary="作品新增",
     *   description="作品新增",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="artwork_name",
     *     in="formData",
     *     description="作品名称",
     *     required=true,
     *     type="string",
     *     default=""
     *   ),
     *   @SWG\Parameter(
     *     name="artwork_img",
     *     in="formData",
     *     description="封面图",
     *     required=true,
     *     type="file",
     *     default=""
     *   ),
     *   @SWG\Parameter(
     *     name="category_id",
     *     in="formData",
     *     description="分类id-小分类的分类ID",
     *     required=true,
     *     type="integer",
     *     default=""
     *   ),
     *   @SWG\Parameter(
     *     name="materials_id",
     *     in="formData",
     *     description="作品材质ID",
     *     required=true,
     *     type="integer",
     *     default=""
     *   ),
     *   @SWG\Parameter(
     *     name="theme_id",
     *     in="formData",
     *     description="作品题材ID",
     *     required=true,
     *     type="integer",
     *     default=""
     *   ),
     *   @SWG\Parameter(
     *     name="is_sell",
     *     in="formData",
     *     description="作品是否售卖(1:售卖, 0:不售卖 2: 已售出)",
     *     required=false,
     *     type="integer",
     *     default=""
     *   ),
     *   @SWG\Parameter(
     *     name="price",
     *     in="formData",
     *     description="作品售卖价格",
     *     required=false,
     *     type="integer",
     *     default=""
     *   ),
     *   @SWG\Parameter(
     *     name="theme_id",
     *     in="formData",
     *     description="作品题材ID",
     *     required=true,
     *     type="integer",
     *     default=""
     *   ),
     *   @SWG\Parameter(
     *     name="picture[]",
     *     in="formData",
     *     description="作品图--选填",
     *     required=true,
     *     type="file",
     *   ),
     *   @SWG\Parameter(
     *     name="picture[]",
     *     in="formData",
     *     description="作品图--选填",
     *     required=false,
     *     type="integer",
     *     default=""
     *   ),
     *   @SWG\Parameter(
     *     name="length",
     *     in="formData",
     *     description="作品属性--长度-宽度-高度-数组-选填",
     *     required=false,
     *     type="integer",
     *     default=""
     *   ),
     *   @SWG\Parameter(
     *     name="width",
     *     in="formData",
     *     description="宽度",
     *     required=false,
     *     type="integer",
     *     default=""
     *   ),
     *   @SWG\Parameter(
     *     name="height",
     *     in="formData",
     *     description="高度",
     *     required=false,
     *     type="integer",
     *     default=""
     *   ),
     *   @SWG\Parameter(
     *     name="content",
     *     in="formData",
     *     description="作品介绍",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="artwork_id",
     *     in="formData",
     *     description="作品主键ID 修改的时候使用",
     *     required=true,
     *     type="integer",
     *   ),
     *   @SWG\Response(response="200", description="successful operation")
     * )
     */
    public function artworkInsert(Request $request)
    {
        $user_id = $request->user['user_id'];
        $key = $request->user['user_key'];
        $authentication_id = $request->user['authentication_id'];
        $authentication_type = $request->user['authentication_type'];
        $artwork_id = $request->artwork_id ?? '';
        $is_sell = $request->is_sell ?? 0;
        $price = $request->price ?? 0;
        $content = $request->input('content') ?? '';
        if (!empty($artwork_id)) {
            $artwork_info = (new Artwork())->where(['id' => $artwork_id])->first();
            if (empty($artwork_info)) {
                return response()->fail(221003);
            }
            if ($artwork_info['user_id'] != $user_id) {
                return response()->fail(221004);
            }
        }
        if (empty($authentication_id)) {
            return response()->fail(200200);
        }
        if (!contentFilter($content)) {
            return response()->fail(221005);
        }
        $check = $this->artworkInsertCheck($request);
        if ($check !== true) {
            return response()->fail($check);
        }
        $artworkInsert = $this->artworkInsertGet($request);
        $artworkInsert['user_id'] = $user_id;
        $artworkInsert['artist_id'] = $authentication_id;
        $artworkInsert['artist_type'] = $authentication_type;
        $artworkInsert['is_sell'] = $is_sell;
        if ($artworkInsert['is_sell'] && $artworkInsert['is_sell'] == Artwork::IS_SELL_YES) {
            if (empty($price) || !is_numeric($price)) {
                return response()->fail(200002);
            }
        }
        $artworkInsert['price'] = $price;
        $oss = new OSS();
        $basePath = 'artwork/img' . $user_id;
        $oss_url = env('PICTURE_UPLOAD_URL', 'https://testyinuo2.oss-cn-beijing.aliyuncs.com');
        if (!empty($request->picture) && is_array($request->picture)) {
            $pictureInsert = $request->picture;
            $picUrl = $request->input('picture');
            if (isset($picUrl) && $picUrl) {
                if (!is_array($picUrl)) {
                    return response()->fail(200224);
                }
                $pictureInsert = array_merge($pictureInsert, $picUrl);
                $pictureInsert = array_unique($pictureInsert);
            }
            foreach ($pictureInsert as $value) {
                if (!is_object($value) || !$value->isValid()) {
                    if (is_string($value)) {
                        $pic_path[] = $value;
                    } else {
                        return response()->fail(503001);
                    }
                } else {
                    $filename = $value->getClientOriginalName();
                    if (empty($filename)) {
                        return response()->fail(503001);
                    }
                    $size = $value->getClientSize();
                    if ($size > $this->pic_max) {
                        return response()->fail(503006);
                    }
                    $extension = strrchr($filename, '.');
                    $PicObject = $basePath . '/' . rand(1000, 9999) . $extension;
                    $pic_url = $oss->upload($value->getRealPath(), $PicObject);
                    if ($pic_url) {
                        $pic_path[] = str_replace($oss_url, "", $pic_url);
                    } else {
                        return response()->fail(200104);
                    }
                }
            }
            $artwork_pic = $pic_path;
        }
        //上传作品封面图
        $basePath = 'artwork/pic' . $user_id;
        $artwork_img = $request->artwork_img;
        if (!is_object($artwork_img) || !$artwork_img->isValid()) {
            if (is_string($artwork_img)) {
                $pic_path = $artwork_img;
            } else {
                return response()->fail(503001);
            }
        } else {
            $filename = $artwork_img->getClientOriginalName();
            if (empty($filename)) {
                return response()->fail(503001);
            }
            $size = $artwork_img->getClientSize();
            if ($size > $this->pic_max) {
                return response()->fail(503006);
            }
            $extension = strrchr($filename, '.');
            $PicObject = $basePath . '/' . rand(1000, 9999) . $extension;
            $pic_url = $oss->upload($artwork_img->getRealPath(), $PicObject);
            if ($pic_url) {
                $pic_path = str_replace($oss_url, "", $pic_url);
            } else {
                return response()->fail(200104);
            }
        }
        $artworkInsert['artwork_img'] = $pic_path;
        $goods_model = new Goods();
        // 索引库开启事物
        DB::beginTransaction();
        try {
            $model = new Artwork();
            if (empty($artwork_id)) {
                $result1 = $model::insertGetId($artworkInsert);
                if (!$result1) {
                    throw new Exception(200107);
                }
                if (!empty($artwork_pic)) {
                    $artwork_pic_insert['artwork_id'] = $result1;
                    foreach ($artwork_pic as $value) {
                        $artwork_pic_insert['created_at'] = date("Y-m-d H:i:s");
                        $artwork_pic_insert['picture'] = $value;

                        $result2 = DB::connection()->table('artwork_pic')
                            ->insert($artwork_pic_insert);
                        if (!$result2) {
                            throw new Exception(200106);
                        }
                    }
                }
                if ($artworkInsert['is_sell'] && $artworkInsert['is_sell'] == 1) {
                    $storeModel = new Store();
                    $memberId = (new Member())->getInfoByKey($key);
                    $storeInfo = $storeModel->createStoreByUserId($authentication_id, $memberId);
                    $good_insert = $goods_model->getInsertData($artworkInsert);
                    $good_insert['artwork_id'] = $result1;
                    $good_insert['artist_id'] = $authentication_id;
                    $good_insert['store_id'] = $storeInfo['store_id'];
                    $good_insert['store_name'] = $storeInfo['store_name'];
                    //同步分类
                    $good_insert['artwork_category_id'] = $artworkInsert['category_id'];
                    $goods_model->insert($good_insert);
                }
                if ($authentication_type == 1) {
                    $where = [
                        'id' => $authentication_id
                    ];
                    $update = [
                        'artwork_time' => date("Y-m-d H:i:s")
                    ];
                    $result4 = (new ArtistPersonal())->where($where)->update($update);
                    if (!$result4) {
                        throw new Exception(200106);
                    }
                }
            } else {
                $condition = ['id' => $artwork_id];
                $storeModel = new Store();
                $memberId = (new Member())->getInfoByKey($key);
                $storeInfo = $storeModel->createStoreByUserId($authentication_id, $memberId);
                //goods_state 商品状态 0下架，1正常，10违规（禁售）
                $where = ['artwork_id' => $artwork_id];
                $goodsInfo = $goods_model->where($where)->first();
                if (empty($goodsInfo)) {
                    $good_insert = $goods_model->getInsertData($artworkInsert);
                    $good_insert['artwork_id'] = $artwork_id;
                    $good_insert['artist_id'] = $authentication_id;
                    $good_insert['goods_state'] = $artworkInsert['is_sell'];
                    $good_insert['goods_price'] = $artworkInsert['price'];
                    $good_insert['store_id'] = $storeInfo['store_id'];
                    $good_insert['store_name'] = $storeInfo['store_name'];
                    //同步分类
                    $good_insert['artwork_category_id'] = $artworkInsert['category_id'];
                    $goods_model->insert($good_insert);
                } else {
                    $update = [
                        'goods_state' => $artworkInsert['is_sell'],
                        'goods_name' => $artworkInsert['artwork_name'],
                        'artwork_category_id' => $artworkInsert['category_id'],
                    ];
                    if ($artworkInsert['is_sell']) {
                        $update['goods_price'] = $artworkInsert['price'];
                    }
                    $goods_model->where($where)->update($update);
                }
                $result1 = $model->where($condition)->update($artworkInsert);
                if (!$result1 && $result1 !== 0) {
                    throw new Exception(200107);
                }
                $result3 = DB::connection()->table('artwork_pic')->where(['artwork_id' => $artwork_id])->delete();
                if (!$result3 && $result3 !== 0) {
                    return response()->fail(200106);
                }
                if (!empty($artwork_pic)) {
                    $artwork_pic_insert['artwork_id'] = $artwork_id;
                    foreach ($artwork_pic as $value) {
                        $artwork_pic_insert['created_at'] = date("Y-m-d H:i:s");
                        $artwork_pic_insert['picture'] = $value;

                        $result2 = DB::connection()->table('artwork_pic')
                            ->insert($artwork_pic_insert);
                        if (!$result2) {
                            throw new Exception(200106);
                        }
                    }
                }
                if ($authentication_type == 1) {
                    $where = [
                        'id' => $authentication_id
                    ];
                    $update = [
                        'artwork_time' => date("Y-m-d H:i:s")
                    ];
                    $result4 = (new ArtistPersonal())->where($where)->update($update);
                    if (!$result4) {
                        throw new Exception(200106);
                    }
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            Log::error('错误详情:获取token-' . $e->getMessage() . '.');
            return response()->fail($e->getMessage());
        }
        return response()->success();
    }


    /**
     * 作品评论添加----也可以做评论回复 使用
     * 品论不需要审核
     * @param Request $request
     * @return mixed
     */
    public function commentInsert(Request $request)
    {
        $user_id = $request->user['user_id'] ?? '';
        $artwork_id = $request->artwork_id ?? '';
        $comment_msg = $request->comment_msg ?? '';
        if (empty($artwork_id) || empty($comment_msg)) {
            return response()->fail(200224);
        }
        if (!contentFilter($comment_msg)) {
            return response()->fail(221005);
        }
        $comment_id = $request->comment_id ?? 0;
        $model = new Artwork();
        $author_id = $model->getAuthorIdByArtworkId($artwork_id);
        if (empty($author_id)) {
            return response()->fail(200033);
        }
        $accepter_id = $author_id;
        if (!empty($comment_id)) {
            $result = ArtworkComment::find($comment_id);
            if (empty($result) && $result->parent > 0) { // 不能评论非1级评论
                return response()->fail(200010);
            }
            $accepter_id = $result->commentator_id;
        }
        if (!empty($accepter_id)) {
            $accepter_info = (new UserIndex())->getUserInfoById($accepter_id);
        }
        $commentInsert = [
            'artwork_id' => $artwork_id,
            'is_examine' => 1,
            'author_id' => $author_id,
            'comment_msg' => $comment_msg,
            'parent_id' => $comment_id,
            'commentator_id' => $user_id,
            'commentator_nickname' => $request->user['user_nickname'] ?: tellHide($request->user['user_real_name']),
            'accepter_id' => $accepter_id,
            'accepter_nickname' => $accepter_info['user_nickname'] ?? '',
        ];
        $model = new ArtworkComment();
        $comment_id = $model->insertGetId($commentInsert);
        if (!$comment_id) {
            return response()->fail(200012);
        }
        $artwork = new Artwork();
        $artwork::where('id', $artwork_id)->increment('comment_num');
        //评论人头像
        $commentInsert['commentator_avatar'] = $request->user['user_avatar']
            ? getPicturePath($request->user['user_avatar'])
            : getMemberAvatarByID($user_id);
        $commentInsert['created_at'] = date('Y-m-d H:i:s');
        $commentInsert['id'] = $comment_id;
        if (!empty($accepter_id)) {
            $model = new UserIndex();
            $model->where(['user_id' => $accepter_id])->increment('comment_num', 1);
            $model->cacheDel($accepter_id);
        }
        return response()->success($commentInsert);
    }


    /**
     * 获取作品评论列表
     * @param Request $request
     * @return mixed
     */
    public function commentList(Request $request)
    {
        if ((int)$request->artwork_id < 1) {
            return response()->fail(200004);
        }
        $this->Model = new Artwork();
        $api_token = $request->api_token;
        if (!empty($api_token)) {
            $this->user = $login_info = (new UserIndex())->getUserInfoByToken($api_token);
            if (!is_array($login_info)) {
                return response()->fail($login_info);
            }
        }
        $condition = [
            'artwork_id' => $request->artwork_id,
            'parent_id' => 0,
        ];
        $field = ['id', 'artwork_id', 'author_id', 'parent_id', 'commentator_id', 'commentator_nickname', 'accepter_id',
            'accepter_nickname', 'comment_msg', 'like_num', 'created_at'];
        $result = (new ArtworkComment())->getList($condition, $field);
        if (empty($result)) {
            return response()->success([]);
        }
        $result['data'] = $this->artworkCommentFactory($result);

        return response()->success($result);
    }

    /**
     * 修改作品状态
     * @Date: 2019/3/28 0028
     * @author: Mr.Liu
     */
    public function editState(Request $request)
    {
        $artworkId = $request->artwork_id;
        $state = $request->state;
        if (empty($artworkId) && empty($state)) {
            return response()->fail(200001);
        }
        (new Artwork())->editStateById($artworkId, $state);
        return response()->success([]);
    }

    public function personalArticleList(Request $request)
    {
        $artist_id = $request->artist_id;
        $artist_type = $request->artist_type;
        $is_sell = $request->is_sell;
        if (empty($artist_id)) {
            return response()->fail(200001);
        }
        $artworkModel = new Artwork();
        $condition = [
            'artist_id' => $artist_id,
            'artist_type' => $artist_type,
            'is_delete' => 0,
            'status' => 1
        ];
        if ($is_sell) {
            $condition['is_sell'] = $is_sell;
        }
        $fields = ['id', 'artwork_name', 'artwork_img', 'is_sell', 'price', 'created_at'];
        $order = 'created_at desc';
        $return = $artworkModel->getArtworkList($condition, $fields, $order);
        if (!isset($return['data']) || empty($return['data'])) {
            return response()->success([]);
        }
        $getPath = function (&$value) {
            $value['artwork_img'] = $value['artwork_img'] ? getPicturePath($value['artwork_img']) : '';
            return $value;
        };
        $return['data'] = array_map($getPath, $return['data']);
        return response()->success($return);
    }

    public function personalCommentsList(Request $request)
    {
        $user_info = $request->user;
        $user_id = $user_info['user_id'];
        $CommentsList = (new ArtworkComment())->where(['accepter_id' => $user_id])
            //->orWhere(['commentator_id' => $user_id])
            ->where([
                //'is_delete' => 0,
                'is_examine' => 1,
            ])
            ->orderBy('created_at', 'desc')
            ->paginate();
        if (isset($CommentsList['data']) && empty($CommentsList['data'])) {
            return response()->success($CommentsList);
        }
        $CommentsList = $CommentsList->toArray();
        $model = new Artwork();
        foreach ($CommentsList['data'] as &$value) {
            $artwork_info = $model->getInfoById($value['artwork_id']);
            $artwork_img = $artwork_info['artwork_img'] ?? '';
            $value['artwork_img'] = $artwork_img ? getPicturePath($artwork_img) : '';
            $value['artwork_name'] = $artwork_info['artwork_name'] ?? '';
            unset($value['like_num'], $value['updated_at']);//去除, $value['is_delete']
            unset($value['is_examine'], $value['is_show'], $value['admin_id']);
        }
        $model = new UserIndex();
        $model->where(['user_id' => $user_id])->update(['comment_num' => 0]);
        $model->cacheDel($user_id);
        return response()->success($CommentsList);
    }

    /**
     * 最新作品更新列表(按艺术家分组)
     * @return mixed
     */
    public function newArtistArtworkList(Request $request)
    {
        if (isset($request->page) && $request->page > 7) {
            return response()->success([]);
        }
        $api_token = $request->api_token;
        if (!empty($api_token)) {
            $login_info = (new UserIndex())->getUserInfoByToken($api_token);
            if (!is_array($login_info)) {
                return response()->fail($login_info);
            }
            $user_id = $login_info['user_id'];
        }
        //获取最新更新作品艺术家
        $model = new ArtistPersonal();
        $this->Model = new Artwork();
        $artwork_fields = ['id', 'artist_id', 'artwork_name', 'artwork_img', 'created_at'];
        $artwork_order = 'updated_at desc';
        $artwork_condition = [
            'artist_type' => 1,
            'is_delete' => 0,
            'status' => 1
        ];
        $take = 4;
        $files = ['id', 'user_id', 'name', 'real_name', 'logo'];
        $where = ['role_id' => 2];
        $artist_list = $model->newArtistArtworkList($files, $where);
        if (!isset($artist_list['data']) || empty($artist_list['data'])) {
            return response()->success([]);
        }
        $user_model = new UserIndex();
        foreach ($artist_list['data'] as $key => &$value) {
            if (isset($user_id) && $user_id) {
                if ($user_id == $value['user_id']) {
                    $value['is_follow'] = 2;//用户自己
                } else {
                    $user_followModel = new UserFollow();
                    $tmp = $user_followModel->getInfo([
                        'user_id' => $user_id,
                        'follow_user_id' => $value['user_id'],
                        'is_delete' => 0
                    ]);
                    $value['is_follow'] = $tmp ? 1 : 0;
                }
            } else {
                $value['is_follow'] = 0;
            }
            $nike_name = $value['name'] ?: $value['real_name'];
            $value['name'] = strlen($nike_name) > 24 ? substr($nike_name, 0, 24) . '...' : $nike_name;
            $value['logo'] = $value['logo'] ? getPicturePath($value['logo']) : getMemberAvatarByID($value['id']);
            $user_info = $user_model->getUserInfoById($value['user_id']);
            if (empty($user_info)) {
                unset($artist_list['data'][$key]);
                continue;
            }
            $value['user_flag'] = $user_info['user_flag'] ?? '';
            $artwork_condition['artist_id'] = $value['id'];
            $value['artwork'] = $this->Model
                ->getArtworkList($artwork_condition, $artwork_fields, $artwork_order, $take);
            if (!empty($value['artwork']) && is_array($value['artwork'])) {
                foreach ($value['artwork'] as &$item) {
                    $item['artwork_img'] = $item['artwork_img'] ? getPicturePath($item['artwork_img']) : '';
                }
            } else {
                unset($artist_list['data'][$key]);
            }
        }
        $artist_list['data'] = array_values($artist_list['data']);
        return response()->success($artist_list);
    }

    /**
     * 作品详情
     * @param Request $request
     * @return mixed
     */
    public function artworkInfo(Request $request)
    {
        $artwork_id = $request->artwork_id;
        if (empty($artwork_id)) {
            return response()->fail(200001);
        }
        $api_token = $request->api_token;
        if (!empty($api_token)) {
            $login_info = (new UserIndex())->getUserInfoByToken($api_token);
            if ($login_info && is_array($login_info)) {
                $user_id = $login_info['user_id'];
                $db_name = $login_info['db'];
            }
        }
        $artworkModel = new Artwork();
        $condition = ['is_delete' => 0, 'id' => $artwork_id];
        //先进行 浏览 +1
        $artwork_info = $artworkModel->ArtworkDetail($condition);
        if (empty($artwork_info)) {
            return response()->fail(221001);
        }
        $artwork_info['content'] = removeTheLabel($artwork_info['content']);
        $artworkModel->where($condition)->increment('click_num');
        $artwork_info = $artwork_info->toArray();
        //$user_info = (new UserIndex())::find($artwork_info['user_id']);
        $user_info = (new UserIndex())->getUserInfoById($artwork_info['user_id']);
        if (!empty($user_info)) {
            $artwork_info['user_name'] = $user_info['user_nickname'] ?: $user_info['user_real_name'];
            $artwork_info['user_flag'] = $user_info['user_flag'] ?? '';
            $artwork_info['user_avatar'] = $user_info['user_avatar'] ? getPicturePath($user_info['user_avatar'])
                : getMemberAvatarByID($user_info['user_id']);
        } else {
            $artwork_info['user_name'] = '';
            $artwork_info['user_flag'] = '';
            $artwork_info['user_avatar'] = '';
        }
        $artwork_info['artwork_img'] = $artwork_info['artwork_img'] ? getPicturePath($artwork_info['artwork_img']) : '';
        if (!empty($artwork_info['artwork_pic'])) {
            if (!is_array($artwork_info['artwork_pic'])) {
                $artwork_info['artwork_pic'] = json_decode($artwork_info['artwork_pic'], true);
            }
            foreach ($artwork_info['artwork_pic'] as &$value) {
                $value = $value['picture'] ? getPicturePath($value['picture']) : '';
            }
        }
        array_unshift($artwork_info['artwork_pic'], $artwork_info['artwork_img']);
        $artwork_info['artist_type'] = (new ArtistType())::find($artwork_info['artist_type'], ['name'])['name'] ?? '';
        $category = new ArtworkCategory();
        $category_info = $category->where(['id' => $artwork_info['category_id']])->first();
        $artwork_info['category_name'] = $category_info['name'] ?? '';
        $category_info = $category->where(['id' => $category_info['parent_id']])->first();
        $artwork_info['category_big'] = $category_info['name'] ?? '';
        $artwork_info['material_name'] = (new ArtworkMaterials())::find($artwork_info['material_id'], ['name'])['name']
            ?? '';
        $artwork_info['theme_name'] = (new ArtworkTheme())::find($artwork_info['theme_id'], ['name'])['name'] ?? '';
        $artwork_info['type_info'] = $artwork_info['type_info']
            ? json_decode($artwork_info['type_info'], true)
            : ['l' => 0, 'w' => 0, 'h' => 0];
        $artwork_info['is_like'] = 0;
        $artwork_info['share_url'] = env('SHARE_URL', '') . "/workDetails?id={$artwork_id}&api_token={$api_token}"; //分享地址
        if (isset($user_id) && !empty($user_id)) {
            $where = [
                'artwork_id' => $artwork_id,
                'user_id' => $user_id,
                'is_delete' => 0,
            ];
            $is_like = DB::connection($db_name)->table('artwork_like')->where($where)->get();
            if ($is_like) {
                $artwork_info['is_like'] = 1;
            }
            if ($user_id == $artwork_info['user_id']) {
                $artwork_info['is_follow'] = 2;//用户自己
            } else {
                $user_followModel = new UserFollow();
                $tmp = $user_followModel->getInfo([
                    'user_id' => $user_id,
                    'follow_user_id' => $artwork_info['user_id'],
                    'is_delete' => 0
                ]);
                $artwork_info['is_follow'] = $tmp ? 1 : 0;
            }
        } else {
            $artwork_info['is_follow'] = 0;
        }
        unset($artwork_info['status']);
        if($artwork_info["price"] <= 0){
            $artwork_info["price"] = null;
        }
        return response()->success($artwork_info);
    }

    /**
     * 作品点赞/取消点赞
     * @param Request $request
     * @return mixed
     */
    public function artworkLike(Request $request)
    {
        $artwork_id = $request->artwork_id;
        $user_id = $request->user['user_id'];
        $db_name = $request->user['db'];
        if (empty($artwork_id) || empty($user_id) || empty($db_name)) {
            return response()->fail(200001);
        }
        $where = ['id' => $artwork_id];
        $result = DB::table('artwork')->where($where)->first();
        if (empty($result)) {
            return response()->fail(221001);
        }
        DB::beginTransaction();
        DB::connection($db_name)->beginTransaction();
        try {
            $where2 = [
                'artwork_id' => $artwork_id,
                'user_id' => $user_id,
            ];
            $res = DB::connection($db_name)->table('artwork_like')->where($where2)->first();

            if ($res) {
                if (is_object($res)) {
                    $res = $res->toArray();
                }
                if ($res['is_delete'] == 1) {
                    $update = ['is_delete' => 0];
                    $status = 1;
                    $result2 = DB::table('artwork')->where($where)->increment('like_num', 1);
                } else {
                    $update = ['is_delete' => 1];
                    $status = 0;
                    $result2 = DB::table('artwork')->where($where)->decrement('like_num', 1);
                }
                $result1 = DB::connection($db_name)->table('artwork_like')->where($where2)->update($update);
            } else {
                $insert = [
                    'artwork_id' => $artwork_id,
                    'user_id' => $user_id,
                    'is_delete' => 0,       //这个条件也可以不写  默认就0
                    'created_at' => date('Y-m-d H:i:s', time())
                ];
                $update['is_delete'] = 0;
                $status = 1;
                $result1 = DB::connection($db_name)->table('artwork_like')->insert($insert);
                $result2 = DB::table('artwork')->where($where)->increment('like_num', 1);
            }

            if (!($result1 && $result2)) {
                throw new \Exception(200005);
            }

            //库数据提交
            DB::commit();
            DB::connection($db_name)->commit();

        } catch (\Exception $e) {
            //库数据回滚
            DB::rollback();
            DB::connection($db_name)->rollback();
            return response()->fail($e->getMessage());
        }
        return response()->success($status);
    }

    /**
     * @SWG\Get(path="/api/artwork/memberArtworkList",
     *   tags={"artwork"},
     *   operationId="/api/artwork/memberArtworkList",
     *   summary="用户作品列表",
     *   description="用户作品列表",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="user_id",
     *     in="query",
     *     description="艺术家的用户id",
     *     required=true,
     *     type="integer",
     *     default=""
     *   ),
     *   @SWG\Parameter(
     *     name="is_sell",
     *     in="query",
     *     description="是否售卖状态0展示1售卖2已售",
     *     required=false,
     *     type="integer",
     *     default=""
     *   ),
     *   @SWG\Response(response="200", description="successful operation",@SWG\Schema(ref="#/definitions/ArtworkListResponse"))
     * )
     */
    /**
     * 用户作品列表
     * @param $user_id 用户id
     * @param Request $request
     * @return mixed
     */
    public function memberArtworkList(Request $request)
    {
        $user_id = $request->user_id;
        $is_sell = $request->is_sell;
        if (empty($user_id)) {
            return response()->fail(200001);
        }
        $artworkModel = new Artwork();
        $condition = [
            'user_id' => $user_id,
            'is_delete' => 0,
        ];
        if (!is_null($is_sell)) {
            $condition['is_sell'] = $is_sell;
        }
        $list = $artworkModel->getArtworkList($condition);
        if (isset($list['data'])) {
            $getPath = function (&$value) {
                $value['artwork_img'] = $value['artwork_img'] ? getPicturePath($value['artwork_img']) : '';
                $value['type_info'] = $value['type_info']
                    ? json_decode($value['type_info'], true)
                    : ['l' => 0, 'w' => 0, 'h' => 0];
                unset($value['is_delete'], $value['content']);
                if($value["price"] <= 0){
                    $value["price"] = null;
                }
                return $value;
            };
            $list['data'] = array_map($getPath, $list['data']);
        }
        return response()->success($list);
    }

    /**
     * 删除作品
     * @param $artwork_id 作品id
     * @param Request $request
     * @return mixed
     */
    public function deleteInfo(Request $request)
    {
        $artwork_id = $request->artwork_id;
        $user_id = $request->user['user_id'];
        if (empty($artwork_id) || empty($user_id)) {
            return response()->fail(200001);
        }
        $condition = ['user_id' => $user_id, 'is_delete' => 0, 'id' => $artwork_id];
        $this->Model = new Artwork();
        $re = $this->Model->updateInfo(['is_delete' => 1], $condition);
        if ($re != 1) {
            return response()->fail(200005);
        }
        $result = (new ArtworkComment())->where(['artwork_id' => $artwork_id])->update(['is_delete' => 1]);
        //艺术家作品售卖下架 goods_state 商品状态 0下架，1正常，10违规（禁售）
        (new Goods())->where(['artwork_id' => $artwork_id])->update(['goods_state' => 0]);
        if (!$result && $result !== 0) {
            return response()->fail(200111);
        }
        return response()->success([]);
    }

    private function artworkCommentFactory($result, $perPage = 0)
    {
        if (count($result) == 0) {
            return $result;
        }
        if (is_object($result)) {
            $result = $result->toArray();
        }
        if (isset($result['data'])) {
            $result = $result['data'];
        }
        if ($this->user) {
            $artwork_comment_like = new ArtworkCommentLike();
            $artwork_comment_like->setConnection($this->user['db']);
            $artwork_comment_ids = array_column($result, 'id');
            $like_list = $artwork_comment_like->where('user_id', $this->user['user_id'])
                ->whereIn('artwork_comment_id', $artwork_comment_ids)
                ->lists('is_delete', 'artwork_comment_id')
                ->toArray();
        }
        $author_ids = array_column($result, 'commentator_id');
        $avatar = getMemberAvatarByID($author_ids);

        $field = ['id', 'artwork_id', 'author_id', 'parent_id', 'commentator_id', 'commentator_nickname', 'accepter_id',
            'accepter_nickname', 'comment_msg', 'like_num', 'created_at'];
        $model = new ArtworkComment();
        if (!empty($perPage)) {
            $model->perPage = $perPage;
        }
        foreach ($result as &$item) {
            $is_like = 0;
            if (isset($like_list[$item['id']])) {
                $is_like = $like_list[$item['id']] == 1 ? 0 : 1;
            }
            $item['commentator_avatar'] = $avatar[$item['commentator_id']] ?? env('PICTURE_DEFAULT_USER', '');
            $item['is_like'] = $is_like;
            $item['child'] = $model->getChildListById($item['id'], $field);   // 获取2级评论
        }
        return $result;
    }

    public function artworkInsertCheck($request)
    {
        $rule = [
            'artwork_name' => "required",                               //作品名称
            'artwork_img' => 'required',                                //封面图
            'category_id' => 'required|regex:/^(?!0+$)^\d*$$/',         //分类id--小分类的分类ID
            //'material_id' => 'required|regex:/^(?!0+$)^\d*$$/',        //作品材质ID
            //'theme_id' => 'required|regex:/^(?!0+$)^\d*$$/',            //作品题材ID
        ];
        $message = [
            '*.required' => 200224,
            '*.regex' => 200002,
        ];
        $validator = Validator::make($request->all(), $rule, $message);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $errors->all()[0];
        }
        //判断是否存在题材和材质id
        if (!empty($this->materialsListFetch($request->category_id)) && empty($request->material_id)) {
            return 200224;
        }
        if (!empty($this->themeListFetch($request->category_id)) && empty($request->theme_id)) {
            return 200224;
        }
        return true;
    }

    public function artworkInsertGet($request)
    {
        $params['artwork_name'] = $request->artwork_name;
        //$params['artwork_img'] = $request->artwork_img;
        $params['category_id'] = $request->category_id;
        if (!empty($request->material_id)) {
            $params['material_id'] = $request->material_id;
        } else {
            $params['material_id'] = 0;
        }
        if (!empty($request->theme_id)) {
            $params['theme_id'] = $request->theme_id;
        } else {
            $params['theme_id'] = 0;
        }
        $type_info = [];
        $type_info['l'] = $request->length ?? 0;
        $type_info['w'] = $request->width ?? 0;
        $type_info['h'] = $request->height ?? 0;
        $params['type_info'] = json_encode($type_info);
        if (!empty($request->artwork_create_time)) {
            $params['artwork_create_time'] = $request->artwork_create_time;
        }
        if (!empty($request->content)) {
            $params['content'] = $request->content;
        }
        return $params;
    }

    public function categoryList(Request $request)
    {
        $pid = $request->pid ?: 0;
        if (!is_numeric($pid)) {
            return response()->fail(200002);
        }
        $return = $this->getCategory($pid);
//        if (is_array($return) && count($return) > 0) {
//            foreach ($return as &$item) {
//                if ($item['parent_id'] == 0) {
//                    unset($item['parent_id']);
//                    $item['child'] = $this->getCategory($item['id']);
//                }
//            }
//        }
        return response()->success($return);
    }

    /*
     * 获取作品二级分类
     */
    public function categoryChild()
    {
//        $model = new ArtworkCategory();
//        $field = ['id', 'name', 'type_info'];
//        $result = $model->select($field)->where('parent_id', '!=', 0)->get();
//        $return = $result ? $result->toArray() : $result;
//        if (is_array($return) && count($return) > 0) {
//            foreach ($return as &$value) {
//                $value['type_info'] = $value['type_info'] ? json_decode($value['type_info'], true)
//                    : ['l' => null, 'w' => null, 'h' => null];
//            }
//        }
//        return response()->success($return);
        $result = config('artwork.artwork_category');
        $return = [];
        $i = -1;
        foreach ($result as $key => $value) {
            $i += 1;
            $return[$i]['name'] = $key;
            //$return[$i]['value'] = $value;
        }
        return response()->success($return);
    }


    private function getCategory($pid, $field = '')
    {
        $model = new ArtworkCategory();
        $field = ['id', 'name', 'type_info', 'parent_id'];
        $where = [
            'parent_id' => $pid
        ];
        $result = $model->select($field)->where($where)->get();
        $return = $result ? $result->toArray() : [];
        if (is_array($return) && count($return) > 0) {
            foreach ($return as &$value) {
                //if ($value['parent_id'] != 0) {
                $value['type_info'] = $value['type_info'] ? json_decode($value['type_info'], true)
                    : ['l' => 0, 'w' => 0, 'h' => 0];
                //}
                if (empty($field)) {
                    unset($value['parent_id']);
                }
            }
        }
        return $return;
    }

    public function materialsList(Request $request)
    {
        $category_id = $request->category_id;
        if (!is_numeric($category_id) || empty($category_id)) {
            return response()->fail(200002);
        }
        $return = $this->materialsListFetch($category_id);
        return response()->success($return);
    }

    public function materialsListFetch($category_id, $field = '')
    {
        $model = new ArtworkMaterials();
        if (empty($field)) {
            $field = ['id', 'name'];
        }
        $where = [
            'category_id' => $category_id
        ];
        $result = $model->select($field)->where($where)->get();
        return $result ? $result->toArray() : $result;
    }


    public function themeList(Request $request)
    {
        $category_id = $request->category_id;
        if (!is_numeric($category_id) || empty($category_id)) {
            return response()->fail(200002);
        }
        $return = $this->themeListFetch($category_id);
        return response()->success($return);
    }


    public function themeListFetch($category_id, $field = '')
    {
        $model = new ArtworkTheme();
        if (empty($field)) {
            $field = ['id', 'name'];
        }
        $where = [
            'category_id' => $category_id
        ];
        $result = $model->select($field)->where($where)->get();
        return $result ? $result->toArray() : $result;
    }


    public function searchArtworkByCategory(Request $request)
    {
        $category_ids = trim($request->category_id);
        $where = [
            'status' => 1,
            'is_delete' => 0
        ];

        $result = config('artwork.artwork_category');
        if (!empty($category_ids)) {
            if (!isset($result[$category_ids])) {
                return response()->fail(200002);
            }
            $category_ids = $result[$category_ids];
        }
        $category_ids = $category_ids ?? [];
        //if (!empty($category_id) && is_array($category_id)) {
        //$where['category_id'] = $category_id;
        //$category_ids = "'" . implode("','", $category_id) . "'";
        //pp('sb');
        //}
        $model = new Artwork();
        $fields = ['id', 'user_id', 'artwork_name', 'artwork_img', 'category_id', 'artist_type', 'artist_id', 'type_info'];
        //$return = $model->getArtworkList($where, $fields, $order = 'id desc', $take = 0, $category_ids);
        $query = $model->select($fields);
        $query->where($where);
        if (!empty($category_ids) && is_array($category_ids)) {
            $query->whereIn('category_id', $category_ids);
        }
        $query->orderBy('updated_at', 'desc');
        $result = $query->paginate($model->page);
        $return = $result ? $result->toArray() : [];
        if (empty($return)) {
            return response()->success($return);
        }
        if (isset($return['data'])) {
            $getPath = function (&$value) {
                $value['artwork_img'] = $value['artwork_img'] ? getPicturePath($value['artwork_img']) : '';
                $user_info = (new UserIndex())->getUserInfoById($value['user_id']);
                if (empty($user_info)) {
                    unset($value);
                    return '';
                }
                $value['artist_name'] = $user_info['user_nickname'] ?? '';
                $value['type_info'] = $value['type_info']
                    ? json_decode($value['type_info'])
                    : ['l' => 0, 'w' => 0, 'h' => 0];
                return $value;
            };
            $return['data'] = array_map($getPath, $return['data']);
            $return['data'] = array_values(array_filter($return['data']));
        }
        return response()->success($return);
    }

    public function searchArtworkByName(Request $request)
    {
        $api_token = $request->api_token;
        if (!empty($api_token)) {
            $login_info = (new UserIndex())->getUserInfoByToken($api_token);
            if (!is_array($login_info)) {
                return response()->fail($login_info);
            }
            $user_id = $login_info['user_id'];
        }
        $artwork_name = trim($request->artwork_name);
        if (empty($artwork_name)) {
            return response()->fail(200224);
        }
        $condition[] = ['artwork_name', 'like', "%$artwork_name%"];
        $where = [
            'status' => 1,
            'is_delete' => 0
        ];
        $model = new Artwork();
        $fields = ['id', 'artwork_name', 'artwork_img'];
        $result = $model->select($fields)->where($condition)
            ->where($where)->paginate();
        $result = $result ? $result->toArray() : $result;
        if (empty($result)) {
            return response()->success($result);
        }
        if (isset($result['data'])) {
            $getPath = function (&$value) {
                if (isset($user_id) && $user_id) {
                    if ($user_id == $value['user_id']) {
                        $value['is_follow'] = 2;//用户自己
                    } else {
                        $user_followModel = new UserFollow();
                        $tmp = $user_followModel->getInfo([
                            'user_id' => $user_id,
                            'follow_user_id' => $value['user_id'],
                            'is_delete' => 0
                        ]);
                        $value['is_follow'] = $tmp ? 1 : 0;
                    }
                } else {
                    $value['is_follow'] = 0;
                }
                $value['artwork_img'] = $value['artwork_img'] ? getPicturePath($value['artwork_img']) : '';
                return $value;
            };
            $return['data'] = array_map($getPath, $result['data']);
        }
        return response()->success($result);
    }

    public function commentsDelete(Request $request)
    {
        $comment_id = $request->comment_id ?? '';
        $user_id = $request->user['user_id'];
        if (empty($comment_id)) {
            return response()->fail(200224);
        }
        $update = [
            'is_delete' => 1
        ];
        $where = [
            'id' => $comment_id,
            'author_id' => $user_id,
        ];
        $model = new ArtworkComment();
        $comment_info = $model->where($where)->first();
        if (empty($comment_info)) {
            return response()->fail(200112);
        }
        if ($comment_info['author_id'] != $user_id) {
            return response()->fail(221004);
        }
        $artwork_id = $comment_info['artwork_id'];
        $result = $model->where($where)->update($update);
        if (!$result && $result !== 0) {
            return response()->fail(200111);
        }
        $orWhere = [
            'parent_id' => $comment_id,
        ];
        $result1 = $model->where($orWhere)->update($update);
        if (!$result1 && $result1 !== 0) {
            return response()->fail(221006);
        }
        $artwork = new Artwork();
        $condition = [
            'id' => $artwork_id
        ];
        $num = $result + $result1;
        $artwork->where($condition)->decrement('comment_num', $num);
        return response()->success([]);
    }

    public function commentsLike(Request $request)
    {
        $comment_id = $request->comment_id;
        $user_id = $request->user['user_id'];
        $db_name = $request->user['db'];
        if (empty($comment_id) || empty($user_id) || empty($db_name)) {
            return response()->fail(200001);
        }
        $where = ['id' => $comment_id];
        $result = DB::table('artwork_comment')->where($where)->first();
        if (empty($result)) {
            return response()->fail(221002);
        }
        $commentator_id = $result['author_id'] ?: 0;
        $artwork_id = $result['artwork_id'] ?: 0;
        DB::beginTransaction();
        DB::connection($db_name)->beginTransaction();
        try {
            $where2 = [
                'artwork_comment_id' => $comment_id,
                'user_id' => $user_id,
            ];
            $res = DB::connection($db_name)->table('artwork_comment_like')->where($where2)->first();

            if ($res) {
                if (is_object($res)) {
                    $res = $res->toArray();
                }
                if ($res['is_delete'] == 1) {
                    $update = ['is_delete' => 0];
                    $status = 1;
                    $result2 = DB::table('artwork_comment')->where($where)->increment('like_num', 1);
                } else {
                    $update = ['is_delete' => 1];
                    $status = 0;
                    $result2 = DB::table('artwork_comment')->where($where)->decrement('like_num', 1);
                }
                $result1 = DB::connection($db_name)->table('artwork_comment_like')->where($where2)->update($update);
            } else {
                $insert = [
                    'artwork_id' => $artwork_id,
                    'artwork_comment_id' => $comment_id,
                    'user_id' => $user_id,
                    'commentator_id' => $commentator_id,       //评论人ID
                    'is_delete' => 0,       //这个条件也可以不写  默认就0
                    'created_at' => date('Y-m-d H:i:s', time())
                ];
                $update['is_delete'] = 0;
                $status = 1;
                $result1 = DB::connection($db_name)->table('artwork_comment_like')->insert($insert);
                $result2 = DB::table('artwork_comment')->where($where)->increment('like_num', 1);
            }

            if (!($result1 && $result2)) {
                throw new \Exception(200005);
            }

            //库数据提交
            DB::commit();
            DB::connection($db_name)->commit();

        } catch (\Exception $e) {
            //库数据回滚
            DB::rollback();
            DB::connection($db_name)->rollback();
            return response()->fail($e->getMessage());
        }
        return response()->success($status);
    }

    public function commentsDetail(Request $request)
    {
        $comment_id = $request->comment_id;
        if (empty($comment_id)) {
            return response()->fail(200224);
        }
        $api_token = $request->api_token;
        if (!empty($api_token)) {
            $login_info = (new UserIndex())->getUserInfoByToken($api_token);
            if (!is_array($login_info)) {
                return response()->fail($login_info);
            }
        }
        $field = ['id', 'artwork_id', 'author_id', 'parent_id', 'commentator_id', 'commentator_nickname', 'accepter_id',
            'accepter_nickname', 'comment_msg', 'like_num', 'created_at'];
        $where = [
            'id' => $comment_id,
            'is_delete' => 0,
            'is_examine' => 1,
            'parent_id' => 0,
        ];
        $return = (new ArtworkComment())->select($field)->where($where)->first();
        if (empty($return)) {
            return response()->success([]);
        }
        $return['commentator_avatar'] = getMemberAvatarByID($return['commentator_id'])
            ?: env('PICTURE_DEFAULT_USER', '');
        $return['is_like'] = 0;
        if (isset($login_info) && $login_info) {
            $artwork_comment_like = new ArtworkCommentLike();
            $artwork_comment_like->setConnection($login_info['db']);
            $where = [
                'artwork_comment_id' => $comment_id,
                'is_delete' => 0
            ];
            $result = $artwork_comment_like->where('user_id', $login_info['user_id'])
                ->where($where)
                ->first();
            if (!empty($result)) {
                $return['is_like'] = 1;
            }
            if ($login_info['user_id'] == $return['commentator_id']) {
                $return['is_follow'] = 2;//用户自己
            } else {
                $user_followModel = new UserFollow();
                $tmp = $user_followModel->getInfo([
                    'user_id' => $login_info['user_id'],
                    'follow_user_id' => $return['commentator_id'],
                    'is_delete' => 0
                ]);
                $return['is_follow'] = $tmp ? 1 : 0;
            }
        } else {
            $return['is_follow'] = 0;
        }
        $field = ['id', 'artwork_id', 'author_id', 'parent_id', 'commentator_id', 'commentator_nickname', 'accepter_id',
            'accepter_nickname', 'comment_msg', 'like_num', 'created_at'];
        $model = new ArtworkComment();
        $return['child'] = $model->getChildListById($comment_id, $field);   // 获取2级评论
        return response()->success($return);
    }

    public function pictureUpload(Request $request)
    {
        $picture = $request->picture;
        $user_info = $request->user;
        if (empty($picture) || !is_array($picture)) {
            return response()->fail(200224);
        }
        $oss = new OSS();
        $basePath = 'artwork/img' . $user_info['user_id'];
        $oss_url = env('PICTURE_UPLOAD_URL', 'http://testyinuo2.oss-cn-beijing.aliyuncs.com');
        foreach ($picture as $value) {
            if (!is_object($value) || !$value->isValid()) {
                if (is_string($value)) {
                    $pic_path[] = $value;
                } else {
                    return response()->fail(503001);
                }
            } else {
                $filename = $value->getClientOriginalName();
                if (empty($filename)) {
                    return response()->fail(503001);
                }
                $size = $value->getClientSize();
                if ($size > $this->pic_max) {
                    return response()->fail(503006);
                }
                $extension = strrchr($filename, '.');
                $PicObject = $basePath . '/' . rand(1000, 9999) . $extension;
                $pic_url = $oss->upload($value->getRealPath(), $PicObject);
                if ($pic_url) {
                    $pic_path[] = str_replace($oss_url, "", $pic_url);
                } else {
                    return response()->fail(200104);
                }
            }
        }
        return response()->success($pic_path);
    }
}
