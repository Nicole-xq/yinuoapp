<?php

namespace App\Http\Controllers\Api;

use App\Models\UserIndex;
use App\Models\Article;
use App\Models\ArticleComment;
use App\Models\ArticleVote;
use App\Models\ArticleLike;
use App\Models\ArticleCommentLike;
use Validator;
use App\Models\ArticleTitle;
use App\Models\ArticleClassify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    // 新闻ID
    const NEWS = 1;
    // 艺评 art evaluation
    const ART_E = 2;

    //用户认证上传图片大小限制 单位 M
    protected $pic_max = 2048 * 1024;

    public function __construct()
    {
        $this->middleware('verify.article');
        $this->middleware('verify.user.login', ['only' => [
            'articleVote', 'articleLike', 'articleCommentLike',
            ]
        ]);
    }

    /**
     * 文章详情 (可未登录)
     * @param int $id
     * @param int $author_id
     * @return mixed
     */
    public function newsInfo(Request $request)
    {
        $condition = ['is_delete' => 0,];
        $tmp_article_info = ArticleTitle::where($condition)->find($request->id);
        $api_token = $request->api_token;
        if (!empty($api_token)) {
            $login_info = (new UserIndex())->getUserInfoByToken($api_token);
            if (!is_array($login_info)) {
                return response()->fail($login_info);
            }
            $user_id = $login_info['user_id'];
        }
        if (empty($tmp_article_info) || $tmp_article_info['status'] != 1 && $tmp_article_info->author_id != $user_id) {
            return response()->fail(403021);
        }
        $user_index = new UserIndex();
        $author_info = $user_index->getUserInfoById($tmp_article_info->author_id);
        if (empty($author_info)) {
            return response()->success();
        }
        $db = $author_info['db'];
        $article_id = $request->id;

        $model_article = (new Article())->setConnection($db);

        $field = ['article_id', 'author_id', 'classify_id', 'author_name as editor_name', 'title', 'content',
            'click_num', 'vote_agree_num', 'vote_disagree_num', 'like_num', 'comment_num', 'is_allowspeak',
            'created_at', 'share_pic', 'share_description', 'pic as picture'];
        $result = $model_article->getInfo(['article_id' => $article_id], $field);
        if (empty($result)) {
            return response()->fail(200009);
        }
        $result->picture = json_decode($result->picture);
        $result->share_description = $result->share_description ?: '艺诺艺术是全球性艺术社交和投资保障平台，是300w+艺术爱好者的聚集地！';
//        $result->title = $result->title ?: '艺诺艺术-新闻';
        $result->share_pic = $result->share_pic ? getPicturePath($result->share_pic) : env('YN_LOGO');
        $result->author_nickname = $author_info['user_nickname'] ?: tellHide($author_info['user_name']);
        $result->author_avatar = getMemberAvatarByID($author_info['user_id']) . '?' . time();
        $result->share_url = env('SHARE_URL', '') . "/artReviewsDetailsIndexTop?id={$result->article_id}";
        $classify = ArticleClassify::where('id', $result['classify_id'])->lists('parent_id')->toArray();
        if (!empty($classify) && $classify[0] == self::ART_E) {
            $result->share_url = env('SHARE_URL', '') . "/artReviewsDetails?id={$result->article_id}";
            $is_vote = true;
            $result->vote = true;
        } else {
            $result->vote = false;
            $result->share_url = env('SHARE_URL', '') . "/artReviewsDetailsIndexTop?id={$result->article_id}";
        }

        if (isset($request->user['user_id']) && $request->user['user_id'] > 0) {
            $condition = [
                'article_id' => $article_id,
                'user_id' => $request->user['user_id'],
            ];

            if (isset($is_vote)) {
                $article_vote = (new ArticleVote())->setConnection($request->user['db']);
                $vote_info = $article_vote->getInfo($condition);
                if (!empty($vote_info)) {
                    $is_vote = $vote_info['is_vote'];
                }
            }


            $article_like = (new ArticleLike())->setConnection($request->user['db']);
            $condition['is_delete'] = 0;
            $like_info = $article_like->getInfo($condition);
            if (!empty($like_info)) {
                $is_like = 1;
            }
        }
        $result->is_like = $is_like ?? 0;
        if (isset($is_vote)) {
            if ($is_vote === true) {
                $is_vote = 0;
            }
            $result->is_vote = $is_vote;
        } else {
            unset($result->vote_agree_num);
            unset($result->vote_disagree_num);
        }

        $this->articleClick($article_id, $db);

        return response()->success($result);
    }

    /**
     * 文章阅读量自增 (可未登录)
     *
     * @param int $article_id
     * @param string $db
     * @return mixed
     */
    private function articleClick($article_id, $db)
    {
        DB::beginTransaction();
        DB::connection($db)->beginTransaction();
        try {
            $result1 = DB::table('article_title')->where('id', $article_id)->increment('click_num', 1);
            $result2 = DB::connection($db)->table('article')
                ->where('article_id', $article_id)
                ->increment('click_num', 1);

            if (!($result1 && $result2)) {
                throw new \Exception(json_encode(['result1' => $result1, 'result2' => $result2]));
            }

            DB::commit();
            DB::connection($db)->commit();

        } catch (\Exception $e) {
            // 3个库数据回滚
            DB::rollback();
            DB::connection($db)->rollback();
            Log::error('错误详情:阅读量---' . $e->getMessage() . '.');
        }
        return '';
    }

    /**
     * 文章点赞 (需登录)
     *
     * @param int $id
     * @return mixed
     */
    public function articleLike(Request $request)
    {
        $article_title = ArticleTitle::where('is_delete', 0)->find($request->id);
        if (empty($article_title)) {
            return response()->fail(200009);
        }

        $article_id = $request->id;
        $author_id = $article_title->author_id;
        $model_like = (new ArticleLike())->setConnection($request->user['db']);
        $condition = [
            'user_id' => $request->user['user_id'],
            'article_id' => $article_id,
        ];
        $article_like = $model_like->getInfo($condition);   // 查询点赞详情

        $author_db = UserIndex::where('user_id', $author_id)->lists('db')->toArray();   // 查询文章所在库
        $author_db = $author_db[0];

        // 3个库开启事物
        DB::beginTransaction();
        DB::connection($request->user['db'])->beginTransaction();
        if ($request->user['db'] == $author_db) {
            DB::connection($author_db)->beginTransaction();
        }
        try {
            $param = [
                'user_id' => $request->user['user_id'],
                'article_id' => $article_id,
            ];

            $where = ['id' => $article_id];

            $where2 = [
                'author_id' => $author_id,
                'article_id' => $article_id,
            ];

            if ($article_like) {
                $update = ['updated_at' => date('Y-m-d H:i:s', time())];
                if ($article_like->is_delete) {
                    $update['is_delete'] = 0;
                    $result1 = DB::table('article_title')->where($where)->increment('like_num', 1);
                    $result2 = DB::connection($author_db)->table('article')->where($where2)->increment('like_num', 1);
                } else {
                    $update['is_delete'] = 1;
                    $result1 = DB::table('article_title')->where($where)->decrement('like_num', 1);
                    $result2 = DB::connection($author_db)->table('article')->where($where2)->decrement('like_num', 1);
                }
                $result3 = DB::connection($request->user['db'])->table('article_like')->where($param)->Update($update);
            } else {
                $param['created_at'] = date('Y-m-d H:i:s', time());
                $result1 = DB::table('article_title')->where($where)->increment('like_num', 1);
                $result2 = DB::connection($author_db)->table('article')->where($where2)->increment('like_num', 1);
                $result3 = DB::connection($request->user['db'])->table('article_like')->insert($param);
            }

            if (!($result1 && $result2 && $result3)) {
                throw new \Exception(json_encode([
                    'result1' => $result1,
                    'result2' => $result2,
                    'result3' => $result3
                ]));
            }

            // 3个库数据提交
            if ($request->user['db'] == $author_db) {
                DB::connection($author_db)->commit();
            }
            DB::commit();
            DB::connection($request->user['db'])->commit();

        } catch (\Exception $e) {
            // 3个库数据回滚
            if ($request->user['db'] == $author_db) {
                DB::connection($author_db)->rollback();
            }
            DB::rollback();
            DB::connection($request->user['db'])->rollback();
            Log::error('错误详情:文章点赞-' . $e->getMessage() . '.');

            return response()->fail(200005);
        }

        return response()->success([]);
    }

    /**
     * 评论点赞 (需登录)
     *
     * @param int $id
     * @return mixed
     */
    public function articleCommentLike(Request $request)
    {
        // 判断评论状态
        $article_comment = ArticleComment::where('is_delete', 0)->find($request->id);

        if (empty($article_comment)) {
            return response()->fail(200010);
        }
        if ($article_comment->is_examine != 1) {
            return response()->fail(200014);
        }

        $model_like = (new ArticleCommentLike())->setConnection($request->user['db']);
        $param = [
            'user_id' => $request->user['user_id'],
            'article_id' => $article_comment->article_id,
            'commentator_id' => $article_comment->commentator_id,
            'article_comment_id' => $article_comment->id,
        ];
        $article_like = $model_like->getInfo($param);   // 查询点赞详情

        DB::beginTransaction();
        DB::connection($request->user['db'])->beginTransaction();
        try {

            $where = ['id' => $article_comment->id];

            if ($article_like) {
                $update = ['updated_at' => date('Y-m-d H:i:s', time())];
                if ($article_like->is_delete) {
                    $update['is_delete'] = 0;
                    $result1 = DB::table('article_comment')->where($where)->increment('like_num', 1);
                } else {
                    $update['is_delete'] = 1;
                    $result1 = DB::table('article_comment')->where($where)->decrement('like_num', 1);
                }
                $result2 = DB::connection($request->user['db'])->table('article_comment_like')
                    ->where($param)
                    ->Update($update);
            } else {
                $param['created_at'] = date('Y-m-d H:i:s', time());
                $result1 = DB::table('article_comment')->where($where)->increment('like_num', 1);
                $result2 = DB::connection($request->user['db'])->table('article_comment_like')->insert($param);
            }

            if (!($result1 && $result2)) {
                throw new \Exception(json_encode(['result1' => $result1, 'result2' => $result2]));
            }

            DB::commit();
            DB::connection($request->user['db'])->commit();

        } catch (\Exception $e) {
            DB::rollback();
            DB::connection($request->user['db'])->rollback();
            Log::error('错误详情: 评论点赞-' . $e->getMessage() . '.');

            return response()->fail(200005);
        }

        return response()->success([]);
    }


    /**
     * 文章投票 (需登录)
     *
     * @param int $id
     * @return mixed
     */
    public function articleVote(Request $request)
    {
        // 判断文章
        $article_title = ArticleTitle::where('is_delete', 0)->find($request->id);
        if (empty($article_title)) {
            return response()->fail(200009);
        }

        $article_id = $request->id;
        $author_id = $article_title->author_id;

        $model_vote = (new ArticleVote())->setConnection($request->user['db']);
        $condition = [
            'user_id' => $request->user['user_id'],
            'article_id' => $article_id,
        ];
        $article_vote = $model_vote->getInfo($condition);   // 查询点赞详情

        if ($article_vote) {
            return response()->success('您已经投过票了');
        }

        $author_db = UserIndex::where('user_id', $author_id)->lists('db')->toArray();   // 查询文章所在库
        $author_db = $author_db[0];

        // 3个库开启事物
        DB::beginTransaction();
        DB::connection($request->user['db'])->beginTransaction();
        if ($request->user['db'] == $author_db) {
            DB::connection($author_db)->beginTransaction();
        }
        try {
            $insert = [
                'user_id' => $request->user['user_id'],
                'article_id' => $article_id,
                'created_at' => date('Y-m-d H:i:s', time()),
            ];

            if ($request->state == 'agree') {
                $vote_key = 'vote_agree_num';
                $insert['is_vote'] = 1;
            } else {
                $vote_key = 'vote_disagree_num';
                $insert['is_vote'] = 2;
            }

            $where = ['id' => $article_id];
            $result1 = DB::table('article_title')->where($where)->increment($vote_key, 1);

            $where2 = ['author_id' => $author_id, 'article_id' => $article_id,];
            $result2 = DB::connection($author_db)->table('article')->where($where2)->increment($vote_key, 1);

            $result3 = DB::connection($request->user['db'])->table('article_vote')->insert($insert);


            if (!($result1 && $result2 && $result3)) {
                throw new \Exception(json_encode([
                    'result1' => $result1,
                    'result2' => $result2,
                    'result3' => $result3
                ]));
            }

            // 3个库数据提交
            if ($request->user['db'] == $author_db) {
                DB::connection($author_db)->commit();
            }
            DB::commit();
            DB::connection($request->user['db'])->commit();

        } catch (\Exception $e) {
            // 3个库数据回滚
            if ($request->user['db'] == $author_db) {
                DB::connection($author_db)->rollback();
            }
            DB::rollback();
            DB::connection($request->user['db'])->rollback();
            Log::error('错误详情:文章投票-' . $e->getMessage() . '.');

            return response()->fail(200005);
        }

        return response()->success([]);
    }

    public function commentList(Request $request)
    {
        $article_id = $request->id;
        if ((int)$article_id < 1) {
            return response()->fail(200004);
        }
        $api_token = $request->api_token;
        if (!empty($api_token)) {
            $login_info = (new UserIndex())->getUserInfoByToken($api_token);
            if (!is_array($login_info)) {
                return response()->fail($login_info);
            }
        }
        $model = new ArticleComment();
        $condition = [
            'article_id' => $article_id,
            'parent_id' => 0,
        ];
        $field = ['id', 'article_id', 'author_id', 'parent_id', 'commentator_id', 'commentator_nickname', 'accepter_id',
            'accepter_nickname', 'msg', 'like_num', 'created_at'];
        $result = $model->getList($condition, $field);
        if (empty($result)) {
            return response()->success([]);
        }
        $return = $result->toArray();
        $result = $return['data'];
        if (isset($login_info) && $login_info) {
            $article_comment_like = new ArticleCommentLike();
            $article_comment_like->setConnection($login_info['db']);
            $data = [];
            if (is_object($result)) {
                $data = $result->toArray();
            }
            if (isset($data['data'])) {
                $data = $data['data'];
            }
            $article_comment_id = array_column($data, 'id');
            $like_list = $article_comment_like->where('user_id', $login_info['user_id'])
                ->whereIn('article_comment_id', $article_comment_id)
                ->lists('is_delete', 'article_comment_id')
                ->toArray();
        }

        $author_ids = array_column($result, 'author_id');
        $avatar = getMemberAvatarByID($author_ids);

        $field = ['id', 'parent_id', 'commentator_id', 'commentator_nickname',
            'accepter_id', 'accepter_nickname', 'msg', 'created_at'];
        foreach ($result as &$item) {
            $is_like = 0;
            if (isset($like_list[$item['id']])) {
                $is_like = $like_list[$item['id']] == 1 ? 0 : 1;
            }
            $item['commentator_avatar'] = $avatar[$item['commentator_id']] ??  env('PICTURE_DEFAULT_USER', '');
            $item['is_like'] = $is_like;
            $item['child'] = $model->getChildListById($item['id'], $field) ?: [];   // 获取2级评论
        }
        $return['data'] = $result;
        return response()->success($return);
    }
}
