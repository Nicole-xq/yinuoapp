<?php
/**
 * Created by PhpStorm.
 * User: Hank
 * Date: 2019/3/24 0024
 * Time: 0:15
 */
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class ExpressController extends Controller
{
    /**
     * @SWG\Post(path="/api/express/allList",
     *   tags={"express"},
     *   operationId="/api/express/allList",
     *   summary="所有物流公司",
     *   description="所有物流公司",
     *   produces={"application/json"},
     *  @SWG\Response(response="200", description="successful operation",@SWG\Schema(type="array", @SWG\Items(ref="#/definitions/MyExpressResponse")))
     * )
     *
     */
    public function allList(){
        $allList = json_decode(application()->shopService->expressAllList());
        return $this->_json($allList);
    }

}