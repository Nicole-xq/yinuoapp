<?php

namespace App\Http\Controllers\Api;

use App\Libs\phpQuery\http;
use App\Libs\phpQuery\OSS;
use App\Libs\phpQuery\WXBizDataCrypt;
use App\Models\UserIndex;
use App\Models\UserToken;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use \App\Libs\phpQuery\sms as phoneSms;
use Mockery\Exception;

class LoginController extends Controller
{
    protected $db = array();
    protected $user_db = '';
    protected $db_num = 0;
    protected $files = null;
    protected $openid;
    protected $unionId = '';
    protected $error = '';

    public function __construct()
    {
        $db = env('DB_NEW_USER', '');
        if (!empty($db)) {
            $db = json_decode($db);
            $this->db = $db;
            $this->db_num = count($db);
        }
    }

    /**
     * @SWG\Post(path="/api/login",
     *   tags={"member"},
     *   operationId="/api/login",
     *   summary="登录",
     *   description="登录",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="username",
     *     in="formData",
     *     description="用户名或手机",
     *     required=true,
     *     type="string",
     *     default=""
     *   ),
     *   @SWG\Parameter(
     *     name="password",
     *     in="formData",
     *     description="密码",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="client",
     *     in="formData",
     *     description="客户端",
     *     required=true,
     *     type="string",
     *     default="android",
     *   ),
     *   @SWG\Response(response="200", description="successful operation"))
     * )
     * )
     */
    /**
     * 用户登录
     * @param $request
     * @return mixed
     */
    public function login(Request $request, $type = null)
    {
        switch ($type) {
            case 'wx':
                $result = $this->wx_login($request);
                break;
            case 'mp':
                $result = $this->mp_login($request);
                break;
            case 'captcha':
                $result = $this->captcha_login($request);
                break;
            case 'sina':
                $result = $this->sina_login($request);
                break;
            case 'qq':
                $result = $this->qq_login($request);
                break;
            default:
                $result = $this->passwordLogin($request);
        }
        if (isset($result['error'])) {
            return response()->fail($result['error']);
        }
        return response()->success($result);
    }


    /**
     * @SWG\Post(path="/api/login/mp",
     *   tags={"mp"},
     *   operationId="/api/login/mp",
     *   summary="小程序获取用户信息",
     *   description="小程序获取用户信息",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="iv",
     *     in="formData",
     *     description="iv",
     *     required=true,
     *     type="string",
     *     default=""
     *   ),
     *   @SWG\Parameter(
     *     name="code",
     *     in="formData",
     *     description="code",
     *     required=true,
     *     type="string",
     *   ),
     *     @SWG\Parameter(
     *     name="encryptedData",
     *     in="formData",
     *     description="encryptedData",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="client",
     *     in="formData",
     *     description="客户端",
     *     required=true,
     *     type="string",
     *     default="mp",
     *   ),
     *   @SWG\Response(response="200", description="successful operation",@SWG\Schema(ref="#/definitions/MpLoginResponse"))
     * )
     * )
     */
    public function mp_login($request)
    {
        //判断用户是否需要登录  1不需要 0需要
        $token = '';
        $validator = $this->loginCheck($request);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return ['error' => $errors->all()[0]];
        }
        //获取微信小程序授权用户信息
        $data = $this->getWxUserInfo($request);
        if (!empty($this->error)) {
            return ['error' => $this->error];
        }
        //判断用户是否第一次登陆
        $user_info = [];
        if (!empty($this->unionId)) {
            $user_index = new UserIndex();
            $condition = ['weixin_unionid' => $this->unionId];
            $user_info = $user_index->getUserInfo($condition);
        }
        $params = [
            'openid' => $this->openid,              //openid
            'unionid' => $this->unionId,            //unionid
            'nick_name' => $data['nickName'],       //用户昵称
            'avatar_url' => $data['avatarUrl'],     //用户头像
            'gender' => $data['gender'],            //用户性别
            'city' => $data['city'],                //城市
            'province' => $data['province'],        //省份
            'language' => $data['language']         //语言
        ];
        if (empty($params)) {
            return ['error' => 200224];
        }
        if (!$user_info) {
            //用户第一次登陆小程序 -----//用户保存信息数组
            $params['phone'] = '';
            $params['api_token'] = $token;
            return $params;
        } else {
            if ($user_info['is_disable']) {
                return ['error' => 300011];
            }
            // 获取token
            $token = $this->getUserToken($user_info, $request->get('client', ''));
            if (!$token) {
                return ['error' => 300010];
            }
            $invite_code = (new UserIndex())->getInviteCodeByKey($user_info['user_key']);
            $params['phone'] = $user_info['user_mobile'];      //用户手机号
            $params['api_token'] = $token;      //用户登录标识
            $params['invite_code'] = $invite_code;//用户邀请码
            return $params;
        }
    }

    public function loginCheck($request)
    {
        $rule = [
            'iv' => 'required',
            'code' => 'required',
            'encryptedData' => 'required',
        ];
        $message = [
            '*.required' => 200224
        ];
        return Validator::make($request->all(), $rule, $message);
    }



    public function getWxUserInfo($request)
    {
        $iv = $request->iv;
        $code = $request->code;
        $encryptedData = $request->encryptedData;
        $appId = env('AUCTION_APP_ID', 'wx1d23bb23f7189ebe');
        $appSecret = env('AUCTION_APP_SECRET', '28e655de831efbdad88c9c0313403bf6');
        $url = 'https://api.weixin.qq.com/sns/jscode2session?appid=';
        $url .= trim($appId) . '&secret=' . trim($appSecret) . '&grant_type=authorization_code&js_code=' . trim($code);
        //获取session_key 用于解密数据
        $grantInfo = http::get($url);
        $grantInfo = json_decode($grantInfo, true);
        if (empty($grantInfo['session_key'])) {
            $this->error = 200228;
            return false;
        }
        $sessionKey = $grantInfo['session_key'];
        $this->openid = $grantInfo['openid'];
        $pc = new WXBizDataCrypt($appId, $sessionKey);
        $errCode = $pc->decryptData($encryptedData, $iv, $data);

        if ($errCode != 0) {
            //解密后的用户信息
            $this->error = $errCode;
            return false;
        }
        if (!is_array($data)) {
            $data = json_decode($data, true);
        }
        if (isset($data['unionId'])) {
            $this->unionId = $data['unionId'];
        }
        $data['session_key'] = $sessionKey;
        return $data;
    }

    /**
     * 密码登录
     * @param Request $request
     * @return mixed
     */
    private function passwordLogin($request)
    {
        $state = $this->loginValidator($request);
        if (isset($state['error'])) {
            return $state;
        }

        $password = $request->password;
        $user_index = new UserIndex();
        $login_info = array();
        $login_info['user_name'] = $request->username;
        $login_info['user_passwd'] = $password;
        $member_info = $user_index->login($login_info); // 验证登录信息
        if (isset($member_info['error'])) {
            // 如果没有账号或者密码错误，调用UCenter
            if ($member_info['error'] == 300003 || $member_info['error'] == 300015) {
                $ucenter = $this->loginUCenter(['username' => $request->username]);
                if (isset($ucenter->status_code) && $ucenter->status_code == 200) {
                    $ucenter_info = $ucenter->data;
                    if ($member_info['error'] == 300003 && $ucenter_info->password == md5($password)) {
                        //修改密码
                        $result = $this->_password($ucenter_info->mobile, $ucenter_info->password);
                    }
                    if ($member_info['error'] == 300015) {
                        $result = $this->_addUser($ucenter_info);
                        if ($ucenter_info->password != md5($password)) {
                            return ['error' => 300003];
                        }
                    }
                    if (isset($result) && $result) {
                        $user_result = $user_index->login($login_info); // 验证登录信息
                        if (empty($user_result['error'])) {
                            $member_info = $user_result->toArray();
                        }
                    }
                }
            }
            if (isset($member_info['error'])) {
                return ['error' => $member_info['error']];
            }
        }

        $token = $this->getUserToken($member_info, $request->client);
        if ($token) {
            $invite_code = (new UserIndex())->getInviteCodeByKey($member_info['user_key']);
            $user_data = [
                'user_id' => $member_info['user_id'],
                'user_name' => $member_info['user_name'],
                'api_token' => $token,
                'invite_code' => $invite_code
            ];
            return $user_data;
        }
        return ['error' => 300010];
    }

    /**
     * 短信登录
     * @param $request
     * @return mixed
     */
    public function  captcha_login($request)
    {
        $state = $this->loginValidator($request);
        if (isset($state['error'])) {
            return $state;
        }

        $phone = $request->username;
        $captcha = $request->captcha;

        $state = (new phoneSms())->checkSmsCaptcha($phone, $captcha, 2);//再次进行验证码验证
        if ($state !== true) {
            return ['error' => 300005];
        }

        $user_index = new UserIndex();
        $user_index->captcha = true;
        $login_info = array();
        $login_info['user_name'] = $request->username;
        $member_info = $user_index->login($login_info); // 验证登录信息
        if (isset($member_info['error'])) {
            $ucenter = $this->loginUCenter(['username' => $request->username]);
            if (isset($ucenter->status_code) && $ucenter->status_code == 200) {
                $ucenter_info = $ucenter->data;
                $result = $this->_addUser($ucenter_info);
                if ($result) {
                    $user_result = $user_index->login($login_info); // 验证登录信息
                    if (empty($user_result['error'])) {
                        $member_info = $user_result->toArray();
                    }
                }
            }
            if (isset($member_info['error'])) {
                return ['error' => $member_info['error']];
            }
        }

        $token = $this->getUserToken($member_info, $request->client);
        if ($token) {
            $invite_code = (new UserIndex())->getInviteCodeByKey($member_info['user_key']);
            $user_data = [
                'user_id' => $member_info['user_id'],
                'user_name' => $member_info['user_name'],
                'api_token' => $token,
                'invite_code' => $invite_code
            ];
            return $user_data;
        }
        return ['error' => 300010];
    }

    /**
     * 微信登录
     * @param $request
     * @return mixed
     */
    private function wx_login($request)
    {
        $unionid = $request->get('unionid', '');
        $user_index = new UserIndex();
        $condition = ['weixin_unionid' => $unionid];
        $user = $user_index->getUserInfo($condition);

        if (empty($user)) {
            $ucenter = $user_index->loginUCenter($condition);
            if (isset($ucenter->status_code) && $ucenter->status_code == 200 && !empty($ucenter->data)) {
                $data = $ucenter->data;
                $condition = ['user_name' => $data->name];
                $user = $user_index->getUserInfo($condition);
                if (empty($user)) {
                    $result = $this->_addUser($data);
                } else {
                    $update = array();
                    $update['user_areainfo'] = $data->areainfo;
                    $update['weixin_unionid'] = $data->weixin_unionid;
                    $update['weixin_open_id'] = $data->weixin_open_id;
                    $result = $user_index->updateUser($condition, $update);
                }
                if ($result) {
                    $user = $user_index->getUserInfo($condition); // 验证登录信息
                }
            }
        }

        if (!empty($user)) { //会员信息存在时自动登录
            $user = $user->toArray();

            if ($user['is_disable']) {
                return ['error' => 300011];
            }
            // 获取token
            $token = $this->getUserToken($user, $request->get('client', ''));
            if (!$token) {
                return ['error' => 300010];
            }
            $invite_code = (new UserIndex())->getInviteCodeByKey($user['user_key']);
            $user_data = [
                'user_id' => $user['user_id'],
                'user_name' => $user['user_name'],
                'api_token' => $token,
                'invite_code' => $invite_code
            ];
            return $user_data;
        }

        return ['error' => 200052];
    }

    /**
     * 新浪登录
     * @param $request
     * @return mixed
     */
    private function sina_login($request)
    {
        $unionid = $request->get('unionid', '');
        $user_index = new UserIndex();
        $condition = ['sina_unionid' => $unionid];
        $user = $user_index->getUserInfo($condition);

        if (empty($user)) {
            $ucenter = $user_index->loginUCenter($condition);
            if (isset($ucenter->status_code) && $ucenter->status_code == 200 && !empty($ucenter->data)) {
                $data = $ucenter->data;
                $condition = ['user_name' => $data->name];
                $user = $user_index->getUserInfo($condition);
                if (empty($user)) {
                    $result = $this->_addUser($data);
                } else {
                    $update = array();
                    $update['sina_unionid'] = $data->sina_unionid;
                    $update['sina_info'] = $data->sina_info;
                    $result = $user_index->updateUser($condition, $update);
                }
                if ($result) {
                    $user = $user_index->getUserInfo($condition); // 验证登录信息
                }
            }
        }

        if (!empty($user)) { //会员信息存在时自动登录
            $user = $user->toArray();

            if ($user['is_disable']) {
                return ['error' => 300011];
            }
            // 获取token
            $token = $this->getUserToken($user, $request->get('client', ''));
            if (!$token) {
                return ['error' => 300010];
            }
            $invite_code = (new UserIndex())->getInviteCodeByKey($user['user_key']);
            $user_data = [
                'user_id' => $user['user_id'],
                'user_name' => $user['user_name'],
                'api_token' => $token,
                'invite_code' => $invite_code
            ];
            return $user_data;
        }

        return ['error' => 200052];
    }

    /**
     * QQ登录
     * @param $request
     * @return mixed
     */
    private function qq_login($request)
    {
        $unionid = $request->get('uid', '');
        $user_index = new UserIndex();
        $condition = ['qq_unionid' => $unionid];
        $user = $user_index->getUserInfo($condition);

        if (empty($user)) {
            $ucenter = $user_index->loginUCenter($condition);
            if (isset($ucenter->status_code) && $ucenter->status_code == 200 && !empty($ucenter->data)) {
                $data = $ucenter->data;
                $condition = ['user_name' => $data->name];
                $user = $user_index->getUserInfo($condition);
                if (empty($user)) {
                    $result = $this->_addUser($data);
                } else {
                    $update = array();
                    $update['qq_openid'] = $data->qq_openid;
                    $update['qq_info'] = $data->qq_info;
                    $result = $user_index->updateUser($condition, $update);
                }
                if ($result) {
                    $user = $user_index->getUserInfo($condition); // 验证登录信息
                }
            }
        }

        if (!empty($user)) { //会员信息存在时自动登录
            $user = $user->toArray();

            if ($user['is_disable']) {
                return ['error' => 300011];
            }
            // 获取token
            $token = $this->getUserToken($user, $request->get('client', ''));
            if (!$token) {
                return ['error' => 300010];
            }
            $invite_code = (new UserIndex())->getInviteCodeByKey($user['user_key']);
            $user_data = [
                'user_id' => $user['user_id'],
                'user_name' => $user['user_name'],
                'api_token' => $token,
                'invite_code' => $invite_code,
            ];
            return $user_data;
        }

        return ['error' => 200052];
    }

    /**
     * 微信注册
     * @param
     * @return mixed
     */
    private function wx_register($request)
    {
        $state = $this->OAuthRegisterValidator($request); // 参数验证
        if (isset($state['error'])) {
            return $state;
        }
        $phone = $request->phone;
        $captcha = $request->captcha;
        $disCode = $request->invite_code;
        $client = $request->client ?? '';
        $registered_source = isset($request->client) ? $request->client : '';
        $source_staff_id = isset($request->source_staff_id) ? $request->source_staff_id : 0;
        // 验证码校验
        $state = (new phoneSms())->checkSmsCaptcha($phone, $captcha, 1);
        if ($state !== true) {
            return ['error' => 300005];
        }

        // 检查是否已注册
        $user_index = new UserIndex();
        $user = $user_index->getUserInfo(['user_mobile' => $phone]);
        if (empty($user)) {
            $user = $user_index->getUserInfo(['user_name' => $phone]);
        }
        $user_key = '';
        $avatar = '';
        $nickname = '';
        if (!empty($user)) {
            $user_key = $user->user_key;
            if ($user->weixin_unionid) {
                return ['error' => 300017];
            }
            if (empty($user->user_avatar)) {
                $avatar = $request->avatar;
            } else {
                $avatar = $user->user_avatar;
            }
            if (empty($user->user_nickname)) {
                $nickname = $request->nickname;
            } else {
                $nickname = $user->user_nickname;
            }
        }

        $key = '';
        $ucenter = $user_index->loginUCenter(['username' => $phone]);
        if (isset($ucenter->status_code) && $ucenter->status_code == 200 && !empty($ucenter->data)) {
            $user = $ucenter->data;
            $key = $user->key;
            if ($user->weixin_unionid) {
                return ['error' => 300017];
            }
            if (empty($user->avatar)) {
                $avatar = $request->avatar;
            } else {
                $avatar = $user->avatar;
            }
            if (empty($user->nickname)) {
                $nickname = $request->nickname;
            } else {
                $nickname = $user->nickname;
            }
        }

        $weixinData = [
            'weixin_unionid' => $request->unionid,
            'weixin_avatar' => $request->avatar
            ];
        if ($client != 'wxMp') {
            $weixinData['weixin_open_id'] = $request->openid;
        }
        $weixin_info = json_encode($weixinData);

        if (!empty($user)) {
            $update = array();
            $update['weixin_unionid'] = $request->unionid;
            $update['weixin_open_id'] = $request->openid;
            $update['weixin_info'] = $weixin_info;
            if (empty($user_key)) {
                //同步用户
                $user->weixin_unionid = $request->unionid;
                if ($client != 'wxMp') {
                    $user->weixin_open_id = $request->openid;
                }
                $user->weixin_info = $weixin_info;
                $user->avatar = $avatar;
                $user->nickname = $nickname;
                $this->_addUser($user);
                $update['key'] = $key;
            } else {
                $update2 = $update;
                $update2['user_avatar'] = $avatar;
                $update2['user_nickname'] = $nickname;
                //更新用户 绑定微信操作
                $condition = ['user_mobile' => $phone];
                $user_index->updateUser($condition, $update2);
                $update['key'] = $user_key;
            }
            // 绑定UCenter，如果没有成功，下次登录会继续绑定
            $update['avatar'] = $avatar;
            $update['nickname'] = $nickname;
            $this->updateUCenter($update);
        } else {
            $datetime = date('Y-m-d H:i:s', time());
            $member = array();
            if (!empty($registered_source)) {
                $member['registered_source'] = $registered_source;
            }
            if (!empty($source_staff_id)) {
                $member['source_staff_id'] = $source_staff_id;
            }
            $member['user_name'] = $phone;
            $member['user_mobile'] = $phone;
            $member['user_mobile_bind'] = 1;
            $member['user_sex'] = $request->sex;
            $member['user_avatar'] = $request->avatar;
            $member['user_nickname'] = $request->nickname;
            $member['weixin_unionid'] = $request->unionid;
            if ($client != 'wxMp') {
                $member['weixin_open_id'] = $request->openid;
            }
            $member['weixin_info'] = $weixin_info;
            $member['created_at'] = $datetime;
            $member['updated_at'] = $datetime;

            $user_id = $this->_register($member, '', true);
            if (!$user_id) {
                return ['error' => 300002];
            }
        }

        $member_info = $user_index->getUserInfo(['user_name' => $phone]);
        if (!empty($member_info)) {
            $member_info = $member_info->toArray();
        }
        if ($member_info['is_disable']) {
            return ['error' => 300011];
        }
        $token = $this->getUserToken($member_info, $request->client);
        $this->createRelation($token, $disCode);
        if ($token) {
            $user_data = [
                'user_id' => $member_info['user_id'],
                'user_name' => $member_info['user_name'],
                'api_token' => $token,
                'phone' => $phone
            ];
            return $user_data;
        } else {
            return ['error' => 300008];
        }
    }

    /**
     * 新浪注册
     * @param
     * @return mixed
     */
    private function sina_register($request)
    {
        // 参数验证
        $state = $this->OAuthRegisterValidator($request);
        if (isset($state['error'])) {
            return $state;
        }
        $phone = $request->phone;
        $captcha = $request->captcha;
        $disCode = $request->invite_code;
        $registered_source = isset($request->client) ? $request->client : '';
        $source_staff_id = isset($request->source_staff_id) ? $request->source_staff_id : 0;
        // 验证码校验
        $state = (new phoneSms())->checkSmsCaptcha($phone, $captcha, 1);
        if ($state !== true) {
            return ['error' => 300005];
        }

        $user_index = new UserIndex();
        // 检查手机号是否已注册
        $user = $user_index->getUserInfo(['user_mobile' => $phone]);
        if (empty($user)) {
            $user = $user_index->getUserInfo(['user_name' => $phone]);
        }
        $user_key = '';
        $avatar = '';
        $nickname = '';
        if (!empty($user)) {
            // 如果已注册，绑定用户，否则添加用户
            $user_key = $user->user_key;
            if ($user->sina_unionid) {
                return ['error' => 300017];
            }
            if (empty($user->user_avatar)) {
                $avatar = $request->avatar;
            } else {
                $avatar = $user->user_avatar;
            }
            if (empty($user->user_nickname)) {
                $nickname = $request->nickname;
            } else {
                $nickname = $user->user_nickname;
            }
        }

        // 校验UCenter
        $key = '';
        $ucenter = $user_index->loginUCenter(['username' => $phone]);
        if (isset($ucenter->status_code) && $ucenter->status_code == 200 && !empty($ucenter->data)) {
            // 请求成功，且有用户
            $user = $ucenter->data;
            $key = $user->key;
            if ($user->sina_unionid) {
                return ['error' => 300017];
            }
            if (empty($user->avatar)) {
                $avatar = $request->avatar;
            } else {
                $avatar = $user->avatar;
            }
            if (empty($user->nickname)) {
                $nickname = $request->nickname;
            } else {
                $nickname = $user->nickname;
            }
        }

        $sina_unionid = $request->unionid;
        $sina_info = json_encode([
            'sina_unionid' => $sina_unionid,
            'sina_avatar' => $request->avatar,
            'sina_sex' => $request->sex,
        ]);
        if (!empty($user)) {
            $update = array();
            $update['sina_unionid'] = $sina_unionid;
            $update['sina_info'] = $sina_info;
            if (empty($user_key)) {
                //同步用户
                $user->sina_unionid = $sina_unionid;
                $user->sina_info = $sina_info;
                $user->avatar = $avatar;
                $user->nickname = $nickname;
                $this->_addUser($user);
                $update['key'] = $key;
            } else {
                $update2 = $update;
                $update2['user_avatar'] = $avatar;
                $update2['user_nickname'] = $nickname;
                //更新用户 绑定新浪操作
                $condition = ['user_key' => $user_key];
                $user_index->updateUser($condition, $update2);
                $update['key'] = $user_key;
            }
            $update['avatar'] = $avatar;
            $update['nickname'] = $nickname;
            // 绑定UCenter，如果没有成功，下次登录会继续绑定
            $this->updateUCenter($update);
        } else {
            $datetime = date('Y-m-d H:i:s', time());
            $member = array();
            if (!empty($registered_source)) {
                $member['registered_source'] = $registered_source;
            }
            if (!empty($source_staff_id)) {
                $member['source_staff_id'] = $source_staff_id;
            }
            $member['user_name'] = $phone;
            $member['user_mobile'] = $phone;
            $member['user_mobile_bind'] = 1;
            $member['user_sex'] = $request->sex;
            $member['user_avatar'] = $request->avatar;
            $member['user_nickname'] = $request->nickname;
            $member['sina_unionid'] = $request->unionid;
            $member['sina_info'] = $sina_info;
            $member['created_at'] = $datetime;
            $member['updated_at'] = $datetime;

            $user_id = $this->_register($member, '', true);
            if (!$user_id) {
                return ['error' => 300002];
            }
        }

        $member_info = $user_index->getUserInfo(['user_name' => $phone]);
        if (!empty($member_info)) {
            $member_info = $member_info->toArray();
        }
        if ($member_info['is_disable']) {
            return ['error' => 300011];
        }
        $token = $this->getUserToken($member_info, $request->client);
        $this->createRelation($token, $disCode);
        if ($token) {
            $user_data = [
                'user_id' => $member_info['user_id'],
                'user_name' => $member_info['user_name'],
                'api_token' => $token,
            ];
            return $user_data;
        } else {
            return ['error' => 300008];
        }
    }

    /**
     * QQ注册
     * @param
     * @return mixed
     */
    private function qq_register($request)
    {
        // 参数验证
        $state = $this->OAuthRegisterValidator($request);
        if (isset($state['error'])) {
            return $state;
        }

        $phone = $request->phone;
        $captcha = $request->captcha;
        $disCode = $request->invite_code;
        $registered_source = isset($request->client) ? $request->client : '';
        $source_staff_id = isset($request->source_staff_id) ? $request->source_staff_id : 0;
        // 验证码校验
        $state = (new phoneSms())->checkSmsCaptcha($phone, $captcha, 1);
        if ($state !== true) {
            return ['error' => 300005];
        }

        $user_index = new UserIndex();
        // 检查手机号是否已注册
        $user = $user_index->getUserInfo(['user_mobile' => $phone]);
        if (empty($user)) {
            $user = $user_index->getUserInfo(['user_name' => $phone]);
        }
        $user_key = '';
        $avatar = '';
        $nickname = '';
        if (!empty($user)) {
            // 如果已注册，绑定用户，否则添加用户
            $user_key = $user->user_key;
            if ($user->qq_unionid) {
                return ['error' => 300017];
            }
            if (empty($user->user_avatar)) {
                $avatar = $request->avatar;
            } else {
                $avatar = $user->user_avatar;
            }
            if (empty($user->user_nickname)) {
                $nickname = $request->nickname;
            } else {
                $nickname = $user->user_nickname;
            }
        }

        // 校验UCenter
        $key = '';
        $ucenter = $user_index->loginUCenter(['username' => $phone]);
        if (isset($ucenter->status_code) && $ucenter->status_code == 200 && !empty($ucenter->data)) {
            // 请求成功，且有用户
            $user = $ucenter->data;
            $key = $user->key;
            if ($user->qq_unionid) {
                return ['error' => 300017];
            }
            if (empty($user->avatar)) {
                $avatar = $request->avatar;
            } else {
                $avatar = $user->avatar;
            }
            if (empty($user->nickname)) {
                $nickname = $request->nickname;
            } else {
                $nickname = $user->nickname;
            }
        }

        $qq_unionid = $request->uid;
        $qq_info = json_encode([
            'qq_unionid' => $qq_unionid,
            'qq_openid' => $request->openid,
            'qq_avatar' => $request->avatar,
            'qq_sex' => $request->sex,
        ]);

        if (!empty($user)) {
            $update = array();
            $update['qq_unionid'] = $qq_unionid;
            $update['qq_info'] = $qq_info;
            if (empty($user_key)) {
                //同步用户
                $user->qq_unionid = $qq_unionid;
                $user->qq_info = $qq_info;
                $user->avatar = $avatar;
                $user->nickname = $nickname;
                $this->_addUser($user);
                $update['key'] = $key;
            } else {
                $update2 = $update;
                $update2['user_avatar'] = $avatar;
                $update2['user_nickname'] = $nickname;
                //更新用户 绑定QQ操作
                $condition = ['user_mobile' => $phone];
                $user_index->updateUser($condition, $update2);
                $update['key'] = $user_key;
            }
            // 绑定UCenter，如果没有成功，下次登录会继续绑定
            $update['avatar'] = $avatar;
            $update['nickname'] = $nickname;
            $this->updateUCenter($update);
        } else {
            $datetime = date('Y-m-d H:i:s', time());
            $member = array();
            if (!empty($registered_source)) {
                $member['registered_source'] = $registered_source;
            }
            if (!empty($source_staff_id)) {
                $member['source_staff_id'] = $source_staff_id;
            }
            $member['user_name'] = $phone;
            $member['user_mobile'] = $phone;
            $member['user_mobile_bind'] = 1;
            $member['user_sex'] = $request->sex;
            $member['user_avatar'] = $request->avatar;
            $member['user_nickname'] = $request->nickname;
            $member['qq_unionid'] = $qq_unionid;
            $member['qq_info'] = $qq_info;
            $member['created_at'] = $datetime;
            $member['updated_at'] = $datetime;

            $user_id = $this->_register($member, '', true);
            if (!$user_id) {
                return ['error' => 300002];
            }
        }

        $member_info = $user_index->getUserInfo(['user_name' => $phone]);
        if (!empty($member_info)) {
            $member_info = $member_info->toArray();
        }
        if ($member_info['is_disable']) {
            return ['error' => 300011];
        }
        $token = $this->getUserToken($member_info, $request->client);
        $this->createRelation($token, $disCode);
        if ($token) {
            $user_data = [
                'user_id' => $member_info['user_id'],
                'user_name' => $member_info['user_name'],
                'api_token' => $token,
            ];
            return $user_data;
        } else {
            return ['error' => 300008];
        }
    }

    public function captcha_register($request)
    {
        $state = $this->registerValidator($request); // 参数验证
        if (isset($state['error'])) {
            return ['error' => $state['error']];
        }

        $phone = $request->phone;
        $captcha = $request->captcha;
        $disCode = $request->invite_code;
        $registered_source = isset($request->client) ? $request->client : '';
        $source_staff_id = isset($request->source_staff_id) ? $request->source_staff_id : 0;

        $state = (new phoneSms())->checkSmsCaptcha($phone, $captcha, 1);//再次进行验证码验证
        if ($state !== true) {
            return ['error' => 300005];
        }

        $user_index = UserIndex::where('user_mobile', $phone)->orWhere('user_name', $phone)->first();
        if (!empty($user_index)) {//检查手机号是否已被注册
            return ['error' => 300006];
        }

        $user_center = $this->loginUCenter(['username' => $phone]);
        if (isset($user_center->status_code) && $user_center->status_code == 200) {
            return ['error' => 300006];
        }

        $datetime = date('Y-m-d H:i:s', time());
        $member = array();
        if (!empty($registered_source)) {
            $member['registered_source'] = $registered_source;
        }
        if (!empty($source_staff_id)) {
            $member['source_staff_id'] = $source_staff_id;
        }
        $member['user_name'] = $phone;
        $member['user_passwd'] = md5(time() . rand(10000, 99999));
        $member['user_mobile'] = $phone;
        $member['user_mobile_bind'] = 1;
        $member['user_nickname'] = $request->user_nickname;
        $member['user_areaid'] = $request->areaid;
        $member['user_cityid'] = $request->cityid;
        $member['user_provinceid'] = $request->provinceid;
        $member['user_areainfo'] = $request->areainfo;
        $member['created_at'] = $datetime;
        $member['updated_at'] = $datetime;
        if (empty($member['user_nickname'])) {
            $member['user_nickname'] = '匿名用户';
        }
        $user_id = $this->_register($member, $request->password, true);
        if (!$user_id) {
            return ['error' => 300002];
        }

        $member_info = UserIndex::find($user_id)->toArray();
        $token = $this->getUserToken($member_info, $request->client);
        $this->createRelation($token, $disCode);
        if ($token) {
            $user_data = [
                'user_id' => $member_info['user_id'],
                'user_name' => $member_info['user_name'],
                'api_token' => $token,
            ];
            return $user_data;
        } else {
            return ['error' => 300008];
        }
    }

    /**
     * 用户注册
     * @param $request
     * @return mixed
     */
    public function register(Request $request, $type = null)
    {
        switch ($type) {
            case 'wx':
                $user_data = $this->wx_register($request);
                break;
            case 'captcha':
                $user_data = $this->captcha_register($request);
                break;
            case 'sina':
                $user_data = $this->sina_register($request);
                break;
            case 'qq':
                $user_data = $this->qq_register($request);
                break;
            default:
                $type = null;
        }

        if ($type) {
            if (empty($user_data['error'])) {
                return response()->success($user_data);
            }
            return response()->fail($user_data['error']);
        }
        $state = $this->registerValidator($request); // 参数验证
        if (isset($state['error'])) {
            return response()->fail($state['error']);
        }

        $phone = $request->phone;
        $captcha = $request->captcha;
        $disCode = $request->invite_code;
        $registered_source = isset($request->client) ? $request->client : '';
        $source_staff_id = isset($request->source_staff_id) ? $request->source_staff_id : 0;

        $state = (new phoneSms())->checkSmsCaptcha($phone, $captcha, 1);//再次进行验证码验证
        if ($state !== true) {
            return response()->fail(300005);
        }

        $user_index = UserIndex::where('user_mobile', $phone)->orWhere('user_name', $phone)->first();
        if (!empty($user_index)) {//检查手机号是否已被注册
            return response()->fail(300006);
        }

        $user_center = $this->loginUCenter(['username' => $phone]);
        if (isset($user_center->status_code) && $user_center->status_code == 200) {
            return response()->fail(300006);
        }

        //图片接收
        $pic = $request->file('user_avatar');
        //图片验证
        if (!empty($pic) && $pic->isValid()) {
            //本地临时文件名
            $this->files = $pic->getRealPath();
        }

        $datetime = date('Y-m-d H:i:s', time());
        $member = array();
        if (!empty($registered_source)) {
            $member['registered_source'] = $registered_source;
        }
        if (!empty($source_staff_id)) {
            $member['source_staff_id'] = $source_staff_id;
        }
        $member['user_name'] = $phone;
        $member['user_passwd'] = md5(trim($request->password));
        $member['user_mobile'] = $phone;
        $member['user_mobile_bind'] = 1;
        $member['user_nickname'] = $request->user_nickname;
        //$member['user_areaid'] = $request->areaid;
        //$member['user_cityid'] = $request->cityid;
        //$member['user_provinceid'] = $request->provinceid;
        //$member['user_areainfo'] = $request->areainfo;
        $member['created_at'] = $datetime;
        $member['updated_at'] = $datetime;
        if (empty($member['user_nickname'])) {
            $member['user_nickname'] = '匿名用户';
        }
        $user_id = $this->_register($member, $request->password, true);
        if (!$user_id) {
            return response()->fail(300002);
        }

        $member_info = UserIndex::find($user_id)->toArray();
        $token = $this->getUserToken($member_info, $request->client);
        $this->createRelation($token, $disCode);
        if ($token) {
            $user_data = [
                'user_id' => $member_info['user_id'],
                'user_name' => $member_info['user_name'],
                'api_token' => $token,
            ];
            return response()->success($user_data);
        } else {
            return response()->fail(300008);
        }
    }

    // 同步账号信息
    private function _addUser($ucenter_info)
    {
        //添加账号
        $datetime = date('Y-m-d H:i:s', time());
        $member = array();
        $member['user_key'] = $ucenter_info->key;
        $member['user_name'] = $ucenter_info->name;
        $member['user_passwd'] = $ucenter_info->password;
        $member['user_mobile'] = $ucenter_info->mobile;
        $member['user_mobile_bind'] = empty($ucenter_info->mobile) ? 0 : 1;
        $member['user_avatar'] = $ucenter_info->avatar;
        $member['user_nickname'] = $ucenter_info->nickname;
        $member['user_areaid'] = $ucenter_info->areaid ?? '';
        $member['user_cityid'] = $ucenter_info->cityid ?? '';
        $member['user_provinceid'] = $ucenter_info->provinceid ?? '';
        $member['user_areainfo'] = $ucenter_info->areainfo ?? '';
        $member['weixin_unionid'] = $ucenter_info->weixin_unionid ?: '';
        $member['weixin_open_id'] = $ucenter_info->weixin_open_id ?: '';
        $member['weixin_info'] = $ucenter_info->weixin_info ?: '';
        $member['sina_unionid'] = $ucenter_info->sina_unionid ?: '';
        $member['sina_info'] = $ucenter_info->sina_info ?: '';
        $member['qq_unionid'] = $ucenter_info->qq_unionid ?: '';
        $member['qq_info'] = $ucenter_info->qq_info ?: '';
        $member['created_at'] = $datetime;
        $member['updated_at'] = $datetime;

        return $this->_register($member);
    }


    /*
     * 注册用户绑定上下级关系
     */
    public function createRelation($token, $disCode = '')
    {
        $shop_url = env('SHOP_URL', '');
        if (empty($shop_url)) {
            return response()->fail(200227);
        }
        $post_data = ['dis_code' => $disCode];
        $invite_url = $shop_url . '/mobile/index.php?act=login&op=createLevelRelation&key=' . $token;
        return http::post($invite_url, $post_data);
    }

    /**
     * 注册入库
     *
     * @param array $member
     * @param string $password
     * @return int
     * */
    private function _register($member, $password = '', $is_ucenter = false)
    {
        $user_index = new UserIndex();
        // 索引库开启事物
        DB::beginTransaction();
        try {
            $user_id = DB::table('user_index')->insertGetId($member);
            if (!$user_id) {
                throw new Exception('user_index表，会员信息保存失败');
            }

            $user_avatar = $member['user_avatar'] ?? '';
            $update = array();
            if ($this->files) {
                //头像上传
                $oss = new OSS();
                //目标文件名
                $object = $oss->get_object_name($user_id);
                $user_avatar = $oss->upload($this->files, $object);
                if ($user_avatar == false) {
                    throw new Exception('图片上传失败');
                }
                $update['user_avatar'] = $user_avatar;
            }
            if ($this->db_num) {
                $this->user_db = $this->db[$user_id % $this->db_num];
                $update['db'] = $this->user_db;
            }
            if ($update) {
                $result2 = DB::table('user_index')->where('user_id', $user_id)->update($update);
                if (!$result2) {
                    throw new Exception('更新db和头像失败');
                }
            }

            if ($this->db_num) {
                $member['user_id'] = $user_id;
                DB::connection($this->user_db)->beginTransaction(); // 开启分库事务
                $result3 = DB::connection($this->user_db)->table('users')->insert($member);
                if (!$result3) {
                    throw new Exception('分库添加失败');
                }
            }

            if ($is_ucenter) {
                // 注册UCenter
                $param = array();
                $param['registered_source'] = $member['registered_source'];
                if (!empty($registered_source)) {
                    $param['registered_source'] = $member['registered_source'];
                }
                if (!empty($source_staff_id)) {
                    $param['source_staff_id'] = $member['source_staff_id'];
                }
                $param['username'] = $member['user_name'];
                $param['password'] = $password ?: '';
                $param['sex'] = $member['user_sex'] ?? '';
                $param['avatar'] = $user_avatar;
                $param['nickname'] = $member['user_nickname'] ?? '';
                $param['provinceid'] = $member['user_provinceid'] ?? '';
                $param['cityid'] = $member['user_cityid'] ?? '';
                $param['areaid'] = $member['user_areaid'] ?? '';
                $param['areainfo'] = $member['user_areainfo'] ?? '';
                $param['weixin_unionid'] = $member['weixin_unionid'] ?? '';
                $param['weixin_open_id'] = $member['weixin_open_id'] ?? '';
                $param['weixin_info'] = $member['weixin_info'] ?? '';
                $param['sina_unionid'] = $member['sina_unionid'] ?? '';
                $param['sina_info'] = $member['sina_info'] ?? '';
                $param['qq_unionid'] = $member['qq_unionid'] ?? '';
                $param['qq_info'] = $member['qq_info'] ?? '';
                $param['created_at'] = $member['created_at'] ?: '';
                $param['updated_at'] = $member['updated_at'] ?: '';

                $ucenter = $this->registerUCenter($param);
                if (isset($ucenter->status_code) && $ucenter->status_code == 200) {
                    $key = $ucenter->data->key;
                    $result4 = $user_index->updateUser(['user_id' => $user_id], ['user_key' => $key]);
                    if (!$result4) {
                        throw new Exception('添加失败');
                    }
                } else {
                    if (isset($ucenter->status_code)) {
                        throw new Exception($ucenter->datas->error);
                    } else {
                        throw new Exception('UCenter注册失败');
                    }
                }
            }

            DB::commit();
            if ($this->db_num) {
                DB::connection($this->user_db)->commit();
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($this->db_num) {
                DB::connection($this->user_db)->rollback();
            }
            Log::error('错误详情:用户注册-' . $e->getMessage() . '.');
            return 0;
        }
        return $user_id;
    }

    /**
     * 修改密码
     * @param $request
     * @return mixed
     */
    public function password(Request $request)
    {
        $state = $this->passwordValidator($request);
        if (isset($state['error'])) {
            return response()->fail($state['error']);
        }
        $phone = $request->phone;
        $captcha = $request->captcha;
        $state = (new phoneSms())->checkSmsCaptcha($phone, $captcha, 3);//再次进行验证码验证
        if ($state !== true) {
            return response()->fail(300005);
        }
        $result = $this->_password($phone, $request->password, true);
        if (!empty($result['error'])) {
            return response()->fail($result['error']);
        }
        return response()->success();
    }

    private function _password($phone, $password, $is_ucenter = false)
    {
        if ($is_ucenter) {
            $new_password = md5(trim($password));
        } else {
            $new_password = $password;
        }
        $user_index = UserIndex::where('user_mobile', $phone)->first();
        if (empty($user_index)) {
            return ['error' => 300012];
        }
        // 如果与原密码相同，直接返回修改成功
        if ($user_index->user_passwd == $new_password) {
            return true;
        }
        DB::beginTransaction();
        try {
            $result = DB::table('user_index')->where('user_mobile', $phone)->update(['user_passwd' => $new_password]);
            if (!$result) {
                throw new Exception('user_index密码修改失败！');
            }

            if ($is_ucenter) {
                // 修改UCenter
                $param = array(
                    'key' => $user_index['user_key'],
                    'password' => $password,
                );
                $result = $this->passwordUCenter($param);
                if (!$result) {
                    throw new Exception('UCenter密码修改失败！');
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            Log::error('错误详情:修改密码---' . $e->getMessage() . '.');
            return ['error' => 300002];
        }
        return true;
    }

    /**
     * 获取移动端登录令牌
     * @param array $member
     * @param string $client
     * @return mixed
     */
    public function getUserToken($member, $client)
    {
        $mb_user_token_info = array();
        $token = md5($member['user_name'] . strval(time()) . strval(rand(0, 999999)));
        $mb_user_token_info['user_id'] = $member['user_id'];
        $mb_user_token_info['user_name'] = $member['user_name'];
        $mb_user_token_info['token'] = $token;
        $mb_user_token_info['login_time'] = date('Y-m-d H:i:s', time());
        $mb_user_token_info['client_type'] = $client;

        $user_token = new UserToken();
        $result = $user_token->add($mb_user_token_info);
        if ($result) {
            $update_info = array(
                'user_login_num' => ($member['user_login_num'] + 1),
                'user_login_time' => time(),
                'user_old_login_time' => $member['user_login_time'],
                'user_login_ip' => getIp(),
                'user_old_login_ip' => $member['user_login_ip']
            );

            // 索引库开启事物
            DB::beginTransaction();
            DB::connection($member['db'])->beginTransaction();
            try {
                $result1 = DB::table('user_index')->where('user_id', $member['user_id'])->update($update_info);
                if (!$result1) {
                    throw new Exception(json_encode(['result1' => $result1]));
                }

                $result2 = DB::connection($member['db'])->table('users')->where('user_id', $member['user_id'])->update($update_info);
                if (!$result2) {
                    throw new Exception(json_encode(['result1' => $result1, 'result2' => $result2,]));
                }

                DB::commit();
                DB::connection($member['db'])->commit();

            } catch (Exception $e) {
                DB::rollback();
                DB::connection($member['db'])->rollback();
                Log::error('错误详情:获取token-' . $e->getMessage() . '.');

                return false;
            }
            return $token;
        }
        return false;
    }

    /**
     * 发送短信验证码
     * @param $request ->phone 手机号
     * @param $request ->type 类型
     * @return mixed
     */
    public function sendCaptcha(Request $request)
    {
        $type_arr = [
            'register' => 1,
            'login' => 2,
            'reset_pwd' => 3,
        ];
        $type = $type_arr[$request->type];
        if ($type == 3) {
            $user_index = new UserIndex();
            $user_info = $user_index->getUserInfo(['user_mobile' => $request->phone]);
            if (empty($user_info)) {
                $ucenter = $this->loginUCenter(['username' => $request->phone]);
                if (isset($ucenter->status_code) && $ucenter->status_code == 300015) {
                    return response()->fail(300012);
                }
            }
        }
        $state = (new phoneSms())->sendCaptcha($request->phone, $type);

        if ($state === true) {
            return response()->success(null);
        }

        if (!is_int($state)) {
            $state = 200005;
        }
        return response()->fail($state);
    }

    // 注册参数验证
    private function registerValidator($request)
    {
        $rules = [
            'phone' => 'required|regex:/^1[0-9]{10}$/',
            'captcha' => 'required|regex:/^[0-9]+$/',
            'password' => 'required|min:6|max:80',
            //'areaid' => 'required|regex:/^[0-9]+$/',
            //'cityid' => 'required|regex:/^[0-9]+$/',
            //'provinceid' => 'required|regex:/^[0-9]+$/',
            //'areainfo' => 'required',
        ];
        $message = [
            'phone.required' => 200001,
            'phone.regex' => 300007,
            'captcha.required' => 200001,
            'captcha.regex' => 200100,
            'password.required' => 200001,
            'password.min' => 200051,
            'password.max' => 200051,
            //'areaid.required' => 200050,
            //'areaid.regex' => 200002,
            //'cityid.required' => 200050,
            //'cityid.regex' => 200002,
            //'provinceid.required' => 200050,
            //'provinceid.regex' => 200002,
            //'areainfo.required' => 200050,
        ];
        if ($request->path() == 'api/register/captcha') {
            unset($rules['password']);
        }

        $validator = Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            $error = $validator->errors();
            return ['error' => $error->all()[0], 'msg' => array_keys($error->toArray())[0]];
        }
        return true;
    }

    // 注册参数验证
    private function OAuthRegisterValidator($request)
    {
        $rules = [
            'phone' => 'required|regex:/^1[0-9]{10}$/',
            'captcha' => 'required|regex:/^[0-9]+$/',
            'unionid' => 'required',
            'nickname' => 'required',
            //'avatar' => 'required',
            //'sex' => 'required|regex:/^[1-2]$/',
        ];
        $message = [
            '*.required' => 200001,
            '*.regex' => 200002,
        ];

        if ($request->client && $request->client != 'wxMp') {
            if ($request->path() == 'api/register/wx') {
                $rules['openid'] = 'required';
            }
        }
        if ($request->path() == 'api/register/qq') {
            unset($rules['unionid']);
            $rules['uid'] = 'required';
            $rules['openid'] = 'required';
        }

        $validator = Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            $error = $validator->errors();
            return ['error' => $error->all()[0], 'msg' => array_keys($error->toArray())[0]];
        }
        return true;
    }

    // 登录参数验证
    private function loginValidator($request)
    {
        $message = [
            'username.required' => 200022,
            'username.regex' => 200021,
            'password.required' => 200028,
            'password.min' => 200029,
            'password.max' => 200029,
            'captcha.required' => 200024,
            'captcha.regex' => 200025,
            'client.required' => 200001,
        ];
        if ($request->path() == 'api/login/captcha') {
            $rules = [
                'username' => 'required|regex:/^1[0-9]{10}$/',
                'client' => 'required',
                'captcha' => 'required|regex:/^[0-9]{4,6}$/',
            ];
            $message['username.required'] = 200023;
        } else {
            $rules = [
                'username' => 'required',
                'client' => 'required',
                'password' => 'required|min:6|max:50',
            ];
        }
        $validator = Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            $error = $validator->errors();
            return ['error' => $error->all()[0], 'msg' => array_keys($error->toArray())[0]];
        }
        return true;
    }

    // 修改密码参数验证码
    private function passwordValidator($request)
    {
        $rules = [
            'phone' => 'required',
            'password' => 'required|min:6|max:80',
            'captcha' => 'required',
            'client' => 'required',
        ];
        $message = [
            'phone.required' => 200001,
            'password.required' => 200001,
            'captcha.required' => 200001,
            'client.required' => 200001,
            'phone.regex' => 200051,
        ];
        $validator = Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            $error = $validator->errors();
            return ['error' => $error->all()[0], 'msg' => array_keys($error->toArray())[0]];
        }

        return true;
    }

    /**
     * 调用ucenter登录接口
     *
     * @param
     * @return mixed
     */
    public function registerUCenter($param)
    {
        $postUrl = 'register';
        $data = $this->_curl($postUrl, $param);
        return $data;
    }

    /**
     * 调用ucenter登录接口
     *
     * @param
     * @return mixed
     */
    public function loginUCenter($param)
    {
        $postUrl = 'login';
        $data = $this->_curl($postUrl, $param);
        return $data;
    }

    /**
     * 调用ucenter登录接口
     *
     * @param
     * @return mixed
     */
    public function updateUCenter($param)
    {
        $postUrl = 'update';
        $data = $this->_curl($postUrl, $param);
        return $data;
    }

    /**
     * 调用ucenter修改密码接口
     *
     * @param
     * @return mixed
     */
    public function passwordUCenter($param)
    {
        $postUrl = 'password';
        $data = $this->_curl($postUrl, $param);
        return $data;
    }

    private function _curl($postUrl, $curlPost)
    {
        $url = env('USER_CENTER', '');
        if (empty($url)) {
            return null;
        }
        $postUrl = $url . $postUrl;
        $ch = curl_init();//初始化curl
        curl_setopt($ch, CURLOPT_URL, $postUrl);//抓取指定网页
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);//要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_POST, TRUE);//post提交方式
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        $data = curl_exec($ch);//运行curl
        curl_close($ch);
        if ($data) {
            return json_decode($data);
        }
        return null;
    }
}
