<?php

namespace App\Http\Controllers\Api;

use App\Logic\Cart;
use App\Models\Artwork;
use App\Models\Shop\Goods;
use App\Models\Shop\Member;
use App\Models\Shop\Store;
use App\Models\UserIndex;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class CartController extends Controller
{
    protected $cartModel;


    public function __construct(Request $request)
    {
        $this->middleware('verify.user.login', ['only' => [
            'getCartList','cartAdd', 'cartDel', 'cartEdit'
        ]]);
        if (!empty($request->api_token)) {
            $this->cartModel = new Cart($request->api_token);
        }
    }

    /**
     * @SWG\Post(path="/api/cart/getCartList",
     *   tags={"cart"},
     *   operationId="/api/cart/getCartList",
     *   summary="购物车商品列表",
     *   description="购物车商品列表",
     *   produces={"application/json"},
     *  @SWG\Response(response="default", description="10000:token过期,10001:无效的token,10002:无效的数据格式"),
     *  @SWG\Response(response="200", description="successful operation",@SWG\Schema(ref="#/definitions/CartListResponse"))
     * )
     */
    public function getCartList()
    {
        $result = $this->cartModel->cartList();
        if ($result && isset($result['code'])) {
            if ($result['code'] == 200) {
                return response()->success($result['datas']);
            } else {
                return response()->fail($result['code'], $result['datas']['error']);
            }
        }
        return response()->fail(503005);
    }

    /**
     * @SWG\Post(path="/api/cart/cartCount",
     *   tags={"cart"},
     *   operationId="/api/cart/cartCount",
     *   summary="购物车商品数量",
     *   description="购物车商品数量",
     *   produces={"application/json"},
     *   @SWG\Response(response="200", description="successful operation")
     * )
     */
    public function cartCount(Request $request)
    {
        $api_token = $request->api_token;
        if (empty($api_token)) {
            return response()->success(['cart_count' => 0]);
        }
        $login_info = (new UserIndex())->getUserInfoByToken($api_token);
        if (!is_array($login_info)) {
            return response()->success(['cart_count' => 0]);
        }
        $this->cartModel = new Cart($api_token);
        $result = $this->cartModel->cartCount();
        if ($result && isset($result['code'])) {
            if ($result['code'] == 200) {
                return response()->success($result['datas']);
            } else {
                return response()->fail($result['code'], $result['datas']['error']);
            }
        }
        return response()->fail(503005);
    }


    /**
     * @SWG\Post(path="/api/cart/cartAdd",
     *   tags={"cart"},
     *   operationId="/api/cart/cartAdd",
     *   summary="购物车添加商品",
     *   description="购物车添加商品",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="artwork_id",
     *     in="formData",
     *     description="售卖作品ID",
     *     required=true,
     *     type="integer",
     *     default=""
     *   ),
     *   @SWG\Response(response="200", description="successful operation")
     * )
     */


    public function cartAdd(Request $request)
    {
        $artwork_id = $request->artwork_id ?? '';
        if (empty($artwork_id)) {
            return response()->fail(200002);
        }
        $model = new Artwork();
        $condition = ['id' => $artwork_id];
        $artwork_info = $model->where($condition)->first();
        if (!$artwork_info) {
            return response()->fail(221003);
        }
        if ($artwork_info['is_sell'] != Artwork::IS_SELL_YES) {
            return response()->fail(224008);
        }
        //作品ID转换商品ID
        $goodInfo = (new Goods())->getGoodsIdByArtworkId($artwork_id);
        $goods_model = new Goods();
        if (empty($goodInfo)) {
            $storeModel = new Store();
            $userInfo = (new UserIndex())->getUserInfoById($artwork_info['user_id']);
            $memberId = (new Member())->getInfoByKey($userInfo['user_key']);
            $storeInfo = $storeModel->createStoreByUserId($artwork_info['artist_id'], $memberId);
            $good_insert = $goods_model->getInsertData($artwork_info);
            $good_insert['artwork_id'] = $artwork_id;
            $good_insert['artist_id'] = $artwork_info['artist_id'];
            $good_insert['goods_state'] = $artwork_info['is_sell'];
            $good_insert['store_id'] = $storeInfo['store_id'];
            $good_insert['store_name'] = $storeInfo['store_name'];
            $goodId = $goods_model->insertGetId($good_insert);
        } else {
            if ($goodInfo['goods_state'] != 1 && $goodInfo['goods_verify'] != 1) {
                $goodUpdate = [
                    'goods_state' => 1,
                    'goods_verify' =>1,
                    'goods_price' => $artwork_info['price']
                ];
                $goods_model->where(['artwork_id' => $artwork_id])->update($goodUpdate);
            }
            if (!$goodInfo['store_id']) {
                $storeModel = new Store();
                $userInfo = (new UserIndex())->getUserInfoById($artwork_info['user_id']);
                $memberId = (new Member())->getInfoByKey($userInfo['user_key']);
                $storeInfo = $storeModel->createStoreByUserId($artwork_info['artist_id'], $memberId);
                $update = [
                    'store_id' => $storeInfo['store_id'],
                    'store_name' => $storeInfo['store_name']
                ];
                $goods_model->where(['artwork_id' => $artwork_id])->update($update);
            }
            $goodId = $goodInfo['goods_id'];
        }
        //post数据
        $postData = [
            'goods_id' => $goodId,
            'quantity' => 1
        ];
        $result = $this->cartModel->cartAdd($postData);
        if ($result && isset($result['code'])) {
            if ($result['code'] == 200) {
                return response()->success($result['datas']);
            } else {
                return response()->fail($result['code'], $result['datas']['error']);
            }
        }
        return response()->fail(503005);
    }


    /**
     * @SWG\Post(path="/api/cart/cartDel",
     *   tags={"cart"},
     *   operationId="/api/cart/cartDel",
     *   summary="购物车删除商品",
     *   description="购物车删除商品",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="cart_id[]",
     *     in="formData",
     *     description="购物车ID",
     *     required=false,
     *     type="integer",
     *     default=""
     *   ),
     *   @SWG\Parameter(
     *     name="cart_id[]",
     *     in="formData",
     *     description="购物车ID",
     *     required=false,
     *     type="integer",
     *     default=""
     *   ),
     *   @SWG\Parameter(
     *     name="cart_id[]",
     *     in="formData",
     *     description="购物车ID",
     *     required=false,
     *     type="integer",
     *     default=""
     *   ),
     *   @SWG\Response(response="200", description="successful operation")
     * )
     */
    public function cartDel(Request $request)
    {
        $cart_id_list = $request->cart_id ?? '';
        if (empty($cart_id_list) && !is_array($cart_id_list)) {
            return response()->fail(200002);
        }
        //post数据
        foreach ($cart_id_list as $cart_id) {
            $postData = [
                'cart_id' => $cart_id
            ];
            $result = $this->cartModel->cartDel($postData);
        }
        if ($result && isset($result['code'])) {
            if ($result['code'] == 200) {
                return response()->success($result['datas']);
            } else {
                return response()->fail($result['code'], $result['datas']['error']);
            }
        }
        return response()->fail(503005);
    }


    /**
     * @SWG\Post(path="/api/cart/cartEdit",
     *   tags={"cart"},
     *   operationId="/api/cart/cartEdit",
     *   summary="修改用户购物车指定商品数量",
     *   description="修改用户购物车指定商品数量",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="cart_id",
     *     in="formData",
     *     description="购物车ID",
     *     required=true,
     *     type="integer",
     *     default=""
     *   ),
     *   @SWG\Parameter(
     *     name="quantity",
     *     in="formData",
     *     description="修改商品数量",
     *     required=true,
     *     type="integer",
     *     default=""
     *   ),
     *   @SWG\Response(response="200", description="successful operation")
     * )
     */
    public function cartEdit(Request $request)
    {
        $cart_id = $request->cart_id ?? '';
        $quantity = $request->quantity ?? '';
        if (empty($cart_id) || empty($quantity) || !is_numeric($quantity)) {
            return response()->fail(200002);
        }
        //post数据
        $postData = [
            'cart_id' => $cart_id,
            'quantity' => $quantity
        ];
        $result = $this->cartModel->cartEdit($postData);
        if ($result && isset($result['code'])) {
            if ($result['code'] == 200) {
                return response()->success($result['datas']);
            } else {
                return response()->fail($result['code'], $result['datas']['error']);
            }
        }
        return response()->fail(503005);
    }
}
