<?php
namespace App\Http\Controllers\Api;

use App\Lib\Helpers\ArrayHelper;
use App\Logic\Bankcard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class BankcardController extends Controller
{
    public $total;
    public $current_page = 1;
    public $per_page = 10;
    public $last_page;
    public $next_page_url = null;
    public $prev_page_url = null;
    public $from;
    public $to;

    public function __construct(Request $request)
    {
        $this->current_page = (int)$request->get('page', 1);
        $this->middleware('verify.user.login');
    }
    /**
     * @SWG\Post(path="/api/bankcard",
     *   tags={"member"},
     *   operationId="api_bankcard",
     *   summary="银行卡列表",
     *   description="银行卡列表",
     *   produces={"application/json"},
     *  @SWG\Response(response="default", description="10000:token过期,10001:无效的token,10002:无效的数据格式"),
     *  @SWG\Response(response="200", description="successful operation",@SWG\Schema(ref="#/definitions/BankcardListResponse"))
     * )
     *
     */
    /**
     * 银行卡列表
     * */
    public function index(Request $request)
    {
        $bankcardLogic = new Bankcard($request->api_token);
        $result = $bankcardLogic->bankcardList(['curpage' => $this->current_page], true);
        if(!$result){
           return $this->_json([]);
        }
        if ($result['code'] == 200) {
            $data_list = $result['datas']['bankcard_list'];
            $total = $result['page_total'] ?? 0;
            $this->from = $this->current_page * $this->per_page - $this->per_page;
            $this->to = count($data_list) + $this->from++;
            $data = array(
                'total' => $total,
                'per_page' => $this->per_page,
                'current_page' => $this->current_page,
                'last_page' => ceil($total / $this->per_page),
                'next_page_url' => $this->next_page_url,
                'prev_page_url' => $this->prev_page_url,
                'from' => $this->from,
                'to' => $this->to,
            );

            if ((int)$this->current_page > (int)$result['page_total']) {
                $data['from'] = null;
                $data['to'] = null;
                $data['data'] = [];
                return response()->success($data);
            }
            $data['data'] = [];
            $data_list = json_decode(json_encode($data_list), true);
            $bankcardList = config("bankCardList");
            foreach ($data_list as $key => $value) {
                $nowCard = empty($bankcardList[$value['bank_name']])?
                    [
                        'logo'=>'',
                        'card_type'=>'银联借记卡',
                        'background'=>'red'
                    ]: $bankcardList[$value['bank_name']];
                $data['data'][$key]['bankcard_id'] = $value['bankcard_id'];
                $data['data'][$key]['user_id'] = $request->user['user_id'];
                $data['data'][$key]['user_name'] = $request->user['user_name'];
                $data['data'][$key]['true_name'] = $value['true_name'];
                $data['data'][$key]['card_type'] = $nowCard['card_type'];
                $data['data'][$key]['bank_name'] = $value['bank_name'];
                $data['data'][$key]['background'] = $nowCard['background'];
                $data['data'][$key]['bank_image'] = $nowCard['logo'];
                $data['data'][$key]['is_default'] = $value['is_default'];
                $data['data'][$key]['bank_card'] = $value['bank_card'];
                $data['data'][$key]['bank_card_show'] = $value['bank_card_show'];
                $data['data'][$key]['bank_card_name'] = $value['bank_card_name'];
            }
            return response()->success($data);
        }
        return response()->fail(503005);
    }


    /**
     * @SWG\Post(path="/api/bankcard/create",
     *   tags={"member"},
     *   operationId="/api/bankcard/create",
     *   summary="添加银行卡",
     *   description="添加银行卡",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="bank_card",
     *     in="formData",
     *     description="银行卡号",
     *     required=true,
     *     type="string",
     *     default=""
     *   ),
     *   @SWG\Parameter(
     *     name="true_name",
     *     in="formData",
     *     description="真实姓名",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="bank_name",
     *     in="formData",
     *     description="银行名称",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Response(response="200", description="successful operation"))
     * )
     * )
     */
    public function create(Request $request)
    {
        $bankcardList = config("bankCardList");
        $rules = [
            'true_name' => 'required',
            'bank_card' => 'required',
            'bank_name' => 'required|in:'.join(array_keys($bankcardList), ",")
//            'id_number' => 'required|',
//            'captcha' => 'required|regex:/^[0-9]{4,6}$/',
        ];
        $message = [
            '*.required' => 200001,
            'bank_name.*' => '银行卡名错误'
        ];
        $this->nValidate($request->all(), $rules, $message);

        $nowCard = $bankcardList[$request->bank_name];
        $data = [
            'key' => $request->api_token,
            'true_name' => $request->true_name,
            'bank_card' => $request->bank_card,
            'card_type' => $nowCard['card_type'],
            'bank_name' => $request->bank_name,
            'bank_image' => '',
//            'type' => 'inside',
        ];
        $bankcardLogic = new Bankcard($request->api_token);
        $result = $bankcardLogic->create($data);
        if (isset($result->code)) {
            if ($result->code == 200) {
                return response()->success($result->datas);
            } else {
                return response()->failError($result->datas->error);
            }
        } else {
            return response()->fail(200005);
        }
    }

    public function captcha(Request $request)
    {
        $phone = $request->get('phone', null);
        if (empty($phone)) {
            return response()->fail(300007);
        }
        $bankcardLogic = new Bankcard($request->api_token);
        $result = $bankcardLogic->bankcard_step2($phone);
        if (isset($result->code)) {
            if ($result->code == 200) {
                return response()->success($result->datas);
            } else {
                return response()->failError($result->datas->error);
            }
        } else {
            return response()->fail(200102);
        }
    }
    /**
     * @SWG\Post(path="/api/bankcard/delete",
     *   tags={"member"},
     *   operationId="/api/bankcard/delete",
     *   summary="删除银行卡",
     *   description="删除银行卡",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="bankcard_id",
     *     in="formData",
     *     description="银行卡ID",
     *     required=true,
     *     type="integer",
     *     default=""
     *   ),
     *  @SWG\Response(response="default", description="200005:操作失败"),
     * )
     *
     */
    /**
     * 银行卡删除操作
     * */
    public function destroy(Request $request)
    {
        $bankcardLogic = new Bankcard($request->api_token);
        $result = $bankcardLogic->delete($request->bankcard_id);
        if (isset($result->code) && $result->code == 200) {
            return response()->success();
        } else {
            return response()->fail(200005);
        }
    }

    /**
     * @SWG\Get(path="/api/bankcard/list",
     *   tags={"member"},
     *   operationId="/api/bankcard/list",
     *   summary="支持的银行卡列表",
     *   description="支持的银行卡列表",
     *   produces={"application/json"},
     *  @SWG\Response(response="default", description="", @SWG\Schema(type="array", @SWG\Items(ref="#/definitions/MyBankCardListResponse"))),
     * )
     *
     */
    public function bankcardList(Request $request)
    {
        return $this->_json(ArrayHelper::mapToList(config("bankCardList"), "bank_name"));
    }

    /**
     * 银行卡删除操作
     * */
    public function bankcard_default(Request $request)
    {
        $bankcardLogic = new Bankcard($request->api_token);
        $result = $bankcardLogic->bankcard_default($request->bankcard_id);
        if (isset($result->code) && $result->code == 200) {
            return response()->success();
        } else {
            return response()->fail(200005);
        }
    }

    public function member_info(Request $request)
    {
        $bankcardLogic = new Bankcard($request->api_token);
        $result = $bankcardLogic->get_member_info();
        if (isset($result->code) && $result->code == 200) {
            $data = [
                'mobile' => $result->datas->mobile ?? null,
                'user_name' => $result->datas->member_name ?? null,
                'true_name' => $result->datas->true_name ?? null,
                'id_number' => $result->datas->id_number ?? null,
            ];
            return response()->success($data);
        } else {
            return response()->success();
        }
    }


}