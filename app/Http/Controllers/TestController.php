<?php
/**
 * Created by Test, 2018/05/29 20:49.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Models\ArtistPersonal;
use App\Models\UserIndex;
use Illuminate\Support\Facades\DB;

class TestController extends Controller
{
    public function index()
    {
        set_time_limit(0);
        //myDump(application()->shopUserService->getBankcardCount(2084));
        //需要添加员工的手机号
        //$insert = '18455954569,18356541464,15555115038,15212320813,18263146516,18226072707,17681290402,13675558002,15385420689,13685595080,15846239326,15555107887,18855941012,13637202770,13384701534,15049032817,18655991811,18855946650,18855995525,15205595125,15623869370,18268192665,18755968930,18055950765,18226988435,18297689720,18855946861,15577131456,18715098342,13226811274,13955986123,15655658575,17681333335,15956962368,18656418176,15556600665,13805864695,18297912470,18226647038,15249941117,18355954044,17755907117,13665595153,15209881289,15623791393,15956945564,18214882193';
        $DB = DB::connection('db_yinuo');
        //$employeeModel = $DB->table('oc_employee');
        //$memberModel = $DB->table('shopnc_member');
        //$distributeModel = $DB->table('shopnc_member_distribute');
        $insert = [
            '18455954569' => '都瑞丰',
            '18356541464' => '李中华',
            '15555115038' => '白泉',
            '15212320813' => '沈飞',
            '18263146516' => '阎万相',
            '18226072707' => '王龙龙',
            '17681290402' => '程艳萍',
            '13675558002' => '王志涛',
            '15385420689' => '毕秋萍',
            '13685595080' => '朱瑞平',
            '15846239326' => '魏峰',
            '15555107887' => '许佳华',
            '18855941012' => '鲁钰',
            '13637202770' => '程嘉斌',
            '13384701534' => '王宪峰',
            '15049032817' => '刘敏怡',
            '18655991811' => '吴逸成',
            '18855946650' => '黄志敏',
            '18855995525' => '汪巧妹',
            '15205595125' => '张倩',
            '15623869370' => '宁静',
            '18268192665' => '吴志波',
            '18755968930' => '唐滟',
            '18055950765' => '杨国平',
            '18226988435' => '司光宏',
            '18297689720' => '徐亚军',
            '18855946861' => '程锦',
            '15577131456' => '蒋高升',
            '18715098342' => '化海潮',
            '13226811274' => '丁红阳',
            '13955986123' => '汪芬',
            '15655658575' => '程浩',
            '17681333335' => '叶彬彬',
            '15956962368' => '万家根',
            '18656418176' => '黎秀涛',
            '15556600665' => '阮宁夫',
            '13805864695' => '洪洁',
            '18297912470' => '宋建',
            '18226647038' => '张冬景',
            '15249941117' => '杨伟伟',
            '18355954044' => '朱嘉君',
            '17755907117' => '潘征',
            '13665595153' => '汪亮',
            '15209881289' => '卫家乐',
            '15623791393' => '邵旭威',
            '15956945564' => '卫勇',
            '18214882193' => '胡新村'
        ];

        $topPhone = 17612157796;
        $topMemberInfo = $DB->table('shopnc_member')->select(['member_id', 'member_name'])
            ->where(['member_mobile' => $topPhone])->first();
        if (!isset($topMemberInfo['member_id']) && empty($topMemberInfo['member_id'])) {
            echo '用户不存在';
            die;
        }
        $topMemberId = $topMemberInfo['member_id'];
        $topMemberName = $topMemberInfo['member_name'] ?: $topPhone;

        foreach ($insert as $phone => $name) {

            //添加员工到员工表
            $sourceStaffInfo = $DB->table('oc_employee')->select('id')
                ->where(['phone' => $phone])->first();
            if (isset($sourceStaffInfo['id']) && !empty($sourceStaffInfo['id'])) {
                echo "</br>用户已存在员工表-" . $phone . "</br>";
                $source_staff_id = $sourceStaffInfo['id'];
            } else {
                $source_staff_id = $DB->table('oc_employee')
                    ->insertGetId(['name' => $name, 'phone' => $phone, 'member_id' => $topMemberId]);
            }
            echo "</br>" . $name . "--的员工ID为-" . $source_staff_id . "</br>";

            //员工存在 获取员工id
            $memberInfo = $DB->table('shopnc_member')->select('member_id')
                ->where(['member_mobile' => $phone])->first();


            if (isset($memberInfo['member_id']) && !empty($memberInfo['member_id'])) {
                $memberId = $memberInfo['member_id'];
                if ($memberInfo['member_id']) {
                    //查询员工下级邀请用户

                    $updateMemberIds = $DB->table('shopnc_member_distribute')
                        ->select('member_id')
                        ->where(['top_member' => $memberId])
                        ->get();

                    //更新员工下级的来源员工ID
                    if (!empty($updateMemberIds)) {
                        $updateMemberIds = array_column($updateMemberIds, 'member_id');
                        $effectRow = $DB->table('shopnc_member')
                            ->whereIn('member_id', $updateMemberIds)
                            ->update(['source_staff_id' => $source_staff_id]);
                        if ($effectRow === false) {
                            echo "</br>用户员工更新失败-" . $phone . "</br>";
                            print_r($updateMemberIds);
                        }

                        //分销表更改用户上级为运营商ID 昵称
                        $topUpdate = [
                            'top_member' => $topMemberId,
                            'top_member_name' => $topMemberName
                        ];
                        $DB->table('shopnc_member_distribute')
                            ->whereIn('member_id', $updateMemberIds)
                            ->update($topUpdate);
                        print_r($updateMemberIds);
                        echo "</br>" . '修改成功' . "</br></br></br>";
                    } else {
                        echo "</br>该员工没有下级邀请用户-" . $phone . "</br></br></br>";
                    }
                }
            } else {
                echo "</br>该员工没有注册用户-" . $phone . "</br></br></br>";
                continue;
            }
        }
        echo '运行完成 !!!' . "</br>";
    }

    public function index22()
    {
        //myDump(application()->shopService->getCartList(2084));
        //myDump(application()->shopService->getCartList(2084));
        myDump(application()->shopService->cartCount(7232));
    }

    public function test(Request $request)
    {
        $userId = $request->user_id;
        if (empty($userId)) {
            pp('缺号用户id---user_id');
        }
        (new UserIndex())->cacheDel($userId);
        return response()->success([]);
    }
}