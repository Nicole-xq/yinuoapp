<?php

namespace App\Http\Middleware;

use Closure;
use Validator;

class VerifyInfoRecommend
{
    /**
     * 返回请求过滤器
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $rules = [
            'article_id' => 'regex:/^[0-9]+$/',
            'classify_id' => 'regex:/^[0-9]+$/',
        ];
        $message = [
            'article_id.regex' => 200002,
            'classify_id.regex' => 200002,
        ];

        $validator = Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            $error = $validator->errors();
            return response()->fail($error->all()[0], array_keys($error->toArray())[0]);
        } elseif (empty($request->article_id) && empty($request->classify_id)) {
            return response()->fail(200001);
        }

        return $next($request);
    }
}
