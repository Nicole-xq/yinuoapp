<?php

namespace App\Http\Middleware;

use Closure;
use Validator;

class VerifyArticle
{
    /**
     * 返回请求过滤器
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $rules = [
            'id' => 'required|regex:/^[0-9]+$/',
        ];
        $message = [
            '*.required' => 200001,
            '*.regex' => 200002,
        ];

        $path = $request->path();

        switch ($path) {
//            case 'api/news.info':   //文章详情
//                $rules['author_id'] = 'required|regex:/^[0-9]+$/';
//                $message['author_id.required'] = 200001;
//                $message['author_id.regex'] = 200002;
//                break;
            case 'api/article.like':  //文章点赞
//                $rules['author_id'] = 'required|regex:/^[0-9]+$/';
//                $message['author_id.required'] = 200001;
//                $message['author_id.regex'] = 200002;
                break;
            case 'api/article.comment.like':  //评论点赞
                // ...
                break;
            case 'api/art.evaluation.vote':  //艺评投票
                $rules['state'] = 'required|regex:/^(dis)?agree$/';
                break;
            case 'api/article.comment':  // 评论
                $rules['msg'] = 'required';
                $rules['comment_id'] = 'regex:/^[0-9]+$/';
                break;
            case 'api/article/commentList':  // 评论
                //$rules['msg'] = 'required';
                //$rules['comment_id'] = 'regex:/^[0-9]+$/';
                break;
            default:
                // ...
        }

        $validator = Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            $error = $validator->errors();
            return response()->fail($error->all()[0], array_keys($error->toArray())[0]);
        }

        return $next($request);
    }
}
