<?php

namespace App\Http\Middleware;

use App\Models\UserIndex;
use App\Models\UserToken;
use Closure;

/**
 * 校验用户是否登录的中间件 必须要登录
 * Class VerifyUserLogin
 * @package App\Http\Middleware
 */
class VerifyUserLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!isset($request->user) || empty($request->user)) {
            if (empty($request->api_token)) {
                return response()->fail(300013);
            }
            $token = $request->api_token;

            $user_token = UserToken::where('token', $token)->first();
            if (empty($user_token)) {
                return response()->fail(300014);
            }
            if ($user_token->user_id) {
                //$user = UserIndex::find($user_token->user_id);
                $user = (new UserIndex())->getUserInfoById($user_token->user_id);
            } else if ($user_token->member_name) {
                $user = UserIndex::where('user_name',$user_token->member_name)->first();
            }
            if(!isset($user) || empty($user)){
                return response()->fail(300013);
            }
            if ($user['is_disable']) {
                return ['error' => 300011];
            }
            $request->user = $user;
        }

        return $next($request);
    }
}
