<?php

namespace App\Http\Middleware;

use App\Models\UserIndex;
use App\Models\UserToken;
use Closure;

/**
 * 需要用户信息的 中间件
 * Class VerifyUser
 * @package App\Http\Middleware
 */
class VerifyUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->user = null;

        if (!empty($request->api_token)) {
            $token = $request->api_token;
            $user_token = UserToken::where('token', $token)->first();

            if (!empty($user_token)) {
                if ($user_token->user_id) {
                    $user = (new UserIndex())->getUserInfoById($user_token->user_id);
                } else if ($user_token->member_name) {
                    $user = UserIndex::where('user_name',$user_token->member_name)->first();
                }

                if (isset($user) && !empty($user)) {
                    $request->user = $user;
                }
            }
        }

        return $next($request);
    }
}
