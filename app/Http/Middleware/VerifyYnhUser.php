<?php

namespace App\Http\Middleware;

use Closure;
use Validator;

class VerifyYnhUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $path = $request->path();
        switch ($path) {
            case 'ynh/register':
                $this->registerValidator($request);
                break;
            case 'ynh/login':
                $this->loginValidator($request);
                break;
            case 'ynh/password.reset':
                $this->passwordValidator($request);
                break;
            default:
                // ***
        }
        return $next($request);
    }


    // 注册参数验证
    private function registerValidator($request)
    {
        $rules = [
            'user_name' => 'required',
            'phone' => 'required|regex:/^1[345789][0-9]{9}$/',
            'captcha' => 'required|regex:/^[0-9]+$/',
            'password' => 'required|min:6|max:80',
        ];
        $message = [
            '*.required' => 200001,
            'phone.regex' => 300007,
            'captcha.regex' => 200100,
            'password.min' => 200051,
            'password.max' => 200051,
        ];

        $validator = Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors();
            lsReturn($errors->all()[0]);
        }
    }


    // 登录参数验证
    private function loginValidator($request)
    {
        $rules = [
            'client' => 'required',
            'username' => 'required',
            'password' => 'required|min:6|max:80',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            lsReturn(200002);
        }
    }

    //修改密码参数验证
    public function passwordValidator($request)
    {
        $rules = [
            'phone' => 'required',
            'password' => 'required|min:6|max:80',
            'captcha' => 'required',
            'client' => 'required',
        ];
        $message = [
            'phone.required' => 200001,
            'password.required' => 200001,
            'captcha.required' => 200001,
            'client.required' => 200001,
            'phone.regex' => 200051,
        ];
        $validator = Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            lsReturn($errors->all()[0]);
        }
    }
}
