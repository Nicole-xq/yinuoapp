<?php

namespace App\Http\Middleware;

use Closure;
use Validator;

class VerifyChat
{
    /**
     * 返回请求过滤器
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $rules = [];
        $message = [
            '*.required' => 200001,
            '*.regex' => 200002,
        ];

        $path = $request->path();

        switch ($path) {
            case 'api/chat':  // 对话列表
                $rules['user_id'] = 'required|regex:/^[0-9]+$/';
                break;
            case 'api/chat/create':  // 发送对话
                $rules['user_id'] = 'required|regex:/^[0-9]+$/';
                $rules['msg'] = 'required';
                break;
            case 'api/chat/delete':  // 删除对话
                $rules['user_id'] = 'required|regex:/^[0-9]+$/';
                break;
            default:
                // ...
        }

        if (!empty($rules)) {
            $validator = Validator::make($request->all(), $rules, $message);

            if ($validator->fails()) {
                $error = $validator->errors();
                return response()->fail($error->all()[0], array_keys($error->toArray())[0]);
            }
        }

        return $next($request);
    }
}
