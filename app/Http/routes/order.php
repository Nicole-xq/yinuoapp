<?php

/*
|--------------------------------------------------------------------------
| 下单订单相关路由
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'order'], function () {
    Route::post('/getBuyInfo', 'OrderController@getBuyInfo');
    Route::post('/createOrder', 'OrderController@createOrder');
    Route::post('/payOrderInfo', 'OrderController@payOrderInfo');
    Route::post('/editOrderPrice', 'OrderController@editOrderPrice');
    Route::post('/orderPay', 'OrderController@orderPay');
    Route::post('/getNewestOrderPayedInfo', 'OrderController@getNewestOrderPayedInfo');
    Route::post('/getList', 'OrderController@getList');
//    Route::post('/getAuctionOrderList', 'OrderController@getAuctionOrderList');
    Route::post('/getOrderInfo', 'OrderController@getOrderInfo');       //获取订单详情
    Route::post('/getExpressInfo', 'OrderController@getExpressInfo');       //获取订单物流详情
    Route::post('/orderCancelOp', 'OrderController@orderCancelOp');
    Route::post('/orderReceiveOp', 'OrderController@orderReceiveOp');
    Route::post('/orderDeleteOp', 'OrderController@orderDeleteOp');
    Route::post('/orderToTransitShipment', 'OrderController@orderToTransitShipment');
});
