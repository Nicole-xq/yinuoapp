<?php

/*
|--------------------------------------------------------------------------
| 下单订单相关路由
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'artistOrder'], function () {
    Route::post('/orderToTransitShipment', 'ArtistOrderController@orderToTransitShipment');
    Route::post('/editOrderPrice', 'ArtistOrderController@editOrderPrice');
    Route::post('/getOrderList', 'ArtistOrderController@getOrderList');
    Route::post('/getClosingOrderList', 'ArtistOrderController@getClosingOrderList');
    Route::post('/getArtistClosingLog', 'ArtistOrderController@getArtistClosingLog');
    Route::post('/getArtistClosingWaitSum', 'ArtistOrderController@getArtistClosingWaitSum');
    Route::post('/artistToClosing', 'ArtistOrderController@artistToClosing');
});
