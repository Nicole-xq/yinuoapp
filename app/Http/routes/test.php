<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to galaxy-client
| and give it the controller to call when that URI is requested.
|
*/

if(!isProduction()) {
//    throw new \Exception("滚犊子");
    Route::get('/test', 'TestController@index');
    Route::get('/test22', 'TestController@index22');
    Route::get('/delCache', 'TestController@test');
    //测试
    Route::get('test/thrift', function () {
        $transport = new \Thrift\Transport\THttpClient('dev.huang.yinuovip.com', 80, '/src/index.php/rpc');
        $transport->setDebug(true);
        $transport = new \Thrift\Transport\TBufferedTransport($transport);
        $protocol = new \Thrift\Protocol\TBinaryProtocol($transport);
        $protocol = new \Thrift\Protocol\TMultiplexedProtocol($protocol, "Server.Rpc.Service.ShopNc.ShopService");
        $client = new \Server\Rpc\Service\ShopNc\ShopServiceClient($protocol);
        $transport->open();
        try {
            $sum = $client->createOrder("e195cb0524b1e494ed3155b63fbb899b", "1", "934|1", 553, "B33kumQPmBH0QiA7xoRCpNkO57ouYZHI0ow",
                "OpkKkOGy4OQSuyeHOpvQFt3broOf4_uXmvZoI9S", "kpBa0YEb21sJ2IjxSwAxlerMmJZ21ok",
                "online", null, null, null, null, null ,null,
                null,null,null, null

                );
        } catch (\Exception $e) {
            showExceptionTrace($e);
        }
        myDump($sum);
    });
    Route::get('test/thrift1', function () {
        try {
            $orderInfo = application()->shopService->getOrderInfo("3381");
            myDump($orderInfo);
        } catch (\Exception $e) {
            showExceptionTrace($e);
        }
    });
}