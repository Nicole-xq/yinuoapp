<?php

namespace App\Exceptions;

use App\Entity\Constant\ApiResponseCode;
use App\Lib\Helpers\LogHelper;
use App\Lib\Helpers\ResponseHelper;
use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Server\Rpc\Common\Shared\ServiceException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
        \App\Exceptions\ResponseException::class,
        ServiceException::class
    ];

    /**
     * 非生成环境下需要报的异常 与 dontReport同时使用
     *
     * @var array
     */
    protected $devReport = [
        \Illuminate\Session\TokenMismatchException::class,
//        \Symfony\Component\HttpKernel\Exception\HttpException::class
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $exception)
    {
        if ($this->shouldReport($exception) || $this->devShouldReport($exception)){
            \Log::error("exception_handler", ['env'=>env('APP_ENV'), LogHelper::getEInfo($exception)]);
            if(!isProduction()){
                \Log::error("exception_handler_showExceptionTrace", [showExceptionTrace($exception, false, true, false)]);
            }
        }
//        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $data = NULL;
        $code = $exception->getCode();
        $message = $exception->getMessage();

        //统一报错 避免用户看到不必要的报错
        if(!($exception instanceof ResponseException) && !($exception instanceof ServiceException)) {
            //这里确保 日志里已经打了 error log
            if(isProduction()){
                $code = ApiResponseCode::FAIL;
                $message = ApiResponseCode::getMsg($code);
            }else{
                $message = showExceptionTrace($exception, false, true, false);
            }
        }
        if ($request->wantsJson()) {
            $response = ResponseHelper::json($code, $data, $message);
            if(!($exception instanceof ResponseException) && !($exception instanceof ServiceException)){
                $response->setStatusCode(500);
            }
        } else {
            //这里异常转换 (html 报错)
            $response = parent::render($request, $exception);
        }
        return $response;
    }
    protected function devShouldReport($exception){
        if(isProduction()){
            return false;
        }
        return !is_null(collect($this->devReport)->first(function ($key, $type) use ($exception) {
            return $exception instanceof $type;
        }));
    }
}
