<?php

namespace App\Exceptions;

/**
 * Class ResponseException
 * code 使用 ResponseCode
 * @package App\Exceptions
 */
class ResponseException extends \Exception{
    protected $_data = NULL;
    protected static $_codeClass = \App\Entity\Constant\ApiResponseCode::class;
    public static function throwException($code, $msg = '', $data = NULL)
    {
        if (!empty($msg)) {
            $message = $msg;
        } else{
            $message = call_user_func([static::$_codeClass, 'getMsg'], $code);
            if (!$message){
                $message = "未知错误[{$code}]";
            }
        }
        $e = new static($message, $code);
        $data !== NULL && $e->_data = $data;
        throw $e;
    }
}