<?php
namespace App\Exceptions;

/**
 * 对外的api响应异常
 * Class ApiResponseException
 * code 使用 ApiResponseCode
 * @package App\Exceptions
 */
class ApiResponseException extends ResponseException {
    protected static $_codeClass = \App\Entity\Constant\ApiResponseCode::class;
}