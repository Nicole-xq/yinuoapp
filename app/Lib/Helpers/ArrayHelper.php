<?php 
namespace App\Lib\Helpers;

class ArrayHelper extends HelperBase {
    /**
     * 把k=>v map形式的数组转换为 list 型
     * ['a'=>['c'=>1,'d'=>2], 'b'=>['c'=>2, 'd'=>3]] --> [['c'=>1,'d'=>2, 'id'=>'a'], ['c'=>2, 'd'=>3, 'id'=>'b']]
     * @param array $data
     * @param string $keyParam key转换为list的属性值; 如果为null将清空key
     * @return array
     */
    public static function mapToList(array $data, $keyParam = "key"){
        $outData = [];
        foreach ($data as $k=>$v){
            if($keyParam!==null){
                $v[$keyParam] = $k;
            }
            $outData[] = $v;
        }
        return $outData;
    }
}