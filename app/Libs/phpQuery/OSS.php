<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/29 0029
 * Time: 11:54
 */

namespace App\Libs\phpQuery;


use OSS\OssClient;
use OSS\Core\OssException;

class OSS
{
    private $ossClient;

    /**
     * OSS constructor.
     * @param bool $isInternal
     */
    public function __construct()
    {
        $Endpoint = env('OSS_ENDPOINT', '');
        $AccessKeyId = env('OSS_ACCESS_ID', '');
        $AccessKeySecret = env('OSS_ACCESS_KEY', '');
        $this->ossClient = new OssClient(
            $AccessKeyId,
            $AccessKeySecret,
            $Endpoint
        );
    }

    /**
     * @param $bucket @目标路径
     * @param $object @目标文件名 / 上传图片对象
     * @param $file @本地临时文件名
     * @return mixed
     */
    public function upload($file, $object, $bucket = '')
    {
        if (!is_string($file)) {
            return false;
        }
        if (empty($bucket)) {
            $bucket = env('OSS_BUCKET', 'testyinuo2');
        }
        if (!$this->doesBucketExist($bucket)) {
            return false;
        }
        $result = $this->ossClient->uploadFile($bucket, $object, $file);
        $result = json_decode(json_encode($result), true);
        if (empty($result)) {
            return false;
        }
        return $result['info']['url'];
    }


    public function get_object_name($user_id)
    {
        return 'head_img/avatar_' . $user_id . '.jpg';
    }

    public function get_background_name($user_id)
    {
        return 'background_img/background_' . $user_id . '.jpg';
    }

    /**
     * 判断object是否存在
     *
     * @param OssClient $ossClient OSSClient实例
     * @param string $bucket bucket名字
     * @return null
     */
    function doesObjectExist($object)
    {
        $bucket = env('OSS_BUCKET', 'testyinuo2');
        if (!$this->doesBucketExist($bucket)) {
            return false;
        }
        $exit = $this->ossClient->doesObjectExist($bucket, $object);
        if ($exit) {
            return true;
        }
        return false;
    }

    /**
     * 删除单个文件
     * @param $bucket
     * @param $object
     * @return bool|null
     */
    public function deleteObject($bucket, $object)
    {
        if (!$this->doesBucketExist($bucket)) {
            return false;
        }
        return $this->ossClient->deleteObject($bucket, $object);
    }

    /**
     * 删除多个文件
     * @param $bucket
     * @param $objects
     * @return bool|null
     */
    public function deleteObjects($bucket, $objects)
    {
        if (!$this->doesBucketExist($bucket)) {
            return false;
        }
        return $this->ossClient->deleteObject($bucket, $objects);
    }

    /*
     * 文件之字符串上传
     */
    public function putObject($object, $content)
    {
        $bucket = env('OSS_BUCKET', 'testyinuo2');
        if (!$this->doesBucketExist($bucket)) {
            return false;
        }
        $this->ossClient->putObject($bucket, $object, $content);
    }


    /**
     * 创建存储空间
     * @param $bucketName
     * @return null
     */
    public function createBucket($bucketName)
    {
        if ($this->doesBucketExist($bucketName)) {
            return true;
        }
        return $this->ossClient->createBucket($bucketName);
    }

    /**
     * 删除存储空间
     * 删除存储空间之前，必须先删除存储空间下的所有文件、LiveChannel和分片上传产生的碎片。
     * @param $bucket
     * @return null
     */
    public function deleteBucket($bucket)
    {
        return $this->ossClient->deleteBucket($bucket);
    }


    /**
     * 判断存储空间是否存在
     * @param $bucket
     * @return boolean
     */
    public function doesBucketExist($bucket)
    {
        $result = $this->ossClient->doesBucketExist($bucket);
        if ($result === true) {
            return $result;
        }
        return false;
    }
}