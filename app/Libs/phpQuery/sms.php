<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/15 0015
 * Time: 16:10
 */
namespace App\Libs\phpQuery;

use \App\Models\Sms as ModelsSms;

class sms
{
    protected $appID = '8SDK-EMY-6699-SERLM';
    protected $secret = '9CF353A96C081EBC';
    protected $url = 'http://shmtn.b2m.cn/simpleinter/sendSMS';

    /**
     * 验证验证码
     * @param string $phone
     * @param string $captcha
     * @param string $log_type
     * @return boolean
     */
    public function checkSmsCaptcha($phone, $captcha, $log_type){
        $condition = array();
        $condition['phone'] = $phone;
        $condition['captcha'] = $captcha;
        $condition['type'] = intval($log_type);
        $sms_log = ModelsSms::where($condition)->first();

        if(empty($sms_log) || empty($sms_log->state) || (strtotime($sms_log->updated_at) < time()-1800)) {//半小时内进行验证为有效
            return false;
        }
        $sms_log->state = 0;
        $sms_log->save();

        return true;
    }


    public function sendCaptcha($phone, $type = 1)
    {
        $sms =  new ModelsSms();
        $result = $sms::where('phone', $phone)->where('type', $type)->where('state',1)->first();

        $captcha = rand(1000, 9999);
        if (!empty($result)) {
            if (strtotime($result->created_at) > (time()-60)) {
                return 200103;
            }
            if (strtotime($result->created_at) > (time()-1800)) {
                $captcha = $result->captcha;
                $result->send_num += 1;
                $result->updated_at = date("Y-m-d H:i:s", time());
                $result->save();
            }
        }

        $state = $this->send($phone, $captcha);
        if ($state !== true) {
            return 200102;
        }

        if (empty($result) || strtotime($result->created_at) <= (time()-1800)) {
            $sms->ip = $this->getIp();
            $sms->phone = $phone;
            $sms->captcha = $captcha;
            $sms->type = $type;
            $sms->save();
        }

        return true;
    }


    public function send($phone , $captcha, $ip = '', $type = '')
    {

        /*
         * 暂时不支持数组
         */
        if (is_array($phone)) {
            //$phone = implode(",", $phone);
            return false;
        }

        //这里手机号 群发的时候 要拼成字符串 用 , 连接

        $pattern = '/^0?1[0-9]{10}$/';
        if (preg_match($pattern, $phone, $matches) == false) {
            //return '手机号没过正则';
            return false;
        }


        $limit = $this->check_limit($phone, $ip);

        if (!$limit)
            return false;



        $timestamp = date('YmdHms',time());
        //echo $this->appID,$this->secret,$this->url;die;
        $sign = md5($this->appID.$this->secret.$timestamp);

        $data = [
            'appId' => $this->appID,
            'timestamp' => $timestamp,
            'sign' => $sign,
            'mobiles' => $phone
        ];
        $data['content'] = "【艺诺艺术有限公司】您好，您的验证码为：{$captcha}";

        $response = http::post($this->url, $data);
        $response = json_decode($response);

        if ($response->code != 'SUCCESS') {
            return $response->code;
        }
        return true;

    }


    //不同环境下获取真实的IP
    public function getIp(){
        //判断服务器是否允许$_SERVER
        if(isset($_SERVER)){
            if(isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
                $realIp = $_SERVER["HTTP_X_FORWARDED_FOR"];
            }elseif(isset($_SERVER["HTTP_CLIENT_IP"])) {
                $realIp = $_SERVER["HTTP_CLIENT_IP"];
            }else{
                $realIp = $_SERVER["REMOTE_ADDR"];
            }
        }else{
            //不允许就使用getenv获取
            if(getenv("HTTP_X_FORWARDED_FOR")){
                $realIp = getenv( "HTTP_X_FORWARDED_FOR");
            }elseif(getenv("HTTP_CLIENT_IP")) {
                $realIp = getenv("HTTP_CLIENT_IP");
            }else{
                $realIp = getenv("REMOTE_ADDR");
            }
        }

        return $realIp;
    }

    private function check_limit($phone, $ip)
    {

        $model = new \App\Models\Sms();
        if (!empty($ip)) {
            $num = $model->getNum(['ip' => $ip]);
            if ($num == false && $num != 0)
                return false;

            if ($num > config('sms.limit')['ip']) {
                return false;
            }
        }

        $user_num = $model->getNum(['phone' => "." . $phone . "."]);
        if ($user_num == false && $user_num != 0)
            return false;

        if ($user_num > config('sms.limit')['user']) {
            return false;
        }
        return true;
    }
}