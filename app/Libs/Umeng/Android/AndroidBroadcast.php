<?php
namespace App\Libs\Umeng\Android;

use App\Libs\Umeng\AndroidNotification;

class AndroidBroadcast extends AndroidNotification {
	public function  __construct() {
		parent::__construct();
		$this->data["type"] = "broadcast";
	}
}