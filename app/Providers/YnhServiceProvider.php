<?php
/**
 * Created by Test, 2018/08/23 15:54.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Providers;


use \Response;
use Illuminate\Support\ServiceProvider;

class YnhServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {

        // $this->sqlSuccessLog();  // 打开注释可记录执行成功的sql，测试使用

        Response::macro('success', function ($data = null) {
            return response()->json([
                'status_code' => 200,
                'message'     => config('errorcode.code')[200],
                'data'        => $data,
            ]);
        });

        Response::macro('fail', function ($code, $data = null) {
            return response()->json([
                'status_code' => $code,
                'message'     => config('errorcode.code')[$code],
                'data'        => $data,
            ]);
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // TODO: Implement register() method.
    }

    // 测试使用
    private function sqlSuccessLog()
    {
        //打印出完整的sql语句，日志storage/log/xxx_query.log
        \DB::listen(
            function ($sql) {
                foreach ($sql->bindings as $i => $binding) {
                    if ($binding instanceof \DateTime) {
                        $sql->bindings[$i] = $binding->format('\'Y-m-d H:i:s\'');
                    } else {
                        if (is_string($binding)) {
                            $sql->bindings[$i] = "'$binding'";
                        }
                    }
                }

                // Insert bindings into query
                $query = str_replace(array('%', '?'), array('%%', '%s'), $sql->sql);

                $query = vsprintf($query, $sql->bindings);

                // Save the query to file
                $file = storage_path('logs' . DIRECTORY_SEPARATOR . date('Y-m-d') . '_query.log');
                $logFile = fopen(
                    $file,
                    'a+'
                );
                chown($file,'www');
                chgrp($file,'www');
                fwrite($logFile, date('Y-m-d H:i:s') . ': ' . $query . PHP_EOL);
                fclose($logFile);
            }
        );
    }

}