<?php

namespace App\Providers;

use App\Lib\Redis\RedisLock;
use Illuminate\Support\ServiceProvider;

/**
 * redis锁服务
 * 请在redis服务之后提供
 * Class RedisLockServiceProvider
 * @package App\Providers
 */
class RedisLockServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton("redisLock", function (){
            return new RedisLock(app("redis")->connection());
        });
    }
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['redisLock'];
    }
}
