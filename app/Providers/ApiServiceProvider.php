<?php

namespace App\Providers;


use App\Entity\Constant\ApiResponseCode;
use App\Lib\Helpers\ResponseHelper;
use \Response;
use Illuminate\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {

        //$this->sqlSuccessLog();  // 打开注释可记录执行成功的sql，测试使用

        Response::macro('success', function ($data = []) {
            return ResponseHelper::json(ApiResponseCode::SUCCESS, $data);
        });

        Response::macro('fail', function ($code, $data = null) {

            if (!empty($data) && is_string($data)) {
                //这里为了兼容原来的写法特殊
                return ResponseHelper::json($code, null, $data);
            }
            return ResponseHelper::json($code, $data);
        });

        Response::macro('failError', function ($error) {
            return ResponseHelper::json(ApiResponseCode::FAIL_ERROR, null, $error);
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // TODO: Implement register() method.
    }

    // 测试使用
    private function sqlSuccessLog()
    {
        //打印出完整的sql语句，日志storage/log/xxx_query.log
        \DB::listen(
            function ($sql) {
                foreach ($sql->bindings as $i => $binding) {
                    if ($binding instanceof \DateTime) {
                        $sql->bindings[$i] = $binding->format('\'Y-m-d H:i:s\'');
                    } else {
                        if (is_string($binding)) {
                            $sql->bindings[$i] = "'$binding'";
                        }
                    }
                }

                // Insert bindings into query
                $query = str_replace(array('%', '?'), array('%%', '%s'), $sql->sql);

                $query = vsprintf($query, $sql->bindings);

                // Save the query to file
                $file = storage_path('logs' . DIRECTORY_SEPARATOR . date('Y-m-d') . '_query.log');
                $logFile = fopen(
                    $file,
                    'a+'
                );
                chown($file, 'www');
                chgrp($file, 'www');
                fwrite($logFile, date('Y-m-d H:i:s') . ': ' . $query . PHP_EOL);
                fclose($logFile);
            }
        );
    }

}