<?php
/**
 * Created by PhpStorm.
 * User: Hank
 * Date: 2019/2/26 0026
 * Time: 下午 20:40
 */

namespace App\Providers\LogProvider;



class LogConfig
{
    private $dingDingProToken = "da8c1f40faeb5ac500d935d0e1d78fde365842e1511b36587de9258ca8cf91a8";
    private $dingDingTestToken = "f484c39671cace0b7fa34830c1a8c8d80fe2d2279bdae3400d56d031b9c82ed1";
    //3.30 3:30:30
    private $dingDingStartTime = null;
    //4.7 23:59:59
    private $dingDingEndTime = null;
    /**
     * @return string
     */
    public function getDingDingProToken()
    {
        return $this->dingDingProToken;
    }

    /**
     * @return string
     */
    public function getDingDingTestToken()
    {
        return $this->dingDingTestToken;
    }

    /**
     * @return null
     */
    public function getDingDingStartTime()
    {
        return $this->dingDingStartTime;
    }

    /**
     * @return null
     */
    public function getDingDingEndTime()
    {
        return $this->dingDingEndTime;
    }



}