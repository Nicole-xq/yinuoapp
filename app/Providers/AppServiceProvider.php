<?php

namespace App\Providers;

use App\Providers\LogProvider\LogConfig;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->_initMonoLog(\Log::getMonolog());
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->_registerDevProvider();
        $this->_rpcRegister();
    }
    private function _registerDevProvider(){
        if (!isProduction()) {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
            $this->app->register(\L5Swagger\L5SwaggerServiceProvider::class);
            $this->app->register(\Iber\Generator\ModelGeneratorProvider::class);
        }
    }
    private function _rpcRegister(){
        $this->app->register(\Galaxy\Framework\Service\ServiceProvider::class);
        $this->app->singleton("shopService", function (){
            $client = app("galaxy-client");
            $client = $client->with('Server.Rpc.Service.ShopNc.ShopService');
            return $client;
        });
        $this->app->singleton("shopUserService", function (){
            $client = app("galaxy-client");
            $client = $client->with('Server.Rpc.Service.ShopNc.UserService');
            return $client;
        });
    }
    private function _initMonoLog(\Monolog\Logger  $logger){
        $logConfig = new LogConfig();
        //日志到钉钉
        $dingdingHandler = new \App\Providers\LogProvider\DingDingHandler(\Monolog\Logger::ERROR,
            isProduction() ? $logConfig->getDingDingProToken() : $logConfig->getDingDingTestToken(),
            true,
            $logConfig->getDingDingStartTime(),
            $logConfig->getDingDingEndTime()
        );
        $logger->pushHandler($dingdingHandler);
    }
}
