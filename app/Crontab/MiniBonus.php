<?php
/**
 * Created by PhpStorm.
 * User: Mr.Liu
 * Date: 2018/9/13 0013
 * Time: 15:33
 */
include "Head.php";

use Illuminate\Support\Facades\DB;

class MiniBonus
{
    protected $model;

    public function run()
    {
        $this->model = DB::table('mini_amount');
        //获取依次递增的金额
        $IncreaseNum = $this->getIncreaseNum();
        $update = ['initial_amount' => DB::raw('initial_amount + ' . $IncreaseNum)];
        $result = DB::table('mini_amount')->update($update);
        if ($result) {
            echo 'success';
            die;
        }
        echo 'fail';
        die;
    }

    public function getIncreaseNum()
    {
        //活动时间
        $days = env('MINI_DAYS', 7);
        $amount = $this->model->lists('initial_amount');
        $count = 5000000;
        //本地递增金额
        return intval(($count - $amount[0]) / ($days * 24));
    }
}

(new MiniBonus())->run();