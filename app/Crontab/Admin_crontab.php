<?php
namespace App\Crontab;
/**
 * Created by Test, 2018/07/18 13:04.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

include "Head.php";

class Admin_crontab
{
    public function index(){
        $minute = date('i');
        $list = DB::table('crontab')->where([['status',1],['start_time','<=',date('Y-m-d H:i:s')],['end_time','>=',date('Y-m-d H:i:s')]])->get();
        if($list){
            foreach($list as $k=>$v){
                if($minute%$v['interval_time'] != 0){
                    continue;
                }
                switch($v['data_source']){
                    case 1:
                        $this->_article_crond($v);
                        break;
                    case 2:
                        $this->_exhibition_crond($v);
                        break;
                    default:
                        break;
                }
                echo date('Y-m-d H:i:s').$v['title']."(ID:{$v['id']})执行完成!\r\n";
            }
        }
    }

    private function _article_crond($data){
        $click_num = 0;
        $like_num = 0;
        if($data['max_click_num'] > 0){
            $click_num = rand($data['min_click_num'],$data['max_click_num']);
        }

        if($data['max_like_num'] > 0){
            $like_num = rand($data['min_like_num'],$data['max_like_num']);
        }
        $db = 'ynys_user_1';
        if($data['data_id'] == 0){
            $id_list = DB::table('article_title')->select('id')->get();
            if(!empty($id_list)){
                foreach($id_list as $id){
                    $id = $id['id'];
                    if($data['max_click_num'] > 0){
                        $click_num = rand($data['min_click_num'],$data['max_click_num']);
                    }

                    if($data['max_like_num'] > 0){
                        $like_num = rand($data['min_like_num'],$data['max_like_num']);
                    }
                    if($like_num == 0 && $click_num == 0){
                        continue;
                    }
                    try{
                        DB::beginTransaction();
                        DB::connection($db)->beginTransaction();

                        $re = DB::update("update article_title set like_num = `like_num`+{$like_num},click_num = `click_num`+{$click_num},admin_click_num = `admin_click_num`+{$click_num},admin_like_num = `admin_like_num`+{$like_num} WHERE id = ?",[$id]);
                        if($re != 1){
                            throw new Exception('common库更新失败');
                        }
                        $re = DB::connection($db)->update("update article set like_num = `like_num`+{$like_num},click_num = `click_num`+{$click_num},admin_click_num = `admin_click_num`+{$click_num},admin_like_num = `admin_like_num`+{$like_num} WHERE article_id = ?",[$id]);
                        if($re != 1){
                            throw new Exception('分库更新失败');
                        }
                        DB::commit();
                        DB::connection($db)->commit();
                    }catch(Exception $e){
                        echo date('Y-m-d H:i:s')."通用计划:{$data['id']},id:{$id}".$e->getMessage()."\r\n";
                        DB::rollback();
                        DB::connection($db)->rollback();
                    }
                }
            }

        }else{
            try{
                DB::beginTransaction();
                DB::connection($db)->beginTransaction();


                $re = DB::update("update article_title set like_num = `like_num`+{$like_num},click_num = `click_num`+{$click_num},admin_click_num = `admin_click_num`+{$click_num},admin_like_num = `admin_like_num`+{$like_num} WHERE id = ?",[$data['data_id']]);
                if($re != 1){
                    throw new Exception('common库更新失败');
                }
                $re = DB::connection($db)->update("update article set like_num = `like_num`+{$like_num},click_num = `click_num`+{$click_num},admin_click_num = `admin_click_num`+{$click_num},admin_like_num = `admin_like_num`+{$like_num} WHERE article_id = ?",[$data['data_id']]);
                if($re != 1){
                    throw new Exception('分库更新失败');
                }
                DB::commit();
                DB::connection($db)->commit();
            }catch(Exception $e){
                echo date('Y-m-d H:i:s')."crond_id:{$data['id']}".$e->getMessage()."\r\n";
                DB::rollback();
                DB::connection($db)->rollback();
            }
        }
    }

    private function _exhibition_crond($data){
        $click_num = 0;
        $like_num = 0;
        if($data['max_click_num'] > 0){
            $click_num = rand($data['min_click_num'],$data['max_click_num']);
        }

        if($data['max_like_num'] > 0){
            $like_num = rand($data['min_like_num'],$data['max_like_num']);
        }

        if($data['data_id'] == 0){
            $id_list = DB::table('exhibition_artwork_list')->select('id')->get();
            if(!empty($id_list)){
                foreach($id_list as $id){
                    $id = $id['id'];
                    if($data['max_click_num'] > 0){
                        $click_num = rand($data['min_click_num'],$data['max_click_num']);
                    }

                    if($data['max_like_num'] > 0){
                        $like_num = rand($data['min_like_num'],$data['max_like_num']);
                    }
                    if($like_num == 0 && $click_num == 0){
                        continue;
                    }
                    $re = DB::update("update exhibition_artwork_list set like_num = `like_num`+{$like_num},click_num = `click_num`+{$click_num},admin_click_num = `admin_click_num`+{$click_num},admin_like_num = `admin_like_num`+{$like_num} WHERE id = ?",[$id]);
                    if($re != 1){
                        echo date('Y-m-d H:i:s')."通用计划:{$data['id']},id:{$id}更新失败";
                    }
                }
            }
        }else{
            if($like_num == 0 && $click_num == 0){
                return;
            }
            $re = DB::update("update exhibition_artwork_list set like_num = `like_num`+{$like_num},click_num = `click_num`+{$click_num},admin_click_num = `admin_click_num`+{$click_num},admin_like_num = `admin_like_num`+{$like_num} WHERE id = ?",[$data['data_id']]);
            if($re != 1){
                echo date('Y-m-d H:i:s')."crond_id:{$data['id']}更新失败";
            }
        }

    }
}
(new Admin_crontab())->index();