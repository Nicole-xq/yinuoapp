<?php
/**
 * Created by Test, 2018/06/11 16:33.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Admin\Models\Exhibition_imageModel
 *
 * @property int $id 自增ID
 * @property int|null $exhibition_id 会展ID
 * @property string|null $exhibition_pic 会展图片名称
 * @property-read \App\Admin\Models\ExhibitionModel|null $exhibition_info
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Exhibition_imageModel whereExhibitionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Exhibition_imageModel whereExhibitionPic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Exhibition_imageModel whereId($value)
 * @mixin \Eloquent
 */
class Exhibition_imageModel extends Model
{
    protected $table = 'exhibition_artwork_pic';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $fillable = ['id','exhibition_pic'];

    public function exhibition_info(){
        $relatedModel = ExhibitionModel::class;
        return $this->belongsTo($relatedModel,'exhibition_id','id');
    }
}