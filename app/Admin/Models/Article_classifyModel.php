<?php
/**
 * Created by Test, 2018/06/06 10:04.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Admin\Models\Article_classifyModel
 *
 * @property int $id 自增ID
 * @property int|null $parent_id 上一级ID
 * @property string|null $type_name 类型名称
 * @property int|null $sort 排序
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_classifyModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_classifyModel whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_classifyModel whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_classifyModel whereTypeName($value)
 * @mixin \Eloquent
 */
class Article_classifyModel extends Model
{
    protected $table = 'article_classify';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public static function boot(){
        parent::boot();

    }

}