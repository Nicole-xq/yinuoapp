<?php
/**
 * Created by Test, 2018/06/11 14:55.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Admin\Models\CorrelationModel
 *
 * @mixin \Eloquent
 */
class CorrelationModel extends Model
{

}