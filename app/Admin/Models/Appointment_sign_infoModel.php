<?php
/**
 * Created by Test, 2018/06/22 11:38.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Admin\Models\Appointment_sign_infoModel
 *
 * @mixin \Eloquent
 */
class Appointment_sign_infoModel extends Model
{
    protected $table = 'appointment_info';
    protected $primaryKey = 'id';
}