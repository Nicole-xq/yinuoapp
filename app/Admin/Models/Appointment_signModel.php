<?php
/**
 * Created by Test, 2018/06/21 15:40.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Admin\Models\Appointment_signModel
 *
 * @property int $id
 * @property int|null $exhibition_id 展览id
 * @property int|null $appointment_id 约展id
 * @property int|null $user_id 报名人id
 * @property int|null $status 状态:0,未审核 1,通过 2,不通过
 * @property int $is_delete 发起人是否删除：0未删，1已删除
 * @property int|null $is_read 是否阅读：1已阅读，0未阅读
 * @property string|null $address 地址信息
 * @property string|null $updated_at 修改时间
 * @property string|null $created_at 添加时间
 * @property-read \App\Admin\Models\Appointment_listModel $exhibition_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Appointment_signModel whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Appointment_signModel whereAppointmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Appointment_signModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Appointment_signModel whereExhibitionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Appointment_signModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Appointment_signModel whereIsDelete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Appointment_signModel whereIsRead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Appointment_signModel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Appointment_signModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Appointment_signModel whereUserId($value)
 * @mixin \Eloquent
 */
class Appointment_signModel extends Model
{
    protected $table = 'appointment_sign';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function exhibition_name(){
        $relatedModel = Appointment_listModel::class;
        return $this->hasOne($relatedModel,'id');
    }
}