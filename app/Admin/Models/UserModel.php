<?php
namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Created by Test, 2018/05/31 14:34.
 *
 * @author serpent.
 * 
 * Copyright (c) 2018 serpent All rights reserved.
 * @property int $user_id 会员id
 * @property string|null $user_key
 * @property string|null $user_name 会员名称
 * @property string|null $user_nickname 会员昵称
 * @property string|null $user_avatar 会员头像
 * @property int|null $user_sex 会员性别 1为男 2为女
 * @property string|null $user_real_name 真实姓名
 * @property string|null $user_birthday 生日
 * @property string|null $user_flag 签名
 * @property string|null $user_passwd 会员密码
 * @property string|null $user_email 会员邮箱
 * @property int|null $user_email_bind 0未绑定1已绑定
 * @property string|null $user_mobile 手机号
 * @property int|null $user_mobile_bind 0未绑定1已绑定
 * @property int|null $user_login_num 登录次数
 * @property string|null $user_login_time 当前登录时间
 * @property string|null $user_old_login_time 上次登录时间
 * @property string|null $user_login_ip 当前登录ip
 * @property string|null $user_old_login_ip 上次登录ip
 * @property string|null $weixin_unionid 微信用户统一标识
 * @property string|null $weixin_info 微信用户相关信息
 * @property string|null $weixin_open_id 微信openID
 * @property string|null $sina_unionid 新浪用户统一标识
 * @property string|null $sina_info 新浪用户相关信息
 * @property string|null $qq_unionid qq用户统一标识
 * @property int|null $user_areaid 地区ID
 * @property int|null $user_cityid 城市ID
 * @property int|null $user_provinceid 省份ID
 * @property string|null $user_areainfo 地区内容
 * @property string|null $alipay_account 支付宝账户
 * @property string|null $alipay_name 支付宝名称
 * @property int|null $user_alipay_bind 支付宝绑定 0否 1是
 * @property string|null $db 数据库
 * @property int|null $authentication_type 认证类型(1: 艺术家认证, 2: 机构认证, 0: 未认证)
 * @property int|null $authentication_id 认证关联ID(艺术家表的艺术家ID,或者机构ID)
 * @property int|null $is_disable 禁用：1是，0否
 * @property string|null $remember_token
 * @property string|null $created_at 添加时间
 * @property string|null $updated_at 修改时间
 * @property string|null $qq_info qq用户相关信息
 * @property string|null $read_time 用户阅读系统推送消息时间
 * @property int|null $fans_num 用户未阅读粉丝消息条数
 * @property int|null $comment_num 用户未阅读评论相关消息条数
 * @property int|null $appointment_num 用户未阅读约展消息条数
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereAlipayAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereAlipayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereAppointmentNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereAuthenticationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereAuthenticationType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereCommentNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereDb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereFansNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereIsDisable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereQqInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereQqUnionid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereReadTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereSinaInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereSinaUnionid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereUserAlipayBind($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereUserAreaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereUserAreainfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereUserAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereUserBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereUserCityid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereUserEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereUserEmailBind($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereUserFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereUserKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereUserLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereUserLoginNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereUserLoginTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereUserMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereUserMobileBind($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereUserName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereUserNickname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereUserOldLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereUserOldLoginTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereUserPasswd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereUserProvinceid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereUserRealName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereUserSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereWeixinInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereWeixinOpenId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\UserModel whereWeixinUnionid($value)
 * @mixin \Eloquent
 */
class UserModel extends Model
{
    protected $table = 'user_index';
    protected $primaryKey = 'user_id';
    public $timestamps = false;

    public static function boot(){
        parent::boot();

        static::saved(function($model){
//            if($model->user_id){
//                $arr = array(
//                    'user_name'=>$model->user_name,
//                    'user_nickname'=>$model->user_nickname,
//                    'user_avatar'=>$model->user_avatar,
//                    'user_sex'=>$model->user_sex,
//                    'user_passwd'=>$model->user_passwd,
//                    'user_mobile'=>$model->user_mobile
//                );
//                User_infoModel::where(['user_id'=>$model->user_id])->update($arr);
//            }else{
            $user = User_infoModel::find($model->user_id);
            if(empty($user)){
                $arr = array(
                    'user_id'=>$model->user_id,
                    'user_name'=>$model->user_name,
                    'user_nickname'=>$model->user_nickname,
                    'user_avatar'=>$model->user_avatar,
                    'user_sex'=>$model->user_sex,
                    'user_passwd'=>$model->user_passwd,
                    'user_mobile'=>$model->user_mobile
                );
                User_infoModel::insertGetId($arr);
            }else{
                $arr = array(
                    'user_name'=>$model->user_name,
                    'user_nickname'=>$model->user_nickname,
                    'user_avatar'=>$model->user_avatar,
                    'user_sex'=>$model->user_sex,
                    'user_passwd'=>$model->user_passwd,
                    'user_mobile'=>$model->user_mobile
                );
                User_infoModel::where(['user_id'=>$model->user_id])->update($arr);
            }
//            }
//            echo '<pre>';
//            var_dump($model);exit;
        });
    }
}