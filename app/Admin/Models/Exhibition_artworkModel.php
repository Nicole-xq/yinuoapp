<?php
/**
 * Created by Test, 2018/07/02 13:16.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Admin\Models\Exhibition_artworkModel
 *
 * @property int $id 自增ID
 * @property int|null $exhibition_id 会展ID
 * @property int|null $artist_id 艺术家ID
 * @property int|null $artwork_id 作品ID
 * @property-read \App\Admin\Models\ExhibitionModel|null $exhibition
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Exhibition_artworkModel whereArtistId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Exhibition_artworkModel whereArtworkId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Exhibition_artworkModel whereExhibitionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Exhibition_artworkModel whereId($value)
 * @mixin \Eloquent
 */
class Exhibition_artworkModel extends Model
{
    protected $table = 'exhibition_artwork_joint';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $fillable = ['artist_id','artwork_id'];

    public function exhibition(){
        return $this->belongsTo(ExhibitionModel::class,'exhibition_id');
    }
}