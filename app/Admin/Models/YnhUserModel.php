<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/8/8 0008
 * Time: 10:09
 */

namespace App\Admin\Models;



use Illuminate\Database\Eloquent\Model;

/**
 * App\Admin\Models\YnhUserModel
 *
 * @property int $id
 * @property int|null $user_id 用户id
 * @property string|null $user_key
 * @property int|null $status 状态:0,未审核 1,通过 2,不通过 3,未申请
 * @property int|null $is_disable 禁用：1是，0否
 * @property \Carbon\Carbon|null $created_at 身份认证时间
 * @property \Carbon\Carbon|null $updated_at 用户状态更新时间
 * @property-read \App\Admin\Models\UserModel $unionArtist
 * @property-read \App\Admin\Models\UserModel $userIndex
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhUserModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhUserModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhUserModel whereIsDisable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhUserModel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhUserModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhUserModel whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhUserModel whereUserKey($value)
 * @mixin \Eloquent
 */
class YnhUserModel extends Model
{
    protected $table = 'ynh_user';

    /**
     * 用户详情 关联类型表
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function userIndex()
    {
        return $this->hasOne(UserModel::class, 'user_id', 'user_id');
    }

    /**
     * 关联艺术家 关联类型表
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function unionArtist()
    {
        return $this->hasOne(UserModel::class, 'user_id', 'id');
    }
}