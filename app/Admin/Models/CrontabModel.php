<?php
/**
 * Created by Test, 2018/07/18 09:45.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Admin\Models\CrontabModel
 *
 * @property int $id
 * @property string|null $title 标题
 * @property int|null $data_source 数据源类型:1,资讯 2,会展
 * @property int|null $min_click_num 最小阅读量
 * @property int|null $max_click_num 最大阅读量
 * @property int|null $min_like_num 最小点赞量
 * @property int|null $max_like_num 最大点赞量
 * @property int|null $interval_time 间隔时间(分钟)
 * @property int|null $sleep_time sleep时间(秒)
 * @property string|null $data_id
 * @property string|null $start_time 开始时间
 * @property string|null $end_time 结束时间
 * @property int|null $status 状态:0,关闭 1,开启
 * @property \Carbon\Carbon|null $updated_at 更新时间
 * @property \Carbon\Carbon|null $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CrontabModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CrontabModel whereDataId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CrontabModel whereDataSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CrontabModel whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CrontabModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CrontabModel whereIntervalTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CrontabModel whereMaxClickNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CrontabModel whereMaxLikeNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CrontabModel whereMinClickNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CrontabModel whereMinLikeNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CrontabModel whereSleepTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CrontabModel whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CrontabModel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CrontabModel whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CrontabModel whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CrontabModel extends Model
{
    protected $table = 'crontab';
    protected $primaryKey = 'id';
}