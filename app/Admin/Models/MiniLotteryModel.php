<?php
/**
 * Created by PhpStorm.
 * User: Mr.Liu
 * Date: 2018/8/31 0031
 * Time: 9:58
 */

namespace App\Admin\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Admin\Models\MiniLotteryModel
 *
 * @property int $id 主键ID
 * @property string $mini_code 用户唯一标识
 * @property int $user_id 活动用户ID
 * @property string $nick_name
 * @property int $type 1: 红包 2: 提现
 * @property float $win_amount 中奖金额或者提现金额
 * @property \Carbon\Carbon $created_at 抽奖时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\MiniLotteryModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\MiniLotteryModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\MiniLotteryModel whereMiniCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\MiniLotteryModel whereNickName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\MiniLotteryModel whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\MiniLotteryModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\MiniLotteryModel whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\MiniLotteryModel whereWinAmount($value)
 * @mixin \Eloquent
 */
class MiniLotteryModel extends Model
{
    protected $table = 'mini_lottery';
}