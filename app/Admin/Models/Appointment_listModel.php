<?php
/**
 * Created by Test, 2018/06/21 17:25.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Admin\Models\Appointment_listModel
 *
 * @property int $id 自增ID
 * @property int|null $exhibition_id 会展ID
 * @property string|null $exhibition_name 展览名称
 * @property int|null $user_id 用户ID
 * @property string|null $user_nickname 用户昵称
 * @property string|null $user_avatar 用户头像
 * @property int|null $go_date 约展时间
 * @property int|null $time_node 时间段：0全天，1上午，2下午
 * @property int|null $sex 约展人性别：0不限，1男，2女
 * @property string|null $address 地址信息
 * @property int $count_num 报名总人数
 * @property \Carbon\Carbon|null $updated_at 修改时间
 * @property \Carbon\Carbon|null $created_at 添加时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Appointment_listModel whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Appointment_listModel whereCountNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Appointment_listModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Appointment_listModel whereExhibitionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Appointment_listModel whereExhibitionName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Appointment_listModel whereGoDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Appointment_listModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Appointment_listModel whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Appointment_listModel whereTimeNode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Appointment_listModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Appointment_listModel whereUserAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Appointment_listModel whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Appointment_listModel whereUserNickname($value)
 * @mixin \Eloquent
 */
class Appointment_listModel extends Model
{
    protected $table = 'appointment_list';
    protected $primaryKey = 'id';
}