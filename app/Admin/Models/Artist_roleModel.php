<?php
/**
 * Created by Test, 2018/06/07 17:33.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Models;

/**
 * App\Admin\Models\Artist_roleModel
 *
 * @property int $role_id 自增ID
 * @property string|null $role_name 角色名称
 * @property int|null $role_type 属性：1个人，2机构
 * @property string|null $role_img 角色徽章
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_roleModel whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_roleModel whereRoleImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_roleModel whereRoleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_roleModel whereRoleType($value)
 * @mixin \Eloquent
 */
class Artist_roleModel extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'artist_role';
    protected $primaryKey = 'role_id';
    public $timestamps = false;
}