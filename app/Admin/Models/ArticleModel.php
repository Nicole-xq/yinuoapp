<?php
/**
 * Created by Test, 2018/06/05 15:29.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Admin\Models\ArticleModel
 *
 * @property int $id
 * @property int|null $classify_id 分类id
 * @property int|null $author_id 作者ID
 * @property string|null $author_name 作者（编辑，可与author_id不一致）
 * @property string|null $title 文章标题
 * @property string|null $picture 文章图片
 * @property string|null $description 描述
 * @property int|null $like_num 点赞量
 * @property int|null $click_num 阅读量
 * @property int|null $admin_like_num 虚拟点赞量
 * @property int|null $admin_click_num 虚拟阅读量
 * @property int|null $vote_agree_num 投票(顶)数量
 * @property int|null $vote_disagree_num 投票(踩)数量
 * @property int|null $comment_num 评论量
 * @property int|null $city_id 城市id
 * @property int|null $is_recommend 推荐：1是，0否。（首页展示）
 * @property int|null $is_top 精选：1是，0否。（设置精选后，列表显示大图）
 * @property int|null $is_delete 删除：1是，0否
 * @property int|null $status 状态:0,未审核 1,通过 2,不通过
 * @property \Carbon\Carbon|null $updated_at 修改时间
 * @property \Carbon\Carbon|null $created_at 添加时间
 * @property-read \App\Admin\Models\Article_infoModel $articleInfo
 * @property mixed $pic
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArticleModel whereAdminClickNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArticleModel whereAdminLikeNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArticleModel whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArticleModel whereAuthorName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArticleModel whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArticleModel whereClassifyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArticleModel whereClickNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArticleModel whereCommentNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArticleModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArticleModel whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArticleModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArticleModel whereIsDelete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArticleModel whereIsRecommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArticleModel whereIsTop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArticleModel whereLikeNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArticleModel wherePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArticleModel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArticleModel whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArticleModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArticleModel whereVoteAgreeNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArticleModel whereVoteDisagreeNum($value)
 * @mixin \Eloquent
 */
class ArticleModel extends Model
{
    protected $table = 'article_title';
    protected $primaryKey = 'id';
    protected $guarded = [];

//    public $timestamps = false;
    public static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
            //修改状态值
            if ($model->status == 0) {
                $model->status = 1;
            }
        });
    }

    public function setPicAttribute($pic)
    {
        if (is_array($pic)) {
            $this->attributes['pic'] = json_encode($pic);
        }
    }

    public function getPicAttribute($pic)
    {
        return json_decode($pic);
    }

    /**
     * 用户详情 关联类型表
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function articleInfo()
    {
        return $this->hasOne(Article_infoModel::class, 'article_id', 'id');
    }

    public function get_article_list($condition = [],$files = '*',$order = 'id desc'){
        $obj = $this::select($files);
        $obj->where($condition);
        $obj->orderByRaw($order);
        $result = $obj->paginate($this->page);
        return $result;
    }
}