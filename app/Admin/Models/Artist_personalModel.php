<?php
/**
 * Created by Test, 2018/06/07 17:13.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Admin\Models\Artist_personalModel
 *
 * @property int $id 自增ID
 * @property int|null $user_id 用户ID
 * @property int|null $role_id 角色ID
 * @property int|null $role_grade 角色等级
 * @property string|null $name 名称
 * @property string|null $artist_type 艺术分类
 * @property string|null $real_name 用户身份证名字
 * @property string|null $identification_num 身份证号码
 * @property string|null $identification_pic 身份证图片(正反面)
 * @property string|null $logo LOGO
 * @property string|null $description 描述
 * @property int|null $article_num 文章数量
 * @property int|null $province_id 省
 * @property int|null $city_id 市
 * @property int|null $area_id 区
 * @property string|null $address 详细地址
 * @property string|null $addr 定位（经纬）
 * @property string|null $authentication_time 认证通过时间
 * @property string|null $updated_at 修改时间
 * @property string|null $created_at 添加时间
 * @property string|null $artwork_time 艺术家作品更新时间
 * @property int|null $is_recommend 推荐：1是，0否
 * @property int|null $store_id 店铺ID
 * @property string|null $shop_url 艺术店铺
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_personalModel whereAddr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_personalModel whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_personalModel whereAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_personalModel whereArticleNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_personalModel whereArtistType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_personalModel whereArtworkTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_personalModel whereAuthenticationTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_personalModel whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_personalModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_personalModel whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_personalModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_personalModel whereIdentificationNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_personalModel whereIdentificationPic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_personalModel whereIsRecommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_personalModel whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_personalModel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_personalModel whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_personalModel whereRealName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_personalModel whereRoleGrade($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_personalModel whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_personalModel whereShopUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_personalModel whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_personalModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_personalModel whereUserId($value)
 * @mixin \Eloquent
 */
class Artist_personalModel extends Model
{
    protected $table = 'artist_personal';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
            if (is_array($model->artist_type)) {
                $model->artist_type = implode(',', $model->artist_type);
            }
        });
    }

}