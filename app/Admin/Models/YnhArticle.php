<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/8/21 0021
 * Time: 11:27
 */

namespace App\Admin\Models;

use App\Admin\Test;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Admin\Models\YnhArticle
 *
 * @property int $id 自增ID
 * @property int $article_id 文章id
 * @property int|null $author_id 作者ID
 * @property string|null $author_name 作者名称
 * @property string|null $title 标题
 * @property int|null $classify_id 文章类型id
 * @property string|null $pic 图片(9张图片没问题)
 * @property string|null $share_pic 分享缩略图
 * @property string|null $share_description 分享描述
 * @property string|null $description 文字描述
 * @property string|null $content 正文
 * @property int|null $province_id 省
 * @property int|null $city_id 城市
 * @property string|null $addr 定位
 * @property int|null $click_num 阅读量
 * @property int|null $like_num 点赞量
 * @property int|null $admin_click_num 虚拟阅读量
 * @property int|null $admin_like_num 虚拟点赞量
 * @property int|null $vote_agree_num 投票(顶)数量
 * @property int|null $vote_disagree_num 投票(踩)数量
 * @property int|null $comment_num 评论量
 * @property int|null $is_allowspeak 是否允许发言：1禁止 0允许
 * @property int|null $is_recommend 推荐：1推荐，0未推荐
 * @property int|null $is_top 精选：1是，0否
 * @property int|null $is_delete 删除：1是，0否
 * @property int|null $status 状态:0,未审核 1,通过 2,不通过
 * @property \Carbon\Carbon|null $updated_at 修改时间
 * @property \Carbon\Carbon|null $created_at 发布时间
 * @property int|null $topic_status 举报审核：0未举报，1被举报，2审核(通过)，3审核(不通过)
 * @property string|null $examine_msg 审核内容
 * @property int|null $admin_id 管理员ID
 * @property int|null $examine_time 审核时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereAddr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereAdminClickNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereAdminLikeNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereAuthorName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereClassifyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereClickNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereCommentNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereExamineMsg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereExamineTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereIsAllowspeak($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereIsDelete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereIsRecommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereIsTop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereLikeNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle wherePic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereShareDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereSharePic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereTopicStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereVoteAgreeNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhArticle whereVoteDisagreeNum($value)
 * @mixin \Eloquent
 */
class YnhArticle extends Model
{
    protected $table = 'article';
    protected $primaryKey = 'article_id';
    protected $connection = 'ynys_user_1';

    public static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
            $modelFilter = [
                'author_name', 'title', 'classify_id', 'pic', 'description', 'content',
                'province_id', 'city_id', 'addr'
            ];
            foreach ($modelFilter as $value) {
                if (isset($model->$value)) {
                    unset($model->$value);
                }
            }
            //修改状态值
            if ($model->status == 0) {
                $model->status = 1;
            }
            $tmp_img = $model->share_pic;
            if (is_array($tmp_img)) {
                foreach ($tmp_img as $k => $v) {
                    if (is_null($v)) {
                        unset($tmp_img[$k]);
                    }
                }
                $tmp_img = json_encode($tmp_img);
            }
            $tmp = array(
                'is_recommend' => $model->is_recommend,
                'is_top' => $model->is_top,
                'status' => $model->status,
            );
            $tmp['updated_at'] = date('Y-m-d H:i:s');
            ArticleModel::where(['id' => $model->article_id])->update($tmp);
        });
    }

    public function setPicAttribute($pic)
    {
        if (is_array($pic)) {
            foreach ($pic as $k => $v) {
                if (is_null($v)) {
                    unset($pic[$k]);
                }
            }
            $this->attributes['pic'] = json_encode($pic);
        }
    }

    public function getPicAttribute($pic)
    {
        return json_decode($pic);
    }
}