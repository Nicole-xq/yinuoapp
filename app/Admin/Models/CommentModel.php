<?php
/**
 * Created by Test, 2018/06/12 09:56.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Admin\Models\CommentModel
 *
 * @property int $id 自增ID
 * @property int|null $article_id 文章ID
 * @property int|null $author_id 作者ID
 * @property int|null $parent_id 上级id
 * @property int|null $commentator_id 评论人ID
 * @property string|null $commentator_nickname 评论人昵称
 * @property int|null $accepter_id 被评论人ID
 * @property string|null $accepter_nickname 被评论人昵称
 * @property string|null $msg 留言信息
 * @property int|null $state 状态：1已读，0未读
 * @property int|null $like_num 点赞数量
 * @property \Carbon\Carbon|null $updated_at 修改时间
 * @property \Carbon\Carbon|null $created_at 添加时间
 * @property int|null $is_delete 删除：1是，0否
 * @property int|null $is_examine 审核：0未审核，1通过，2未通过
 * @property int|null $is_show 评论列表是否展示：1都显示，2仅评论人显示，3仅被评论人显示，3都不显示
 * @property int|null $admin_id 审核人
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CommentModel whereAccepterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CommentModel whereAccepterNickname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CommentModel whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CommentModel whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CommentModel whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CommentModel whereCommentatorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CommentModel whereCommentatorNickname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CommentModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CommentModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CommentModel whereIsDelete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CommentModel whereIsExamine($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CommentModel whereIsShow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CommentModel whereLikeNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CommentModel whereMsg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CommentModel whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CommentModel whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\CommentModel whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CommentModel extends Model
{
    protected $table = 'article_comment';
    protected $primaryKey = 'id';

}