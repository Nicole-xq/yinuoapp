<?php
/**
 * Created by Test, 2018/06/08 17:29.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Admin\Models\Artwork_categoryModel
 *
 * @property int $id 自增ID
 * @property int|null $parent_id 上级ID
 * @property string|null $name 类型名称
 * @property string|null $type_info 属性名称
 * @property string|null $updated_at 修改时间
 * @property string|null $created_at 添加时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artwork_categoryModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artwork_categoryModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artwork_categoryModel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artwork_categoryModel whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artwork_categoryModel whereTypeInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artwork_categoryModel whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Artwork_categoryModel extends Model
{
    protected $table = 'artwork_category';
    protected $primaryKey = 'id';
    public $timestamps = false;
}