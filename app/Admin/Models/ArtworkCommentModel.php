<?php
/**
 * Created by PhpStorm.
 * User: Mr.Liu
 * Date: 2018/10/19 0019
 * Time: 15:28
 */

namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Admin\Models\ArtworkCommentModel
 *
 * @property int $id 自增ID
 * @property int|null $artwork_id 作品ID
 * @property int|null $author_id 作者ID
 * @property int|null $parent_id 上级id
 * @property int|null $commentator_id 评论人ID
 * @property string|null $commentator_nickname 评论人昵称
 * @property int|null $accepter_id 被评论人ID
 * @property string|null $accepter_nickname 被评论人昵称
 * @property string|null $comment_msg 留言信息
 * @property int|null $state 状态：1已读，0未读
 * @property int|null $like_num 点赞数量
 * @property \Carbon\Carbon $updated_at 修改时间
 * @property \Carbon\Carbon $created_at 添加时间
 * @property int|null $is_delete 删除：1是，0否
 * @property int|null $is_examine 审核：0未审核，1通过，2未通过
 * @property int|null $is_show 评论列表是否展示：1都显示，2仅评论人显示，3仅被评论人显示，3都不显示
 * @property int|null $admin_id 审核人
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArtworkCommentModel whereAccepterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArtworkCommentModel whereAccepterNickname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArtworkCommentModel whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArtworkCommentModel whereArtworkId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArtworkCommentModel whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArtworkCommentModel whereCommentMsg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArtworkCommentModel whereCommentatorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArtworkCommentModel whereCommentatorNickname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArtworkCommentModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArtworkCommentModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArtworkCommentModel whereIsDelete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArtworkCommentModel whereIsExamine($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArtworkCommentModel whereIsShow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArtworkCommentModel whereLikeNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArtworkCommentModel whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArtworkCommentModel whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\ArtworkCommentModel whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ArtworkCommentModel extends Model
{
    protected $table = 'artwork_comment';
    protected $primaryKey = 'id';

}