<?php
/**
 * Created by Test, 2018/06/20 15:05.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Admin\Models\User_infoModel
 *
 * @property int $user_id 自增用户ID
 * @property string|null $user_name 用户名
 * @property string|null $user_passwd 密码
 * @property string|null $user_nickname 昵称
 * @property string|null $user_avatar 头像
 * @property string|null $user_real_name 真实姓名
 * @property int|null $user_sex 性别：0未设置 1男 2女
 * @property string|null $user_birthday 年龄（时间戳）
 * @property string|null $user_flag 签名
 * @property string|null $user_provinceid 省份ID
 * @property string|null $user_cityid 城市ID
 * @property string|null $user_areaid 地区ID
 * @property string|null $user_areainfo 地区内容
 * @property string|null $user_addr 定位
 * @property string|null $user_mobile 安全手机（可登陆）
 * @property int|null $user_mobile_bind 0未绑定1已绑定
 * @property string|null $user_email 安全邮箱（可登陆）
 * @property string|null $weixin_unionid 微信unionid
 * @property string|null $weixin_open_id 微信openid
 * @property string|null $weixin_info 微信详情
 * @property string|null $sina_unionid 新浪用户统一标识
 * @property string|null $sina_info 新浪用户相关信息
 * @property string|null $qq_unionid QQ用户统一标识
 * @property string|null $qq_info QQ用户相关信息
 * @property string|null $user_question 密码提问
 * @property string|null $user_answer 密码回答
 * @property int|null $user_login_type 账号安全状态：1正常 0异常
 * @property string|null $user_reg_ip 注册IP
 * @property int|null $user_login_num 登录次数
 * @property string|null $user_login_time 当前登录时间
 * @property string|null $user_login_ip 当前登录ip
 * @property int|null $user_old_login_time 上次登录时间
 * @property string|null $user_old_login_ip 上次登录ip
 * @property int|null $is_disable 禁用：1是，0否
 * @property string|null $user_key 用户唯一标识
 * @property \Carbon\Carbon|null $updated_at 修改时间
 * @property \Carbon\Carbon|null $created_at 注册时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereIsDisable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereQqInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereQqUnionid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereSinaInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereSinaUnionid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUserAddr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUserAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUserAreaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUserAreainfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUserAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUserBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUserCityid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUserEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUserFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUserKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUserLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUserLoginNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUserLoginTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUserLoginType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUserMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUserMobileBind($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUserName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUserNickname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUserOldLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUserOldLoginTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUserPasswd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUserProvinceid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUserQuestion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUserRealName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUserRegIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereUserSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereWeixinInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereWeixinOpenId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\User_infoModel whereWeixinUnionid($value)
 * @mixin \Eloquent
 */
class User_infoModel extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'user_id';
    protected $connection = 'ynys_user_1';

}