<?php
/**
 * Created by Test, 2018/06/08 16:10.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Admin\Models\Artist_organizationModel
 *
 * @property int $id 自增ID
 * @property int|null $user_id 用户ID
 * @property int|null $role_id 角色ID
 * @property int|null $role_grade 角色等级
 * @property string|null $artist_type 艺术分类
 * @property string|null $name 名称
 * @property string|null $nick_name 机构-公司简称
 * @property string|null $real_name 用户身份证名字
 * @property string|null $identification_num 身份证号码
 * @property string|null $identification_pic 身份证图片(正反面)
 * @property string|null $logo LOGO
 * @property string|null $description 描述
 * @property int|null $article_num 文章数量
 * @property int|null $province_id 省
 * @property int|null $city_id 市
 * @property int|null $area_id 区
 * @property string|null $address 详细地址
 * @property string|null $addr 定位（经纬）
 * @property string|null $updated_at 修改时间
 * @property string|null $created_at 添加时间
 * @property string|null $phone_num 机构-联系电话
 * @property string|null $brand_pic 机构-商标专利图片
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_organizationModel whereAddr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_organizationModel whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_organizationModel whereAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_organizationModel whereArticleNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_organizationModel whereArtistType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_organizationModel whereBrandPic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_organizationModel whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_organizationModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_organizationModel whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_organizationModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_organizationModel whereIdentificationNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_organizationModel whereIdentificationPic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_organizationModel whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_organizationModel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_organizationModel whereNickName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_organizationModel wherePhoneNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_organizationModel whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_organizationModel whereRealName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_organizationModel whereRoleGrade($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_organizationModel whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_organizationModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_organizationModel whereUserId($value)
 * @mixin \Eloquent
 */
class Artist_organizationModel extends Model
{
    protected $table = 'artist_organization';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public static function boot(){
        parent::boot();
        static::saving(function($model){
            if(is_array($model->artist_type)){
                $model->artist_type = implode(',',$model->artist_type);
            }
        });

    }
}