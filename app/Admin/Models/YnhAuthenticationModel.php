<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/8/6 0006
 * Time: 16:38
 */

namespace App\Admin\Models;


use App\Models\ArtistOrganization;
use App\Models\Ynh\YnhAuthenticationApply;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;

/**
 * App\Admin\Models\YnhAuthenticationModel
 *
 * @property int $apply_id
 * @property int|null $user_id 用户ID(这里的user_id 取的是user_index的ID)
 * @property string|null $real_name 个人-真实姓名, 机构-公司名称
 * @property string|null $real_avatar 个人-个人照片
 * @property string|null $trinity_num 机构-三证合一营业执照号
 * @property string|null $document_pic 个人-奖项证书图片, 机构-营业执照图片
 * @property string|null $identification_num 个人-用户身份证号码, 机构-法人身份证号码
 * @property string|null $identification_pic 身份证图片(正1反2面)
 * @property string|null $name 机构-法人姓名
 * @property string|null $logo LOGO
 * @property int|null $role_id 角色ID
 * @property string|null $role_grade 角色等级
 * @property string|null $artist_type 艺术分类(创作分类)
 * @property int|null $province_id 省
 * @property int|null $city_id 市
 * @property int|null $area_id 区
 * @property string|null $address 详细地址
 * @property string|null $addr 定位（经纬）
 * @property string|null $description 简介
 * @property int|null $is_valid 状态：0未审核、1审核通过、2审核不通过
 * @property string|null $fail_cause 审核失败原因
 * @property int|null $authentication_type 认证类型(1: 艺术家认证, 2: 机构认证)
 * @property int|null $authentication_id 认证关联ID(艺术家表的艺术家ID,或者机构ID)
 * @property \Carbon\Carbon|null $created_at 创建时间
 * @property \Carbon\Carbon|null $updated_at 修改时间
 * @property string|null $nick_name 个人-认证昵称, 机构-公司简称
 * @property string|null $phone_num 个人-手机号, 机构-联系电话
 * @property int|null $user_sex 个人-用户性别 1为男 2为女, 默认 男
 * @property int|null $nation 目前分1中国,2海外
 * @property string|null $artwork_pic 个人-作品图片
 * @property string|null $brand_pic 机构-商标专利图片
 * @property string|null $authentication_time 认证通过时间
 * @property int|null $is_recommend 推荐：1是，0否
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereAddr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereApplyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereArtistType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereArtworkPic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereAuthenticationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereAuthenticationTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereAuthenticationType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereBrandPic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereDocumentPic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereFailCause($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereIdentificationNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereIdentificationPic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereIsRecommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereIsValid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereNation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereNickName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel wherePhoneNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereRealAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereRealName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereRoleGrade($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereTrinityNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\YnhAuthenticationModel whereUserSex($value)
 * @mixin \Eloquent
 */
class YnhAuthenticationModel extends Model
{
    protected $table = 'ynh_authentication_apply';
    protected $primaryKey = 'apply_id';

    public static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
            unset($model['real_avatar'], $model['role_id'], $model['artist_type']);
            unset($model['nation'], $model['user_sex']);
            if (is_array($model->artist_type)) {
                $model->artist_type = implode(',', $model->artist_type);
            }
            //修改状态值
            if ($model->is_valid == 0) {
                $model->is_valid = 1;
            }
            if ($model->is_valid == 1) {
                $update['status'] = 1;
                if (!empty($model->authentication_id)) {
                    $update['user_id'] = $model->user_id;
                    if ($model->authentication_type == 1) {
                        $art_model = new Artist_personalModel();
                    } else {
                        $art_model = new Artist_organizationModel();
                    }
                    $art_model->where(['id' => $model->authentication_id])->update(['user_id' => $model->user_id]);
                    $UserIndex_update['authentication_id'] = $model->authentication_id;
                } else {
                    $app_id = $model->apply_id;
                    $insertInfo = (new YnhAuthenticationApply())->where(['apply_id' => $app_id])->first();
                    if ($model->authentication_type == 1) {
                        $insert['user_id'] = $insertInfo['user_id'] ?? '';
                        $insert['role_id'] = $insertInfo['role_id'] ?? 0;
                        $insert['role_grade'] = $insertInfo['role_grade'] ?? 0;
                        $insert['name'] = $insertInfo['nick_name'] ?? '';
                        $insert['artist_type'] = $insertInfo['artist_type'] ?? '';
                        $insert['real_name'] = $insertInfo['real_name'] ?? '';
                        $insert['logo'] = $insertInfo['real_avatar'] ?? '';
                        $insert['identification_num'] = $insertInfo['identification_num'] ?? '';
                        $insert['identification_pic'] = $insertInfo['identification_pic'] ?? '';
                        $insert['description'] = $insertInfo['description'] ?? '';
                        $insert['province_id'] = $insertInfo['province_id'] ?? '';
                        $insert['city_id'] = $insertInfo['city_id'] ?? '';
                        $insert['area_id'] = $insertInfo['area_id'] ?? '';
                        //$insert['address'] = $insertInfo['address'] ?? '';
                        //$insert['addr'] = $insertInfo['addr'] ?? '';
                        $insert['created_at'] = date('Y-m-d H:i:s');
                        $insert['authentication_time'] = date('Y-m-d H:i:s');
                        $insert['is_recommend'] = $model->is_recommend ?? 0;
                        $artist_id = Artist_personalModel::insertGetId($insert);
                        //清楚redis 最新认证推荐艺术家三位
                        if ($model->is_recommend) {
                            Redis::del('recommend_three_artist');
                        }
                    } else {
                        $insert['user_id'] = $insertInfo['user_id'] ?? '';
                        $insert['role_id'] = $insertInfo['role_id'] ?? '';
                        $insert['role_grade'] = $insertInfo['role_grade'] ?? 0;
                        $insert['name'] = $insertInfo['name'] ?? '';
                        $insert['nick_name'] = $insertInfo['nick_name'] ?? '';
                        $insert['artist_type'] = $insertInfo['artist_type'] ?? '';
                        $insert['real_name'] = $insertInfo['real_name'] ?? '';
                        $insert['logo'] = $insertInfo['logo'] ?? '';
                        $insert['brand_pic'] = $insertInfo['brand_pic'] ?? '';
                        $insert['identification_num'] = $insertInfo['identification_num'] ?? '';
                        $insert['identification_pic'] = $insertInfo['identification_pic)'] ?? '';
                        $insert['description'] = $insertInfo['description'] ?? '';
                        $insert['province_id'] = $insertInfo['province_id'] ?? '';
                        $insert['city_id'] = $insertInfo['city_id'] ?? '';
                        $insert['area_id'] = $insertInfo['area_id'] ?? '';
                        $insert['address'] = $insertInfo['address'] ?? '';
                        $insert['phone_num'] = $insertInfo['phone_num'] ?? '';
                        $insert['addr'] = $insertInfo['addr'] ?? '31.16768,121.42922';
                        $insert['created_at'] = date('Y-m-d H:i:s');
                        $artist_id = ArtistOrganization::insertGetId($insert);
                    }
                    $UserIndex_update['authentication_id'] = $artist_id;
                }
                $UserIndex_update['authentication_type'] = $model->authentication_type;
                UserModel::where(['user_id' => $model->user_id])->update($UserIndex_update);
                $model->authentication_id = $artist_id;
                $model->authentication_time = date('Y-m-d H:i:s');
            } else {
                $update['status'] = 2;
            }
            YnhUserModel::where(['user_id' => $model->user_id])->update($update);
            $cache_key = 'user_index_' . $model->user_id;
            Redis::del($cache_key, 'user_info');
        });
    }

    function getIdentificationPicAttribute($json)
    {
        if (is_string($json)) {
            return json_decode($json);
        }
        return $json;
    }

}