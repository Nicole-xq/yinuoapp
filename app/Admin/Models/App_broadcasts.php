<?php

namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Admin\Models\App_broadcasts
 *
 * @property int $id 主键
 * @property string $title 标题
 * @property string $subtitle 副标题
 * @property string $description 描述
 * @property string $goto_type 跳转动作：(wap/native)
 * @property string $param 参数(wap链接，文章ID，展览ID，……）
 * @property string $msg_class 推广类型: 1资讯、2展览、3活动
 * @property string $state 发送状态: 0未发送、1已发送、2定时发送
 * @property string|null $send_time 发送时间
 * @property int $admin_id 管理员ID
 * @property string $admin_name 管理员账号
 * @property \Carbon\Carbon $updated_at 修改时间
 * @property \Carbon\Carbon $created_at 创建时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\App_broadcasts whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\App_broadcasts whereAdminName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\App_broadcasts whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\App_broadcasts whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\App_broadcasts whereGotoType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\App_broadcasts whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\App_broadcasts whereMsgClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\App_broadcasts whereParam($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\App_broadcasts whereSendTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\App_broadcasts whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\App_broadcasts whereSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\App_broadcasts whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\App_broadcasts whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class App_broadcasts extends Model
{

}
