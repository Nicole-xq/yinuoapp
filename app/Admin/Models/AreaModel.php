<?php
/**
 * Created by Test, 2018/06/13 16:56.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Admin\Models\AreaModel
 *
 * @property int $area_id 索引ID
 * @property string $area_name 地区名称
 * @property int $area_parent_id 地区父ID
 * @property int $area_sort 排序
 * @property int $area_deep 地区深度，从1开始
 * @property string|null $area_region 大区名称
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\AreaModel whereAreaDeep($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\AreaModel whereAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\AreaModel whereAreaName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\AreaModel whereAreaParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\AreaModel whereAreaRegion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\AreaModel whereAreaSort($value)
 * @mixin \Eloquent
 */
class AreaModel extends Model
{
    protected $table = 'area';
    protected $primaryKey = 'area_id';
    public $timestamps = false;
}