<?php
/**
 * Created by Test, 2018/06/07 20:59.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Models;

/**
 * App\Admin\Models\Artist_type_jointModel
 *
 * @property int|null $artist_id 艺术家ID
 * @property int|null $artist_type_id 艺术类别表ID
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Admin\Models\Artist_type_jointModel[] $artist_type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_type_jointModel whereArtistId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_type_jointModel whereArtistTypeId($value)
 * @mixin \Eloquent
 */
class Artist_type_jointModel extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'artist_type_joint';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function artist_type(){
        return $this->belongsToMany(Artist_type_jointModel::class);
    }
}