<?php
/**
 * Created by Test, 2018/06/07 21:12.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Admin\Models\Artist_typeModel
 *
 * @property int $id 自增ID
 * @property string|null $name 类别名称
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_typeModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Artist_typeModel whereName($value)
 * @mixin \Eloquent
 */
class Artist_typeModel extends Model
{
    protected $table = 'artist_type';
    protected $primaryKey = 'id';
    public $timestamps = false;
}