<?php
/**
 * Created by Test, 2018/06/05 16:09.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Models;

use App\Admin\Test;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Admin\Models\Article_infoModel
 *
 * @property int $id 自增ID
 * @property int $article_id 文章id
 * @property int|null $author_id 作者ID
 * @property string|null $author_name 作者名称
 * @property string|null $title 标题
 * @property int|null $classify_id 文章类型id
 * @property string|null $pic 图片(9张图片没问题)
 * @property string|null $share_pic 分享缩略图
 * @property string|null $share_description 分享描述
 * @property string|null $description 文字描述
 * @property string|null $content 正文
 * @property int|null $province_id 省
 * @property int|null $city_id 城市
 * @property string|null $addr 定位
 * @property int|null $click_num 阅读量
 * @property int|null $like_num 点赞量
 * @property int|null $admin_click_num 虚拟阅读量
 * @property int|null $admin_like_num 虚拟点赞量
 * @property int|null $vote_agree_num 投票(顶)数量
 * @property int|null $vote_disagree_num 投票(踩)数量
 * @property int|null $comment_num 评论量
 * @property int|null $is_allowspeak 是否允许发言：1禁止 0允许
 * @property int|null $is_recommend 推荐：1推荐，0未推荐
 * @property int|null $is_top 精选：1是，0否
 * @property int|null $is_delete 删除：1是，0否
 * @property int|null $status 状态:0,未审核 1,通过 2,不通过
 * @property \Carbon\Carbon|null $updated_at 修改时间
 * @property \Carbon\Carbon|null $created_at 发布时间
 * @property int|null $topic_status 举报审核：0未举报，1被举报，2审核(通过)，3审核(不通过)
 * @property string|null $examine_msg 审核内容
 * @property int|null $admin_id 管理员ID
 * @property int|null $examine_time 审核时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereAddr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereAdminClickNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereAdminLikeNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereAuthorName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereClassifyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereClickNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereCommentNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereExamineMsg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereExamineTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereIsAllowspeak($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereIsDelete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereIsRecommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereIsTop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereLikeNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel wherePic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereShareDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereSharePic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereTopicStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereVoteAgreeNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Article_infoModel whereVoteDisagreeNum($value)
 * @mixin \Eloquent
 */
class Article_infoModel extends Model
{
    protected $table = 'article';
    protected $primaryKey = 'article_id';
    protected $connection = 'ynys_user_1';

    public static function boot(){
        parent::boot();
        static::saving(function($model){
            //修改状态值
            if ($model->status == 0) {
                $model->status = 1;
            }
//            echo '<pre>';
//            var_dump($model);exit;
            $tmp_img = $model->pic;
            if(is_array($tmp_img)){
                foreach($tmp_img as $k=>$v){
                    if(is_null($v)){
                        unset($tmp_img[$k]);
                    }
                }
                $tmp_img = json_encode($tmp_img);
            }
            $tmp = array(
                'classify_id'=>$model->classify_id,
                'author_id'=>$model->author_id,
                'author_name'=>$model->author_name,
                'title'=>$model->title,
                'picture'=>$tmp_img,//is_array($model->pic)?json_encode($model->pic):$model->pic,
                'description'=>$model->description,
                'is_recommend'=>$model->is_recommend,
                'is_top'=>$model->is_top,
                'city_id'=>$model->city_id,
                'status'=>$model->status,
                'is_delete'=>$model->is_delete
            );
            if(isset($model->article_id)){
                $tmp['updated_at'] = date('Y-m-d H:i:s');
                ArticleModel::where(['id'=>$model->article_id])->update($tmp);
//                $model->updated = $tmp['updated_at'];
            }else{
                $tmp['created_at'] = date('Y-m-d H:i:s');
                $article_id = ArticleModel::insertGetId($tmp);
                $model->article_id = $article_id;
            }
        });
    }

    public function setPicAttribute($pic){
//        print_R($pic);exit;
        if(is_array($pic)){
            foreach($pic as $k=>$v){
                if(is_null($v)){
                    unset($pic[$k]);
                }
            }
            $this->attributes['pic'] = json_encode($pic);
        }
    }

    public function getPicAttribute($pic){
        return json_decode($pic);
    }

//    public function getContentAttribute($content){
//        return htmlspecialchars_decode($content);
//    }
//    public function setContentAttribute($content){
//        $this->attributes['content'] = htmlspecialchars($content);
//    }
//    public function setCityIdAttribute($city){
//        if(is_array($city)){
//            $this->attributes['city_id'] = $city[0];
//        }
//    }
//
//    public function getCityIdAttribute($city){
//        return array($city);
//    }
}