<?php
/**
 * Created by PhpStorm.
 * User: Mr.Liu
 * Date: 2018/8/30 0030
 * Time: 10:00
 */

namespace App\Admin\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Admin\Models\MiniRecordModel
 *
 * @property int $id 主键ID
 * @property string|null $openid 小程序用户open_id
 * @property string $nick_name nick_name
 * @property int|null $is_invited 0 : 用户自主访问小程序 1: 用户受邀请访问小程序
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\MiniRecordModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\MiniRecordModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\MiniRecordModel whereIsInvited($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\MiniRecordModel whereNickName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\MiniRecordModel whereOpenid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\MiniRecordModel whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class MiniRecordModel extends Model
{
    protected $table = 'mini_record';
}