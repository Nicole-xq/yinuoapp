<?php
/**
 * Created by Test, 2018/06/27 15:56.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Admin\Models\SlideshowModel
 *
 * @property int $id
 * @property string|null $name 轮播图名称
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $created_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Admin\Models\Slideshow_infoModel[] $slideshow_info
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\SlideshowModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\SlideshowModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\SlideshowModel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\SlideshowModel whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SlideshowModel extends Model
{
    protected $table = 'slideshow';
    protected $primaryKey = 'id';


    function slideshow_info(){
        return $this->hasMany(Slideshow_infoModel::class,'slideshow_id');
    }

}