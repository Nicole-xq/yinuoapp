<?php
/**
 * Created by Test, 2018/06/27 16:05.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Admin\Models\Slideshow_infoModel
 *
 * @property int $id
 * @property int $slideshow_id
 * @property string|null $title 标题
 * @property string|null $img 图片
 * @property string|null $url 链接
 * @property-read \App\Admin\Models\SlideshowModel $slideshow
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Slideshow_infoModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Slideshow_infoModel whereImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Slideshow_infoModel whereSlideshowId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Slideshow_infoModel whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Admin\Models\Slideshow_infoModel whereUrl($value)
 * @mixin \Eloquent
 */
class Slideshow_infoModel extends Model
{
    protected $table = 'slideshow_info';
    protected $primaryKey = 'id';
    protected $fillable = ['title','img','url'];
    public $timestamps = false;

    function slideshow(){
        return $this->belongsTo(SlideshowModel::class,'painter_id');
    }
}