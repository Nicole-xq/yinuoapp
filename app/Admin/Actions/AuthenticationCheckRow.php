<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/8/8 0008
 * Time: 9:19
 */

namespace App\Admin\Actions;

use Encore\Admin\Admin;

class AuthenticationCheckRow
{
    protected $id;

    public function __construct($id, $user_key = '')
    {
        $this->id = $id;
        $this->user_key = $user_key;
    }

    protected function script()
    {
        return <<<SCRIPT

$('.grid-check-row').unbind('click').click(function() {
        $.ajax({
            method: 'get',
            url: '/admin/authentication/check_info?type='+ $(this).data('type')
            +'&id='+$(this).data('id'),
            data: {
            },
            success: function (data) {
                $.pjax.reload('#pjax-container');
                if (data == 1) {
                    toastr.success('成功');
                }else{
                    toastr.error('失败');
                }
            }
        });
});

SCRIPT;
    }

    protected function render()
    {
        Admin::script($this->script());

        return "<a class='btn btn-xs btn-success fa fa-check grid-check-row' data-type='1' data-id='{$this->id}'></a>
                <a class='btn btn-xs btn-warning fa fa-remove grid-check-row' data-type='2' data-id='{$this->id}'></a>";
    }

    public function __toString()
    {
        return $this->render();
    }
}