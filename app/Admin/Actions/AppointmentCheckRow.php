<?php
/**
 * Created by Test, 2018/06/22 16:57.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Actions;

use Encore\Admin\Admin;

class AppointmentCheckRow
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    protected function script()
    {
        return <<<SCRIPT

$('.grid-check-row').unbind('click').click(function() {
        $.ajax({
            method: 'get',
            url: '/admin/appointment_sign/check_info?type='+ $(this).data('type')+'&id='+$(this).data('id'),
            data: {
            },
            success: function (data) {
                $.pjax.reload('#pjax-container');

                if (data == 1) {
                    toastr.success('成功');
                }else{
                    toastr.error('失败');
                }
            }
        });
});

SCRIPT;
    }

    protected function render()
    {
        Admin::script($this->script());

        return "<a class='btn btn-xs btn-success fa fa-check grid-check-row' data-type='1' data-id='{$this->id}'></a>
                <a class='btn btn-xs btn-warning fa fa-remove grid-check-row' data-type='2' data-id='{$this->id}'></a>";
    }

    public function __toString()
    {
        return $this->render();
    }
}