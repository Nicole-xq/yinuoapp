<?php

use Illuminate\Routing\Router;

Admin::registerHelpersRoutes();

Route::group([
    'prefix'        => config('admin.prefix'),
    'namespace'     => Admin::controllerNamespace(),
    'middleware'    => ['web', 'admin'],
], function (Router $router) {
    $router->get('/article_review/{id}/get_area','YnhArticleReview@get_area');
    $router->get('/exhibition/get_artwork','ExhibitionController@get_artwork');
    $router->get('/exhibition/{id}/get_artwork','ExhibitionController@get_artwork');
    $router->get('/article/{id}/get_area','ArticleController@get_area');
    $router->get('/article/get_area','ArticleController@get_area');
    $router->get('/artist_organization/{id}/get_area','Artist_organizationController@get_area');
    $router->get('/artist_organization/get_area','Artist_organizationController@get_area');
    $router->get('/artist_personal/{id}/get_area','Artist_personalController@get_area');
    $router->get('/artist_personal/get_area','Artist_personalController@get_area');
    $router->get('/comment/check_info','CommentController@check_info');
    $router->get('/artwork_comment/check_info','ArtworkCommentController@check_info');
    $router->get('/appointment_sign/check_info','Appointment_signController@check_info');

    //ynh
    $router->get('/ynh_user/check_info','YnhUser@check_info');
    //单条详情
    $router->get('/article_review/check_info','YnhArticleReview@check_info');
    $router->get('/personal_authentication/check_info','YnhPersonalAuthentication@check_info');


    $router->resource('crontab',CrontabController::class);
    $router->resource('slideshow',SlideshowController::class);
    $router->resource('appointment_list',Appointment_listController::class);
    $router->resource('appointment_sign',Appointment_signController::class);
    $router->resource('user',UserController::class);
    $router->resource('article',ArticleController::class);
    $router->resource('article_classify',Article_classifyController::class);
    $router->resource('artist_personal',Artist_personalController::class);
    $router->resource('artist_organization',Artist_organizationController::class);
    $router->resource('artwork',ArtworkController::class);
    $router->resource('exhibition',ExhibitionController::class);
    $router->resource('comment',CommentController::class);
    $router->resource('artwork_comment',ArtworkCommentController::class);
    $router->resource('app_broadcast',AppBroadcastController::class);
    $router->get('/', 'HomeController@index');

    //Ynh
    //用户管理
    $router->resource('ynh_user', YnhUser::class);
    //个人认证
    $router->resource('personal_authentication', YnhPersonalAuthentication::class);
    //机构认证
    $router->resource('organization_authentication', YnhOrganizationAuthentication::class);
    //文章发布审核
    $router->resource('article_review', YnhArticleReview::class);
    //作品发布审核
    $router->resource('artwork_review', YnhArtworkReview::class);

    //小程序
    $router->resource('mini_user_analysis', MiniUserController::class);
    $router->resource('mini_amount_analysis', MiniAmountController::class);


});
