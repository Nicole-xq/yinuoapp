<?php

namespace App\Admin\Controllers;

use App\Admin\Models\App_broadcasts;
use App\Admin\Models\Article_classifyModel;
use App\Admin\Models\ArticleModel;
use App\Admin\Models\ExhibitionModel;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;
use App\Logic\notification;
use Illuminate\Support\MessageBag;
use Validator;

class AppBroadcastController extends Controller
{
    public $msg_name = [
        '1' => 'news',
        '2' => 'exhibition',
        '3' => 'activity',
        '4' => 'vote',
    ];
    public $goto_type = [
        '1' => 'native',
        '2' => 'native',
        '3' => 'wap',
        '4' => 'native',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        return redirect(url('admin/app_broadcast/create'));

        // 消息列表，待完善
        return Admin::content(function (Content $content) {
            $content->header('推送管理');
            $content->description('消息列表');
            $content->body($this->grid());//指向grid方法显示表格
        });
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('消息管理');
            $content->description('创建消息');
            $content->body($this->form());
        });
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|max:24',
            'subtitle' => 'required|max:24',
            'description' => 'required|max:50',
            'msg_class' => 'required',
            'param' => 'required',
            'state' => 'required',
        ];
        if ($request->get('msg_class') == '3') {
            $rules['param'] .= '|url';
        } else {
            $rules['param'] .= '|numeric';
        }
        $message = [
            '*.required' => '请填写信息',
            '*.url' => '请填写正确链接',
            '*.numeric' => '请填写数字格式',
            'title.max' => '限制24个字符',
            'subtitle.max' => '限制24个字符',
            'description.max' => '限制50个字符',
        ];
        $validator = Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            return redirect($request->header('referer'))->withErrors($validator)->withInput();
        }
        if ($request->msg_class == 1) {
            $article = ArticleModel::where('id', $request->param)->first();
            if (empty($article)) {
                $validator = Validator::make($request->all(), ['param' => 'max:0'], ['*.max' => '文章不存在']);
            } else if ($article->is_delete) {
                $validator = Validator::make($request->all(), ['param' => 'max:0'], ['*.max' => '文章已删除']);
            } else if ($article->status == 0) {
                $validator = Validator::make($request->all(), ['param' => 'max:0'], ['*.max' => '文章未审核']);
            } else if ($article->status == 2) {
                $validator = Validator::make($request->all(), ['param' => 'max:0'], ['*.max' => '文章未审核通过']);
            }
        }
        if ($request->msg_class == 2) {
            $exhibition = ExhibitionModel::where('id', $request->param)->first();
            if (empty($exhibition)) {
                $validator = Validator::make($request->all(), ['param' => 'max:0'], ['*.max' => '会展不存在']);
            } else if ($exhibition->is_delete) {
                $validator = Validator::make($request->all(), ['param' => 'max:0'], ['*.max' => '会展已删除']);
            }
        }
        if ($validator->fails()) {
            return redirect($request->header('referer'))->withErrors($validator)->withInput();
        }
        if ($request->msg_class == 1 && isset($article)) {
            $class_id = $article->classify_id;
            do {
                $article_classify = Article_classifyModel::find($class_id);
                $class_id = $article_classify->parent_id;
            } while ($class_id > 0);
            $msg_class = $article_classify->id;
        } else {
            $msg_class = $request->msg_class;
        }

        $AndroidState = null;
        $IOSState = null;
        $goto_type = $this->goto_type[$request->msg_class];
        $param = $request->all();
        $param['goto_type'] = $goto_type;
        $param['extra'] = [
            'msg_class' => $msg_class,
            'msg_name' => $this->msg_name[$msg_class],
            'param' => $request->get('param', null),
        ];

        if ($request->state == 1) {// 立即发送
            $notification = new notification();
            list($AndroidState, $IOSState) = $notification->sendAppBroadCast($param);
        }

        $app_broadcasts = new App_broadcasts;

        $app_broadcasts->title = $request->title;
        $app_broadcasts->subtitle = $request->subtitle;
        $app_broadcasts->description = $request->description;
        $app_broadcasts->param = $request->param;
        $app_broadcasts->goto_type = $goto_type;
        $app_broadcasts->msg_class = $request->msg_class;
        $app_broadcasts->state = $request->state;
        $app_broadcasts->admin_id = Admin::user()->id;
        $app_broadcasts->admin_name = Admin::user()->username;

        $result = $app_broadcasts->save();

        if ($result) {
            $message = [
                'title' => '成功',
                'message' => '保存成功',
            ];
        } else {
            $message = [
                'title' => '失败',
                'message' => '保存失败',
            ];
        }
        if ($request->state == 1) {// 立即发送
            if ($AndroidState) {
                $message['message'] .= "<br>" . 'Android 发送成功';
            } else {
                $message['message'] .= "<br>" . 'Android 发送失败';
            }
            if ($IOSState) {
                $message['message'] .= "<br>" . 'ios 发送成功';
            } else {
                $message['message'] .= "<br>" . 'ios 发送失败';
            }
        }
        $success = new MessageBag($message);
        return back()->with(compact('success'));
    }

    /**
     * Alias of method row.
     *
     * @param mixed $content
     *
     * @return Content
     */
    public function body($content)
    {
        return $this->row($content);
    }

    /**
     * Add one row for content body.
     *
     * @param $content
     *
     * @return $this
     */
    public function row($content)
    {
        if ($content instanceof Closure) {
            $row = new Row();
            call_user_func($content, $row);
            $this->addRow($row);
        } else {
            $this->addRow(new Row($content));
        }

        return $this;
    }

    /**
     * Add Row.
     *
     * @param Row $row
     */
    protected function addRow(Row $row)
    {
        $this->rows[] = $row;
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(App_broadcasts::class, function (Grid $grid) {
            $grid->id('ID')->sortable();
            $grid->title('标题');
            $grid->subtitle('副标题');
            $grid->description('描述');
            $grid->param('消息参数');
            $grid->msg_class('推广类型')->display(function ($state) {
                switch ($state) {
                    case '1':
                        $tmp = '资讯';
                        break;
                    case '2':
                        $tmp = '展览';
                        break;
                    default:
                        $tmp = '活动';
                        break;
                }
                return $tmp;
            });
            $grid->state('发送状态')->display(function ($state) {
                switch ($state) {
                    case 1:
                        $tmp = '<span style=\'color:greenyellow\'>已发送</span>';
                        break;
                    default:
                        $tmp = '未发送';
                        break;
                }
                return $tmp;
            });
            $grid->admin_id('管理员ID');
            $grid->admin_name('管理员账号');
            $grid->updated_at('修改时间')->sortable();
            $grid->created_at('创建时间')->sortable();
            $grid->actions(function ($actions) {
                $actions->disableEdit();
                $actions->disableDelete();
            });
//            $grid->disableActions();
            $grid->disableRowSelector();
            $grid->disableExport();
//            $grid->disableBatchDeletion();
        });
    }

    /**
     * 创建一个表单生成器.
     *
     * @return Form
     */
    public function form()
    {
        return Admin::form(App_broadcasts::class, function (Form $form) {
            $msg_class = [
                '1' => '资讯消息',
                '2' => '展览消息',
                '3' => '活动消息',
            ];
            $form->radio('msg_class', '推广类型')->options($msg_class)->value('1');
            $form->text('title', '标题')->rules('required');
            $form->text('subtitle', '副标题')->rules('required');
            $form->text('description', '描述')->rules('required');
            $form->text('param', '必要参数')->rules('required')->placeholder('资讯ID，展览ID，活动链接 . . .');
            $send_state = [
                '1' => '立即发送',
                '0' => '暂不发送',
            ];
            $form->radio('state', '发送状态')->options($send_state)->default('1');
        });
    }

}