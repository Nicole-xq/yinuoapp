<?php
/**
 * Created by Test, 2018/06/08 16:08.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Controllers;

use App\Admin\Models\AreaModel;
use App\Admin\Models\Artist_organizationModel;
use App\Admin\Models\Artist_roleModel;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;

class Artist_organizationController extends Controller
{
    use ModelForm;

    function index()
    {
        return Admin::content(function (Content $content) {
//            admin_toastr('laravel-admin 提示','success');
            $content->header('机构管理');//这里是页面标题
            $content->description('机构列表');//这里是详情描述
//
            $content->body($this->grid());//指向grid方法显示表格
        });
    }

     /**
     * Edit interface.
     *
     * @param $id
     *
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('机构管理');
            $content->description('编辑');
            $content->body($this->form()->edit($id));
        });
    }


    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('机构管理');
            $content->description('创建');
            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Artist_organizationModel::class,function (Grid $grid) {
            $grid->id('ID')->sortable();
            $grid->name('机构名称');
            $grid->role_id('角色')->display(function($role_id){

                $re = Artist_roleModel::find($role_id);
                if($re){
                    return $re->role_name;
                }
            });
            $grid->user_id('是否认证')->display(function($user_id){
                if($user_id){
                    return '是';
                }else{
                    return '否';
                }
            });
            $grid->filter(function($filter){
                $filter->like('name','名称');
            });
            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableDelete();
            });
            $grid->disableRowSelector();
            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {
        return Admin::form(Artist_organizationModel::class,function (Form $form) {
            $form->text('name','机构名称')->rules('required');
            $form->text('user_id','关联用户id');
            $form->radio('role_id','角色')->values(Artist_roleModel::where(['role_type'=>2])->select()->pluck('role_name','role_id'));
            $form->image('logo','LOGO')->rules('required');
            $form->ckeditor('description','介绍');
            $form->select('province_id','省')->options(AreaModel::where(['area_parent_id'=>0])->select()->pluck('area_name','area_id'))->load('city_id','get_area');
            $form->select('city_id','市')->options(function($id){
                if($id){
                    $tmp = AreaModel::find($id);
                    $re = AreaModel::where(['area_parent_id'=>$tmp->area_parent_id])->select()->pluck('area_name','area_id');
                    $arr = array();
                    foreach($re as $k=>$v){
                        $arr[] = array(
                            'id'=>$k,
                            'text'=>$v
                        );
                    }
                    return $re;
                }
            })->load('area_id','get_area');
            $form->select('area_id','区')->options(function($id){
                if($id){
                    $tmp = AreaModel::find($id);
                    $re = AreaModel::where(['area_parent_id'=>$tmp->area_parent_id])->select()->pluck('area_name','area_id');
                    $arr = array();
                    foreach($re as $k=>$v){
                        $arr[] = array(
                            'id'=>$k,
                            'text'=>$v
                        );
                    }
                    return $re;
                }
            });
            $form->text('address','地址')->rules('required');
            $form->text('addr','经纬度')->default('0,0')->rules('required');
            $form->saving(function(Form $form){
                if($form->addr && $form->model()->addr != $form->addr){
                    $tmp = explode(',',$form->addr);
                    if(!empty($tmp)){
                        $form->addr = trim($tmp[1]).','.trim($tmp[0]);
                    }
                }
            });

        });

    }

    function get_area(){
        $area_id = $_GET['q'];
        $list = AreaModel::where(['area_parent_id'=>$area_id])->select()->pluck('area_name','area_id');
        $arr = array();
        foreach($list as $k=>$v){
            $arr[] = array(
                'id'=>$k,
                'text'=>$v
            );
        }
        return $arr;
    }
}