<?php
/**
 * Created by PhpStorm.
 * User: Mr.Liu
 * Date: 2018/8/30 0030
 * Time: 17:56
 */

namespace App\Admin\Controllers;

use App\Admin\Models\MiniLotteryModel;
use App\Admin\Models\YnhAuthenticationModel;
use App\Models\Ynh\YnhAuthenticationApply;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\Box;
use Encore\Admin\Widgets\Chart\Bar;
use Encore\Admin\Widgets\Chart\Doughnut;
use Encore\Admin\Widgets\Chart\Line;
use Encore\Admin\Widgets\Chart\Pie;
use Encore\Admin\Widgets\Chart\PolarArea;
use Encore\Admin\Widgets\Chart\Radar;
use Encore\Admin\Widgets\Collapse;
use Encore\Admin\Widgets\InfoBox;
use Encore\Admin\Widgets\Tab;
use Encore\Admin\Widgets\Table;
use Illuminate\Support\Facades\DB;

class MiniAmountController extends Controller
{
    use ModelForm;

    public function index()
    {
        // 消息列表，待完善
        return Admin::content(function (Content $content) {
            $content->header('小程序管理');
            $content->description('财务分析');
            $content->body($this->grid());//指向grid方法显示表格
        });
    }

    public function grid()
    {
        $model = new MiniLotteryModel();
        $create = 'created_at';
        $amount_count_where = [
            'type' => 1
        ];
        $transfer_count_where = [
            'type' => 2
        ];
        $today_begin = date('Y-m-d 00:00:00');
        $today_end = date('Y-m-d H:i:s');
        $yesterday_begin =date('Y-m-d 00:00:00', strtotime("-1 day"));
        $yesterday_end = date('Y-m-d H:i:s', strtotime("-1 day"));
        $amount_count = DB::table('mini_amount')->lists('real_amount')[0];
        $transfer_count = $model->where($transfer_count_where)->sum('win_amount');
        $today_amount = $model->where($amount_count_where)
            ->where($create, '>', $today_begin)
            ->where($create, '<=', $today_end)
            ->sum('win_amount');
        $today_transfer = $model->where($transfer_count_where)
            ->where($create, '>', $today_begin)
            ->where($create, '<=', $today_end)
            ->sum('win_amount');
        $yesterday_amount = $model->where($amount_count_where)
            ->where($create, '>', $yesterday_begin)
            ->where($create, '<=', $yesterday_end)
            ->sum('win_amount');
        $yesterday_transfer = $model->where($transfer_count_where)
            ->where($create, '>', $yesterday_begin)
            ->where($create, '<=', $yesterday_end)
            ->sum('win_amount');
        $data = [
            'amount_count' => $amount_count ?: '0.00',
            'transfer_count' => abs($transfer_count) ?: '0.00',
            'today_amount' => $today_amount ?: '0.00',
            'today_transfer' => abs($today_transfer) ?: '0.00',
            'yesterday_amount' => $yesterday_amount ?: '0.00',
            'yesterday_transfer' => abs($yesterday_transfer) ?: '0.00',
            'size' => 6
        ];
        return view('admin.amountAnalysis', ['data' => $data]);
    }
}