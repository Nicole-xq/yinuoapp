<?php
/**
 * Created by Test, 2018/06/21 13:18.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Controllers;

use App\Admin\Models\Appointment_listModel;
use App\Admin\Models\Appointment_signModel;
use App\Admin\Models\ExhibitionModel;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;

class Appointment_listController extends Controller
{
    use ModelForm;
    function index()
    {
        return Admin::content(function (Content $content){
//            admin_toastr('laravel-admin 提示','success');
            $content->header('约展管理');//这里是页面标题
            $content->description('约展列表');//这里是详情描述
//
            $content->body($this->grid());//指向grid方法显示表格
        });
    }

     /**
     * Edit interface.
     *
     * @param $id
     *
     * @return Content
     */
//    public function edit($id)
//    {
//        return Admin::content(function (Content $content) use ($id) {
//            $content->header('约展管理');
//            $content->description('编辑');
//            $content->body($this->grid1($id));
//        });
//    }
    /**
     * Create interface.
     *
     * @return Content
     */
//    public function create()
//    {
//        return Admin::content(function (Content $content) {
//            $content->header('新闻管理');
//            $content->description('添加新闻');
//            $content->body($this->form());
//        });
//    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(ExhibitionModel::class,function (Grid $grid) {
            $grid->id('ID')->sortable();
//            $grid->column('nickname','发起人')->display(function(){
//                $info = Appointment_listModel::find($this->appointment_id);
//                return $info->user_nickname;
//            });
            $grid->title('展览名称');
//            $grid->column('exhibition_name','展览名称')->display(function(){
//                $info = Appointment_listModel::find($this->appointment_id);
//                return $info->exhibition_name;
//            });
            $grid->column('appointment_num','发起约展数量')->display(function(){
                $count = Appointment_listModel::where('exhibition_id',$this->id)->count();
                return $count;
            })->label('warning');
            $grid->column('sign_num','总报名数')->display(function(){
                $check_true = Appointment_signModel::where(['exhibition_id'=>$this->id,'status'=>1])->count();
                return $check_true;
            })->label('info');

            $grid->column('appointment_sign_num','待审核')->display(function(){
                $check_false = Appointment_signModel::where(['exhibition_id'=>$this->id,'status'=>0])->count();
                return $check_false;
            })->label('warning');


            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $check_false = Appointment_signModel::where(['exhibition_id'=>$actions->row->id,'status'=>0])->count();
                if($check_false > 0){
                    $actions->append(
                        '<span><a href="/admin/appointment_sign/'.$actions->getKey().'">审核</a></span>'
                    );
                }
            $actions->disableEdit();
                $actions->disableDelete();
            });
//            $grid->filter(function($filter){
//                $filter->is('classify_id','栏目')->select(Article_classifyModel::all()->pluck('type_name','id'));
//                $filter->like('title','标题');
//                $filter->is('is_recommend','推荐')->select(['1'=>'已推荐','0'=>'未推荐']);
//                $filter->is('is_top','精选')->select(['0'=>'未精选','1'=>'已精选']);
//            });
            $grid->disableFilter();
            $grid->disableExport();
            $grid->disableRowSelector();
            $grid->disableCreation();
//            $grid->disableActions();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {
        return Admin::form(Article_infoModel::class,function (Form $form) {
            $form->text('article_id','ID')->readOnly();
            $form->text('author_name','作者名称')->rules('required');
            $form->multipleImage('pic','图片');
            $form->text('author_id','关联用户id');
            $form->text('title','标题')->rules('required');
            $form->select('classify_id','栏目')->options(Article_classifyModel::all()->pluck('type_name','id'))->rules('required');
            $form->textarea('description','描述')->rules('required');
            $form->textarea('share_description','分享描述')->rules('required');
            $form->image('share_pic','分享图片')->move('/test')->rules('required');
            $form->ckeditor('content', '内容')->rules('required');
            $form->switch('is_recommend','推荐')->states(['是'=>1,'否'=>0]);
            $form->switch('is_top','精选')->states(['是'=>1,'否'=>0]);
            $form->select('province_id','省')->options(AreaModel::where(['area_parent_id'=>0])->select()->pluck('area_name','area_id'))->load('city_id','get_area');
            $form->select('city_id','市')->options(function($id){
                if($id){
                    $tmp = AreaModel::find($id);
                    $re = AreaModel::where(['area_parent_id'=>$tmp->area_parent_id])->select()->pluck('area_name','area_id');
                    $arr = array();
                    foreach($re as $k=>$v){
                        $arr[] = array(
                            'id'=>$k,
                            'text'=>$v
                        );
                    }
                    return $re;
                }
            });
        });
    }

}