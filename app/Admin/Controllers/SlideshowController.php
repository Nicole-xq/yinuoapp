<?php
/**
 * Created by Test, 2018/06/27 15:55.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Controllers;

use App\Admin\Models\SlideshowModel;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Support\MessageBag;

class SlideshowController extends Controller
{
    use ModelForm;

    function index(){
//        return admin_toastr('laravel-admin 提示','success');exit;
        return Admin::content(function (Content $content) {
//            admin_toastr('laravel-admin 提示','success');
            $content->header('轮播图管理');//这里是页面标题
            $content->description('轮播图列表');//这里是详情描述
//
            $content->body($this->grid());//指向grid方法显示表格
        });
    }


    /**
     * Edit interface.
     *
     * @param $id
     *
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('轮播图管理');
            $content->description('编辑');
            $content->body($this->form()->edit($id));
        });
    }


    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('轮播图管理');
            $content->description('创建轮播图');
            $content->body($this->form());
        });
    }

//    public function store()
//    {
//        print_R($_POST);
//    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(SlideshowModel::class,function (Grid $grid) {
            $grid->id('Id');
            $grid->name('轮播图名称');
            $grid->actions(function (Grid\Displayers\Actions $actions) {
            $actions->disableDelete();
//                }
            });
            $grid->disableRowSelector();
            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {
        return Admin::form(SlideshowModel::class,function (Form $form) {
//            $form->display('id', 'ID');
            $form->text('name','轮播图名称')->rules('required');
            $form->hasMany('slideshow_info','详细配置',function(Form\NestedForm $form){
//                echo '<pre>';
//                var_dump($form->model());exit;
//                return $form->slideshow_info;
                $form->text('title','标题');
                $form->image('img','图片')->dir('slideshow');
//                $form->text('img','test_img');
                $form->text('url','链接地址');
            });

        });
    }
}