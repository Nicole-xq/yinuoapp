<?php
/**
 * Created by Test, 2018/06/12 09:50.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Controllers;

use App\Admin\Actions\CommentCheckRow;
use App\Admin\Models\Article_infoModel;
use App\Admin\Models\ArticleModel;
use App\Admin\Models\CommentModel;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Widgets\Collapse;
use Encore\Admin\Widgets\Tab;
use Illuminate\Support\Facades\DB;
use think\Exception;


class CommentController extends Controller
{
    use ModelForm;
    function index()
    {
        return Admin::content(function (Content $content) {
//            admin_toastr('laravel-admin 提示','success');
            $content->header('评论管理');//这里是页面标题
            $content->description('评论列表');//这里是详情描述

            $content->body($this->grid());//指向grid方法显示表格
        });
    }

     /**
     * Edit interface.
     *
     * @param $id
     *
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('评论管理');
            $content->description('编辑');
//            $tmp = $this->form();
//            $tmp->button('dddd')->on('click','alert(33333333);');
//            $content->body($tmp->edit($id));
            $content->body($this->form()->edit($id));
        });
    }


    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('评论管理');
            $content->description('添加');
            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(CommentModel::class,function (Grid $grid) {
//            $grid->getKeyName('user_id');
            $grid->id('ID')->sortable();
            $grid->is_examine('审核状态')->display(function($status){
                if($status == 1){
                    return '<i class="fa fa-check"></i>';
                }elseif($status == 2){
                    return '<i class="fa fa-remove"></i>';
                }else{
                    return $status;
                }

            });
            $grid->commentator_nickname('评论人');
            $grid->msg('评论内容');
            $grid->accepter_nickname('被评论人');
            $grid->article_id('关联')->display(function($id){
                $info = ArticleModel::find($id);
                if($info){
                    return $info->title;
                }
            });

            $grid->actions(function (Grid\Displayers\Actions $actions) {
                if($actions->row->is_examine == 0){
                    $actions->append(new CommentCheckRow($actions->getkey()));
                }
                $actions->disableEdit();
                $actions->disableDelete();
            });
            $grid->disableCreation();
            $grid->disableRowSelector();
            $grid->filter(function($filter){
                $filter->is('is_examine','审核状态')->select(['0'=>'未审核','1'=>'通过','2'=>'不通过']);
            });

            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {
        return Admin::form(CommentModel::class,function (Form $form) {


        });
    }

    public function check_info(){
        $status = $_GET['type'];
        $id = $_GET['id'];
        try{
            DB::beginTransaction();
            DB::connection('ynys_user_1')->beginTransaction();
            $re = CommentModel::where(['is_examine'=>0,'id'=>$id])->update(['is_examine'=>$status,'admin_id'=>Admin::user()->id]);
            if($re != 1){
                throw new Exception('fail');
            }
            $info = CommentModel::find($id);
            $re1 = ArticleModel::where('id',$info['article_id'])->increment('comment_num');
            if($re1 !=1){
                throw new Exception('fail1');
            }
            $re2 = Article_infoModel::where('article_id',$info['article_id'])->increment('comment_num');
            if($re2 !=1){
                throw new Exception('fail2');
            }
            DB::commit();
            DB::connection('ynys_user_1')->commit();
            echo 1;exit;
        }catch(Exception $e){
            DB::rollback();
            DB::connection('ynys_user_1')->rollback();
            echo $e->getMessage();
            echo 2;exit;
        }
        if($re == 1){
            echo 1;
        }else{
            echo 2;
        }
        exit;
        echo 'type'.$_GET['type'].'////id:'.$_GET['id'];exit;
    }
}