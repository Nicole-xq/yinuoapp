<?php
/**
 * Created by Test, 2018/06/26 09:55.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Controllers;

use App\Admin\Models\SettingModel;
use App\Admin\Models\UserModel;
use Encore\Admin\Form;
//use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\Box;
use Encore\Admin\Widgets\Carousel;
use Encore\Admin\Widgets\Tab;
use Encore\Admin\Widgets\Table;
use function foo\func;
use Illuminate\Support\MessageBag;

class Index_bannerController extends Controller
{
    use ModelForm;

    function index(){
//        return admin_toastr('laravel-admin 提示','success');exit;
        return Admin::content(function (Content $content) {
//            admin_toastr('laravel-admin 提示','success');
            $content->header('App管理');//这里是页面标题
            $content->description('首页轮播图');//这里是详情描述
            $content->row(function(Row $row){
                $row->column(12,function (Column $column){
                    $items = [
    [
        'image' => 'https://testyinuo2.oss-cn-beijing.aliyuncs.com/image/12.jpg',
        'caption' => '一剪梅',
        'url'=>'urlurlurlurlurlurlurlurlurlurl'
    ],
    [
        'image' => 'https://testyinuo2.oss-cn-beijing.aliyuncs.com/ueditor/y2j1e1530068542.jpg',
        'caption' => '爱啥啥',
        'url'=>'urlurlurlurlurlurlurlurlurlurl'
    ],
    [
        'image' => 'https://testyinuo2.oss-cn-beijing.aliyuncs.com/image/8.jpg',
        'caption' => '这是啥',
        'url'=>'urlurlurlurlurlurlurlurlurlurl'
    ],
];
                    $carousel = new Carousel($items);
                    $carousel->title('轮播图');
//                    $items = array(1,2,3,4,5,6,7);
//                    $table = new Table('我是header',$items);
                    $headers = ['Id', 'Email', 'Name', 'Company'];
                    new Form\Field\Image();
$rows = [
    [1, 'labore21@yahoo.com', 'Ms. Clotilde Gibson', 'Goodwin-Watsica'],
    [2, 'omnis.in@hotmail.com', 'Allie Kuhic', 'Murphy, Koepp and Morar'],
    [3, 'quia65@hotmail.com', 'Prof. Drew Heller', 'Kihn LLC'],
    [4, 'xet@yahoo.com', 'William Koss', 'Becker-Raynor'],
    [5, 'ipsa.aut@gmail.com', 'Ms. Antonietta Kozey Jr.'],
];

$table = new Table($headers, $rows);
                    $tab = new Tab();
                    for($i = 1;$i < 5;$i++){
                        $tab->add('test'.$i,$table);
                    }
                    $tab->title('tab测试');
                    $column->append($tab);
                });
            });
//
//            $content->body($this->form());//指向grid方法显示表格
        });
    }


    /**
     * Edit interface.
     *
     * @param $id
     *
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('用户管理');
            $content->description('编辑');
            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {
        return Admin::form(SettingModel::class,function (Form $form) {
//            $form->model()->where(['id'=>1]);
//            $form->tools(function(Form\Tools $tools){
//                $tools->disableBackButton();
//                $tools->disableListButton();
//            });
//            $form->text('name','名字');
            $form->hasMany('banner_arr','测试',function($form){
                $form->text('t1','名称');
                $form->image('img','图片');
            });
//            $form->hasMany('test',function(Form\NestedForm $form){
//                $form->display('id','ID');
//                $form->image('exhibition_pic');
//            });
            $form->slider('test')->options(['max' => 100, 'min' => 1, 'step' => 1, 'postfix' => 'years old']);
            $form->divide();
            $form->divide();
            $form->disableReset();

//            $form->display('id', 'ID');
//            $form->listbox('test','demo')->options([1 => 'foo', 2 => 'bar', 3 => 'Option name']);
//            $form->text('user_name','用户名')->rules('required');
//            $form->text('user_nickname','用户昵称')->rules('required');
//            $form->text('user_mobile','用户手机')->rules('required');
//            $form->image('user_avatar','头像')->dir('head_img')->rules('required|image');
//            $form->radio('user_sex','性别')->options(array(1=>'男',2=>'女'))->default(1);
//            $form->password('user_passwd','用户密码')->rules('required|confirmed');
//            $form->password('user_passwd_confirmation','确认密码')->rules('required')
//                ->default(function($form){
//                    return $form->model()->user_passwd;
//                });
//            $form->ignore(['user_passwd_confirmation']);
            $form->saving(function (Form $form) {
                try{
//                    $this->check_info($form);
                }catch(\Exception $e){
                    $error = new MessageBag([
                        'title'   => 'error',
                        'message' => $e->getMessage(),
                    ]);
                    return back()->with(compact('error'));
                }
//                $error[] = new MessageBag([
//                    'title'   => 'title111...',
//                    'message' => 'message....',
//                ]);
//                $error[] = new MessageBag([
//                    'title'   => 'title222...',
//                    'message' => 'message....',
//                ]);

                if ($form->user_passwd && $form->model()->user_passwd != $form->user_passwd) {
                    $form->user_passwd = bcrypt($form->user_passwd);
                }
            });
        });
    }
}