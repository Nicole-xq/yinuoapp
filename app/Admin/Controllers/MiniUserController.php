<?php
/**
 * Created by PhpStorm.
 * User: Mr.Liu
 * Date: 2018/8/29 0029
 * Time: 20:43
 */

namespace App\Admin\Controllers;

use App\Http\Requests\Request;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\Chart\Bar;
use Encore\Admin\Widgets\Chart\Pie;
use Encore\Admin\Widgets\Collapse;
use Encore\Admin\Widgets\Tab;
use Illuminate\Support\Facades\DB;

class MiniUserController extends Controller
{
    use ModelForm;

    public function index()
    {
        return Admin::content(function (Content $content) {
            $between = [];
            $between['begin'] = $_GET['begin'] ?? '';
            $between['end'] = $_GET['end'] ?? '';
            $content->header('会员分析');
            $content->description('访问量统计');
            $content->body($this->grid($between));
        });
    }

    public function grid($between)
    {
        /*
         * 用户分析统计数据
         */
        //$tmp = $this->userDataGet();
        $create = 'created_at';
        $today_begin = date('Y-m-d 00:00:00');
        $today_end = date('Y-m-d') . ' 23:59:59';
        $yesterday_begin = date('Y-m-d 00:00:00', strtotime("-1 day"));
        $yesterday_end = date('Y-m-d H:i:s', strtotime("-1 day"));
        $fields = [
            'id', 'created_at'
        ];
        //TODO 获取今日访问数据
//        $today_num = DB::table('mini_record')->select($fields)
//            ->where($create, '>', $today_begin)
//            ->get();
//        $today = $this->dateFormatData($today_num);
//
//        //获取昨日访问数据
//        $yesterday_num = DB::table('mini_record')->select($fields)
////            ->where($create, '>', $today_begin)
//            ->where($create, '<=', $yesterday_end)
//            ->get();
//        $yesterday = $this->dateFormatData($yesterday_num);

        //获取今日通过小程序注册的用户
        $begin = $yesterday_begin;
        if (isset($between['begin'])) {
            $begin = $between['begin'] ? $between['begin'] . ' 00:00:00' : $yesterday_begin;
        }
        $end = $today_end;
        if (isset($between['end'])) {
            $end = $between['end'] ? $between['end'] . ' 23:59:59' : $today_end;
        }
        $member_new_data = DB::table('mini_user')->select(['id', 'is_mini', 'created_at'])
            ->where(['is_mini' => 1])
            ->whereBetween($create, [$begin, $end])
            ->get();
        $member_old_data = DB::table('mini_user')->select(['id', 'is_mini', 'created_at'])
            ->where(['is_mini' => 2])
            ->whereBetween($create, [$begin, $end])
            ->get();
        //新老用户数据 第一个折线图
        $member_new_data = $this->dateFormatData($member_new_data);
        $member_old_data = $this->dateFormatData($member_old_data);
        $result = $this->getFinalResult($member_new_data, $member_old_data);
        $firstData = $result['main'];
        $member_new = $result['new'];
        $member_old = $result['old'];
        //满十八元用户数据 第二个折线图
        //提现用户
        $withdraw_amount = DB::table('mini_lottery')
            ->select(DB::raw('count(distinct user_id) as count'))
            ->where('win_amount', '>=', 18)
            ->where(['type' => 2])
            ->first();
        $user_amount = DB::table('mini_user')
            ->select(DB::raw('count(distinct user_id) as count'))
            ->where('amount', '>=', 18)
            ->first();
        $count = $withdraw_amount['count'] + $user_amount['count'];
        //用户分享次数数据 第三个折线图
        $user_new_data = DB::table('mini_share_record')->select(['id', 'is_mini', 'created_at'])
            ->where(['is_mini' => 1])
            ->whereBetween($create, [$begin, $end])
            ->get();
        $user_old_data = DB::table('mini_share_record')->select(['id', 'is_mini', 'created_at'])
            ->where(['is_mini' => 2])
            ->whereBetween($create, [$begin, $end])
            ->get();
        $user_new_num = $this->dateFormatData($user_new_data);
        $user_old_num = $this->dateFormatData($user_old_data);
        $result = $this->getFinalResult($user_new_num, $user_old_num);
        $thirdData = $result['main'];
        $thirdNew = $result['new'];
        $thirdOld = $result['old'];
        //邀请用户访问次数数据 第四个折线图
        //用户自主访问小程序
        $user_new_invite = DB::table('mini_record')->select(['id', 'is_invited', 'created_at'])
            ->where(['is_invited' => 0])
            ->whereBetween($create, [$begin, $end])
            ->get();
        //用户受邀请访问小程序
        $user_old_invite = DB::table('mini_record')->select(['id', 'is_invited', 'created_at'])
            ->where(['is_invited' => 1])
            ->whereBetween($create, [$begin, $end])
            ->get();
        $new_invite_data = $this->fourFormatData($user_new_invite);
        $old_invite_data = $this->fourFormatData($user_old_invite);
        $fourData = array_merge_recursive($new_invite_data, $old_invite_data);
        $member_liberty = [];
        $member_passive = [];
        foreach ($fourData as $key => &$value) {
            if (!isset($value['liberty'])) {
                $value['liberty'] = 0;
            }
            if (!isset($value['passive'])) {
                $value['passive'] = 0;
            }
            $member_liberty[$key] = $value['liberty'];
            $member_passive[$key] = $value['passive'];
        }
        ksort($fourData);
        $member_record = [
            'title' => '新用户/老用户',
            'categories' => array_keys($firstData),
            'series' => [
                //昨日数据//今日数据
                [
                    'name' => '新会员',
                    'data' => array_values($member_new),
                ],
                //昨日数据//今日数据
                [
                    'name' => '老会员',
                    'data' => array_values($member_old),
                ],
            ],
            'text' => '用户注册人数 (人)',
            'unit' => '人'
        ];
        $withdraw_record = [
            'title' => '满18元人数',
            'categories' => [
                '总计'
            ],
            'series' => [
                [
                    'name' => '总计',
                    'data' => [
                        $count
                    ],
                ]
            ],
            'text' => '用户金额满18人数 (人)',
            'unit' => '人'
        ];
        $share_record = [
            'title' => '分享次数',
            'categories' => array_keys($thirdData),
            'series' => [
                //昨日数据//今日数据
                [
                    'name' => '新会员',
                    'data' => array_values($thirdNew),
                ],
                //昨日数据//今日数据
                [
                    'name' => '老会员',
                    'data' => array_values($thirdOld),
                ]
            ],
            'text' => '分享次数 (次)',
            'unit' => '次'
        ];
        $invite_record = [
            'title' => '邀请进来人次',
            'categories' => array_keys($fourData),
            'series' => [
                //昨日数据//今日数据
                [
                    'name' => '受邀请访问',
                    'data' => array_values($member_liberty),
                ],
                //昨日数据//今日数据
//                [
//                    'name' => '受邀请访问',
//                    'data' => array_values($member_passive),
//                ],
            ],
            'text' => '邀请访问人次 (次)',
            'unit' => '次'
        ];
        $userData = [
            $member_record,
            $withdraw_record,
            $share_record,
            $invite_record
        ];
        return view(
            'admin.userAnalysis',
            [
                'userData' => $userData,
            ]
        );
    }

    public function userDataGet()
    {
        $create = 'created_at';
        $register = 'register_time';
        $today_begin = date('Y-m-d 00:00:00');
        $yesterday_begin = date('Y-m-d 00:00:00', strtotime("-1 day"));
        $yesterday_end = date('Y-m-d H:i:s', strtotime("-1 day"));
        $fields = [
            'id', 'created_at'
        ];
        //TODO 获取今日访问数据
        $today_num = DB::table('mini_record')->select($fields)
            ->where($create, '>', $today_begin)
            ->get();
        $today = $this->dateFormatData($today_num);

        //获取昨日访问数据
        $yesterday_num = DB::table('mini_record')->select($fields)
//            ->where($create, '>', $today_begin)
            ->where($create, '<=', $yesterday_end)
            ->get();
        $yesterday = $this->dateFormatData($yesterday_num);

        //访问量/跳出数
        $tmp1 = array(
            array(
                'data' => $today,
                'name' => '今日',
            ),
            array(
                'data' => $yesterday,
                'name' => '昨日',
            )
        );

        //会员数/新授权/新会员/老会员
        //获取今日会员数据

        $tmp2 = array(
            array(
                'data' => $today,
                'name' => '今日',
            ),
            array(
                'data' => $yesterday,
                'name' => '昨日',
            )
        );
        //新老会员分享次数  按照用户逇注册时间来, 之前之前注册的都属于老会员
        $fields = [
            'id', 'created_at'
        ];
        //获取今日分享数据
        $today_share_new = DB::table('mini_share_record')->select($fields)
            ->where($create, '>', $today_begin)
            ->where($register, '>', $today_begin)
            ->get();
        $today_share_new = $this->dateFormatData($today_share_new);
        $today_share_old = DB::table('mini_share_record')->select($fields)
            ->where($create, '>', $today_begin)
            ->where($register, '>', $yesterday_begin)
            ->get();
        $today_share_old = $this->dateFormatData($today_share_old);
        //获取昨日分享数据
        $yesterday_share_new = DB::table('mini_share_record')->select($fields)
            ->where($create, '>', $today_begin)
            ->where($create, '<=', $yesterday_end)
            ->where($register, '>', $today_begin)
            ->get();
        $yesterday_share_new = $this->dateFormatData($yesterday_share_new);
        $yesterday_shara_old = DB::table('mini_share_record')->select($fields)
            ->where($create, '>', $today_begin)
            ->where($create, '<=', $yesterday_end)
            ->where($register, '>', $yesterday_begin)
            ->get();
        $yesterday_shara_old = $this->dateFormatData($yesterday_shara_old);
        $tmp3 = array(
            array(
                'data' => $today_share_new,
                'name' => '今天新会员分享次数',
            ),
            array(
                'data' => $today_share_old,
                'name' => '今天老会员分享次数',
            ),
            array(
                'data' => $yesterday_share_new,
                'name' => '昨天新会员分享次数',
            ),
            array(
                'data' => $yesterday_shara_old,
                'name' => '昨天老会员分享次数',
            )
        );
        $categories = [
            '00:00-01:59', '02:00-03:59', '04:00-5:59', '06:00-7:59', '08:00-09:59', '10:00-11:59',
            '12:00-13:59', '14:00-15:59', '16:00-17:59', '16:00-17:59', '20:00-21:59', '22:00-23:59'
        ];
        return array(
            'data1' => array(
                'title' => '访问量/跳出数',
                'categories' => $categories,
                'series' => $tmp1
            ),
            'data2' => array(
                'title' => '会员数/新授权/新会员/老会员',
                'categories' => $categories,
                'series' => $tmp2
            ),
            'data3' => array(
                'title' => '新会员分享次数/老会员分享次数',
                'categories' => $categories,
                'series' => $tmp3
            )
        );
    }

    public function dateFormatData($data)
    {
        $return = [];
        ksort($data);
        foreach ($data as $item) {
            $item['created_at'] = date('Y-m-d', strtotime($item['created_at']));
            if (isset($item['is_mini'])) {
                if ($item['is_mini'] == 1) {
                    $sign = 'new';
                } else {
                    $sign = 'old';
                }
                if (!key_exists($item['created_at'], $return)) {
                    $return[$item['created_at']][$sign] = 1;
                } else {
                    $return[$item['created_at']][$sign] += 1;
                }
            } else {
                if (!key_exists($item['created_at'], $return)) {
                    $return[$item['created_at']] = 1;
                } else {
                    $return[$item['created_at']] += 1;
                }
            }
        }
        return $return;
    }

    public function fourFormatData($data)
    {
        $return = [];
        ksort($data);
        foreach ($data as $item) {
            $item['created_at'] = date('Y-m-d', strtotime($item['created_at']));
            if (isset($item['is_invited'])) {
                if ($item['is_invited'] == 0) {
                    $sign = 'liberty';
                } else {
                    $sign = 'passive';
                }
                if (!key_exists($item['created_at'], $return)) {
                    $return[$item['created_at']][$sign] = 1;
                } else {
                    $return[$item['created_at']][$sign] += 1;
                }
            }
        }
        return $return;
    }

    public function getFinalResult($member_new_data, $member_old_data)
    {
        $firstData = array_merge_recursive($member_new_data, $member_old_data);
        $member_new = [];
        $member_old = [];
        foreach ($firstData as $key => &$value) {
            if (!isset($value['new'])) {
                $value['new'] = 0;
            }
            if (!isset($value['old'])) {
                $value['old'] = 0;
            }
            $member_new[$key] = $value['new'];
            $member_old[$key] = $value['old'];
        }
        ksort($firstData);
        return [
            'main' => $firstData,
            'new' => $member_new,
            'old' => $member_old,
        ];
    }
}