<?php
/**
 * Created by Test, 2018/06/22 11:11.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Controllers;

use App\Admin\Actions\AppointmentCheckRow;
use App\Admin\Models\Appointment_listModel;
use App\Admin\Models\Appointment_sign_infoModel;
use App\Admin\Models\Appointment_signModel;
use App\Admin\Models\UserModel;
use App\Models\UserIndex;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Illuminate\Database\ConnectionResolver;
use Illuminate\Database\ConnectionResolverInterface;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class Appointment_signController extends Controller
{
    use ModelForm;
    function index($id)
    {
        return Admin::content(function (Content $content) use ($id){
            $content->header('约展管理');//这里是页面标题
            $content->description('约展列表');//这里是详情描述
//
            $content->body($this->grid($id));//指向grid方法显示表格
        });
    }

     /**
     * Edit interface.
     *
     * @param $id
     *
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('约展管理');
            $content->description('编辑');
            $content->body($this->grid($id));
        });
    }

    protected function grid($id){
        return Admin::grid(Appointment_signModel::class,function (Grid $grid) use ($id) {
            $grid->model()->where(['exhibition_id'=>$id,'status'=>0]);
            $grid->id('ID')->sortable();
            $grid->column('user_name','发起人')->display(function(){
                $info = Appointment_listModel::find($this->appointment_id);
                return $info->user_nickname;
            });
            $grid->column('sign_name','参与人')->display(function(){
                $db = UserModel::find($this->user_id);
                if($db){

                    $re = DB::connection($db->db)->table('appointment_info')->where(['appointment_sign_id'=>$this->id])->first();
                    if($re){
                        return $re['name'];
                    }
                }
            });
            $grid->column('msg','留言')->display(function(){
                $db = UserModel::find($this->user_id);
                if($db){

                    $re = DB::connection($db->db)->table('appointment_info')->where(['appointment_sign_id'=>$this->id])->first();
                    if($re){
                        $length = strlen($re['msg']);
                        $l_num = 30;
                        $tmp_num = ceil($length/$l_num);
                        for($i = 1;$i<=$tmp_num;$i++){
                            $tmp_arr[] = mb_substr($re['msg'],($i-1)*$l_num,$l_num);
                        }
                        $str = implode("<br>",$tmp_arr);
                        return $str;
                        return $re['msg'];
                    }
                }
            });
            $grid->column('status','状态')->label('info');
            $grid->actions(function(Grid\Displayers\Actions $actions){
                if($actions->row->status == 0){
                    $actions->append(new AppointmentCheckRow($actions->getKey()));
                }
                $actions->disableDelete();
                $actions->disableEdit();
            });
            $grid->disableCreation();
            $grid->disableRowSelector();
            $grid->disableExport();
        });
    }
    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {
        return Admin::form(Article_infoModel::class,function (Form $form) {
            $form->text('article_id','ID')->readOnly();
            $form->text('author_name','作者名称')->rules('required');
            $form->multipleImage('pic','图片');
            $form->text('author_id','关联用户id');
            $form->text('title','标题')->rules('required');
            $form->select('classify_id','栏目')->options(Article_classifyModel::all()->pluck('type_name','id'))->rules('required');
            $form->textarea('description','描述')->rules('required');
            $form->textarea('share_description','分享描述')->rules('required');
            $form->image('share_pic','分享图片')->move('/test')->rules('required');
            $form->ckeditor('content', '内容')->rules('required');
            $form->switch('is_recommend','推荐')->states(['是'=>1,'否'=>0]);
            $form->switch('is_top','精选')->states(['是'=>1,'否'=>0]);
            $form->select('province_id','省')->options(AreaModel::where(['area_parent_id'=>0])->select()->pluck('area_name','area_id'))->load('city_id','get_area');
            $form->select('city_id','市')->options(function($id){
                if($id){
                    $tmp = AreaModel::find($id);
                    $re = AreaModel::where(['area_parent_id'=>$tmp->area_parent_id])->select()->pluck('area_name','area_id');
                    $arr = array();
                    foreach($re as $k=>$v){
                        $arr[] = array(
                            'id'=>$k,
                            'text'=>$v
                        );
                    }
                    return $re;
                }
            });
        });
    }

    public function check_info(){
        $status = $_GET['type'];
        $id = $_GET['id'];
        try{
            DB::beginTransaction();
            $re = Appointment_signModel::where(['status'=>0,'id'=>$id])->update(['status'=>$status]);
            if ($status == 1) {
                $model = new UserIndex();
                $result = Appointment_listModel::where(['id'=>$id])->first();
                $model->where(['user_id' => $result['user_id']])->increment('comment_num', 1);
                $model->cacheDel($result['user_id']);
            }
            if($re != 1){
                throw new Exception('状态更新失败');
            }
            if($status == 1){
                $tmp_info = Appointment_signModel::find($id);
                $re = Appointment_listModel::where('id',$tmp_info['appointment_id'])->increment('count_num');
                if($re != 1){
                    throw new Exception('报名人数更新失败');
                }
            }
            DB::commit();
            echo 1;exit;
        }catch(Exception $e){
            DB::rollback();
            $error = new MessageBag([
                'title'   => 'error',
                'message' => $e->getMessage(),
            ]);
            return back()->with(compact('error'));
            echo 2;exit;
        }
//        $re = Appointment_signModel::where(['status'=>0,'id'=>$id])->update(['status'=>$status]);
//        if($re == 1){
//            echo 1;
//        }else{
//            echo 2;
//        }
//        exit;
    }
}