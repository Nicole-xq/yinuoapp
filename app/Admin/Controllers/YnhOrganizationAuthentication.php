<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/8/16 0016
 * Time: 11:43
 */

namespace App\Admin\Controllers;

use App\Admin\Actions\AuthenticationCheckRow;
use App\Admin\Models\AreaModel;
use App\Admin\Models\Artist_roleModel;
use App\Admin\Models\Artist_typeModel;
use App\Admin\Models\UserModel;
use App\Admin\Models\YnhAuthenticationModel;
use App\Admin\Models\YnhUserModel;
use App\Http\Requests\Request;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Form\NestedForm;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\Tab;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\MessageBag;

class YnhOrganizationAuthentication extends Controller
{
    use ModelForm;
    public $head_name = '认证管理';
    public $apply_id = '';

    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('机构认证');//这里是页面标题
            $content->description('认证列表');//这里是详情描述
            $content->body($this->grid());//指向grid方法显示表格
        });
    }

    public function grid()
    {
        return Admin::grid(YnhAuthenticationModel::class, function (Grid $grid) {
            if (empty($this->apply_id)) {
                $grid->model()->where(['authentication_type' => 2]);
            } else {
                $grid->model()->where('apply_id', $this->apply_id);
            }
            $grid->apply_id('ID')->sortable();
            $grid->real_name('机构名称');
            $grid->role_id('角色ID')->display(function ($id) {
                if (empty($id)) {
                    return '';
                }
                $sql = "select * from `artist_role` where `role_id` = $id";
                $info = DB::select($sql);
                return $info[0]['role_name'] ?? '';
            });
            $grid->trinity_num('三合一码');
            //$grid->document_pic('机构证书');
            $grid->authentication_id('认证机构ID')->display(function ($authentication_id) {
                return $authentication_id ?: '新增认证';
            })->label('info');
            $grid->address('机构地址');
            $grid->filter(function ($filter) {
                $filter->is('is_valid', '审核状态')->select(['0' => '未审核', '1' => '通过', '2' => '不通过']);
            });
            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableDelete();
            });
            $grid->disableCreation();
            $grid->disableExport();
            $grid->disableRowSelector();
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     *
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header($this->head_name);
            $content->description('编辑');
            $this->apply_id = $id;
            $content->body($this->form()->edit($id));
        });
    }

    public function form()
    {
        return Admin::form(YnhAuthenticationModel::class, function (Form $form) {
            $form->text('real_name', '公司名称')->readOnly();
            $form->text('nick_name', '公司简称')->readOnly();
            $form->select('role_id', '角色ID')
                ->options(Artist_roleModel::select()->pluck('role_name', 'role_id'))->readOnly();
            $form->image('logo', 'LOGO')->readOnly();
            $form->text('trinity_num', '三证合一码')->readOnly();
//            $form->image('identification_pic', '证书')->readOnly();
            $form->select('province_id','省')->options(AreaModel::where(['area_parent_id'=>0])->select()->pluck('area_name','area_id'))->load('city_id','get_area');
            $form->select('city_id','市')->options(function($id){
                if($id){
                    $tmp = AreaModel::find($id);
                    $re = AreaModel::where(['area_parent_id'=>$tmp->area_parent_id])->select()->pluck('area_name','area_id');
                    $arr = array();
                    foreach($re as $k=>$v){
                        $arr[] = array(
                            'id'=>$k,
                            'text'=>$v
                        );
                    }
                    return $re;
                }
            });
            $form->select('area_id','区')->options(function($id){
                if($id){
                    $tmp = AreaModel::find($id);
                    $re = AreaModel::where(['area_parent_id'=>$tmp->area_parent_id])->select()->pluck('area_name','area_id');
                    $arr = array();
                    foreach($re as $k=>$v){
                        $arr[] = array(
                            'id'=>$k,
                            'text'=>$v
                        );
                    }
                    return $re;
                }
            });
            $form->text('address', '地址')->readOnly();
            $form->text('phone_num', '联系电话')->readOnly();
            $form->text('description', '简介')->readOnly();
            $form->text('name', '法人姓名')->readOnly();
            $form->text('identification_num', '法人身份证号码')->readOnly();
            $form->multipleImage('identification_pic', '法人身份证图片')->readOnly();
            //$form->multipleImage('brand_pic', '商标')->readOnly();
            // 添加开关操作
            $states = [
                'on' => ['value' => 1, 'text' => '通过', 'color' => 'success'],
                'off' => ['value' => 2, 'text' => '拒绝', 'color' => 'danger'],
            ];
            $form->switch('is_valid', '认证审核')->states($states);
            $form->text('fail_cause', '失败原因');
            $form->saving(function (Form $form) {
                if ($form->is_valid != 2) {
                    $organization_info = YnhAuthenticationModel::find($form->organization_id);
                    $form->city_id = $organization_info['city_id'];
                    if (!empty($form->model()->authentication_id)) {
                        $id = $form->model()->authentication_id;
                        $where = [
                            'authentication_type' => 2,
                            'authentication_id' => $id,
                        ];
                        $is_valid = (new UserModel())->where($where)->get()->toArray();
                        if (!empty($is_valid)) {
                            $error = new MessageBag([
                                'title' => 'error',
                                'message' => '该机构已被绑定 !',
                            ]);
                            return back()->with(compact('error'));
                        }
                    }
                }
            });
        });
    }
}