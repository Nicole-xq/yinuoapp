<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/8/10 0010
 * Time: 17:49
 */

namespace App\Admin\Controllers;


namespace App\Admin\Controllers;

use App\Admin\Models\AreaModel;
use App\Admin\Models\Article_classifyModel;
use App\Admin\Models\Article_infoModel;
use App\Admin\Models\ArticleModel;
use App\Admin\Models\YnhArticle;
use App\Admin\Models\YnhUserModel;
use App\Http\Requests\Request;
use App\Models\ArticleClassify;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Content;

class YnhArticleReview extends Controller
{
    use ArticleModelForm;

    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('文章审核');//这里是页面标题
            $content->description('审核列表');//这里是详情描述
            $content->body($this->grid());//指向grid方法显示表格
        });
    }


    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(ArticleModel::class, function (Grid $grid) {
            $grid->model()->where(['is_delete' => 0]);
            $grid->id('ID')->sortable();
            $grid->title('标题');
            $grid->author_name('编辑');
            $grid->classify_id('栏目')->display(function ($id) {
                $info = Article_classifyModel::find($id);
                if ($info) {
                    return $info->type_name;
                }
            });
            $grid->created_at('发布时间');

            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableDelete();
            });
            $grid->filter(function ($filter) {
                $filter->is('classify_id', '栏目')->select(Article_classifyModel::all()->pluck('type_name', 'id'));
                $filter->like('title', '标题');
                $filter->is('is_recommend', '推荐')->select(['1' => '已推荐', '0' => '未推荐']);
                $filter->is('is_top', '精选')->select(['0' => '未精选', '1' => '已精选']);
            });
            $grid->status('用户状态')->display(function ($status) {
                switch ($status) {
                    case 0:
                        $return = "<span class='label bg-yellow'>未审核</span>";
                        break;
                    case 1:
                        $return = "<span class='label bg-green'>已通过</span>";
                        break;
                    case 2:
                        $return = "<span class='label bg-red'>不通过</span>";
                        break;
                    default:
                        $return = "<span class='label bg-light-blue'>未审核</span>";
                        break;
                }
                return $return;
            });
            $grid->filter(function($filter){
                $filter->is('status','审核状态')->select(['0'=>'未审核','1'=>'通过','2'=>'不通过']);
            });
            $grid->disableCreation();
            $grid->disableExport();
            $grid->disableRowSelector();
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     *
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('新闻管理');
            $content->description('编辑');
            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {
        return Admin::form(YnhArticle::class,function (Form $form) {
            $form->text('article_id','ID')->readOnly();
            $form->text('author_name','编辑')->readOnly();
            //TODO
            //$form->multipleImage('pic','图片');
            $form->text('author_id','关联用户id')->readOnly();
            $form->text('title','标题')->readOnly();
            $form->select('classify_id','栏目')
                ->options(
                    Article_classifyModel::where('parent_id','<>',0)
                        ->select()
                        ->pluck('type_name','id')
                )->readOnly();
            //$form->multipleImage('pic','图片');
            $form->textarea('description','描述')->readOnly();
            $form->textarea('share_description','分享描述');
            $form->image('share_pic','分享图片')->move('/share');
//            $form->ckeditor('content','内容')->readOnly();
            $form->textarea('content','内容')->readOnly();
            $form->switch('is_recommend','推荐')->states(['是'=>1,'否'=>0]);
            $form->switch('is_top','精选')->states(['是'=>1,'否'=>0]);
            $states = [
                'on' => ['value' => 1, 'text' => '通过', 'color' => 'success'],
                'off' => ['value' => 2, 'text' => '拒绝', 'color' => 'danger'],
            ];
            $form->switch('status', '显示审核')->states($states);
            $form->select('province_id','省')
                ->options(
                    AreaModel::where(['area_parent_id'=>0])
                        ->select()
                        ->pluck('area_name','area_id')
                )->load('city_id','get_area')->readOnly();
            $form->select('city_id','市')->options(function($id){
                if($id){
                    $tmp = AreaModel::find($id);
                    $re = AreaModel::where(['area_parent_id'=>$tmp->area_parent_id])
                        ->select()
                        ->pluck('area_name','area_id');
                    $arr = array();
                    foreach($re as $k=>$v){
                        $arr[] = array(
                            'id'=>$k,
                            'text'=>$v
                        );
                    }
                    return $re;
                }
            })->readOnly();
        });
    }

    public function get_area(){
        $area_id = $_GET['q'];
        $list = AreaModel::where(['area_parent_id'=>$area_id])->select()->pluck('area_name','area_id');
        $arr = array();
        foreach($list as $k=>$v){
            $arr[] = array(
                'id'=>$k,
                'text'=>$v
            );
        }
        return $arr;
    }

    public function check_info(Request $request)
    {
        var_dump($request->all());
        die;
    }
}