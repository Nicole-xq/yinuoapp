<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/8/10 0010
 * Time: 17:49
 */

namespace App\Admin\Controllers;


namespace App\Admin\Controllers;

use App\Admin\Models\AreaModel;
use App\Admin\Models\Article_classifyModel;
use App\Admin\Models\Artwork_categoryModel;
use App\Admin\Models\ArtworkModel;
use App\Admin\Models\User_infoModel;
use App\Admin\Models\UserModel;
use App\Http\Requests\Request;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Content;

class YnhArtworkReview extends Controller
{
    use ArticleModelForm;

    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('作品审核');//这里是页面标题
            $content->description('审核列表');//这里是详情描述
            $content->body($this->grid());//指向grid方法显示表格
        });
    }


    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(ArtworkModel::class, function (Grid $grid) {
            $grid->model()->where(['is_delete' => 0]);
            $grid->id('ID')->sortable();
            $grid->artwork_name('作品名');
            $grid->user_id('作者昵称')->display(function ($id) {
                $info = UserModel::find($id);
                if ($info) {
                    return $info->user_name;
                }
            });
            $grid->category_id('栏目')->display(function ($id) {
                $info = Artwork_categoryModel::find($id);
                if ($info) {
                    return $info->name;
                }
            });
            $grid->created_at('发布时间');

            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableDelete();
            });
            $grid->filter(function ($filter) {
                //$filter->is('category_id', '栏目')->select(Article_classifyModel::all()->pluck('type_name', 'id'));
                $filter->like('artwork_name', '作品名');
            });
            $grid->status('用户状态')->display(function ($status) {
                switch ($status) {
                    case 0:
                        $return = "<span class='label bg-yellow'>未审核</span>";
                        break;
                    case 1:
                        $return = "<span class='label bg-green'>已通过</span>";
                        break;
                    case 2:
                        $return = "<span class='label bg-red'>不通过</span>";
                        break;
                    default:
                        $return = "<span class='label bg-light-blue'>未审核</span>";
                        break;
                }
                return $return;
            });
            $grid->filter(function($filter){
                $filter->is('status','审核状态')->select(['0'=>'未审核','1'=>'通过','2'=>'不通过']);
            });
            $grid->disableCreation();
            $grid->disableExport();
            $grid->disableRowSelector();
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     *
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('新闻管理');
            $content->description('编辑');
            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {
        return Admin::form(ArtworkModel::class,function (Form $form) {
            $form->text('id','ID')->readOnly();
            $form->text('author_name','编辑')->readOnly();
            //TODO
            //$form->multipleImage('pic','图片');
            $form->text('artist_id','艺术家ID')->readOnly();
            $form->text('title','标题')->readOnly();
            $form->select('category_id','栏目')
                ->options(
                    Artwork_categoryModel::where('parent_id','<>',0)
                        ->select()
                        ->pluck('name','id')
                )->rules('required');
            $form->textarea('share_description','分享描述');
            $form->image('share_pic','分享图片')->move('/share');
            $form->textarea('content','作品介绍')->readOnly();
            $states = [
                'on' => ['value' => 1, 'text' => '通过', 'color' => 'success'],
                'off' => ['value' => 2, 'text' => '拒绝', 'color' => 'danger'],
            ];
            $form->switch('status', '显示审核')->states($states);
        });
    }

    public function get_area(){
        $area_id = $_GET['q'];
        $list = AreaModel::where(['area_parent_id'=>$area_id])->select()->pluck('area_name','area_id');
        $arr = array();
        foreach($list as $k=>$v){
            $arr[] = array(
                'id'=>$k,
                'text'=>$v
            );
        }
        return $arr;
    }

    public function check_info(Request $request)
    {
        var_dump($request->all());
        die;
    }
}