<?php
/**
 * Created by Test, 2018/07/17 14:17.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Controllers;

use App\Admin\Models\CrontabModel;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Support\MessageBag;
use Mockery\Exception;

class CrontabController extends Controller
{
    use ModelForm;
    public $states = [
        'on'  => ['value' => 1, 'text' => '打开', 'color' => 'success'],
        'off' => ['value' => 0, 'text' => '关闭', 'color' => 'danger'],
    ];

    function index(){
        return Admin::content(function (Content $content) {
            $content->header('计划任务管理');//这里是页面标题
            $content->description('计划列表');//这里是详情描述
            $content->body($this->grid());//指向grid方法显示表格
        });
    }


    /**
     * Edit interface.
     *
     * @param $id
     *
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('计划任务管理');
            $content->description('编辑');
            $content->body($this->form()->edit($id));
        });
    }


    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('计划任务管理');
            $content->description('创建计划');
            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(CrontabModel::class,function (Grid $grid) {
            $grid->id('ID','任务ID')->sortable();
            $grid->title('任务名称')->sortable();
            $grid->status('状态')->switch($this->states);

            $grid->actions(function (Grid\Displayers\Actions $actions) {

                $actions->disableDelete();
            });

            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {
        return Admin::form(CrontabModel::class,function (Form $form) {
//            $form->display('id', 'ID');
            $form->text('title','任务名称')->rules('required');
            $form->radio('data_source','数据源类型')->options(['1'=>'资讯','2'=>'展览'])->load('data_id','get_data');
            $form->text('data_id','关键ID')->rules('required');
            $form->slider('interval_time','间隔时间')->options(['min'=>1,'max'=>59,'step'=>1,'postfix'=>'分钟/次']);
            $form->datetime('start_time','开始时间');
            $form->datetime('end_time','结束时间');
//            $form->slider('test222','延时')->options(['min'=>1,'max'=>59,'step'=>0,'postfix'=>'秒']);
            $form->number('min_click_num','最小点击')->default(0);
            $form->number('max_click_num','最大点击')->default(0);
            $form->number('min_like_num','最小点赞量')->default(0);
            $form->number('max_like_num','最大点赞量')->default(0);
            $form->switch('status','状态')->states($this->states);
        });
    }

    public function get_data(){
        $type = $_GET['q'];
        $list = [];
        switch($type){
            case 1:
                break;
            case 2:
                break;
            default:
                break;
        }
        return $list;
    }
}