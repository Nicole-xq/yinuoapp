<?php
/**
 * Created by Test, 2018/06/06 10:03.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Controllers;

use App\Admin\Models\Article_classifyModel;
use App\Admin\Models\Article_infoModel;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Illuminate\Support\Facades\DB;

class Article_classifyController extends Controller
{
    use ModelForm;
    function index()
    {
        return Admin::content(function (Content $content) {
//            admin_toastr('laravel-admin 提示','success');
            $content->header('新闻管理');//这里是页面标题
            $content->description('新闻列表');//这里是详情描述
//
            $content->body($this->grid());//指向grid方法显示表格
        });
    }

     /**
     * Edit interface.
     *
     * @param $id
     *
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('栏目管理');
            $content->description('编辑');
//            $tmp = $this->form();
//            $tmp->button('dddd')->on('click','alert(33333333);');
//            $content->body($tmp->edit($id));
            $content->body($this->form()->edit($id));
        });
    }


    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('栏目管理');
            $content->description('创建');
            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Article_classifyModel::class,function (Grid $grid) {

            $all = Article_classifyModel::all(['id','type_name']);
            if(!empty($all)){
                foreach($all as $v){
                    $grid->tmp[$v['id']] = $v['type_name'];
                }
            }
            $grid->model()->where('parent_id','!=',0);
//            $tmp = $tmp1->get_info(1);
//            print_R($all);exit;
//            $grid->getKeyName('user_id');
            $grid->id('ID')->sortable();
            $grid->type_name('栏目名称');
            $grid->parent_id('所属栏目')->display(function($parent_id){
                if($parent_id == 0){
                    return '无';
                }
                $re = DB::table('article_classify')->where(['id'=>$parent_id])->get();
                return ($re[0]['type_name']);
            });
//            $grid->article_id('文章ID')->sortable();
//            $grid->author_name('作者');
//            $grid->add_time('发布时间');

//            $grid->user_sex('性别')->display(function($sex){
//                return $sex == 1?'男':'女';
//            });
//            $grid->user_mobile('手机号');
//            $grid->user_email('邮箱');
//            $grid->user_cityid('城市')->display(function($city){
//                return '城市'.$city;
//            });
//
            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableDelete();
            });
            $grid->disableRowSelector();
            $grid->disableCreation();
            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {
//        print_R($re1);exit;
        return Admin::form(Article_classifyModel::class,function (Form $form) {
            $re = Article_classifyModel::all()->pluck('type_name','id');
            $classify_arr = array_merge(array('无'),$re->toArray());

            $form->select('parent_id','上级栏目')->options($classify_arr)->rules('required');
            $form->text('type_name','栏目名称')->rules('required');

            $form->saving(function (Form $form) {
                if ($form->user_passwd && $form->model()->user_passwd != $form->user_passwd) {
                    $form->user_passwd = bcrypt($form->user_passwd);
                }
            });
        });

    }

}