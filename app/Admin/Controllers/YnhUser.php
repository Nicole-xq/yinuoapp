<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/8/6 0006
 * Time: 15:54
 */

namespace App\Admin\Controllers;


use App\Admin\Models\AreaModel;
use App\Admin\Models\YnhUserModel;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Content;


class YnhUser extends Controller
{
    use ModelForm;

    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('用户管理');//这里是页面标题
            $content->description('用户列表');//这里是详情描述
            $content->body($this->grid());//指向grid方法显示表格
        });
    }


    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(YnhUserModel::class, function (Grid $grid) {
            $grid->user_id('ID')->sortable();
            $grid->column('userIndex.user_nickname', '会员昵称');
            $grid->column('userIndex.user_sex', '性别')->display(function ($sex) {
                switch ($sex) {
                    case 1:
                        $tmp = '男';
                        break;
                    case 2:
                        $tmp = '女';
                        break;
                    default:
                        $tmp = '未知';
                        break;
                }
                return $tmp;
            });
            $grid->column('userIndex.user_name', '手机号');
            $grid->column('userIndex.user_cityid', '城市')->display(function ($city_id) {
                $info = AreaModel::find($city_id);
                if ($info) {
                    return $info->area_name;
                }
                return '未知地区' . $city_id;
            });
            $grid->status('用户状态')->display(function ($status) {
                switch ($status) {
                    case 0:
                        $return = "<span class='label bg-yellow'>未审核</span>";
                        break;
                    case 1:
                        $return = "<span class='label bg-green'>已通过</span>";
                        break;
                    case 2:
                        $return = "<span class='label bg-red'>不通过</span>";
                        break;
                    default:
                        $return = "<span class='label bg-light-blue'>未申请</span>";
                        break;
                }
                return $return;
            });

            $grid->is_disable('是否删除')->display(function ($is_disable) {
                switch ($is_disable) {
                    case 0:
                        $return = "<span class='label bg-green'>未禁用</span>";
                        break;
                    case 1:
                        $return = "<span class='label bg-red'>已禁用</span>";
                        break;
                    default:
                        $return = "<span class='label bg-light-blue'>异常</span>";
                        break;
                }
                return $return;
            });
            $grid->created_at('用户创建时间');
            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableDelete();
            });
            $grid->filter(function ($filter) {
                $filter->is('user_nickname', '昵称');
                $filter->is('user_mobile', '手机号');
            });
            $grid->disableCreation();
            $grid->disableRowSelector();
            $grid->disableExport();
        });
    }


    /**
     * Edit interface.
     *
     * @param $id
     *
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('用户管理');
            $content->description('编辑');
            $content->body($this->form()->edit($id));
        });
    }


    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {
        return Admin::form(YnhUserModel::class, function (Form $form) {
            $form->text('user_id', '用户ID')->readOnly();
            $form->text('user_key', '用户key')->readOnly();
            $form->select('status', '用户状态')
                ->options([
                    0 => '未审核',
                    1 => '通过',
                    2 => '不通过',
                    3 => '未申请',
                ])->readOnly();
            $states = [
                'on' => ['value' => 1, 'text' => '解禁', 'color' => 'success'],
                'off' => ['value' => 0, 'text' => '禁用', 'color' => 'danger'],
            ];
            $form->switch('is_disable', '用户禁用')->states($states);
        });
    }
}