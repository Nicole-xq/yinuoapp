<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/8/13 0013
 * Time: 10:57
 */

namespace App\Admin\Controllers;

use App\Admin\Models\AreaModel;
use App\Admin\Models\Article_classifyModel;
use App\Admin\Models\Article_infoModel;
use App\Admin\Models\ArticleModel;
use App\Admin\Models\YnhUserModel;
use App\Models\ArticleClassify;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Content;


class YnhArticleController extends Controller
{
    use ModelForm;

    public function index($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('约展管理');//这里是页面标题
            $content->description('约展列表');//这里是详情描述
            $content->body($this->grid($id));//指向grid方法显示表格
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid($id)
    {
        return Admin::grid(ArticleModel::class, function (Grid $grid) {
            $grid->model()->where(['status' => 1, 'is_delete' => 0]);
            $grid->id('ID')->sortable();
            $grid->title('标题');
            $grid->author_name('编辑');
            $grid->classify_id('栏目')->display(function ($id) {
                $info = Article_classifyModel::find($id);
                if ($info) {
                    return $info->type_name;
                }
            });
            $grid->created_at('发布时间');

            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableDelete();
            });
            $grid->filter(function ($filter) {
                $filter->is('classify_id', '栏目')->select(Article_classifyModel::all()->pluck('type_name', 'id'));
                $filter->like('title', '标题');
                $filter->is('is_recommend', '推荐')->select(['1' => '已推荐', '0' => '未推荐']);
                $filter->is('is_top', '精选')->select(['0' => '未精选', '1' => '已精选']);
            });

            $grid->disableExport();
        });
    }


    /**
     * Edit interface.
     *
     * @param $id
     *
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('约展管理');
            $content->description('编辑');
            $content->body($this->grid($id));
        });
    }


    public function check_info()
    {
        $status = $_GET['type'];
        $id = $_GET['id'];
        try {
            DB::beginTransaction();
            $re = Appointment_signModel::where(['status' => 0, 'id' => $id])->update(['status' => $status]);
            if ($re != 1) {
                throw new Exception('状态更新失败');
            }
            if ($status == 1) {
                $tmp_info = Appointment_signModel::find($id);
                $re = Appointment_listModel::where('id', $tmp_info['appointment_id'])->increment('count_num');
                if ($re != 1) {
                    throw new Exception('报名人数更新失败');
                }
            }
            DB::commit();
            echo 1;
            exit;
        } catch (Exception $e) {
            DB::rollback();
            $error = new MessageBag([
                'title' => 'error',
                'message' => $e->getMessage(),
            ]);
            return back()->with(compact('error'));
            echo 2;
            exit;
        }
    }
}