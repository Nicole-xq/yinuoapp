<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/8/16 0016
 * Time: 11:43
 */

namespace App\Admin\Controllers;

use App\Admin\Actions\AuthenticationCheckRow;
use App\Admin\Models\AreaModel;
use App\Admin\Models\Artist_personalModel;
use App\Admin\Models\Artist_roleModel;
use App\Admin\Models\Artist_typeModel;
use App\Admin\Models\UserModel;
use App\Admin\Models\YnhAuthenticationModel;
use App\Admin\Models\YnhUserModel;
use App\Http\Requests\Request;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Form\NestedForm;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\Tab;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\MessageBag;

class YnhPersonalAuthentication extends Controller
{
    use ModelForm;
    public $head_name = '认证管理';

    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('个人认证');//这里是页面标题
            $content->description('认证列表');//这里是详情描述
            $content->body($this->grid());//指向grid方法显示表格
        });
    }

    public function grid()
    {
        return Admin::grid(YnhAuthenticationModel::class, function (Grid $grid) {
            $grid->model()->where(['authentication_type' => 1]);
            $grid->apply_id('ID')->sortable();
            $grid->real_name('认证昵称');
            $grid->artist_type('绘画类型')->display(function ($ids) {
                if (empty($ids)) {
                    return '';
                }
                $sql = "select * from `artist_type` where id in (" . $ids . ")";
                $info = DB::select($sql);
                if ($info) {
                    $str = '';
                    foreach ($info as $value) {
                        $str .= $value['name'] . ',';
                    }
                    return $str;
                }
            });
            $grid->phone_num('手机号');
            $grid->filter(function ($filter) {
                $filter->is('user_sex', '性别')->select(['1' => '男', '2' => '女']);
            });
            $grid->role_id('角色ID')->display(function ($id) {
                if (empty($id)) {
                    return '';
                }
                $sql = "select * from `artist_role` where `role_id` = $id";
                $info = DB::select($sql);
                return $info[0]['role_name'] ?? '';
            });
            $grid->identification_num('身份证号码');
            //$grid->identification_pic('身份证图片');
            $grid->authentication_id('认证艺术家ID')->display(function ($authentication_id) {
                return $authentication_id ?: '新增认证';
            })->label('info');
            $grid->created_at('申请时间');
            $grid->filter(function ($filter) {
                $filter->is('is_valid', '审核状态')->select(['0' => '未审核', '1' => '通过', '2' => '不通过']);
            });
            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableDelete();
            });
            $grid->disableCreation();
            $grid->disableExport();
            $grid->disableRowSelector();
        });

    }

    /**
     * Edit interface.
     *
     * @param $id
     *
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header($this->head_name);
            $content->description('编辑');
            $content->body($this->form()->edit($id));
        });
    }

    public function form()
    {
        return Admin::form(YnhAuthenticationModel::class, function (Form $form) {
            $form->text('apply_id', 'ID')->readOnly();
            $form->text('real_name', '真实姓名')->readOnly();
            $form->text('nick_name', '认证昵称')->readOnly();
            $form->text('phone_num', '手机号')->readOnly();
            $form->select('user_sex', '性别')
                ->options([
                    1 => '男',
                    2 => '女',
                ])->readOnly();
            $form->multipleImage('identification_pic', '用户身份证图片')->readonly();
            $form->select('nation', '国籍')
                ->options([
                    1 => '中国',
                    2 => '海外',
                ])->readOnly();
            $form->image('real_avatar', '个人照片')->readonly();

            //个人照片
            //作品图片
            //if (!empty($form->model()->artwork_pic)) {
            //  $form->multipleImage('artwork_pic', '作品图片')->readonly();
            //}
            //证书奖项
            //if (!empty($form->model()->brand_pic)) {
              //  $form->multipleImage('brand_pic', '奖项证书')->readonly();
            //}
            $form->multipleSelect('artist_type', '绘画类型')
                ->options(Artist_typeModel::select()->pluck('name', 'id'))->readOnly();
            $form->select('role_id', '个人认证类型')
                ->options(Artist_roleModel::select()->pluck('role_name', 'role_id'))->readOnly();
            //$form->text('role_id', '角色ID')->rules('required');
            //$form->multipleImage('logo', '用户头像')->readOnly();
            $form->text('identification_num', '身份证号码')->readOnly();
            //$form->multipleImage('identification_pic', '用户身份证图片');
            //$form->multipleImage('identification_pic', '用户身份证图片');
//            $form->select('province_id', '省')
//                ->options(AreaModel::select()->pluck('area_name', 'area_id'))->readOnly();
//            $form->select('city_id', '市')
//                ->options(AreaModel::select()->pluck('area_name', 'area_id'))->readOnly();
            //$form->select('province_id','省')->options(AreaModel::where(['area_parent_id'=>0])->select()->pluck('area_name','area_id'))->load('city_id','get_area');
            //var_dump($form->model());exit();
                $form->select('province_id', '省')->options(function ($id) {
                    if ($id) {
                        $tmp = AreaModel::find($id);
                        $re = AreaModel::where(['area_parent_id' => $tmp->area_parent_id])->select()->pluck('area_name', 'area_id');
                        $arr = array();
                        foreach ($re as $k => $v) {
                            $arr[] = array(
                                'id' => $k,
                                'text' => $v
                            );
                        }
                        return $re;
                    }
                });
                $form->select('city_id', '市')->options(function ($id) {
                    if ($id) {
                        $tmp = AreaModel::find($id);
                        $re = AreaModel::where(['area_parent_id' => $tmp->area_parent_id])->select()->pluck('area_name', 'area_id');
                        $arr = array();
                        foreach ($re as $k => $v) {
                            $arr[] = array(
                                'id' => $k,
                                'text' => $v
                            );
                        }
                        return $re;
                    }
                });
                $form->select('area_id', '区')->options(function ($id) {
                    if ($id) {
                        $tmp = AreaModel::find($id);
                        $re = AreaModel::where(['area_parent_id' => $tmp->area_parent_id])->select()->pluck('area_name', 'area_id');
                        $arr = array();
                        foreach ($re as $k => $v) {
                            $arr[] = array(
                                'id' => $k,
                                'text' => $v
                            );
                        }
                        return $re;
                    }
                });
//            $form->select('area_id', '区')
//                ->options(AreaModel::select()->pluck('area_name', 'area_id'))->readOnly();
            $form->text('address', '地址')->readOnly();
            $form->text('description', '简介')->readOnly()->readOnly();
            // 添加开关操作
            $states = [
                'on' => ['value' => 1, 'text' => '通过', 'color' => 'success'],
                'off' => ['value' => 2, 'text' => '拒绝', 'color' => 'danger'],
            ];
            $form->switch('is_recommend','推荐')->states(['是'=>1,'否'=>0]);
            $form->switch('is_valid', '认证审核')->states($states);
            $form->text('fail_cause', '失败原因');
            $form->saving(function (Form $form) {
                if ($form->is_valid != 'off') {
                    $user_id = $form->model()->user_id;
                    $where = [
                        'user_id' => $user_id
                    ];
                    $is_user = (new Artist_personalModel())->where($where)->get()->toArray();
                    if (!empty($is_user)) {
                        $error = new MessageBag([
                            'title' => 'error',
                            'message' => '该用户已经认证成功 !',
                        ]);
                        return back()->with(compact('error'));
                    }
                }
            });
        });
    }
}