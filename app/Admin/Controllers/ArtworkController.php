<?php
/**
 * Created by Test, 2018/06/08 16:53.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Controllers;

use App\Admin\Models\Article_categoryModel;
use App\Admin\Models\Artist_organizationModel;
use App\Admin\Models\Artist_personalModel;
use App\Admin\Models\Artwork_categoryModel;
use App\Admin\Models\ArtworkModel;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;

class ArtworkController extends Controller
{
    use ModelForm;
    function index()
    {
        return Admin::content(function (Content $content) {
//            admin_toastr('laravel-admin 提示','success');
            $content->header('作品管理');//这里是页面标题
            $content->description('作品列表');//这里是详情描述
//
            $content->body($this->grid());//指向grid方法显示表格
        });
    }

     /**
     * Edit interface.
     *
     * @param $id
     *
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('作品管理');
            $content->description('编辑');
            $content->body($this->form()->edit($id));
        });
    }


    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('艺术家管理');
            $content->description('创建');
            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(ArtworkModel::class,function (Grid $grid) {
            $grid->id('ID')->sortable();
            $grid->artwork_name('作品名称');
            $grid->artist_id('作者')->display(function($id){
                $artist = Artist_personalModel::find($id);
                if($artist){
                    return $artist->name;
                }
                $artist = Artist_organizationModel::find($id);
                if($artist){
                    return $artist->name;
                }
                return '匿名用户';
            });
            $grid->category_id('作品分类')->display(function($id){
                $class_info = Artwork_categoryModel::find($id);
                if($class_info){
                    return $class_info->name;
                }
            });
            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableDelete();
            });
            $grid->disableRowSelector();
            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {
        return Admin::form(ArtworkModel::class,function (Form $form) {
            $form->select('category_id','分类')->options(Artwork_categoryModel::where('parent_id','<>',0)->select()->pluck('name','id'))->rules('required');
            $form->text('artist_id','艺术家ID')->rules('required');
            $form->text('artwork_name','作品名称')->rules('required');
            $form->image('artwork_img','作品图片')->rules('required');
            $form->datetime('artwork_create_time','创作时间')->format('YYYY-MM-DD HH:mm:ss');
            $form->text('l','长')->default(function($form){
                $info = json_decode($form->model()->type_info,true);
                return $info['l'];
            });
            $form->text('w','宽')->default(function($form){
                $info = json_decode($form->model()->type_info,true);
                return $info['w'];
            });
            $form->text('h','高')->default(function($form){
                $info = json_decode($form->model()->type_info,true);
                return $info['h'];
            });
            $form->ckeditor('content','作品简介');
            $form->switch('is_delete','删除')->states(['是'=>1,'否'=>0]);

                 if ($form->user_passwd && $form->model()->user_passwd != $form->user_passwd) {
                    $form->user_passwd = bcrypt($form->user_passwd);
                }
        });

    }
}