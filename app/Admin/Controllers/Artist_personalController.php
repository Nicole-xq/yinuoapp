<?php
/**
 * Created by Test, 2018/06/07 17:08.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Controllers;

use App\Admin\Models\AreaModel;
use App\Admin\Models\Artist_personalModel;
use App\Admin\Models\Artist_roleModel;
use App\Admin\Models\Artist_typeModel;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Illuminate\Support\Facades\Request;

class Artist_personalController extends Controller
{
    use ModelForm;
    function index()
    {
        return Admin::content(function (Content $content) {
//            admin_toastr('laravel-admin 提示','success');
            $content->header('艺术家管理');//这里是页面标题
            $content->description('艺术家列表');//这里是详情描述
//
            $content->body($this->grid());//指向grid方法显示表格
        });
    }

     /**
     * Edit interface.
     *
     * @param $id
     *
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('艺术家管理');
            $content->description('编辑');
            $content->body($this->form()->edit($id));
        });
    }


    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('艺术家管理');
            $content->description('创建');
            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Artist_personalModel::class,function (Grid $grid) {
            $grid->id('ID')->sortable();
            $grid->name('名称');
            $grid->role_id('角色')->display(function($role_id){

                $re = Artist_roleModel::find($role_id);
                if($re){
                    return $re->role_name;
                }
            });
            $grid->user_id('是否认证')->display(function($user_id){
                if($user_id){
                    return '是';
                }else{
                    return '否';
                }
            });
            $grid->filter(function($filter){
                $filter->like('name','名称');
            });
            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableDelete();
            });
            $grid->disableRowSelector();
            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {
//        print_R($re1);exit;
        return Admin::form(Artist_personalModel::class,function (Form $form) {
            $form->text('name','姓名')->rules('required');
            $form->text('user_id','关联用户id');
            $form->radio('role_id','角色')->options(Artist_roleModel::where(['role_type'=>1])->select()->pluck('role_name','role_id'));
            $form->checkbox('artist_type','艺术分类',true)->options(Artist_typeModel::all()->pluck('name','id'));
            $form->image('logo','头像')->rules('required');
            $form->text('shop_url','店铺地址');
            $form->ckeditor('description','介绍');
            $form->select('province_id','省')->options(AreaModel::where(['area_parent_id'=>0])->select()->pluck('area_name','area_id'))->load('city_id','get_area');
            $form->select('city_id','市')->options(function($id){
                if($id){
                    $tmp = AreaModel::find($id);
                    $re = AreaModel::where(['area_parent_id'=>$tmp->area_parent_id])->select()->pluck('area_name','area_id');
                    $arr = array();
                    foreach($re as $k=>$v){
                        $arr[] = array(
                            'id'=>$k,
                            'text'=>$v
                        );
                    }
                    return $re;
                }
            })->load('area_id','get_area');
            $form->select('area_id','区')->options(function($id){
                if($id){
                    $tmp = AreaModel::find($id);
                    $re = AreaModel::where(['area_parent_id'=>$tmp->area_parent_id])->select()->pluck('area_name','area_id');
                    $arr = array();
                    foreach($re as $k=>$v){
                        $arr[] = array(
                            'id'=>$k,
                            'text'=>$v
                        );
                    }
                    return $re;
                }
            });

        });

    }

    function get_area(){
        $area_id = $_GET['q'];//('q');
        $list = AreaModel::where(['area_parent_id'=>$area_id])->select()->pluck('area_name','area_id');
//        print_R($list);exit;
        $arr = array();
        foreach($list as $k=>$v){
            $arr[] = array(
                'id'=>$k,
                'text'=>$v
            );
        }
        return $arr;
    }


}