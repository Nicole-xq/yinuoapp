<?php
/**
 * Created by Test, 2018/06/11 11:28.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Controllers;

use App\Admin\Models\Artist_organizationModel;
use App\Admin\Models\Artist_personalModel;
use App\Admin\Models\ArtworkModel;
use App\Admin\Models\ExhibitionModel;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;

class ExhibitionController extends Controller
{
    public $head_name = '展览管理';
    use ModelForm;

    function index()
    {
        return Admin::content(function (Content $content) {
//            admin_toastr('laravel-admin 提示','success');
            $content->header($this->head_name);//这里是页面标题
            $content->description('展览列表');//这里是详情描述
//
            $content->body($this->grid());//指向grid方法显示表格
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     *
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header($this->head_name);
            $content->description('编辑');
//            $tmp = $this->form();
//            $tmp->button('dddd')->on('click','alert(33333333);');
//            $content->body($tmp->edit($id));
            $content->body($this->form()->edit($id));
        });
    }


    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('展览管理');
            $content->description('创建');
            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(ExhibitionModel::class, function (Grid $grid) {


            $grid->id('ID')->sortable();
            $grid->title('展览名称');
            $grid->start_time('开展时间');
            $grid->end_time('结束时间');
            $grid->sponsor('主办单位');

            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableDelete();
            });
            $grid->filter(function ($filter){
               $filter->like('title','标题');
            });
            $grid->disableRowSelector();
            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {
        return Admin::form(ExhibitionModel::class, function (Form $form) {
            $form->text('title','展览名称')->rules('required');
            $form->datetime('start_time','开始时间')->format('YYYY-MM-DD HH:mm:ss')->rules('required');
            $form->datetime('end_time','结束时间')->format('YYYY-MM-DD HH:mm:ss')->rules('required');
            $form->datetime('begin_time','开幕时间')->format('YYYY-MM-DD HH:mm:ss')->rules('required');
            $form->multipleSelect('exhibition_curator','策展人')->options(Artist_personalModel::where(['role_id'=>4])->select()->pluck('name','id'));
            $form->text('sponsor','主办方');
            $form->hidden('city_id');
            $form->text('joint_sponsor','联合主办');
            $form->text('undertake','承办');
            $form->select('organization_id','机构')->options(Artist_organizationModel::all()->pluck('name','id'))->rules('required');
            $form->multipleSelect('exhibition_artist','参展艺术家')->options(Artist_personalModel::all()->pluck('name','id'));
            $form->hasMany('exhibition_artwork_join','艺术家作品',function(Form\NestedForm $form){
                $form->select('artist_id','艺术家')->options(Artist_personalModel::all()->pluck('name','id'))->load('artwork_id','get_artwork');
                $form->select('artwork_id','参展作品')->options(function($id){
                    if($id){
                        $tmp = ArtworkModel::find($id);
                        $artist_id = $tmp->artist_id;
                        $list = ArtworkModel::where(['artist_id'=>$artist_id, 'is_delete'=>0])
                            ->select()->pluck('artwork_name','id');
                        return $list;
                    }
                });
            });
            $form->multipleImage('img_list','展览图集');
            $form->ckeditor('content','展览介绍');
            $charge = [
                'on'  => ['value' => 1, 'text' => '是', 'color' => 'success'],
                'off' => ['value' => 0, 'text' => '否', 'color' => 'danger'],
            ];
            $recommend = [
                'on'  => ['value' => 1, 'text' => '是', 'color' => 'success'],
                'off' => ['value' => 0, 'text' => '否', 'color' => 'danger'],
            ];
            $form->switch('is_charge','是否收费')->states($charge);
            $form->switch('is_recommend','推荐')->states($recommend);

            $form->saving(function (Form $form) {
                $organization_info = Artist_organizationModel::find($form->organization_id);
                $form->city_id = $organization_info['city_id'];
            });
        });

    }

    public function get_artwork(){
        $id = $_GET['q'];
        $list = ArtworkModel::where(['artist_id'=>$id])->select()->pluck('artwork_name','id');
        $arr = array();
        foreach($list as $k=>$v){
            $arr[] = array(
                'id'=>$k,
                'text'=>$v
            );
        }
        return $arr;
    }

}