<?php
/**
 * Created by Test, 2018/05/30 18:41.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Admin\Controllers;

use App\Admin\Controllers\Controller;
use App\Admin\Models\AreaModel;
use App\Admin\Models\UserModel;
use App\Model\BrandModel;//引用模型
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Support\MessageBag;
use Mockery\Exception;

class UserController extends Controller
{
    use ModelForm;

    function index(){
//        return admin_toastr('laravel-admin 提示','success');exit;
        return Admin::content(function (Content $content) {
//            admin_toastr('laravel-admin 提示','success');
            $content->header('用户管理');//这里是页面标题
            $content->description('用户列表');//这里是详情描述
//
            $content->body($this->grid());//指向grid方法显示表格
        });
    }


    /**
     * Edit interface.
     *
     * @param $id
     *
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('用户管理');
            $content->description('编辑');
            $content->body($this->form()->edit($id));
        });
    }


    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('用户管理');
            $content->description('创建用户');
            $content->body($this->form());
        });
    }

//    public function store()
//    {
//        print_R($_POST);
//    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(UserModel::class,function (Grid $grid) {
//            $grid->getKeyName('user_id');
            $grid->user_id('ID')->sortable();
            $grid->user_nickname('会员昵称')->sortable();
            $grid->user_sex('性别')->display(function($sex){
                switch($sex){
                    case 1:
                        $tmp = '男';
                        break;
                    case 2:
                        $tmp = '女';
                        break;
                    default:
                        $tmp = '未知';
                        break;
                }
                return $tmp;
            });
            $grid->user_mobile('手机号');
            $grid->user_email('邮箱');
            $grid->user_cityid('城市')->display(function($city_id){
                $info = AreaModel::find($city_id);
                if($info){
                    return $info->area_name;
                }
                return '未知地区'.$city_id;
            });
            $grid->created_at('注册时间');
            $grid->user_login_time('最后登录时间')->display(function($time){
                return date('Y-m-d H:i:s',$time);
            });
            $grid->user_login_ip('最后登录IP');
//
            $grid->actions(function (Grid\Displayers\Actions $actions) {
//            $actions->append('<a href="/edit">
//    <i class="fa fa-edit"></i>
//</a>');
            $actions->disableDelete();
//                }
            });
            $grid->filter(function($filter){
//                $filter->is('user_sex','性别')->select(['0'=>'未知','1'=>'男','2'=>'女']);
                $filter->is('user_nickname','昵称');
                $filter->is('user_mobile','手机号');
            });
//
//            $grid->tools(function (Grid\Tools $tools) {
//                $tools->batch(function (Grid\Tools\BatchActions $actions) {
//                    $actions->disableDelete();
//                });
//            });
//
//            $grid->disableActions();
            $grid->disableRowSelector();
            $grid->disableCreation();
            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {
        return Admin::form(UserModel::class,function (Form $form) {
//            $form->display('id', 'ID');
            $form->text('user_name','用户名')->rules('required')->readonly();
            $form->text('user_nickname','用户昵称')->rules('required')->readonly();
            $form->text('user_mobile','用户手机')->readonly();
            $form->image('user_avatar','头像')->dir('head_img')->rules('required|image')->readonly();
            $form->radio('user_sex','性别')->options(array(1=>'男',2=>'女'))->default(1)->readonly();
            $form->password('user_passwd','用户密码')->rules('required|confirmed')->readonly();
            $form->password('user_passwd_confirmation','确认密码')->rules('required')
                ->default(function($form){
                    return $form->model()->user_passwd;
                })->readonly();
            $form->hidden('db');
            $form->hidden('user_key');
            $form->ignore(['user_passwd_confirmation']);
            $form->saving(function (Form $form) {
                $form->db = 'ynys_user_1';
                if ($form->user_passwd && $form->model()->user_passwd != $form->user_passwd) {
                    $form->user_passwd = md5($form->user_passwd);
                }
                try{
                    $this->check_info($form);
                    if($form->user_key == ''){
                        $param = array();
                        $param['username'] = $form->user_name;
                        $param['password'] = $form->user_passwd;
                        $form->user_key = $this->get_user_key($param);
                    }
                }catch(Exception $e){
                    $error = new MessageBag([
                        'title'   => 'error',
                        'message' => $e->getMessage(),
                    ]);
                    return back()->with(compact('error'));
                }
            });
        });
    }

    public function check_info($form){
        if($form->user_mobile !=$form->model()->user_mobile){
            $re = UserModel::where(['user_mobile'=>$form->user_mobile])->get()->toArray();
            if($re){
                throw new Exception('手机号已存在');
            }
        }
        if($form->user_name != $form->model()->user_name){
            $re = UserModel::where(['user_name'=>$form->user_name])->get()->toArray();
            if($re){
                throw new Exception('用户名已存在!');
            }
        }
    }

    public function get_user_key($param)
    {
        $user_center = $this->loginUCenter($param['username']);
        if (isset($user_center->status_code) && $user_center->status_code == 200) {
            return $user_center->data->key;
        }

        $ucenter = $this->registerUCenter($param);
        if (isset($ucenter->status_code) && $ucenter->status_code == 200) {
            return $ucenter->data->key;
        } else {

            throw new Exception('UCenter注册失败');
        }
    }

    /**
     * 调用ucenter登录接口
     *
     * @param
     * @return mixed
     */
    public function loginUCenter($param)
    {
        $postUrl = 'login';
        $data = $this->_curl($postUrl, $param);
        return $data;
    }

    public function registerUCenter($param)
    {
        $postUrl = 'register';
        $data = $this->_curl($postUrl, $param);
        return $data;
    }

    private function _curl($postUrl, $curlPost)
    {
        $url = env('USER_CENTER','');
        if (empty($url)) {
            return null;
        }
        $postUrl = $url.$postUrl;
        $ch = curl_init();//初始化curl
        curl_setopt($ch, CURLOPT_URL, $postUrl);//抓取指定网页
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);//要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_POST, TRUE);//post提交方式
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        $data = curl_exec($ch);//运行curl
        curl_close($ch);
        if ($data) {
            return json_decode($data);
        }
        return null;
    }

}