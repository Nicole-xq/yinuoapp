<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/8/6 0006
 * Time: 15:54
 */

namespace App\Admin\Controllers;


use App\Admin\Actions\AuthenticationCheckRow;
use App\Admin\Models\AreaModel;
use App\Admin\Models\Artist_roleModel;
use App\Admin\Models\Artist_typeModel;
use App\Admin\Models\YnhAuthenticationModel;
use App\Admin\Models\YnhUserModel;
use App\Http\Requests\Request;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Form\NestedForm;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\Tab;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\MessageBag;

class YnhArtistAuthentication extends Controller
{
    use ModelForm;
    public $head_name = '认证管理';
    public $apply_id = '';


    public function personalDetail($id)
    {
        $this->apply_id = $id;
        return Admin::content(function (Content $content) use ($id) {
            $content->header('认证详情');//这里是页面标题
            $content->description('个人认证');//这里是详情描述

            $content->body($this->getPersonal());//指向grid方法显示表格
        });
    }

    public function getPersonal()
    {
        return Admin::grid(YnhAuthenticationModel::class, function (Grid $grid) {
            $grid->model()->where(['apply_id' => $this->apply_id, 'is_valid' => 0]);
            $grid->apply_id('ID')->sortable();
            $grid->user_key('user_key');
            $grid->real_name('用户姓名');
            $grid->artist_type('艺术分类');
            $grid->role_id('角色ID');
            $grid->logo('用户真实头像');
            $grid->identification_num('身份证号码');
            $grid->identification_pic('身份证图片');
            $grid->address('用户地址');
            $grid->authentication_id('认证艺术家ID')->display(function ($authentication_id) {
                return $authentication_id ?: '未绑定';
            })->label('info');
            $grid->created_at('申请时间');
            $grid->actions(function (Grid\Displayers\Actions $actions) {
                if ($actions->row->is_valid == 0) {
                    $actions->append(new AuthenticationCheckRow($actions->getKey()));
                }
                $actions->disableDelete();
            });
            $grid->disableExport();
        });
    }

    public function organizationDetail($id)
    {
        $this->apply_id = $id;
        return Admin::content(function (Content $content) use ($id) {
            $content->header('认证详情');//这里是页面标题
            $content->description('机构认证');//这里是详情描述
            $content->body($this->getOrganization());//指向grid方法显示表格
        });
    }

    public function getOrganization()
    {
        return Admin::grid(YnhAuthenticationModel::class, function (Grid $grid) {
            $grid->model()->where(['apply_id' => $this->apply_id, 'is_valid' => 0]);
            $grid->apply_id('ID')->sortable();
            $grid->real_name('机构名称');
            $grid->logo('LOGO');
            $grid->role_id('角色ID');
            $grid->trinity_num('三合一码');
            $grid->document_pic('机构证书');
            $grid->address('机构地址');
            $grid->authentication_id('认证机构ID')->display(function ($authentication_id) {
                return $authentication_id ?: '未绑定';
            })->label('info');
            $grid->address('机构地址');
            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableDelete();
                //$actions->disableEdit();
            });
            $grid->disableExport();
        });
    }


    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('认证管理');
            $content->description('认证列表');

            $content->row(function (Row $row) {

                $row->column(12, function (Column $column) {

                    $tab = new Tab();
                    $tab->add('个人认证', $this->grid('personal'));
                    $tab->add('机构认证', $this->grid());
                    $tab->title('认证列表(所有)');
                    $column->append($tab);
                });
            });
        });
    }


    /**
     * Edit interface.
     *
     * @param $id
     *
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header($this->head_name);
            $content->description('编辑');
            $this->apply_id = $id;
            $content->body($this->form()->edit($id));
        });
    }

    public function personalForm()
    {
        return Admin::form(YnhAuthenticationModel::class, function (Form $form) {
            $form->text('apply_id', 'ID')->readOnly();
            $form->text('real_name', '真实姓名')->readOnly();
            $form->multipleSelect('artist_type', '创作分类')
                ->options(Artist_typeModel::select()->pluck('name', 'id'))->readOnly();
            $form->select('role_id', '角色ID')
                ->options(Artist_roleModel::select()->pluck('role_name', 'role_id'))->readOnly();
            //$form->text('role_id', '角色ID')->rules('required');
            $form->text('logo', '用户身份证名字')->readOnly();
            $form->text('identification_num', '身份证号码')->readOnly();
            //$form->multipleImage('identification_pic', '用户身份证图片');
            $form->select('province_id', '省')
                ->options(AreaModel::select()->pluck('area_name', 'area_id'))->readOnly();
            $form->select('province_id', '市')
                ->options(AreaModel::select()->pluck('area_name', 'area_id'))->readOnly();
            $form->select('area_id', '区')
                ->options(AreaModel::select()->pluck('area_name', 'area_id'))->readOnly();
            $form->text('address', '地址')->readOnly();
            $form->text('description', '简介')->readOnly();
            // 添加开关操作
            $states = [
                'on' => ['value' => 1, 'text' => '通过', 'color' => 'success'],
                'off' => ['value' => 2, 'text' => '拒绝', 'color' => 'danger'],
            ];
            $form->switch('is_valid', '认证审核')->states($states);
            $form->text('fail_cause', '失败原因');
            $form->saving(function (Form $form) {
                var_dump($form);exit;
            });
        });

    }

    public function organizationForm()
    {
        return Admin::form(YnhAuthenticationModel::class, function (Form $form) {
            $form->text('real_name', '真实姓名')->rules('required')->readOnly();
            $form->select('role_id', '角色ID')
                ->options(Artist_roleModel::select()->pluck('role_name', 'role_id'))->readOnly();
            $form->text('logo', 'LOGO')->rules('required')->readOnly();
            $form->text('trinity_num', '三证合一码')->readOnly();
            $form->multipleImage('document_pic', '证书')->readOnly();
            //$form->multipleImage('identification_pic', '用户身份证图片');
            $form->select('province_id', '省')
                ->options(AreaModel::select()->pluck('area_name', 'area_id'))->rules('required')->readOnly();
            $form->select('province_id', '市')
                ->options(AreaModel::select()->pluck('area_name', 'area_id'))->rules('required')->readOnly();
            $form->select('area_id', '区')
                ->options(AreaModel::select()->pluck('area_name', 'area_id'))->rules('required')->readOnly();
            $form->text('address', '地址')->readOnly();
            $form->text('description', '简介')->readOnly();
            $form->text('fail_cause', '失败原因');
            $form->saving(function (Form $form) {
                $organization_info = YnhAuthenticationModel::find($form->organization_id);
                $form->city_id = $organization_info['city_id'];
            });
        });
    }


    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {
        $apply_info = (new YnhAuthenticationModel())->find($this->apply_id);
        if ($apply_info['authentication_type'] == 1) {
            return Admin::form(YnhAuthenticationModel::class, function (Form $form) {
                $form->text('apply_id', 'ID')->readOnly();
                $form->text('real_name', '真实姓名')->readOnly();
                $form->multipleSelect('artist_type', '创作分类')
                    ->options(Artist_typeModel::select()->pluck('name', 'id'))->readOnly();
                $form->select('role_id', '角色ID')
                    ->options(Artist_roleModel::select()->pluck('role_name', 'role_id'))->readOnly();
                //$form->text('role_id', '角色ID')->rules('required');
                //$form->multipleImage('logo', '用户头像')->readOnly();
                $form->text('identification_num', '身份证号码')->readOnly();
                //$form->multipleImage('identification_pic', '用户身份证图片');
                $form->select('province_id', '省')
                    ->options(AreaModel::select()->pluck('area_name', 'area_id'))->readOnly();
                $form->select('province_id', '市')
                    ->options(AreaModel::select()->pluck('area_name', 'area_id'))->readOnly();
                $form->select('area_id', '区')
                    ->options(AreaModel::select()->pluck('area_name', 'area_id'))->readOnly();
                $form->text('address', '地址')->readOnly();
                $form->text('description', '简介')->readOnly();
                // 添加开关操作
                $states = [
                    'on' => ['value' => 1, 'text' => '通过', 'color' => 'success'],
                    'off' => ['value' => 2, 'text' => '拒绝', 'color' => 'danger'],
                ];
                $form->switch('is_valid', '认证审核')->states($states);
                $form->text('fail_cause', '失败原因');
                $form->saving(function (Form $form) {
                    var_dump($form);exit;
                });
            });
        } else {
            return Admin::form(YnhAuthenticationModel::class, function (Form $form) {
                $form->text('real_name', '真实姓名')->readOnly();
                $form->select('role_id', '角色ID')
                    ->options(Artist_roleModel::select()->pluck('role_name', 'role_id'))->readOnly();
                $form->text('logo', 'LOGO')->readOnly();
                $form->text('trinity_num', '三证合一码')->readOnly();
                $form->multipleImage('document_pic', '证书')->readOnly();
                //$form->multipleImage('identification_pic', '用户身份证图片');
                $form->select('province_id', '省')
                    ->options(AreaModel::select()->pluck('area_name', 'area_id'))->readOnly();
                $form->select('province_id', '市')
                    ->options(AreaModel::select()->pluck('area_name', 'area_id'))->readOnly();
                $form->select('area_id', '区')
                    ->options(AreaModel::select()->pluck('area_name', 'area_id'))->readOnly();
                $form->text('address', '地址')->readOnly();
                $form->text('description', '简介')->readOnly();
                // 添加开关操作
                $states = [
                    'on' => ['value' => 1, 'text' => '通过', 'color' => 'success'],
                    'off' => ['value' => 2, 'text' => '拒绝', 'color' => 'danger'],
                ];
                $form->switch('is_valid', '认证审核')->states($states);
                $form->text('fail_cause', '失败原因');
                $form->saving(function (Form $form) {
                    $organization_info = YnhAuthenticationModel::find($form->organization_id);
                    $form->city_id = $organization_info['city_id'];
                });
            });
        }

    }

    public function check_info()
    {
        $status = $_GET['type'];
        $id = $_GET['id'];
        try {
            DB::beginTransaction();
            $re = YnhAuthenticationModel::where(['is_valid' => 0, 'apply_id' => $id])->update(['is_valid' => $status]);
            if ($re != 1) {
                throw new Exception('状态更新失败');
            }
            if ($status == 1) {
                $user_info = YnhAuthenticationModel::find($id);
                $user_id = $user_info['user_id'];
                $re = YnhUserModel::where(['user_id' => $user_id])->update(['status' => $status]);
                if ($re != 1) {
                    throw new Exception('用户状态信息保存失败');
                }
            }
            DB::commit();
            echo 1;
            exit;
        } catch (Exception $e) {
            DB::rollback();
            $error = new MessageBag([
                'title' => 'error',
                'message' => $e->getMessage(),
            ]);
            return back()->with(compact('error'));
            echo 2;
            exit;
        }
    }


    /**
     * Make a grid builder.
     * @param $type
     * @return Grid
     */
    protected function grid($type = '')
    {
        if ($type == 'personal') {
            return Admin::grid(YnhAuthenticationModel::class, function (Grid $grid) {
                if (empty($this->apply_id)) {
                    $grid->model()->where(['authentication_type' => 1, 'is_valid' => 0]);
                } else {
                    $grid->model()->where('apply_id', $this->apply_id);
                }
                $grid->apply_id('ID')->sortable();
                $grid->real_name('用户姓名');
                $grid->artist_type('艺术分类')->display(function ($ids) {
                    if (empty($ids)) {
                        return '';
                    }
                    $sql = "select * from `artist_type` where id in (" . $ids . ")";
                    $info = DB::select($sql);
                    if ($info) {
                        $str = '';
                        foreach ($info as $value) {
                            $str .= $value['name'] . ',';
                        }
                        return $str;
                    }
                });
                $grid->role_id('角色ID')->display(function ($id) {
                    if (empty($id)) {
                        return '';
                    }
                    $sql = "select * from `artist_role` where `role_id` = $id";
                    $info = DB::select($sql);
                    return $info[0]['role_name'] ?? '';
                });
                $grid->logo('用户真实头像');
                $grid->identification_num('身份证号码');
                //$grid->identification_pic('身份证图片');
                $grid->authentication_id('认证艺术家ID')->display(function ($authentication_id) {
                    return $authentication_id ?: '未绑定';
                })->label('info');
                $grid->created_at('申请时间');
                $grid->disableExport();
                $grid->actions(function (Grid\Displayers\Actions $actions) {
//                    $check_false = YnhAuthenticationModel::where(['apply_id' => $actions->row->apply_id])->count();
//                    if ($check_false > 0) {
//                        $actions->append(
//                            '<span><a href="/admin/artist_authentication_personal/' . $actions->getKey() . '">
//                                    审核</a></span>'
//                        );
//                    }
                    //$actions->disableEdit();
                    $actions->disableDelete();
                });
            });
        }
        return Admin::grid(YnhAuthenticationModel::class, function (Grid $grid) {
            if (empty($this->apply_id)) {
                $grid->model()->where(['authentication_type' => 2, 'is_valid' => 0]);
            } else {
                $grid->model()->where('apply_id', $this->apply_id);
            }
            $grid->apply_id('ID')->sortable();
            $grid->real_name('机构名称');
            $grid->logo('LOGO');
            $grid->role_id('角色ID')->display(function ($id) {
                if (empty($id)) {
                    return '';
                }
                $sql = "select * from `artist_role` where `role_id` = $id";
                $info = DB::select($sql);
                return $info[0]['role_name'] ?? '';
            });
            $grid->trinity_num('三合一码');
            $grid->document_pic('机构证书');
            $grid->authentication_id('认证机构ID')->display(function ($authentication_id) {
                return $authentication_id ?: '未绑定';
            })->label('info');
            $grid->address('机构地址');
            $grid->disableExport();
            $grid->actions(function (Grid\Displayers\Actions $actions) {
                //$actions->disableEdit();
                $actions->disableDelete();
            });
        });
    }
}