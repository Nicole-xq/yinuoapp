<?php

/**
 * Laravel-admin - admin builder based on Laravel.
 * @author z-song <https://github.com/z-song>
 *
 * Bootstraper for Admin.
 *
 * Here you can remove builtin form field:
 * Encore\Admin\Form::forget(['map', 'editor']);
 *
 * Or extend custom form field:
 * Encore\Admin\Form::extend('php', PHPEditor::class);
 *
 * Or require js and css assets:
 * Admin::css('/packages/prettydocs/css/styles.css');
 * Admin::js('/packages/prettydocs/js/main.js');
 *
 */

use App\Admin\Extensions\Form\uEditor;
use App\Admin\Extensions\WangEditor;
use App\Admin\Extensions\Form\CKEditor;
use Encore\Admin\Form;

Form::extend('ueditor', uEditor::class);
Form::extend('weditor',WangEditor::class);
Form::extend('ckeditor',CKEditor::class);

Encore\Admin\Form::forget(['map', 'editor']);

Admin::js('/packages/admin/AdminLTE/js/highcharts.js');
Admin::js('/packages/admin/AdminLTE/js/double-date.js');
\Encore\Admin\Grid\Column::extend('color', function ($value, $color) {

    return "<span style='color: $color'>$value</span>";
});
