<?php
namespace App\Logic;

class MemberMsg
{
    public $token;
    public $page = 10;
    public $curpage = 1;

    public function __construct($token)
    {
        $this->token = $token;
    }

    public function message_list($param)
    {
        if (isset($param['curpage']) && $param['curpage'] > 0) {
            $this->curpage = $param['curpage'];
        }
        $postUrl = '?act=member_chat&op=message_list&key=' . $this->token . '&page=' . $this->page . '&curpage=' . $this->curpage;
        return $this->_curl($postUrl);
    }

    public function setReadState()
    {
        $postUrl = '?act=member_chat&op=setReadState&type=2&key=' . $this->token;
        return $this->_curl($postUrl);
    }

    public function message_delete($message_id)
    {
        $postUrl = '?act=member_chat&op=message_delete&key=' . $this->token . '&message_id=' . $message_id;
        return $this->_curl($postUrl);
    }

    public function message_detail($message_id)
    {
        $postUrl = '?act=member_chat&op=message_detail&key=' . $this->token . '&message_id=' . $message_id;
        return $this->_curl($postUrl);
    }

    public function get_msg_count()
    {
        $postUrl = '?act=member_chat&op=get_msg_count&type=2&key=' . $this->token;
        return $this->_curl($postUrl);
    }

    private function _curl($postUrl, $curlPost = [])
    {
        $url = env('SHOPNC_MOBILE', '');
        if (empty($url)) {
            return null;
        }
        $postUrl = $url . $postUrl;
        $ch = curl_init();//初始化curl
        curl_setopt($ch, CURLOPT_URL, $postUrl);//抓取指定网页
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);//要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_POST, true);//post提交方式
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        $data = curl_exec($ch);//运行curl
        curl_close($ch);
        if ($data) {
            return json_decode($data);
        }
        return null;
    }
}