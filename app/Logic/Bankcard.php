<?php
namespace App\Logic;

class Bankcard
{
    public $token;
    public $page = 10;
    public $curpage = 1;

    public function __construct($token)
    {
        $this->token = $token;
    }

    public function bankcardList($param, $assoc = false)
    {
        if (isset($param['curpage']) && $param['curpage'] > 0) {
            $this->curpage = $param['curpage'];
        }
        $postUrl = '?act=member_card&op=index&key=' . $this->token . '&page=' . $this->page . '&curpage=' . $this->curpage;
        return $this->_curl($postUrl,[], $assoc);
    }

    public function create($param)
    {
        $postUrl = '?act=member_card&op=bankcard_step3';
        return $this->_curl($postUrl, $param);
    }

    public function delete($bankcard_id)
    {
        $postUrl = '?act=member_card&op=delete&key=' . $this->token . '&bankcard_id=' . $bankcard_id;
        return $this->_curl($postUrl);
    }

    public function bankcard_default($bankcard_id)
    {
        $postUrl = '?act=member_card&op=bankcard_default&key=' . $this->token . '&bankcard_id=' . $bankcard_id;
        return $this->_curl($postUrl);
    }

    public function get_member_info()
    {
        $postUrl = '?act=member_card&op=get_member_info&key=' . $this->token;
        return $this->_curl($postUrl);
    }

    // 发送验证码
    public function bankcard_step2($phone)
    {
        $postUrl = '?act=member_card&op=bankcard_step2&key=' . $this->token . '&phone=' . $phone;
        return $this->_curl($postUrl);
    }

    private function _curl($postUrl, $curlPost = [], $assoc = false)
    {
        $url = env('SHOPNC_MOBILE', '');
        if (empty($url)) {
            return null;
        }
        $postUrl = $url . $postUrl;
        $ch = curl_init();//初始化curl
        curl_setopt($ch, CURLOPT_URL, $postUrl);//抓取指定网页
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);//要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_POST, true);//post提交方式
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        $data = curl_exec($ch);//运行curl
        curl_close($ch);
        if ($data) {
            return json_decode($data, $assoc);
        }
        return null;
    }
}