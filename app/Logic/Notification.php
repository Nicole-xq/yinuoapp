<?php

namespace App\Logic;

use App\Libs\Umeng\Android\AndroidBroadcast;
use App\Libs\Umeng\Ios\IOSBroadcast;
use Exception;
use Illuminate\Support\Facades\Log;


class notification
{
    protected $android = '';
    protected $ios = '';

    public function __construct()
    {
        $this->android = config('notification.umeng.android');
        $this->ios = config('notification.umeng.ios');
        $this->timestamp = strval(time());
    }

    /**
     * 发送app广播
     * @param array $sendMessageData [
     *      'ticker'=>'通知栏提示文字'
     *      'title'=>'通知标题'
     *      'text'=>'通知文字描述'
     *      'production_mode'=>'正式/测试模式（true/false）（已使用系统配置）'
     *      'extra'=>[
     *           'goto_type'=>'跳转wap或使用原生（wap/native）',
     *           'url'=>'跳转地址',
     *           ...
     *      ]
     * ]
     **/
    public function sendAppBroadCast($sendMessageData)
    {
        $message_android = [
            'ticker' => $sendMessageData['subtitle'],
            'title' => $sendMessageData['title'],
            'text' => $sendMessageData['description'],
            'after_open' => 'go_app',
            'extra' => $sendMessageData['extra'],
        ];
        $AndroidState = $this->sendAndroidBroadcast($message_android);

        $message_ios = [
            'alert' => [
                'title' => $sendMessageData['title'],
                'subtitle' => $sendMessageData['subtitle'],
                'body' => $sendMessageData['description']
            ],
            'badge' => 0,
            'sound' => 'chime',
            'extra' => $sendMessageData['extra'],
        ];
        $IOSState = $this->sendIOSBroadcast($message_ios);
        return [$AndroidState, $IOSState];
    }

    // Android 广播
    public function sendAndroidBroadcast($message)
    {
        try {
            $brocast = new AndroidBroadcast();
            $brocast->setAppMasterSecret($this->android['app_master_secret']);
            $brocast->setPredefinedKeyValue("appkey", $this->android['appkey']);
            $brocast->setPredefinedKeyValue("timestamp", $this->timestamp);
            $brocast->setPredefinedKeyValue("ticker", $message['ticker']);
            $brocast->setPredefinedKeyValue("title", $message['title']);
            $brocast->setPredefinedKeyValue("text", $message['text']);
            $brocast->setPredefinedKeyValue("after_open", $message['after_open']);
            $brocast->setPredefinedKeyValue("production_mode", $this->android['production_mode']);
            foreach ($message['extra'] as $key => $value) {
                $brocast->setExtraField($key, $value);
            }
            $brocast->send();
        } catch (Exception $e) {
            Log::error('友盟推送AndroidBroadcast：' . $e->getMessage());
            return false;
        }
        return true;
    }

    // iOS 广播
    public function sendIOSBroadcast($message)
    {
        try {
            $brocast = new IOSBroadcast();
            $brocast->setAppMasterSecret($this->ios['app_master_secret']);
            $brocast->setPredefinedKeyValue("appkey", $this->ios['appkey']);
            $brocast->setPredefinedKeyValue("timestamp", $this->timestamp);
            $brocast->setPredefinedKeyValue("alert", $message['alert']);
            $brocast->setPredefinedKeyValue("badge", $message['badge']);
            $brocast->setPredefinedKeyValue("sound", $message['sound']);
            $brocast->setPredefinedKeyValue("production_mode", $this->ios['production_mode']);
            foreach ($message['extra'] as $key => $value) {
                $brocast->setCustomizedField($key, $value);
            }
            $brocast->send();
        } catch (Exception $e) {
            Log::error('友盟推送IOSBroadcast：' . $e->getMessage());
            return false;
        }
        return true;
    }

}