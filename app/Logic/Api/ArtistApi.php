<?php
namespace App\Logic\Api;

use App\Models\ArtistPersonal;

/**
 * Class UserApi
 * @package App\Logic\Api
 */
class ArtistApi extends BaseApi {
    /**
     * @param $userId
     * @param array $column
     * @return ArtistPersonal|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null
     */
    function getArtistPersonalInfoByUserId(
        $userId,
        $column = ['*']
    ){
        return ArtistPersonal::whereUserId(intval($userId))->first(is_array($column) ? $column : $column);
    }
}