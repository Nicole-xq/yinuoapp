<?php

namespace App\Logic\Api\Providers;

use App\Logic\Api\ArtistApi;
use App\Logic\Api\FeedbackApi;
use App\Logic\Api\MemberApi;
use Illuminate\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton("artistApi", function (){
                return new ArtistApi();
        });
        $this->app->singleton("feedbackApi", function (){
            return new FeedbackApi();
        });
        $this->app->singleton("memberApi", function (){
            return new MemberApi();
        });
    }


    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['artistApi', 'feedbackApi', 'memberApi'];
    }
}
