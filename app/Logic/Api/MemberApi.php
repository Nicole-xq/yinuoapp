<?php
/**
 * Created by PhpStorm.
 * User: Hank
 * Date: 2019/3/26 0026
 * Time: 下午 13:06
 */

namespace App\Logic\Api;


use App\Models\Shop\Member;

class MemberApi extends BaseApi
{
    /**
     * 获得shopNc用户信息
     * @param $phone
     * @param array $columns
     * @return Member|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null
     */
    public function getShopNcMemberInfoByPhone($phone, $columns = ['*']){
        return Member::whereMemberMobile($phone)->first($columns);
    }
}