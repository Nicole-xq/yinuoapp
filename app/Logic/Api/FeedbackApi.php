<?php
/**
 * Created by PhpStorm.
 * User: Hank
 * Date: 2019/3/26 0026
 * Time: 上午 10:50
 */

namespace App\Logic\Api;


use App\Models\UserFeedback;

class FeedbackApi extends BaseApi
{
    /**
     * 意见反馈
     * @param $userId integer 用户id
     * @param $context string 内容
     * @param $module string 模块
     * @param string $client ios | android | unknown
     * @return bool
     */
    public function feedback($userId, $context, $module = "app_system", $client = 'unknown')
    {
        $model = new UserFeedback();
        $model->user_id = $userId;
        $model->content = $context;
        $model->module = $module;
        $model->client = $client;
        return $model->save();
    }
}