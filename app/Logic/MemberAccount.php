<?php

namespace App\Logic;

class MemberAccount
{
    public $token;

    public function __construct($token)
    {
        $this->token = $token;
    }

    // 检测是否绑定手机号
    public function get_mobile_info()
    {
        $postUrl = '?act=member_account&op=get_mobile_info&flag_whole=1&key=' . $this->token;
        return $this->_curl($postUrl);
    }

    // 发送验证码
    public function modify_paypwd_step2()
    {
        $postUrl = '?act=member_account&op=modify_paypwd_step2&key=' . $this->token;
        return $this->_curl($postUrl);
    }

    // 修改支付密码
    public function new_modify_paypwd($param)
    {
        $postUrl = '?act=member_account&op=new_modify_paypwd&key=' . $this->token;
        return $this->_curl($postUrl, $param);
    }

    // 判断是否有支付密码
    public function check_init_paypwd()
    {
        $postUrl = '?act=member_account&op=check_init_paypwd&key=' . $this->token;
        return $this->_curl($postUrl);
    }

    // 判断是否有支付密码
    public function pd_cash_add($param)
    {
        $postUrl = '?act=member_index&op=pd_cash_add&key=' . $this->token;
        return $this->_curl($postUrl,$param);
    }

    private function _curl($postUrl, $curlPost = [])
    {
        $url = env('SHOPNC_MOBILE', '');
        if (empty($url)) {
            return null;
        }
        $postUrl = $url . $postUrl;
        $ch = curl_init();//初始化curl
        curl_setopt($ch, CURLOPT_URL, $postUrl);//抓取指定网页
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);//要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_POST, true);//post提交方式
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        $data = curl_exec($ch);//运行curl
        curl_close($ch);
        if ($data) {
            return json_decode($data);
        }
        return null;
    }
}