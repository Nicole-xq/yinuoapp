<?php

namespace App\Logic;

class Member
{
    public $token;
    public $page = 10;
    public $curpage = 1;

    public function __construct($token = '')
    {
        $this->token = $token;
    }

    public function member_index()
    {
        $postUrl = '?act=member_index&op=index&key=' . $this->token;
        return $this->_curl($postUrl);
    }

    public function receiverAddress()
    {
        $postUrl = '?act=member_address&op=address_list&key=' . $this->token;
        return $this->_curl($postUrl);
    }

    public function receiverAddressAdd($data = [])
    {
        $postUrl = '?act=member_address&op=address_add&key=' . $this->token;
        return $this->_curl($postUrl, $data);
    }

    public function receiverAddressEdit($data = [])
    {
        $postUrl = '?act=member_address&op=address_edit&key=' . $this->token;
        return $this->_curl($postUrl, $data);
    }

    public function receiverAddressDel($data = [])
    {
        $postUrl = '?act=member_address&op=address_del&key=' . $this->token;
        return $this->_curl($postUrl, $data);
    }

    public function receiverAddressCity($pid)
    {
        $postUrl = '?act=area&op=area_list&area_id=' . $pid;
        return $this->_curl($postUrl);
    }

    private function _curl($postUrl, $curlPost = [])
    {
        $url = env('SHOPNC_MOBILE', '');
        if (empty($url)) {
            return null;
        }
        $postUrl = $url . $postUrl;
        $ch = curl_init();//初始化curl
        curl_setopt($ch, CURLOPT_URL, $postUrl);//抓取指定网页
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);//要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_POST, true);//post提交方式
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        $data = curl_exec($ch);//运行curl
        curl_close($ch);
        if ($data) {
            return json_decode($data);
        }
        return null;
    }
}