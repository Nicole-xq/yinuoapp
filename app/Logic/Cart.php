<?php
/**
 * Created by PhpStorm.
 * User: Mr.Liu
 * Date: 2019/3/14 0014
 * Time: 14:53
 */

namespace App\Logic;



class Cart
{
    public $token;

    public function __construct($token)
    {
        $this->token = $token;
    }

    /*
     * 购物车列表
     */
    public function cartList()
    {
        $postUrl = '?act=member_cart&op=cart_list&key=' . $this->token;
        return $this->_curl($postUrl);
    }

    public function cartCount()
    {
        $postUrl = '?act=member_cart&op=cart_count&key=' . $this->token;
        return $this->_curl($postUrl);
    }

    public function cartAdd($postData)
    {
        $postUrl = '?act=member_cart&op=cart_add&key=' . $this->token;
        return $this->_curl($postUrl, $postData);
    }

    public function cartDel($postData)
    {
        $postUrl = '?act=member_cart&op=cart_del&key=' . $this->token;
        return $this->_curl($postUrl, $postData);
    }

    public function cartEdit($postData)
    {
        $postUrl = '?act=member_cart&op=cart_edit_quantity&key=' . $this->token;
        return $this->_curl($postUrl, $postData);
    }

    private function _curl($postUrl, $curlPost = [])
    {
        $url = env('SHOPNC_MOBILE', '');
        if (empty($url)) {
            return null;
        }
        $postUrl = $url . $postUrl;
        $ch = curl_init();//初始化curl
        curl_setopt($ch, CURLOPT_URL, $postUrl);//抓取指定网页
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);//要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_POST, true);//post提交方式
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        $data = curl_exec($ch);//运行curl
        curl_close($ch);
        if ($data) {
            return json_decode($data, true);
        }
        return $data;
    }
}