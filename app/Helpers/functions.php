<?php
/**
 * 数据处理统一调用函数
 *
 * @package    function
 */

/**
 * 根据时间获取年龄
 *
 * @return int
 * */
function birthday($birthday)
{
    $age = strtotime($birthday);
    if ($age === false) {
        return false;
    }
    list($y1, $m1, $d1) = explode("-", date("Y-m-d", $age));
    $now = strtotime("now");
    list($y2, $m2, $d2) = explode("-", date("Y-m-d", $now));
    $age = $y2 - $y1;
    if ((int)($m2 . $d2) < (int)($m1 . $d1))
        $age -= 1;
    return $age;
}

/*
 * 内部方法调用返回数据
 */
function getReturn($data = [], $status = true)
{
    $return = [
        'code' => 200,
        'data' => $data,
    ];
    if (!$status) {
        $return['code'] = 201;
        return $return;
    }
    return $return;
}

/*
@desc：生成随机字符串
@param $len 要生成的字符串长度
@return str 生成的字符串
*/
function randstr($len)
{
    $pattern = '1234567890aBCDEFGHIJKLOMNOPQRSTUVWXYZ';
    $strlen = mb_strlen($pattern) - 1;
    $str = "";
    for ($i = 0; $i < $len; $i++) {
        $str .= $pattern[mt_rand(0, $strlen)];
    }
    return $str;
}

/**
 * 取得IP
 *
 * @return string
 */
function getIp()
{
    if (@$_SERVER['HTTP_CLIENT_IP'] && $_SERVER['HTTP_CLIENT_IP'] != 'unknown') {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (@$_SERVER['HTTP_X_FORWARDED_FOR'] && $_SERVER['HTTP_X_FORWARDED_FOR'] != 'unknown') {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return preg_match('/^\d[\d.]+\d$/', $ip) ? $ip : '';
}

/**
 * 取得图片地址
 * $picture只能是一维数组or字符串
 *
 * @param array|string $picture
 * @return array|string
 */
function getPicturePath($picture, $hight = 0)
{
    if (empty($picture)) {
        return $picture;
    }
    $path = env('PICTURE_UPLOAD_URL', '');
    if (is_string($picture)) {
        if ($hight != 0) {
            $picture .= '?x-oss-process=image/resize,h_' . $hight;
        }
        if (substr($picture, 0, 4) == 'http') {
            return $picture;
        }
        if (substr($picture, 0, 1) == '/') {
            return $path . $picture;
        }
        return $path . '/' . $picture;
    }

    if (is_array($picture)) {
        $picture_path = [];
        foreach ($picture as $k => $v) {
            if (is_string($v)) {
                if ($hight != 0) {
                    $v .= '?x-oss-process=image/resize,h_' . $hight;
                }
                if (substr($v, 0, 4) == 'http') {
                    $picture_path[] = $v;
                } else {
                    if (substr($v, 0, 1) == '/') {
                        $picture_path[] = $path . $v;
                    } else {
                        $picture_path[] = $path . '/' . $v;
                    }
                }
            }
        }
        if (!empty($picture_path)) {
            $picture = $picture_path;
        }
    }
    return $picture;
}



/*
 * 富文本转义纯文本
 */
function removeTheLabel($content) {
    if (empty($content)) {
        return '';
    }
    $return = htmlspecialchars_decode($content);
    $return = str_replace("&nbsp;","", $return);
    $return = strip_tags($return);
    return $return;
}



/**
 * 成员头像
 * @param string|array $user_id
 * @param string $user_avatar
 * @return string
 */
function getMemberAvatarByID($user_id, $member_avatar = '')
{
    return getUserAvatar($user_id);

    if (is_string($member_avatar)) {
        if ($member_avatar && substr($member_avatar, 0, 4) == 'http') {
            return $member_avatar;
        }
    }

    if (!empty($user_id) && is_numeric($user_id)) {
        $ali_path = env('PICTURE_UPLOAD_URL', '');
        $upload_path = env('PICTURE_UPLOAD_PATH', '');

        if ($ali_path && $upload_path) {
            //头像文件名
            $url = $upload_path . '/' . "avatar_{$user_id}.jpg";
            //判断头像文件是否存在
            $oss = new \App\Libs\phpQuery\OSS();
            $exit = $oss->doesObjectExist($url);

            if ($exit) {
                return $ali_path . '/' . $url;
            }
        }
    }
    return env('PICTURE_DEFAULT_USER', '');
}

function getUserAvatar($user_id)
{
    $user_avatar = env('PICTURE_DEFAULT_USER', '');
    if (empty($user_id)) {
        return $user_avatar;
    }

    $model_user = new \App\Models\UserIndex();
    if (is_array($user_id)) {
        $user_id = array_unique($user_id);
        $user_info = $model_user->getUserInfoByIds($user_id);
        if (empty($user_info)) {
            return $user_avatar;
        }
        $avatar_arr = [];
        foreach ($user_id as $value) {
            $user_info = $model_user->getUserInfoById($value);
            if (empty($user_info)) {
                $avatar_arr[$value] = getMemberAvatarByID($value);
                continue;
            }
            if (empty($user_info['user_avatar'])) {
                $avatar_arr[$user_info['user_id']] = getMemberAvatarByID($user_info['user_id']);
            } else {
                $avatar_arr[$user_info['user_id']] = getPicturePath($user_info['user_avatar']);
            }
        }
        if (!empty($avatar_arr)) {
            return $avatar_arr;
        }
        $user_key = $user_info->pluck('user_key', 'user_id')->toJson();
    } else {
        $user_info = $model_user->getUserInfoById($user_id);
        if (!empty($user_info['user_avatar'])) {
            return getPicturePath($user_info['user_avatar']);
        }
        if (empty($user_info)) {
            return $user_avatar;
        }
        $user_key = $user_info['user_key'];
    }

    $param = array('key' => $user_key);
    $result = $model_user->avatarUCenter($param);

    if (isset($result->status_code) && $result->status_code == 200 && !empty($result->data)) {
        $data = json_decode(json_encode($result->data), true);
        if (is_array($user_id)) {
            $user_key = json_decode($user_key, true);
            $avatar = [];
            foreach ($user_id as $k => $v) {
                if (isset($user_key[$v]) && isset($data[$user_key[$v]])) {
                    $avatar[$v] = $data[$user_key[$v]];
                } else {
                    $avatar[$v] = $user_avatar;
                }
            }
            return $avatar;
        } else {
            return $data[$user_key] ?: $user_avatar;
        }
    }
    if (is_array($user_id)) {
        $avatar = [];
        foreach ($user_id as $k => $v) {
            $avatar[$v] = $user_avatar;
        }
        return $avatar;
    }
    return $user_avatar;
}


/**
 * 手机号处理
 * @param int $tell
 * @return string
 * */
function tellHide($tell)
{
    if (preg_match('/^0?(13|15|17|18|14)[0-9]{9}$/i', $tell)) {
        $str1 = substr($tell, 0, 3);
        $str2 = substr($tell, -4, 4);
        return $str1 . '****' . $str2;
    } else {
        return $tell;
    }
}


function pp($lists)
{
    if (is_string($lists)) {
        echo $lists;
        die;
    } else {
        if (is_object($lists)) {
            $lists = json_encode($lists);
            $lists = json_decode($lists, true);
        }
        echo '<pre>';
        print_r((array)$lists);
        die;
    }
}


function lsReturn($data = '')
{
    $arr = [
        'status_code' => 200,
        'message' => 'success',
        'data' => []
    ];

    if (empty($data) && $data !== 0) {
        $arr['data'] = [];
        echo json_encode($arr);
        die;
    }

    if (is_object($data)) {
        $data = json_encode($data);
        $data = json_decode($data, true);
    }

    if (is_array($data)) {
        if (isset($data['code'])) {
            $arr['status_code'] = $data['code'];

            if (in_array($arr['status_code'], array_keys(config('errorcode.code'))))
                $arr['message'] = config('errorcode.code')[$arr['status_code']];
            $arr['data'] = $data['data'];
            echo json_encode($arr);
            die;
        } else {
            $arr['data'] = $data;
            echo json_encode($arr);
            die;
        }
    } else {
        if (is_numeric($data)) {
            if (in_array($data, array_keys(config('errorcode.code')))) {
                $arr['message'] = config('errorcode.code')[$data];
                $arr['status_code'] = $data;
                echo json_encode($arr);
                die;
            }
            $arr['message'] = $data;
            echo json_encode($arr);
            die;
        }
        $arr['status_code'] = 201;
        $arr['message'] = $data;
        echo json_encode($arr);
        die;
    }
}

function contentFilter($str)
{
    $filter = require('filter.php');
    if (empty($str)) {
        return true;
        //return '';
    }
    foreach ($filter as $val) {
        $val = trim($val);//这里注意,取出来的值有空格要去除
        preg_match('/' . $val . '/', $str, $matches);//匹配值
        if (!empty($matches[0])) {
            return false;
            //$str = str_replace($val, getFilter($val), $str);//替换掉敏感词
        }
    };
    return true;
    //return $str;//转换成***之后的;留言内容,接下来入库
}

/**
function getFilter($val)
{
    $len = ceil(strlen($val) / 3);
    $place = '';
    for ($i = 0; $i < $len; $i++) {
        $place .= '*';
    }
    return $place;
}
*/