<?php

namespace App\Models\Shop;

use App\Models\ArtistPersonal;
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    /**
     * 关联到DB链接
     * @var string
     */
    protected $table = 'shopnc_store';

    public $timestamps = false;

    public function __construct()
    {
        parent::__construct();
        $this->setConnection('yinuo');
    }


    /*
     * 认证的艺术家开设个人店铺
     */
    public function createStoreByUserId($artistId, $memberId)
    {
        $storeInfo = $this->where(['artist_id' => $artistId])->first();
        if ($storeInfo) {
            return $storeInfo;
        }
        $artistInfo = (new ArtistPersonal())->getArtistInfo($artistId);
        $storeInsert = [];
        $storeInsert['store_name'] = $artistInfo['name'];       //店铺名称
        $storeInsert['grade_id'] = 0;         //店铺等级
        $storeInsert['member_id'] = $memberId;        //转换后的商城用户ID
        $storeInsert['member_name'] = $artistInfo['nick_name'] ?: $artistInfo['name'];      //会员名称
        $storeInsert['seller_name'] = $artistInfo['nick_name'];      //店主卖家用户名
        $storeInsert['sc_id'] = 0;        //店铺分类
        $storeInsert['store_company_name'] = $artistInfo['name']; //店铺公司名称
        $storeInsert['area_info'] = $artistInfo['address'];        //地区内容，冗余数据
        $storeInsert['store_address'] = $artistInfo['address'] ?: '';        //详细地区
        $storeInsert['store_state'] = 1;      //店铺状态，0关闭，1开启，2审核中
        $storeInsert['store_time'] = time();       //店铺时间
        $storeInsert['store_label'] = $artistInfo['logo'];      //店铺logo
        $storeInsert['store_avatar'] = $artistInfo['logo'];     //店铺头像
        $storeInsert['artist_id'] = $artistId;        //艺术家店铺艺术家ID
        $store_id = $this->insertGetId($storeInsert);
        $storeInsert['store_id'] = $store_id;
        return $storeInsert;
        //TODO
        //$storeInsert['store_banner'] = $artistInfo[''];     //店铺横幅
        //$storeInsert['store_end_time'] = $artistInfo[''];       //店铺关闭时间
        //$storeInsert['record_number'] = $artistInfo[''];        //入驻标号
        //$storeInsert['store_zy'] = $artistInfo[''];     //主营商品

        //可以全部走默认值
//        $storeInsert['store_domain_times'] = $artistInfo[''];       //二级域名修改次数
//        $storeInsert['store_recommend'] = $artistInfo[''];      //推荐，0为否，1为是，默认为0
//        $storeInsert['store_theme'] = $artistInfo[''];      //店铺当前主题
//        $storeInsert['store_credit'] = $artistInfo[''];     //店铺信用
//        $storeInsert['store_desccredit'] = $artistInfo[''];     //描述相符度分数
//        $storeInsert['store_servicecredit'] = $artistInfo[''];      //服务态度分数
//        $storeInsert['store_deliverycredit'] = $artistInfo[''];     //发货速度分数
//        $storeInsert['store_collect'] = $artistInfo[''];        //店铺收藏数量
//        $storeInsert['store_slide'] = $artistInfo[''];      //店铺幻灯片
//        $storeInsert['store_slide_url'] = $artistInfo[''];      //店铺幻灯片链接
//        $storeInsert['store_stamp'] = $artistInfo[''];      //店铺印章
//        $storeInsert['store_printdesc'] = $artistInfo[''];      //打印订单页面下方说明文字
//        $storeInsert['store_sales'] = $artistInfo[''];      //店铺销量
//        $storeInsert['store_presales'] = $artistInfo[''];       //售前客服
//        $storeInsert['store_aftersales'] = $artistInfo[''];     //售后客服
//        $storeInsert['store_workingtime'] = $artistInfo[''];        //工作时间
//        $storeInsert['store_free_price'] = $artistInfo[''];     //超出该金额免运费，大于0才表示该值有效
//        $storeInsert['store_decoration_switch'] = $artistInfo[''];      //店铺装修开关(0-关闭 装修编号-开启)
//        $storeInsert['store_decoration_only'] = $artistInfo[''];        //开启店铺装修时，仅显示店铺装修(1-是 0-否
//        $storeInsert['store_decoration_image_count'] = $artistInfo[''];     //店铺装修相册图片数量
//        $storeInsert['is_own_shop'] = $artistInfo[''];      //是否自营店铺 1是 0否
//        $storeInsert['bind_all_gc'] = $artistInfo[''];      //自营店是否绑定全部分类 0否1是
//        $storeInsert['store_vrcode_prefix'] = $artistInfo[''];      //商家兑换码前缀
//        $storeInsert['mb_title_img'] = $artistInfo[''];     //手机店铺 页头背景图
//        $storeInsert['mb_sliders'] = $artistInfo[''];       //手机店铺 轮播图链接地址
//        $storeInsert['left_bar_type'] = $artistInfo[''];        //店铺商品页面左侧显示类型 1默认 2商城相关分类品牌商品推荐
//        $storeInsert['deliver_region'] = $artistInfo[''];       //店铺默认配送区域
//        $storeInsert['store_type'] = $artistInfo[''];       //店铺类型 0商家 1艺术家
//        $storeInsert['delayed_time'] = $artistInfo[''];     //店铺拍卖延时周期
//        //使用默认值
//        $storeInsert['area_id'] = $artistInfo[''];      //地区ID
//        $storeInsert['city_id'] = $artistInfo[''];      //城市ID
//        $storeInsert['province_id'] = $artistInfo[''];  //省份ID
//        $storeInsert['store_zip'] = $artistInfo[''];        //邮政编码
//        $storeInsert['store_close_info'] = $artistInfo['']; //店铺关闭原因
//        $storeInsert['store_sort'] = $artistInfo[''];       //店铺排序
//        $storeInsert['store_keywords'] = $artistInfo[''];       //店铺seo关键字
//        $storeInsert['store_description'] = $artistInfo[''];        //店铺seo描述
//        $storeInsert['store_qq'] = $artistInfo[''];     //QQ
//        $storeInsert['store_ww'] = $artistInfo[''];     //阿里旺旺
//        $storeInsert['store_phone'] = $artistInfo[''];      //商家电话
//        $storeInsert['store_domain'] = $artistInfo[''];     //店铺二级域名
    }
}
