<?php

namespace App\Models\Shop;

use Illuminate\Database\Eloquent\Model;
use DB;

class Goods extends Model
{
    /**
     * 关联到DB链接
     * @var string
     */
    protected $table = 'shopnc_goods';

    public $timestamps = false;

    public function __construct()
    {
        parent::__construct();
        $this->setConnection(env('SHOP', 'yinuo'));
    }

    public function getInfoById($id)
    {
        return $this->where(['artwork_id' => $id])->first();
    }

    public function getInsertData($data)
    {
        $good_insert = [];
        $good_insert['goods_name'] = $data['artwork_name'];
        $good_insert['gc_id'] = 0;
        $good_insert['gc_id_1'] = 0;
        $good_insert['gc_id_2'] = 0;
        $good_insert['gc_id_3'] = 0;
        $good_insert['goods_name'] = $data['artwork_name'] ?? '';
        $good_insert['goods_price'] = $data['price'] ?? 0;
        //商品主图
        $good_insert['goods_image'] = $data['artwork_img'] ?? '';
        //库存报警0
        //$good_insert['goods_storage_alarm'] = 0;
        //商品内容
        $good_insert['goods_body'] = $data['artwork_name'] ?? '';
        //重量或者体积
        //$good_insert['goods_trans_v'] = $data['artwork_name'] ?? '';
        //是否特殊商品(下单后不可退款退货) 0 不是 1 是
        $good_insert['is_special'] = 1;
        //销售类型：1普通商品、2可议价商品、3新手专区商品、4: 艺术家作品
        $good_insert['sales_model'] = 4;

        $good_insert['goods_state'] = $data['is_sell'];//1售卖商品0下架商品
        $good_insert['goods_verify'] = 1;//审核通过
        $good_insert['goods_storage'] = 1;//商品库存
        return $good_insert;
    }

    public function add()
    {
        $model_goods = Model('lib_goods');
        $_array = array();

        //comm表
        $_array['goods_attr'] = serialize($_POST['attr']);
        $_array['goods_custom'] = serialize($_POST['custom']);
        $_array['goods_img'] = serialize($_POST['img']);
        $_array['goods_body'] = $_POST['g_body'];
        $_array['mobile_body'] = $_POST['m_body'];

        $_array['goods_trans_kg'] = floatval($_POST['goods_trans_kg']);
        $_array['goods_trans_v'] = floatval($_POST['goods_trans_v']);

        $_array['goods_addtime'] = time();
        $_array['goods_edittime'] = time();
        //商品视频
        $_array['goods_video'] = $_POST['video_path'];

        $state = $model_goods->addGoods($_array);
    }


    public function getGoodsIdByArtworkId($artwork_id)
    {
        return $this->where(['artwork_id' => $artwork_id])->first();
    }
}
