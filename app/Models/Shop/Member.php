<?php
/**
 * Created by PhpStorm.
 * User: Mr.Liu
 * Date: 2019/3/13 0013
 * Time: 20:28
 */

namespace App\Models\Shop;

/**
 * Class Member
 *
 * @property int $member_id 会员id
 * @property string|null $member_key 用户唯一标识key
 * @property string $member_name 会员名称
 * @property string|null $member_truename 会员名称
 * @property string|null $member_avatar 会员头像
 * @property int|null $member_sex 会员性别
 * @property string|null $member_real_name 真实姓名
 * @property string|null $member_using_mobile 正在使用的手机
 * @property string|null $member_profile 会员简介
 * @property string|null $member_birthday 生日
 * @property string $member_passwd 会员密码
 * @property string|null $member_paypwd 支付密码
 * @property string $member_email 会员邮箱
 * @property int $member_email_bind 0未绑定1已绑定
 * @property string|null $member_mobile 手机号
 * @property int $member_mobile_bind 0未绑定1已绑定
 * @property string|null $member_qq qq
 * @property string|null $member_ww 阿里旺旺
 * @property int $member_login_num 登录次数
 * @property string $member_time 会员注册时间
 * @property string $member_login_time 当前登录时间
 * @property string $member_old_login_time 上次登录时间
 * @property string|null $member_login_ip 当前登录ip
 * @property string|null $member_old_login_ip 上次登录ip
 * @property string|null $member_qqopenid qq互联id
 * @property string|null $member_qqinfo qq账号相关信息
 * @property string|null $member_sinaopenid 新浪微博登录id
 * @property string|null $member_sinainfo 新浪账号相关信息序列化值
 * @property string|null $weixin_unionid 微信用户统一标识
 * @property string|null $weixin_info 微信用户相关信息
 * @property string|null $weixin_open_id 微信openID
 * @property int $member_points 会员积分
 * @property float $available_predeposit 预存款可用金额
 * @property float $freeze_predeposit 预存款冻结金额
 * @property float $available_rc_balance 可用充值卡余额
 * @property float $freeze_rc_balance 冻结充值卡余额
 * @property int $inform_allow 是否允许举报(1可以/2不可以)
 * @property int $is_buy 会员是否有购买权限 1为开启 0为关闭
 * @property int $is_allowtalk 会员是否有咨询和发送站内信的权限 1为开启 0为关闭
 * @property int $member_state 会员的开启状态 1为开启 0为关闭
 * @property int $member_snsvisitnum sns空间访问次数
 * @property int|null $member_areaid 地区ID
 * @property int|null $member_cityid 城市ID
 * @property int|null $member_provinceid 省份ID
 * @property string|null $member_areainfo 地区内容
 * @property string|null $member_privacy 隐私设定
 * @property int $member_exppoints 会员经验值
 * @property float|null $trad_amount 佣金总额
 * @property string|null $auth_message 审核意见
 * @property int|null $distri_state 分销状态 0未申请 1待审核 2已通过 3未通过 4清退 5退出
 * @property string|null $bill_user_name 收款人姓名
 * @property string|null $bill_type_code 结算账户类型
 * @property string|null $bill_type_number 收款账号
 * @property string|null $bill_bank_name 开户行
 * @property float|null $freeze_trad 冻结佣金
 * @property string|null $distri_code 分销代码
 * @property int|null $distri_time 申请时间
 * @property int|null $distri_handle_time 处理时间
 * @property int $distri_show 分销中心是否显示 0不显示 1显示
 * @property int|null $quit_time 退出时间
 * @property int|null $distri_apply_times 申请次数
 * @property int|null $distri_quit_times 退出次数
 * @property string|null $alipay_account 支付宝账户
 * @property string|null $alipay_name 支付宝名称
 * @property int|null $member_alipay_bind 支付宝绑定 0否 1是
 * @property float|null $available_margin 保证金账户
 * @property float|null $default_amount 违约金金额
 * @property float|null $freeze_margin 冻结保证金金额
 * @property int|null $member_type 会员类型 0注册会员 1普通会员 2特一级会员 3特二级会员 4艺术家
 * @property int|null $member_role 会员角色：0 注册会员，1 内部会员，2 导入会员
 * @property int|null $freeze_points 冻结积分
 * @property int|null $bind_member_id 绑定会员编号
 * @property string|null $bind_member_name 绑定会员名称
 * @property int|null $bind_time 绑定时间
 * @property float|null $available_commis 已返佣金
 * @property float $freeze_commis 待返佣金
 * @property int|null $is_artist 是否是艺术家 0:否   1:是
 * @property float|null $td_amount 购买特定专区商品金额
 * @property int|null $register_award 是否领取过注册奖励
 * @property int|null $source 用户来源  1,张雄艺术
 * @property int|null $distribute_lv_1 一级合伙人
 * @property int|null $distribute_lv_2 二级合伙人
 * @property int|null $recommended 是否已介绍拍乐赚专场0:未介绍,1:已介绍
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereAlipayAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereAlipayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereAuthMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereAvailableCommis($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereAvailableMargin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereAvailablePredeposit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereAvailableRcBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereBillBankName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereBillTypeCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereBillTypeNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereBillUserName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereBindMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereBindMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereBindTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereDefaultAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereDistriApplyTimes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereDistriCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereDistriHandleTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereDistriQuitTimes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereDistriShow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereDistriState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereDistriTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereDistributeLv1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereDistributeLv2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereFreezeCommis($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereFreezeMargin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereFreezePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereFreezePredeposit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereFreezeRcBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereFreezeTrad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereInformAllow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereIsAllowtalk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereIsArtist($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereIsBuy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberAlipayBind($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberAreaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberAreainfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberCityid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberEmailBind($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberExppoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberLoginNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberLoginTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberMobileBind($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberOldLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberOldLoginTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberPasswd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberPaypwd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberPoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberPrivacy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberProfile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberProvinceid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberQq($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberQqinfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberQqopenid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberRealName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberSinainfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberSinaopenid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberSnsvisitnum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberTruename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberUsingMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereMemberWw($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereQuitTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereRecommended($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereRegisterAward($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereTdAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereTradAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereWeixinInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereWeixinOpenId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Member whereWeixinUnionid($value)
 * @mixin \Eloquent
 */
class Member extends BaseModel
{
    /**
     * 关联到DB链接
     * @var string
     */
    protected $table = 'shopnc_member';

    public $timestamps = false;

    public function __construct()
    {
        parent::__construct();
    }

    public function getInfoByKey($key)
    {
        $member_info = $this->where(['member_key' => $key])->first();
        if (!$member_info || !isset($member_info['member_id'])) {
            return 0;
        }
        return $member_info['member_id'];
    }
}