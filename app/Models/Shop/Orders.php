<?php

namespace App\Models\Shop;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    /**
     * 关联到DB链接
     * @var string
     */
    protected $table = 'shopnc_orders';

    public $timestamps = false;

    public function __construct()
    {
        parent::__construct();
        $this->setConnection(env('SHOP', 'yinuo'));
    }
}
