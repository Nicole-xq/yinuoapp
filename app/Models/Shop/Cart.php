<?php

namespace App\Models\Shop;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    /**
     * 关联到DB链接
     * @var string
     */
    protected $table = 'shopnc_cart';

    public $timestamps = false;

    public function __construct()
    {
        parent::__construct();
        $this->setConnection('yinuo');
    }


    public function editCartPriceByGoodId($goodId, $price)
    {
        $cartUpdate = ['goods_price' => $price];
        $where = ['goods_id' => $goodId];
        $result = $this->where($where)->update($cartUpdate);
        if ($result === false) {
            return false;
        }
        return true;
    }
}
