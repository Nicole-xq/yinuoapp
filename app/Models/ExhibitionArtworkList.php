<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ExhibitionArtworkList
 *
 * @property int $id 自增ID
 * @property string|null $title 标题
 * @property int|null $start_time 开始时间
 * @property int|null $end_time 结束时间
 * @property int|null $begin_time 开幕时间
 * @property string|null $curator 策展人
 * @property string|null $sponsor 主办
 * @property string|null $joint_sponsor 协办
 * @property string|null $undertake 承办
 * @property int|null $organization_id 机构ID 多对一 暂无默认
 * @property int|null $city_id 城市ID
 * @property string|null $content 展览介绍
 * @property int|null $like_num 点赞量
 * @property int|null $click_num 阅读量
 * @property int|null $admin_like_num 虚拟点赞量
 * @property int|null $admin_click_num 虚拟阅读量
 * @property int|null $is_charge 收费：1是，0否
 * @property int|null $is_recommend 推荐：1是，0否
 * @property int|null $is_delete 删除：1是，0否
 * @property string|null $created_at 添加时间
 * @property string|null $updated_at 修改时间
 * @property string|null $img_list 图集
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AppointmentList[] $artworkAppoint
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ArtistPersonal[] $artworkArtists
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ExhibitionArtworkPic[] $artworkPics
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AppointmentList[] $artworkUsers
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserIndex[] $artworkUsersAll
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Artwork[] $artworks
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkList whereAdminClickNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkList whereAdminLikeNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkList whereBeginTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkList whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkList whereClickNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkList whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkList whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkList whereCurator($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkList whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkList whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkList whereImgList($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkList whereIsCharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkList whereIsDelete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkList whereIsRecommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkList whereJointSponsor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkList whereLikeNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkList whereOrganizationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkList whereSponsor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkList whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkList whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkList whereUndertake($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkList whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ExhibitionArtworkList extends Model
{
    protected $field = [
        'id' => 'id',
        'title' => 'title',
        'img_list' => 'img_list',
        'start_time' => 'start_time',
        'end_time' => 'end_time',
        'begin_time' => 'begin_time',
        'organization_id' => 'organization_id',
        'created_at' => 'created_at',
        'like_num' => 'like_num',
        'click_num' => 'click_num',
        'is_recommend' => 'is_recommend',
    ];

    //该展览下边的约展的列表数目限制  为 五条
    protected $takeNum = 50;

    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'exhibition_artwork_list';

    protected $primaryKey = 'exhibition_id';

    public $timestamps = false;


    /**
     * 当前会展列表 数据表
     * 与 参展艺术家 ArtistPersonal 数据表关联关系
     */

    public function artworkArtists()
    {
        return $this->belongsToMany(
            'App\Models\ArtistPersonal',
            'exhibition_artwork_joint',
            $this->primaryKey,
            'artist_id'
        )->withPivot('artwork_id');
    }

    /**
     * 当前会展列表 数据表
     * 与 约展用户 ArtistPersonal 数据表关联关系
     */

    public function artworkUsersAll()
    {
        return $this->belongsToMany(
            'App\Models\UserIndex',
            'appointment_list',
            $this->primaryKey,
            'user_id'
        );
    }

    /**
     * 当前会展列表 数据表
     * 与 约展列表关系 AppointmentList 数据表关联关系
     */

    public function artworkUsers()
    {
        return $this->hasMany(
            'App\Models\AppointmentList',
            $this->primaryKey,
            'id'
        );
    }


    /**
     * 当前会展列表 数据表
     * 与 约展用户 ArtistPersonal 数据表关联关系
     */

    public function artworks()
    {
        return $this->belongsToMany(
            'App\Models\Artwork',
            'exhibition_artwork_joint',
            $this->primaryKey,
            'artwork_id'
        );
    }

    /**
     * 当前会展列表 数据表
     * 与 参展艺术家 ArtistPersonal 数据表关联关系
     */

    public function artworkPics()
    {
        return $this->hasMany(
            'App\Models\ExhibitionArtworkPic',
            $this->primaryKey,
            'id'
        );
    }


    /**
     * 当前会展列表 数据表
     * 与 约展表 数据表关联关系
     */

    public function artworkAppoint()
    {
        return $this->hasMany(
            'App\Models\AppointmentList',
            $this->primaryKey,
            'id'
        );
    }

    /**
     * 艺评首页--艺展列表,艺展图片,参展艺术家信息
     * 显示推荐  时间倒叙
     */
    public function fetchExhibitions($where)
    {
        //本表查询字段
        $fields = array_values($this->field);
        $query = $this::with([
            'artworkAppoint' => function ($query) {
                $query->select([
                    'appointment_list.id',
                    'appointment_list.user_avatar',
                    'appointment_list.user_id',
                    'appointment_list.exhibition_id']);
            }
        ]);
        if ($where) {
            $query = $query->where($where);
        }
        $query = $query->orderBy('begin_time', 'desc');
        $query = $query->select($fields);
        $query = $query->paginate($this->perPage);
        return $query->toarray();
    }


    /**
     * 艺展的搜索展示列表
     * @'is_charge' '1:收费, 0不收费'; 即将开始和即将结束的艺展 艺展的地区限定
     */
    public function searchExhibitions($where, $field)
    {
        //本表查询字段
        $fields = $this->field;
        if (empty($field)) {
            $query = $this::with([
                'artworkAppoint' => function ($query) {
                    $query->select([
                        'appointment_list.id',
                        'appointment_list.user_avatar',
                        'appointment_list.user_id',
                        'appointment_list.exhibition_id'
                    ]);
                    $query->take($this->takeNum);
                }
            ]);
            $query->where($where);
            $query->where('end_time', '>=', time());
            $query->orderBy('begin_time', 'desc');
            $query->select($fields);
            return $query->paginate($this->perPage);
        } else {
            $query = $this::with([
                'artworkAppoint' => function ($query) {
                    $query->select([
                        'appointment_list.id',
                        'appointment_list.user_avatar',
                        'appointment_list.user_id',
                        'appointment_list.exhibition_id'
                    ]);
                    $query->take($this->takeNum);
                }
            ]);
            if (!empty($exhibition_ids)) {
                $query->whereIn('exhibition_artwork_list.id', $exhibition_ids);
            }
            $query->where($where);
            if ($field == 'start_time') {
                $query->where('start_time', '>=', time());
            } else {
                $query->where('start_time', '<=', time());
                $query->where('end_time', '>=', time());
            }
            if ($field == 'start_time') {
                $query->orderBy('start_time', 'asc');
            } else {
                $query->orderBy('end_time', 'asc');
            }
            $query->select($fields);
            return $query->paginate($this->perPage);
        }
    }

    /**
     * 艺展的详情页   艺展  约展用户 艺展图片 艺术家
     */
    public function getExhibition($exhibition)
    {
        //本表查询字段
        $fields = [
            'id', 'title', 'start_time', 'end_time', 'begin_time', 'organization_id',
            'created_at', 'like_num', 'click_num', 'is_charge', 'sponsor', 'joint_sponsor',
            'undertake', 'content', 'img_list'
        ];
        $where = [
            'is_delete' => 0,
            'id' => $exhibition
        ];
        return $this::with([
            'artworkUsers' => function ($query) {
                $query->select(['user_id', 'user_avatar', 'exhibition_id', 'count_num', 'id as appoint_id']);
            },
            'artworkPics'
        ])
            ->where($where)
            ->select($fields)
            ->first();
    }

    /**
     * 艺展的详情页
     */
    public function getArtworkInfo($id)
    {
        //本表查询字段
        $fields = array_values($this->field);
        $fields[] = 'is_charge';
        $fields[] = 'sponsor';
        $fields[] = 'joint_sponsor';
        $fields[] = 'undertake';
        $where = [
            'is_delete' => 0,
            'id' => $id
        ];
        return $this::with([
            'artworkUsers' => function ($query) {
                $query->select(['user_id', 'user_avatar', 'exhibition_id', 'count_num']);
            },
            'artworkArtists' => function ($query) {
                $query->select([
                    'artist_personal.id',
                    'artist_personal.user_id',
                    'artist_personal.role_id',
                    'artist_personal.name',
                    'artist_personal.logo']);
            },
            'artworkPics'
        ])
            ->where($where)
            ->select($fields)
            ->first($this->perPage)
            ->toarray();
    }


    /**
     * 根据艺展的ID 显示参展的用户
     */
    public function showUser($id)
    {
        return $this::with('artworkAppoint')
            ->where(['is_delete' => 0, 'id' => $id])
            ->orderBy('add_time', 'asc')
            ->paginate($this->perPage);
    }

    /*
     * 根据ids 获取对应集合的数据
     */
    public function getExhibitionInfo($exhibition_ids)
    {
        $field = ['id', 'title', 'img_list'];
        return $this::whereIn('id', $exhibition_ids)
            ->select($field)
            ->get()
            ->toarray();
    }
}
