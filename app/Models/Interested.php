<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Interested
 *
 * @property int $initiator_user_id 发起约展用户ID
 * @property int $participants_user_id 参与约展用户ID
 * @property int|null $interested 是否感兴趣：0无操作、1感兴趣、2不感兴趣
 * @property \Carbon\Carbon|null $updated_at 更新时间
 * @property \Carbon\Carbon|null $created_at 创建时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Interested whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Interested whereInitiatorUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Interested whereInterested($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Interested whereParticipantsUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Interested whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Interested extends Model
{
    protected $table = 'interested';
    protected $primaryKey = 'initiator_user_id';


}
