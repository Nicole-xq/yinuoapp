<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ArtistOrganization
 *
 * @property int $id 自增ID
 * @property int|null $user_id 用户ID
 * @property int|null $role_id 角色ID
 * @property int|null $role_grade 角色等级
 * @property string|null $artist_type 艺术分类
 * @property string|null $name 名称
 * @property string|null $nick_name 机构-公司简称
 * @property string|null $real_name 用户身份证名字
 * @property string|null $identification_num 身份证号码
 * @property string|null $identification_pic 身份证图片(正反面)
 * @property string|null $logo LOGO
 * @property string|null $description 描述
 * @property int|null $article_num 文章数量
 * @property int|null $province_id 省
 * @property int|null $city_id 市
 * @property int|null $area_id 区
 * @property string|null $address 详细地址
 * @property string|null $addr 定位（经纬）
 * @property string|null $updated_at 修改时间
 * @property string|null $created_at 添加时间
 * @property string|null $phone_num 机构-联系电话
 * @property string|null $brand_pic 机构-商标专利图片
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistOrganization whereAddr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistOrganization whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistOrganization whereAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistOrganization whereArticleNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistOrganization whereArtistType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistOrganization whereBrandPic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistOrganization whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistOrganization whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistOrganization whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistOrganization whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistOrganization whereIdentificationNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistOrganization whereIdentificationPic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistOrganization whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistOrganization whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistOrganization whereNickName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistOrganization wherePhoneNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistOrganization whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistOrganization whereRealName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistOrganization whereRoleGrade($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistOrganization whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistOrganization whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistOrganization whereUserId($value)
 * @mixin \Eloquent
 */
class ArtistOrganization extends Model
{

    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'artist_organization';

    public $timestamps = false;

    public function get_exhibition_list_byCity($city_id)
    {
        $where = ['city_id' => $city_id];
        return $this->where($where)->lists('id');
    }

    public function getOrganizationList($organization_ids)
    {
        return $this::whereIn('id', $organization_ids)
            ->get()
            ->toArray();
    }


    public function getArtistInfo($artist_id)
    {
        //本表查询字段
        $fields = [
            '*'
        ];
        $where = [
            'id' => $artist_id
        ];
        return $this::where($where)
            ->select($fields)
            ->first();
    }


    public function get_organization_list($condition = [], $files = '*', $order = 'id desc', $limit = 0, $NotNull = '')
    {
        $obj = $this::select($files);
        $obj->where($condition);
        if (!empty($NotNull)) {
            $obj->WhereNotNull($NotNull);
        }
        $obj->orderByRaw($order);
        if ($limit == 0) {
            $result = $obj->paginate($this->page)->toArray();
        } else {
            $result = $obj->take($limit)->get()->toArray();
        }
        return $result;
    }
}
