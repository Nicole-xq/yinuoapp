<?php
/**
 * Created by Test, 2018/10/08 17:10.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserFollow
 *
 * @property int $id
 * @property int $user_id 用户id
 * @property int|null $follow_user_id 被关注用户id
 * @property int|null $is_delete 是否取关 0：未取关 1：已取关
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserFollow whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserFollow whereFollowUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserFollow whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserFollow whereIsDelete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserFollow whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserFollow whereUserId($value)
 * @mixin \Eloquent
 */
class UserFollow extends Model
{
    protected $table = 'user_follow';
    protected $primaryKey = 'id';

    public function getInfo($condition){
        return $this->where($condition)->first();
    }

    public function updateInfo($param,$condition){
        return $this->where($condition)->update($param);
    }

    public function addInfo($param){
        return $this->insertGetId($param);
    }

    public function getList($condition,$fields = '*',$order = 'updated_at desc'){
        return $this->select($fields)->where($condition)->orderByRaw($order)->paginate($this->page)->toArray();
    }

    public function getCount($condition)
    {
        return $this->where($condition)->count();
    }
    /**
     * 获取用户关注数量
     * @param $user_id
     * @return mixed
     */
    public function followsNumGet($user_id)
    {
        $where = [
            'user_id' => $user_id,
            'is_delete' => 0,
        ];
        return $this->where($where)->count();
    }
    /**
     * 获取用户关注数量
     * @param $user_id
     * @return mixed
     */
    public function fansNumGet($user_id)
    {
        $where = [
            'follow_user_id' => $user_id,
            'is_delete' => 0,
        ];
        return $this->where($where)->count();
    }


    public function fansGet($user_id)
    {
        $filed = ['id', 'created_at'];
        $where = ['follow_user_id' => $user_id, 'is_delete' => 0];
        return $this->select($filed)
            ->where($where)
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public function doesLikeJudgeByIds($follow_user_ids, $user_id)
    {
        $where = [
            'user_id' => $user_id,      //关注人ID
            'is_delete' => 0,
        ];
        $query = $this->where($where);
        if (is_array($follow_user_ids)) {
            $query->whereIn('follow_user_id', $follow_user_ids);
        } else {
            $query->where(['follow_user_id' => $follow_user_ids]);
        }
        $like_list = $query->lists('follow_user_id');
        return $like_list ? $like_list->toArray() : $like_list;
    }
}