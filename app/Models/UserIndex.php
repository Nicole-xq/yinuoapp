<?php

namespace App\Models;

use App\Libs\phpQuery\http;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;

/**
 * App\Models\UserIndex
 *
 * @property int $user_id 会员id
 * @property string|null $user_key
 * @property string|null $user_name 会员名称
 * @property string|null $user_nickname 会员昵称
 * @property string|null $user_avatar 会员头像
 * @property int|null $user_sex 会员性别 1为男 2为女
 * @property string|null $user_real_name 真实姓名
 * @property string|null $user_birthday 生日
 * @property string|null $user_flag 签名
 * @property string|null $user_passwd 会员密码
 * @property string|null $user_email 会员邮箱
 * @property int|null $user_email_bind 0未绑定1已绑定
 * @property string|null $user_mobile 手机号
 * @property int|null $user_mobile_bind 0未绑定1已绑定
 * @property int|null $user_login_num 登录次数
 * @property string|null $user_login_time 当前登录时间
 * @property string|null $user_old_login_time 上次登录时间
 * @property string|null $user_login_ip 当前登录ip
 * @property string|null $user_old_login_ip 上次登录ip
 * @property string|null $weixin_unionid 微信用户统一标识
 * @property string|null $weixin_info 微信用户相关信息
 * @property string|null $weixin_open_id 微信openID
 * @property string|null $sina_unionid 新浪用户统一标识
 * @property string|null $sina_info 新浪用户相关信息
 * @property string|null $qq_unionid qq用户统一标识
 * @property int|null $user_areaid 地区ID
 * @property int|null $user_cityid 城市ID
 * @property int|null $user_provinceid 省份ID
 * @property string|null $user_areainfo 地区内容
 * @property string|null $alipay_account 支付宝账户
 * @property string|null $alipay_name 支付宝名称
 * @property int|null $user_alipay_bind 支付宝绑定 0否 1是
 * @property string|null $db 数据库
 * @property int|null $authentication_type 认证类型(1: 艺术家认证, 2: 机构认证, 0: 未认证)
 * @property int|null $authentication_id 认证关联ID(艺术家表的艺术家ID,或者机构ID)
 * @property int|null $is_disable 禁用：1是，0否
 * @property string|null $remember_token
 * @property string|null $created_at 添加时间
 * @property string|null $updated_at 修改时间
 * @property string|null $qq_info qq用户相关信息
 * @property string|null $read_time 用户阅读系统推送消息时间
 * @property int|null $fans_num 用户未阅读粉丝消息条数
 * @property int|null $comment_num 用户未阅读评论相关消息条数
 * @property int|null $appointment_num 用户未阅读约展消息条数
 * @property-read \App\Models\ExhibitionArtworkList $artworks
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereAlipayAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereAlipayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereAppointmentNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereAuthenticationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereAuthenticationType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereCommentNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereDb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereFansNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereIsDisable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereQqInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereQqUnionid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereReadTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereSinaInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereSinaUnionid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereUserAlipayBind($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereUserAreaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereUserAreainfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereUserAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereUserBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereUserCityid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereUserEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereUserEmailBind($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereUserFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereUserKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereUserLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereUserLoginNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereUserLoginTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereUserMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereUserMobileBind($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereUserName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereUserNickname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereUserOldLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereUserOldLoginTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereUserPasswd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereUserProvinceid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereUserRealName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereUserSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereWeixinInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereWeixinOpenId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserIndex whereWeixinUnionid($value)
 * @mixin \Eloquent
 */
class UserIndex extends Model
{
    /*
     * 随机取出数据限制
     */
    protected $limit = 2;

    /*
     * 缓存hash key
     */
    const CACHE_KEY = 'user_index_';

    /*
     * 缓存hash key
     */
    const USER_INFO = 'user_info';

    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'user_index';

    protected $primaryKey = 'user_id';

    public $timestamps = false;
    public $captcha = false;


    /**
     * 当用户表 信息表
     * 与 会展列表 数据表关联关系
     */

    public function artworks()
    {
        return $this->hasOne('App\Models\ExhibitionArtworkList', 'id', 'exhibition_id');
    }

    /*
     * 当前用户表 信息表
     * 显示在 艺评列表 根据ID 获取用户信息
     */
    public function getUserInfo($condition)
    {
        if (empty($condition) || !is_array($condition))
            return [];
        return $this->where($condition)
            ->orderBy('created_at', 'desc')
            ->first();
    }

    /*
     * 当前用户表 信息表
     * 显示在 艺评列表 根据ID 获取用户信息
     */
    public function getUserInfoById($id)
    {
        $redis = new Redis();
        if ($id < 1) {
            return [];
        }
        //$redis::del(self::CACHE_KEY . $id, self::USER_INFO);
        if ($redis::hexists(self::CACHE_KEY . $id, self::USER_INFO)) {
            //TODO
            return json_decode($redis::hget(self::CACHE_KEY . $id, self::USER_INFO), true);
        }
        $where = ['user_id' => $id];
        $user_info = $this->getUserInfo($where);
        if (empty($user_info)) {
            return [];
        }else{
            $user_info = $user_info->toArray();
            if (!empty($user_info['authentication_type']) && !empty($user_info['authentication_id'])) {
                if ($user_info['authentication_type'] == 1) {
                    $model = new ArtistPersonal();
                    $artist_info = $model->find($user_info['authentication_id']);
                    $sex = $artist_info['identification_num'] ? $this->getSex($artist_info['identification_num']) : 0;
                    $nick_name = $artist_info['name'] ?: $artist_info['real_name'];
                    $real_name = $artist_info['real_name'];
                    $logo = getPicturePath($artist_info['logo']);
                } else {
                    $model = new ArtistOrganization();
                    $artist_info = $model->find($user_info['authentication_id']);
                    $nick_name = $artist_info['nick_name'] ?: $artist_info['name'];
                    $real_name = $artist_info['name'];
                    $logo = getPicturePath($artist_info['logo']);
                }
                if (isset($sex) && !empty($sex)) {
                    $user_info['user_sex'] = $sex;
                }
                if (!empty($nick_name)) {
                    $user_info['user_nickname'] = $nick_name;
                }
                if (!empty($real_name)) {
                    $user_info['user_real_name'] = $real_name;
                }
                $user_info['user_avatar'] = $logo;
                $user_info['user_flag'] = $user_info['user_flag'] ?:  '这个人很懒，没有留下签名~';
            }
        }
        $user_info['invite_code'] = $this->getInviteCodeByKey($user_info['user_key']);
        $redis::hset(self::CACHE_KEY . $id, self::USER_INFO, json_encode($user_info));
        return $user_info;
    }



    /*
     * 获取商城邀请码
     */
    public function getInviteCodeByKey($key)
    {
        $shop_url = env('SHOP_URL', '');
        if (empty($shop_url)) {
            return response()->fail(200227);
        }
        $post_data = ['user_key' => $key];
        $invite_url = $shop_url . '/mobile/index.php?act=login&op=getInviteCodeByKey';
        $result = http::post($invite_url, $post_data);
        $result = json_decode($result, true);
        if (is_array($result) && $result['code'] == 200) {
            return $result['datas'];
        }
        return '';
    }



    public function getSex($identification_num)
    {
        $sexInt = (int)substr($identification_num, 16, 1);
        return $sexInt % 2 === 0 ? 2 : 1; //返回用户性别
    }

    public function cacheDel($id)
    {
        $redis = new Redis();
        if ($id < 1) {
            return false;
        }
        $redis::del(self::CACHE_KEY . $id, self::USER_INFO);
        return true;
    }


    public function getUserInfoByToken($token)
    {
        $user_token = UserToken::where('token', $token)->first();
        if (empty($user_token)) {
            return 300013;
        }
        if ($user_token->user_id) {
            $user = UserIndex::find($user_token->user_id);
        }
        if ($user_token->member_name) {
            $user = UserIndex::where('user_name', $user_token->member_name)->first();
        }
        if (!isset($user) || empty($user)) {
            return 300013;
        }
        return $user->toArray();
    }

    /*
     * 根据ids 获取用户信息
     */
    public function getUserInfoByIds($ids)
    {
        $field = ['user_id', 'user_key', 'user_sex', 'user_name', 'user_avatar'];
        return $this::whereIn('user_id', $ids)->select($field)->get();
    }

    public function updateUser($condition, $update)
    {
        $result = $this->where($condition)->update($update);
        return $result;
    }

    /**
     * 用户登录
     * @param $login_info
     * @return mixed
     */
    public function login($login_info)
    {
        $user_name = $login_info['user_name'];

        $user_info = $this->where('user_name', $user_name)->first();
        if (empty($user_info) && preg_match('/^0?(1)[0-9]{10}$/i', $user_name)) {//根据会员名没找到时查手机号
            $user_info = $this->where('user_mobile', $user_name)->first();
        }
        if (empty($user_info) && (strpos($user_name, '@') > 0)) {//按邮箱和密码查询会员
            $user_info = $this->where('user_email', $user_name)->first();
        }
        if (!empty($user_info)) {
            if ($user_info['is_disable']) {
                return ['error' => 300011];
            }

            if ($this->captcha) {
                return $user_info;
            }
            $password = md5($login_info['user_passwd']);
            if ($user_info['user_passwd'] != $password) {
                return ['error' => 300003];
            }

            return $user_info;
        } else {
            return ['error' => 300015];
        }
    }

    /**
     * 调用ucenter登录接口
     *
     * @param
     * @return mixed
     */
    public function loginUCenter($param)
    {
        $postUrl = 'login';
        $data = $this->_curl($postUrl, $param);
        return $data;
    }

    /**
     * 调用ucenter登录接口
     *
     * @param
     * @return mixed
     */
    public function avatarUCenter($param)
    {
        $postUrl = 'avatar';
        $data = $this->_curl($postUrl, $param);
        return $data;
    }

    /**
     * 调用ucenter修改接口
     *
     * @param
     * @return mixed
     */
    public function updateUCenter($param)
    {
        $postUrl = 'update';
        $data = $this->_curl($postUrl, $param);
        return $data;
    }

    // 请求UCenter
    private function _curl($postUrl, $curlPost)
    {
        $url = env('USER_CENTER', '');
        if (empty($url)) {
            return null;
        }
        $postUrl = $url . $postUrl;
        $ch = curl_init();//初始化curl
        curl_setopt($ch, CURLOPT_URL, $postUrl);//抓取指定网页
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);//要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_POST, TRUE);//post提交方式
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        $data = curl_exec($ch);//运行curl
        curl_close($ch);
        if ($data) {
            return json_decode($data);
        }
        return null;
    }
}
