<?php
/**
 *
 *
 * Created by PhpStorm.
 * User: ZSF
 * Date: 18/06/08
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * App\Models\ArticleComment
 *
 * @property int $id 自增ID
 * @property int|null $article_id 文章ID
 * @property int|null $author_id 作者ID
 * @property int|null $parent_id 上级id
 * @property int|null $commentator_id 评论人ID
 * @property string|null $commentator_nickname 评论人昵称
 * @property int|null $accepter_id 被评论人ID
 * @property string|null $accepter_nickname 被评论人昵称
 * @property string|null $msg 留言信息
 * @property int|null $state 状态：1已读，0未读
 * @property int|null $like_num 点赞数量
 * @property \Carbon\Carbon|null $updated_at 修改时间
 * @property \Carbon\Carbon|null $created_at 添加时间
 * @property int|null $is_delete 删除：1是，0否
 * @property int|null $is_examine 审核：0未审核，1通过，2未通过
 * @property int|null $is_show 评论列表是否展示：1都显示，2仅评论人显示，3仅被评论人显示，3都不显示
 * @property int|null $admin_id 审核人
 * @property-read \App\Models\ArticleCommentLike $articleCommentLike
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleComment whereAccepterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleComment whereAccepterNickname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleComment whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleComment whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleComment whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleComment whereCommentatorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleComment whereCommentatorNickname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleComment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleComment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleComment whereIsDelete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleComment whereIsExamine($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleComment whereIsShow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleComment whereLikeNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleComment whereMsg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleComment whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleComment whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleComment whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ArticleComment extends Model
{
    /**
     * 关联到模型的数据表
     *
     * @var string
     */
    protected $table = 'article_comment';

    // 删除状态：否
    const STATUE_DEL = 0;
    // 审核状态：通过
    const STATUE_EXAMINE = 1;

    /**
     * articleLike 关联评论点赞表
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function articleCommentLike()
    {
        return $this->hasOne(ArticleCommentLike::class, 'article_comment_id', 'id');
    }

    /**
     * 查询列表
     *
     * @param array $condition
     * @param array|string $field
     * @return mixed
     */
    public function getList($condition, $field = '*')
    {
        $where = [
            'is_delete' => self::STATUE_DEL,
            'is_examine' => self::STATUE_EXAMINE,
        ];
        return $this->select($field)
            ->where($condition)->where($where)
            ->orderBy('like_num', 'desc')
            ->orderBy('created_at', 'desc')
            ->orderBy('id', 'desc')
            ->paginate($this->perPage, '[*]', 'cPage');
    }

    /**
     * 查询列表
     *
     * @param array $condition
     * @param array|string $field
     * @return mixed
     */
    public function getMyComment($condition, $field = '*')
    {
        $condition['is_delete'] = self::STATUE_DEL;
        return $this->select($field)
            ->where($condition)->where('is_show','<','3')
            ->orderBy('created_at', 'desc')
            ->orderBy('like_num', 'desc')
            ->orderBy('id', 'desc')
            ->paginate();
    }

    /**
     * 查询列表
     *
     * @param array $condition
     * @param array|string $field
     * @return mixed
     */
    public function getBeComment($condition, $field = '*')
    {
        $condition['is_delete'] = self::STATUE_DEL;
        $condition['is_examine'] = self::STATUE_EXAMINE;
        return $this->select($field)
            ->where($condition)
//            ->where('commentator_id','<>', $condition['accepter_id'])
            ->where(function ($query) {
                $query->where('is_show','1')
                    ->orWhere('is_show','3');
            })
            ->orderBy('created_at', 'desc')
            ->orderBy('like_num', 'desc')
            ->orderBy('id', 'desc')
            ->paginate();
    }

    /*
     * 获取被评论条数
     */
    public function countNumGet($user_id)
    {
        $where = [
            'accepter_id' => $user_id,
            'is_delete' => self::STATUE_DEL,
            'is_examine' => self::STATUE_EXAMINE,
        ];
        return $this->where($where)->count();
    }

    /**
     * 查询子列表
     *
     * @param array $condition
     * @param array|string $field
     * @return mixed
     */
    public function getChildListById($parent_id, $field = '*')
    {
        $condition = [
            'parent_id' => $parent_id,
        ];
        return $this->getList($condition, $field);
    }

    /**
     * 查询详情
     *
     * @param array $condition
     * @param array|string $field
     * @param array $order
     * @return mixed
     */
    public function getInfo($condition, $field = '*', $order = 'id desc')
    {
        $condition['is_delete'] = self::STATUE_DEL;
        $condition['is_examine'] = self::STATUE_EXAMINE;
        return $this->select($field)
            ->where($condition)
            ->orderByRaw($order)
            ->first();
    }

    public function insertGetId($insert)
    {
        return DB::table($this->table)->insertGetId($insert);
    }
}