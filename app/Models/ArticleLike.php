<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ArticleLike
 *
 * @mixin \Eloquent
 */
class ArticleLike extends Model
{
    /**
     * 关联到模型的数据表
     *
     * @var string
     */
    protected $table = 'article_like';

    /**
     * 获取点赞详情
     *
     * @return mixed
     */
    public function getInfo($condition, $field = '*')
    {
        return self::select($field)->where($condition)->first();
    }

    public function getList($condition,$field = '*'){
        return $this->select($field)->where($condition)->paginate($this->prePage)->toArray();
    }

    public function countLikeNum($user_id)
    {
        $where = [
            'user_id' => $user_id,
            'is_delete' => 0,
        ];
        return $this->where($where)->count();
    }

    public function doesLikeJudgeByIds($article_ids, $user_id, $db)
    {
        $article_like = (new ArticleLike())->setConnection($db);
        $where = [
            'is_delete' => 0,
            'user_id' => $user_id
        ];
        $like_list = $article_like->where($where)
            ->whereIn('article_id', $article_ids)
            ->lists('article_id');
        return $like_list ? $like_list->toArray() : $like_list;
    }
}
