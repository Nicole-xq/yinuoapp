<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ArtworkCommentLike
 *
 * @mixin \Eloquent
 */
class ArtworkCommentLike extends Model
{
    /**
     * 关联到模型的数据表
     *
     * @var string
     */
    protected $table = 'artwork_comment_like';
}
