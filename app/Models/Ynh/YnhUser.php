<?php

namespace App\Models\Ynh;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Ynh\YnhUser
 *
 * @property int $id
 * @property int|null $user_id 用户id
 * @property string|null $user_key
 * @property int|null $status 状态:0,未审核 1,通过 2,不通过 3,未申请
 * @property int|null $is_disable 禁用：1是，0否
 * @property \Carbon\Carbon|null $created_at 身份认证时间
 * @property \Carbon\Carbon|null $updated_at 用户状态更新时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhUser whereIsDisable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhUser whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhUser whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhUser whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhUser whereUserKey($value)
 * @mixin \Eloquent
 */
class YnhUser extends Model
{
    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'ynh_user';

    public function getAuthenticationInfo($where)
    {
        return $this::where($where)->first();
    }

    public function authenticationInsert($insert)
    {
        return $this::insert($insert);
    }
}
