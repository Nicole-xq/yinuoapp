<?php

namespace App\Models\Ynh;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Ynh\ArtistType
 *
 * @property int $id 自增ID
 * @property string|null $name 类别名称
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\ArtistType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\ArtistType whereName($value)
 * @mixin \Eloquent
 */
class ArtistType extends Model
{
    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'artist_type';
}
