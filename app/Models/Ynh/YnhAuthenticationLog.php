<?php

namespace App\Models\Ynh;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Ynh\YnhAuthenticationLog
 *
 * @mixin \Eloquent
 */
class YnhAuthenticationLog extends Model
{
    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'ynh_authentication_log';
}
