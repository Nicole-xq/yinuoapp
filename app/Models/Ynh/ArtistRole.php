<?php

namespace App\Models\Ynh;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Ynh\ArtistRole
 *
 * @property int $role_id 自增ID
 * @property string|null $role_name 角色名称
 * @property int|null $role_type 属性：1个人，2机构
 * @property string|null $role_img 角色徽章
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\ArtistRole whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\ArtistRole whereRoleImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\ArtistRole whereRoleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\ArtistRole whereRoleType($value)
 * @mixin \Eloquent
 */
class ArtistRole extends Model
{
    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'artist_role';
}
