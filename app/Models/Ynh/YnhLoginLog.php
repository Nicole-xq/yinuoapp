<?php

namespace App\Models\Ynh;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Ynh\YnhLoginLog
 *
 * @property int $id
 * @property int $user_id
 * @property string $ip
 * @property \Carbon\Carbon|null $created_at 艺诺号用户登录时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhLoginLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhLoginLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhLoginLog whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhLoginLog whereUserId($value)
 * @mixin \Eloquent
 */
class YnhLoginLog extends Model
{
    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'ynh_login_log';

    public function loginLogInsert($insert)
    {
        return $this::insert($insert);
    }
}
