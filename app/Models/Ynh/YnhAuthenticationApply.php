<?php

namespace App\Models\Ynh;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Ynh\YnhAuthenticationApply
 *
 * @property int $apply_id
 * @property int|null $user_id 用户ID(这里的user_id 取的是user_index的ID)
 * @property string|null $real_name 个人-真实姓名, 机构-公司名称
 * @property string|null $real_avatar 个人-个人照片
 * @property string|null $trinity_num 机构-三证合一营业执照号
 * @property string|null $document_pic 个人-奖项证书图片, 机构-营业执照图片
 * @property string|null $identification_num 个人-用户身份证号码, 机构-法人身份证号码
 * @property string|null $identification_pic 身份证图片(正1反2面)
 * @property string|null $name 机构-法人姓名
 * @property string|null $logo LOGO
 * @property int|null $role_id 角色ID
 * @property string|null $role_grade 角色等级
 * @property string|null $artist_type 艺术分类(创作分类)
 * @property int|null $province_id 省
 * @property int|null $city_id 市
 * @property int|null $area_id 区
 * @property string|null $address 详细地址
 * @property string|null $addr 定位（经纬）
 * @property string|null $description 简介
 * @property int|null $is_valid 状态：0未审核、1审核通过、2审核不通过
 * @property string|null $fail_cause 审核失败原因
 * @property int|null $authentication_type 认证类型(1: 艺术家认证, 2: 机构认证)
 * @property int|null $authentication_id 认证关联ID(艺术家表的艺术家ID,或者机构ID)
 * @property \Carbon\Carbon|null $created_at 创建时间
 * @property \Carbon\Carbon|null $updated_at 修改时间
 * @property string|null $nick_name 个人-认证昵称, 机构-公司简称
 * @property string|null $phone_num 个人-手机号, 机构-联系电话
 * @property int|null $user_sex 个人-用户性别 1为男 2为女, 默认 男
 * @property int|null $nation 目前分1中国,2海外
 * @property string|null $artwork_pic 个人-作品图片
 * @property string|null $brand_pic 机构-商标专利图片
 * @property string|null $authentication_time 认证通过时间
 * @property int|null $is_recommend 推荐：1是，0否
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereAddr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereApplyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereArtistType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereArtworkPic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereAuthenticationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereAuthenticationTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereAuthenticationType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereBrandPic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereDocumentPic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereFailCause($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereIdentificationNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereIdentificationPic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereIsRecommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereIsValid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereNation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereNickName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply wherePhoneNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereRealAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereRealName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereRoleGrade($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereTrinityNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ynh\YnhAuthenticationApply whereUserSex($value)
 * @mixin \Eloquent
 */
class YnhAuthenticationApply extends Model
{
    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'ynh_authentication_apply';

    /**
     * @param $condition
     * @param string $field
     * @param string $read_time
     * @return mixed
     */
    public function recommendArtistFetch($condition, $field = '*')
    {
        $query = $this->select($field);
        $query->where($condition);
        $query->orderBy('updated_at', 'desc');
        $query->groupBy('user_id');
        return $query->paginate();
    }

    public function get_artist_list($condition = [], $fields = '*', $order = 'id desc', $limit = 0)
    {
        $obj = $this::select($fields);
        $obj->where($condition);
        //if (!empty($NotNull)) {
        //  $obj->WhereNotNull($NotNull);
        //}
        $obj->groupBy('user_id');
        $obj->orderByRaw($order);
        if ($limit == 0) {
            $result = $obj->paginate($this->page)->toArray();
        } else {
            $result = $obj->take($limit)->get()->toArray();
        }
        return $result;
    }
}
