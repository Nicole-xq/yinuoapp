<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Users
 *
 * @mixin \Eloquent
 */
class Users extends Model
{
    protected $primaryKey = 'user_id';

}