<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ExhibitionArtworkPic
 *
 * @property int $id 自增ID
 * @property int|null $exhibition_id 会展ID
 * @property string|null $exhibition_pic 会展图片名称
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ExhibitionArtworkList[] $artworks
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkPic whereExhibitionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkPic whereExhibitionPic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkPic whereId($value)
 * @mixin \Eloquent
 */
class ExhibitionArtworkPic extends Model
{


    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'exhibition_artwork_pic';

    public $timestamps = false;

    /**
     * 当前会展图片 信息表
     * 与 会展列表 数据表关联关系
     */

    public function artworks()
    {
        return $this->hasMany('App\Models\ExhibitionArtworkList', 'id', 'exhibition_id');
    }

    /**
     * 根据艺展ID 获取所有的图片
     */

    public function showExhibitionPicList($id)
    {
        $where = [
            'exhibition_id' => $id
        ];
        return $this::where($where)->get()->toarray();

    }


}
