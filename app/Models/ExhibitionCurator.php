<?php
/**
 * Created by Test, 2018/06/29 15:37.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ExhibitionCurator
 *
 * @property int $curator_id 策展人id
 * @property int|null $exhibition_id 展览id
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionCurator whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionCurator whereCuratorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionCurator whereExhibitionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionCurator whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ExhibitionCurator extends Model
{
    protected $table = 'exhibition_curator';

    public $timestamp = false;

}