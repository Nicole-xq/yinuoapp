<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Sms
 *
 * @property int $id
 * @property string|null $ip 用户IP
 * @property string|null $phone 手机号
 * @property int|null $user_id 用户ID,注册为0
 * @property string|null $captcha 短信验证码
 * @property string|null $content 发送内容
 * @property int $type 短信类型:1为注册,2为登录,3为找回密码,默认为1
 * @property int|null $send_num 发送次数
 * @property int|null $state 状态：1有效、0无效
 * @property string|null $error 错误信息
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon $created_at 创建时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sms whereCaptcha($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sms whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sms whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sms whereError($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sms whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sms whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sms wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sms whereSendNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sms whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sms whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sms whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sms whereUserId($value)
 * @mixin \Eloquent
 */
class Sms extends Model
{
    //protected $pageSize = 10;               //要求取出数据表所有的数据的时候 默认显示取十条

    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'sms_log';

    //protected $primaryKey = 'user_id';

    public function addSmsInfo($arr)
    {
        return $this::insert($arr);
    }

    public function getNum($condition)
    {
        $begin = date('Y-m-d', time()) . ' 00:00:00';
        $end = date('Y-m-d', time()) . ' 23:59:59';

        return $this::where($condition)
            ->whereBetween('created_at', [$begin, $end])
            ->count();
    }

    public function getCode($condition)
    {
        return $this::where($condition)
            ->orderBy('id', 'desc')
            ->orderBy('created_at', 'desc')
            ->first();
    }

}
