<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ExhibitionArtworkJoint
 *
 * @property int $id 自增ID
 * @property int|null $exhibition_id 会展ID
 * @property int|null $artist_id 艺术家ID
 * @property int|null $artwork_id 作品ID
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkJoint whereArtistId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkJoint whereArtworkId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkJoint whereExhibitionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtworkJoint whereId($value)
 * @mixin \Eloquent
 */
class ExhibitionArtworkJoint extends Model
{
    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'exhibition_artwork_joint';

    public $timestamps = false;



    public function get_artists_artworks_byExhibitionId($exhibition_id, $artist_id)
    {
        $where = [
            'exhibition_id' => $exhibition_id,
            'artist_id' => $artist_id
        ];
        return $this::where($where)
            ->lists('artwork_id')
            ->toarray();
    }

    public function get_artworkIds_byExhibitionId($exhibition_id, $artist_ids)
    {
        $where = [
            'exhibition_id' => $exhibition_id,
        ];
        return $this::where($where)
            ->whereIn('artist_id' ,$artist_ids)
            ->get()
            ->toarray();
    }




}
