<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ArtworkPic
 *
 * @property int $id 自增ID
 * @property int|null $artwork_id 作品表ID
 * @property string|null $picture 作品图
 * @property string|null $created_at 添加时间
 * @property string|null $updated_at 修改时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkPic whereArtworkId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkPic whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkPic whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkPic wherePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkPic whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ArtworkPic extends Model
{

    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'artwork_pic';

    //protected $primaryKey = 'user_id';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
