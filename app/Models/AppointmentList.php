<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * App\Models\AppointmentList
 *
 * @property int $id 自增ID
 * @property int|null $exhibition_id 会展ID
 * @property string|null $exhibition_name 展览名称
 * @property int|null $user_id 用户ID
 * @property string|null $user_nickname 用户昵称
 * @property string|null $user_avatar 用户头像
 * @property int|null $go_date 约展时间
 * @property int|null $time_node 时间段：0全天，1上午，2下午
 * @property int|null $sex 约展人性别：0不限，1男，2女
 * @property string|null $address 地址信息
 * @property int $count_num 报名总人数
 * @property string|null $updated_at 修改时间
 * @property string|null $created_at 添加时间
 * @property-read \App\Models\ExhibitionArtworkList $appointExhibition
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserIndex[] $appointUsers
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserIndex[] $appointmentUsers
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppointmentList whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppointmentList whereCountNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppointmentList whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppointmentList whereExhibitionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppointmentList whereExhibitionName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppointmentList whereGoDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppointmentList whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppointmentList whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppointmentList whereTimeNode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppointmentList whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppointmentList whereUserAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppointmentList whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppointmentList whereUserNickname($value)
 * @mixin \Eloquent
 */
class AppointmentList extends Model
{
    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'appointment_list';

    public $timestamps = false;

    protected $field = [
        'id' => 'id',
        'exhibition_id' => 'exhibition_id',
        'user_id' => 'user_id',
        'user_avatar' => 'user_avatar',
        'user_nickname' => 'user_nickname',
        'sex' => 'sex',
        'count_num' => 'count_num',
        'created_at' => 'created_at',
        'address' => 'address',
        'time_node' => 'time_node',
        'go_date' => 'go_date'
    ];

    /**
     * 当前约展列表 数据表
     * 与 用户表 数据表关联关系
     */

    public function appointUsers()
    {
        return $this->hasMany(
            'App\Models\UserIndex',
            'user_id',
            'uid'
        );
    }

    public function appointmentUsers()
    {
        return $this->belongsToMany(
            'App\Models\UserIndex',
            'appointment_sign',
            'appointment_id',
            'user_id'
        );
    }


    /**
     * 当前会展列表 数据表
     * 与 约展表 数据表关联关系
     */

    public function appointExhibition()
    {
        return $this->hasOne(
            'App\Models\ExhibitionArtworkList',
            'id',
            'exhibition_id'
        );
    }

    /**
     * 艺展的展示列表中的参展的用户
     * @author ls 17612157796@163.com
     * @api
     * @return array
     */
    public function showUserList()
    {
        $sql = 'SELECT * FROM `appointment_list` INNER JOIN `user_index` ON';
        $sql .= ' `user_index`.`id` = `appointment_list`.`user_id` WHERE `appointment_list`.`user_id` = 1';
        return DB::select($sql);
    }


    //约展列表,艺展图片,约展用户信息
    public function showAppointmentList($exhibition_id)
    {
        $where = [
            'exhibition_id' => $exhibition_id
        ];
        $field = array_values($this->field);
        return $this::with([
            'appointExhibition' => function ($query) {
                $query->select(['id', 'img_list', 'title', 'begin_time', 'organization_id',]);
                $query->orderBy('begin_time', 'desc');
            }
        ])
            ->where($where)
            ->orderBy('created_at', 'desc')
            ->select($field)
            ->paginate($this->perPage);
    }

    //我的约展 我发起的约展的列表
    public function showMyAppointmentList($user_id)
    {
        $field = array_values($this->field);
        $where = [
            'user_id' => $user_id
        ];
        return $this::with([
            'appointExhibition' => function ($query) {
                $query->select(['id', 'title', 'begin_time', 'img_list', 'organization_id']);
                $query->orderBy('begin_time', 'desc');
            }
        ])
            ->where($where)
            ->orderBy('updated_at', 'desc')
            ->orderBy('created_at', 'desc')
            ->select($field)
            ->paginate($this->perPage);
    }

    //约展详情,用户信息,艺展信息,展览图片,展示美术馆名称
    public function showAppointment($appoint_id)
    {
        $field = array_values($this->field);
        $where = [
            'id' => $appoint_id
        ];
        return $this::with([
            'appointExhibition' => function ($query) {
                $query->select(['id', 'title', 'begin_time', 'end_time', 'img_list', 'organization_id']);
                $query->orderBy('begin_time', 'desc');
            }
        ])
            ->where($where)
            ->select($field)
            ->first();
    }

    public function addAppointment($arr)
    {
        return $this::insert($arr);
    }


    /**
     * 和TA约 约展报名记录  查询所有的报名记录
     */
    public function showInfo($id)
    {
        $where = [
            'initiator_id' => $id               //发起人id
        ];
        return $this->where($where)
            ->paginate($this->perPage);
    }


    /*
     * 获取约展列表看 是否已经发起约展
     */
    public function getAppointment($exhibition_id, $user_id)
    {
        $where = [
            'exhibition_id' => $exhibition_id,
            'user_id' => $user_id
        ];
        return $this::where($where)
            ->get();
    }


}
