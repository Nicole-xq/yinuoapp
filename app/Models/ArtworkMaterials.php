<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ArtworkMaterials
 *
 * @property int $id
 * @property int|null $category_id
 * @property string|null $name
 * @property string|null $updated_at
 * @property string|null $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkMaterials whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkMaterials whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkMaterials whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkMaterials whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkMaterials whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ArtworkMaterials extends Model
{

    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'artwork_materials';

    public $timestamps = false;

    protected $primaryKey = 'id';

    public function getMaterialsNameById($id)
    {
        return $this->where('id', $id)
            ->list('name');
    }
}
