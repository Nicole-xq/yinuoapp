<?php
/**
 * Created by Test, 2018/10/08 15:58.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ArtworkLike
 *
 * @mixin \Eloquent
 */
class ArtworkLike extends Model
{
    protected $table = 'artwork_like';
    protected $primaryKey = 'id';

    public function getList($condition,$field = '*'){
        return $this->select($field)->where($condition)->paginate($this->prePage)->toArray();
    }


    public function countLikeNum($user_id)
    {
        $where = [
            'user_id' => $user_id,
            'is_delete' => 0,
        ];
        return $this->where($where)->count();
    }
}