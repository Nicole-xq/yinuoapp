<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * App\Models\ArticleTitle
 *
 * @property int $id
 * @property int|null $classify_id 分类id
 * @property int|null $author_id 作者ID
 * @property string|null $author_name 作者（编辑，可与author_id不一致）
 * @property string|null $title 文章标题
 * @property string|null $picture 文章图片
 * @property string|null $description 描述
 * @property int|null $like_num 点赞量
 * @property int|null $click_num 阅读量
 * @property int|null $admin_like_num 虚拟点赞量
 * @property int|null $admin_click_num 虚拟阅读量
 * @property int|null $vote_agree_num 投票(顶)数量
 * @property int|null $vote_disagree_num 投票(踩)数量
 * @property int|null $comment_num 评论量
 * @property int|null $city_id 城市id
 * @property int|null $is_recommend 推荐：1是，0否。（首页展示）
 * @property int|null $is_top 精选：1是，0否。（设置精选后，列表显示大图）
 * @property int|null $is_delete 删除：1是，0否
 * @property int|null $status 状态:0,未审核 1,通过 2,不通过
 * @property string|null $updated_at 修改时间
 * @property string|null $created_at 添加时间
 * @property-read \App\Models\UserIndex $articleAuthor
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ArticleLike[] $articleLike
 * @property-read \App\Models\ArticleClassify $classify
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ArticleComment[] $comments
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTitle whereAdminClickNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTitle whereAdminLikeNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTitle whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTitle whereAuthorName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTitle whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTitle whereClassifyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTitle whereClickNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTitle whereCommentNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTitle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTitle whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTitle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTitle whereIsDelete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTitle whereIsRecommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTitle whereIsTop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTitle whereLikeNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTitle wherePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTitle whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTitle whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTitle whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTitle whereVoteAgreeNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTitle whereVoteDisagreeNum($value)
 * @mixin \Eloquent
 */
class ArticleTitle extends Model
{
    /**
     * 关联到模型的数据表
     *
     * @var string
     */
    protected $table = 'article_title';
    // 获取条数
    public $take = 6;
    // 是否分页
    public $is_page = true;
    // 用户信息
    public $user = null;
    // 管理员操作
    protected $admin_act = false;

    // 删除状态：否
    const STATUE_DEL = 0;
    // 评论审核状态：通过
    const STATUE_EXAMINE = 1;

    public $timestamps = false;

    public function perPage($perPage)
    {
        $this->perPage = $perPage;
    }


    public function __construct($param = [])
    {
        if (empty($param['admin'])) {
            $this->admin_act = true;
        }
    }

    /**
     * classify 关联类型表
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function classify()
    {
        return $this->hasOne(ArticleClassify::class, 'id', 'classify_id');
    }

    /**
     * comments 关联评论表
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(ArticleComment::class, 'article_id', 'id');
    }

    /**
     * articleUsers 关联用户表
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function articleAuthor()
    {
        return $this->hasOne(UserIndex::class, 'user_id', 'author_id');
    }

    /**
     * articleLike 关联点赞表
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function articleLike()
    {
        return $this->hasMany(ArticleLike::class, 'article_id');
    }

    /**
     * articleList 文章列表
     *
     * @param array $condition
     * @param array|string $class_id
     * @param array|string $field
     * @param string $order
     * @return mixed
     */
    public function articleList(array $condition, $class_id = '1', $field = '*', $order = 'created_at desc')
    {
        $obj = $this::select($field);

        if (count($condition)) {
            foreach ($condition as $k => $v) {
                if (is_array($v)) {
                    $obj->where($k, $v[0], $v[1]);
                    unset($condition[$k]);
                }
            }
        }

        $classify = $this->getArticleClassify($class_id);
        if (!empty($classify)) {
            if (count($classify) == 1) {
                $condition['classify_id'] = $classify;
            } else {
                $obj->whereIn('classify_id', $classify);
            }
        } else {
            $condition['classify_id'] = $class_id;
        }

        if ($this->admin_act) {
            $condition['is_delete'] = self::STATUE_DEL;
            $condition['status'] = self::STATUE_EXAMINE;
        }
        $obj->where($condition)->orderByRaw($order);

        // 是否分页
        if ($this->is_page) {
            $result = $obj->paginate($this->perPage);
        } else {
            $result = $obj->take($this->take)->get();
        }

        return $result ? $result->toArray() : $result;
        //return $result;
    }

    /**
     * indexRecommend 首页推荐列表
     *
     * @param array $condition
     * @param int $class_id
     * @param array|string $field
     * @return mixed
     */
    public function indexRecommend(array $condition, $class_id, $field = '*')
    {
        $class_id = $this->getArticleClassify($class_id);

        if ($this->admin_act) {
            $condition['is_delete'] = self::STATUE_DEL;
            $condition['status'] = self::STATUE_EXAMINE;
        }
        $result = ArticleTitle::with(['classify' => function ($query) {
            $query->select('id', 'type_name');
        }])
            ->select($field)
            ->where($condition)
            ->whereIn('classify_id', $class_id)
            ->orderBy('created_at', 'desc')
            ->paginate($this->perPage);
        return $result ? $result->toArray() : $result;
        //return $result;
    }

    public function getArtistNum($ids)
    {
        //pp($ids);
        $field = "`id`,`author_id`";
        $sql = "SELECT {$field} FROM `article_title` WHERE `classify_id` = '2' AND `author_id` IN ({$ids})";
        return DB::select($sql);

    }

    /**
     * 获取新闻子分类
     *
     * @return array
     */
    private function getArticleClassify($classify_id)
    {
        return ArticleClassify::where('parent_id', $classify_id)->lists('id', 'type_name')->toArray();
    }

    /**
     * 获取文章列表
     *
     * @article_ids array
     * @field array
     * @return ArticleTitle
     * */
    public function getByIds($article_ids, $field = '*')
    {
        $condition = [
            'is_delete' => self::STATUE_DEL,
            'status' => self::STATUE_EXAMINE,
        ];
        return ArticleTitle::select($field)->where($condition)->whereIn('id', $article_ids)->get();
    }

    public function articleInfoGet($article_id)
    {
        $where = ['id' => $article_id];
        return $this->where($where)->first();
    }

    public function getInfoById($id)
    {
        return $this->where(['id' => $id])->first();
    }


    public function countNumGet($user_id)
    {
        $return = [
            'article_num' => 0,
            'click_num' => 0,
            'like_num' => 0,
            'comment_num' => 0,
            'created_at' => '',
        ];
        $sql = "SELECT COUNT(*) as article_num, SUM(`click_num`) as click_num, max(`created_at`) as 'created_at',";
        //$sql .= " SUM(`like_num`) as like_num,";
        $sql .= " SUM(`comment_num`) as comment_num";
        $sql .= " FROM `article_title`";
        $sql .= " WHERE";
        $sql .= " `is_delete` = '0' AND";
        $sql .= " `author_id` = " . $user_id . " AND";
        $sql .= " status = 1";
        $result = DB::select($sql);
        if (empty($result) || !is_array($result)) {
            return $return;
        }
        return $result[0];
    }

    public function articleCountGet($user_id)
    {
        $where = [
            'author_id' => $user_id,
            'status' => 1,  //状态:0,未审核 1,通过 2,不通过
            'is_delete' => 0,
        ];
        return $this->where($where)->count();
    }
}
