<?phpnamespace App\Models;use Illuminate\Database\Eloquent\Model;/**
 * App\Models\ArtworkComment
 *
 * @property int $id 自增ID
 * @property int|null $artwork_id 作品ID
 * @property int|null $author_id 作者ID
 * @property int|null $parent_id 上级id
 * @property int|null $commentator_id 评论人ID
 * @property string|null $commentator_nickname 评论人昵称
 * @property int|null $accepter_id 被评论人ID
 * @property string|null $accepter_nickname 被评论人昵称
 * @property string|null $comment_msg 留言信息
 * @property int|null $state 状态：1已读，0未读
 * @property int|null $like_num 点赞数量
 * @property string $updated_at 修改时间
 * @property string $created_at 添加时间
 * @property int|null $is_delete 删除：1是，0否
 * @property int|null $is_examine 审核：0未审核，1通过，2未通过
 * @property int|null $is_show 评论列表是否展示：1都显示，2仅评论人显示，3仅被评论人显示，3都不显示
 * @property int|null $admin_id 审核人
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkComment whereAccepterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkComment whereAccepterNickname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkComment whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkComment whereArtworkId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkComment whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkComment whereCommentMsg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkComment whereCommentatorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkComment whereCommentatorNickname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkComment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkComment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkComment whereIsDelete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkComment whereIsExamine($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkComment whereIsShow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkComment whereLikeNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkComment whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkComment whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkComment whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ArtworkComment extends Model{    /**     * 关联到模型的数据表     * @var string     */    protected $table = 'artwork_comment';    public $timestamps = false;    protected $primaryKey = 'id';    // 删除状态：否    const STATUE_DEL = 0;    // 审核状态：通过    const STATUE_EXAMINE = 1;    /**     * 查询列表     *     * @param array $condition     * @param array|string $field     * @return mixed     */    public function getList($condition, $field = '*')    {        $where = [            'is_delete' => self::STATUE_DEL,            'is_examine' => self::STATUE_EXAMINE,        ];        $result = $this->select($field)            ->where($condition)->where($where)            ->orderBy('created_at', 'desc')            ->orderBy('id', 'desc')            ->paginate($this->perPage);        return $result ? $result->toArray() : $result;    }    public function countNumGet($user_id)    {        $where = [            'is_delete' => self::STATUE_DEL,            'is_examine' => self::STATUE_EXAMINE,            'accepter_id' => $user_id,        ];        return $this->where($where)->count();    }    /**     * 查询子列表     *     * @param array $condition     * @param array|string $field     * @return mixed     */    public function getChildListById($parent_id, $field = '*')    {        $condition = [            'parent_id' => $parent_id,        ];        $result = $this->getList($condition, $field);        if (isset($result['data']) && $result['data']) {            foreach ($result['data'] as &$value) {                $value['commentator_avatar'] = getMemberAvatarByID($value['commentator_id']);            }            if (isset($this->user) && $this->user) {                if ($this->user['user_id'] == $value['commentator_id']) {                    $value['is_follow'] = 2;                } else {                    $value['is_follow'] = (new UserFollow())                        ->doesLikeJudgeByIds($this->user['user_id'], $value['commentator_id']) ? 1 : 0;                }            }        }        return $result;    }    public function hotCommentChildFetch($parent_id, $field = '*', $take = 10)    {        $where = [            'parent_id' => $parent_id,            'is_delete' => self::STATUE_DEL,            'is_examine' => self::STATUE_EXAMINE,        ];        $result = $this->select($field)            ->where($where)->where($where)            ->orderBy('like_num', 'desc')            ->orderBy('created_at', 'asc')            ->orderBy('id', 'asc')            ->take($take);        return $result;    }}