<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserToken
 *
 * @property int $token_id 令牌编号
 * @property int $member_id 用户编号
 * @property string $member_name 用户名
 * @property int|null $user_id 用户ID
 * @property string|null $user_name 用户名
 * @property string $token 登录令牌
 * @property int $login_time 登录时间
 * @property string $client_type 客户端类型 android wap
 * @property string|null $openid 微信支付jsapi的openid缓存
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserToken whereClientType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserToken whereLoginTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserToken whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserToken whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserToken whereOpenid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserToken whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserToken whereTokenId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserToken whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserToken whereUserName($value)
 * @mixin \Eloquent
 */
class UserToken extends Model
{
    const DB_DEFAULT = 'yinuo';
    const DB_DEFAULT_USER_TABLE = 'shopnc_member';
    protected $table = 'shopnc_mb_user_token';

    public function __construct(array $attributes = [])
    {
        $this->setConnection(self::DB_DEFAULT);
        parent::__construct($attributes);
    }

    public function add($data)
    {
        return $this->insert($data);
    }

    public function getUserInfoByToken($api_token)
    {
        if (empty($api_token)) {
            return [];
        }

        $user_token = UserToken::where('token', $api_token)->first();

        if (empty($user_token)) {
            return [];
        }
        if ($user_token->user_id) {
            $user = UserIndex::find($user_token->user_id);
        }
        if ($user_token->member_name) {
            $user = UserIndex::where('user_name',$user_token->member_name)->first();
        }
        return $user ? $user->toArray() : [];
    }
}
