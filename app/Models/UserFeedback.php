<?php

namespace App\Models;


/**
 * Class UserFeedback
 *
 * @property int $id 用户反馈表
 * @property int|null $user_id 用户id
 * @property string|null $module 功能模块
 * @property string|null $content 内容
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 修改时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserFeedback whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserFeedback whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserFeedback whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserFeedback whereModule($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserFeedback whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserFeedback whereUserId($value)
 * @mixin \Eloquent
 * @property string|null $client 客户端
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserFeedback whereClient($value)
 */
class UserFeedback extends BaseModel
{
    protected $table = 'user_feedback';

    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'module',
        'content',
        'client'
    ];

    protected $guarded = [];

        
}