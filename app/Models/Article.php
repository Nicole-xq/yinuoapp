<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Article
 *
 * @property-read \App\Models\ArticleLike $articleLike
 * @mixin \Eloquent
 */
class Article extends Model
{

    /**
     * 关联到模型的数据表
     *
     * @var string
     */
    protected $table = 'article';

    /**
     * articleLike 关联点赞表
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function articleLike()
    {
        return $this->hasOne(ArticleLike::class, 'article_id', 'article_id');
    }

    /**
     * 获取详情
     *
     * @param array $condition
     * @return mixed
     */
    public function getInfo($condition, $field = '*')
    {
        return $this->select($field)->where($condition)->first();
    }

}
