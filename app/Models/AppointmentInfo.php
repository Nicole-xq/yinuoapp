<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AppointmentInfo
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AppointmentList[] $appointInfo
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserIndex[] $appointUser
 * @mixin \Eloquent
 */
class AppointmentInfo extends Model
{

    //protected $pageSize = 10;               //要求取出数据表所有的数据的时候 默认显示取十条

    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'appointment_info';

    //protected $primaryKey = 'user_id';

    public $timestamps = false;

    /**
     * 当前约展详情 数据表
     * 与 约展列表 数据表关联关系
     *
     */

    public function appointInfo()
    {
        return $this->hasMany('App\Models\AppointmentList', 'id', 'appointment_id');
    }

    /**
     * 当前约展详情 数据表
     * 与 约展列表 数据表关联关系
     *
     */

    public function appointUser()
    {
        return $this->hasMany('App\Models\UserIndex','appointment_list', 'id', 'appointment_id');
    }

    /**
     * 和TA 约, 添加至约展详情
     */
    public function addAppointment($arr)
    {
        return $this::insert($arr);
    }



    /**
     * 和TA约 约展报名记录  发起人查询
     */
    public function showInfo($appointment_sign_ids, $user_ids)
    {
            $query = $this::whereIn('user_id', $user_ids);
            return $query->whereIn('appointment_sign_id',$appointment_sign_ids)
            ->get()
            ->toarray();
    }



    /*
     * 和 ta 约的时候 先判断是不是都已经在约中了呢
     */
    public function getAppointment($arr, $connect)
    {
        $where = [
            'appointment_id' => $arr['appointment_id'],
            'user_id' => $arr['user_id']
        ];
        return $this::setConnection($connect)
            ->where($where)
            ->first();
    }


}
