<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ArtworkTheme
 *
 * @property int $id
 * @property int|null $category_id
 * @property string|null $name
 * @property string|null $updated_at
 * @property string|null $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkTheme whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkTheme whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkTheme whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkTheme whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkTheme whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ArtworkTheme extends Model
{

    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'artwork_theme';

    public $timestamps = false;

    protected $primaryKey = 'id';

    public function getThemeNameById($id)
    {
        return $this->where('id', $id)
            ->list('name');
    }
}
