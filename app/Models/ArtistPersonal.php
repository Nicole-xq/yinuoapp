<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * App\Models\ArtistPersonal
 *
 * @property int $id 自增ID
 * @property int|null $user_id 用户ID
 * @property int|null $role_id 角色ID
 * @property int|null $role_grade 角色等级
 * @property string|null $name 名称
 * @property string|null $artist_type 艺术分类
 * @property string|null $real_name 用户身份证名字
 * @property string|null $identification_num 身份证号码
 * @property string|null $identification_pic 身份证图片(正反面)
 * @property string|null $logo LOGO
 * @property string|null $description 描述
 * @property int|null $article_num 文章数量
 * @property int|null $province_id 省
 * @property int|null $city_id 市
 * @property int|null $area_id 区
 * @property string|null $address 详细地址
 * @property string|null $addr 定位（经纬）
 * @property string|null $authentication_time 认证通过时间
 * @property string|null $updated_at 修改时间
 * @property string|null $created_at 添加时间
 * @property string|null $artwork_time 艺术家作品更新时间
 * @property int|null $is_recommend 推荐：1是，0否
 * @property int|null $store_id 店铺ID
 * @property string|null $shop_url 艺术店铺
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Artwork[] $artworks
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistPersonal whereAddr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistPersonal whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistPersonal whereAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistPersonal whereArticleNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistPersonal whereArtistType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistPersonal whereArtworkTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistPersonal whereAuthenticationTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistPersonal whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistPersonal whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistPersonal whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistPersonal whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistPersonal whereIdentificationNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistPersonal whereIdentificationPic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistPersonal whereIsRecommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistPersonal whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistPersonal whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistPersonal whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistPersonal whereRealName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistPersonal whereRoleGrade($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistPersonal whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistPersonal whereShopUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistPersonal whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistPersonal whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtistPersonal whereUserId($value)
 * @mixin \Eloquent
 */
class ArtistPersonal extends Model
{
    /*
     * 随机取出数据限制
     */
    protected $limit = 20;

    /*
     * 数据表的用户ID
     */
    protected $userKey = 'user_id';

    /*
     * 数据表的主键
     */
    protected $primaryKey = 'id';


    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'artist_personal';

    public $timestamps = false;


    /**
     * 当前艺术家 数据表
     * 与 作品表关系 Artwork 数据表关联关系
     */

    public function artworks()
    {
        return $this->hasMany('App\Models\Artwork', 'artist_id', 'id');
    }

    /*
     * 艺评首页 顶部推荐用户随机 获取$this->limit 条数
     */
    public function getRandomArtistInfo()
    {
        $fields = "`id`, `name`, `logo`, `user_id`";
        $sql = "SELECT {$fields} FROM `{$this->table}` WHERE `role_id` = '3'";
        $sql .= " LIMIT {$this->limit}";
        return DB::select($sql);
    }

    /*
     * 展览详情  根据艺术家ID 获取艺术家 头像 名称 及其作品名称 作品图片
     */
    public function getArtworkInfo($artist_id)
    {
        //本表查询字段
        $fields = [
            'id', 'user_id', 'name', 'logo'
        ];
        $where = [
            'id' => $artist_id,
        ];
        return $this::with([
            'artworks' => function ($query) {
                $query->select(['id', 'artwork_name', 'artwork_img', 'artist_id']);
            }
        ])
            ->where($where)
            ->orderBy('updated_at', 'desc')
            ->select($fields)
            ->first()
            ->toarray();
    }


    public function getArtistInfo($artist_id)
    {
        //本表查询字段
        $fields = [
            '*'
        ];
        $where = [
            'id' => $artist_id
        ];
        return $this::where($where)
            ->select($fields)
            ->first();
    }

    public function getArtistListByIds($artist_ids)
    {
        //本表查询字段
        $fields = [
            'id', 'user_id', 'role_id', 'name', 'logo'
        ];
        return $this::whereIn('id', $artist_ids)
            ->select($fields)
            ->get();
    }

    public function get_artist_list($condition = [], $files = '*', $order = 'id desc', $limit = 0, $NotNull = '')
    {
        $obj = $this::select($files);
        $obj->where($condition);
        if (!empty($NotNull)) {
            $obj->WhereNotNull($NotNull);
        }
        $obj->orderByRaw($order);
        if ($limit == 0) {
            $result = $obj->paginate($this->page)->toArray();
        } else {
            $result = $obj->take($limit)->get()->toArray();
        }
        return $result;
    }


    public function newArtistArtworkList($files = '*', $where = [])
    {
        $result = $this->select($files)->where($where)->orderBy('artwork_time', 'desc')->paginate();
        return $result ? $result->toArray() : $result;
    }
}
