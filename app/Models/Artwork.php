<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * App\Models\Artwork
 *
 * @property int $id 自增ID
 * @property int|null $user_id 用户id
 * @property int|null $artist_id 艺术家ID
 * @property int|null $artist_type 艺术家类型1个人2机构
 * @property string|null $artwork_name 作品名
 * @property string|null $artwork_img 作品图
 * @property int|null $category_id 作品分类id
 * @property string|null $artwork_create_time 创作时间
 * @property int|null $material_id 作品材质id
 * @property int|null $theme_id 题材id
 * @property string|null $type_info 作品属性
 * @property string|null $content 作品介绍
 * @property int|null $like_num 点赞量
 * @property int|null $click_num 阅读量
 * @property int|null $comment_num 评论量
 * @property int|null $is_delete 是否删除  0：未删除 1：已删除
 * @property string $created_at 添加时间
 * @property string $updated_at 修改时间
 * @property int|null $status 状态:0,未审核 1,通过 2,不通过
 * @property int $is_sell 是否售卖, 1:售卖, 0:不售卖
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ArtworkPic[] $artworkPic
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Artwork whereArtistId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Artwork whereArtistType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Artwork whereArtworkCreateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Artwork whereArtworkImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Artwork whereArtworkName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Artwork whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Artwork whereClickNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Artwork whereCommentNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Artwork whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Artwork whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Artwork whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Artwork whereIsDelete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Artwork whereIsSell($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Artwork whereLikeNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Artwork whereMaterialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Artwork whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Artwork whereThemeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Artwork whereTypeInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Artwork whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Artwork whereUserId($value)
 * @mixin \Eloquent
 */
class Artwork extends Model
{

    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'artwork';

    public $timestamps = false;

    protected $primaryKey = 'id';

    const IS_SELL_YES = 1;      //售卖作品
    const IS_SELL_NO = 0;       //展示作品
    const IS_SELL_SOLD = 2;     //已售出
    const IS_SELL_TRADING = 3;  //交易中


    /**
     * 当前作品表 数据表
     * 与 作品图片表 数据表关联关系
     */

    public function artworkPic()
    {
        return $this->hasMany('App\Models\ArtworkPic', 'artwork_id', 'id');
    }

    /*
     * 根据ids 获取作品信息
     */
    public function getArtworkByIds($ids)
    {
        $field = ['id', 'artwork_name', 'artwork_img', 'artist_id'];
        return $this::whereIn('id', $ids)
            ->select($field)
            ->get()
            ->toarray();
    }


    public function ArtworkDetail($where)
    {
        $field = [
            '*'
        ];
        return $this::with([
            'artworkPic' => function ($query) {
                $query->select(['id', 'artwork_id', 'picture']);
                $query->orderBy('created_at', 'desc');
            }
        ])
            ->where($where)
            ->select($field)
            ->first();
    }

//    public function getArtworkList($param){
//        return $this->where($param)->select('')->paginate($this->page)->toArray();
//    }

    public function getArtworkList($condition = [], $fields = '*', $order = 'id desc', $take = 0, $category_ids =[])
    {
        $obj = $this::select($fields);
        $obj->where($condition);
        if (!empty($category_ids)) {
            $obj->whereIn('category_id', $category_ids);
        }
        $obj->orderByRaw($order);
        if (empty($take)) {
            $result = $obj->paginate($this->page);
        } else {
            $result = $obj->take($take)->get();
        }
        return $result ? $result->toArray() : $result;
    }

    public function getInfoById($id)
    {
        return $this->where(['id' => $id])->first();
    }


    public function countNumGet($user_id)
    {
        $sql = "SELECT COUNT(*) as artwork_num, SUM(`click_num`) as click_num, max(`created_at`) as 'created_at',";
        //$sql .= " SUM(`like_num`) as like_num,";
        $sql .= " SUM(`comment_num`) as comment_num";
        $sql .= " FROM `artwork`";
        $sql .= " WHERE";
        $sql .= " `is_delete` = '0' AND";
        $sql .= " `user_id` = " . $user_id . " AND";
        $sql .= " status = 1";
        $result = DB::select($sql);
        if (empty($result)) {
            return [
                'artwork_num' => 0,
                'click_num' => 0,
                'like_num' => 0,
                'comment_num' => 0,
                'created_at' => '',
            ];
        }
        return $result[0];
    }

    /**
     * 作品软删除
     * @param $param
     * @param $condition
     * @return mixed
     */
    public function updateInfo($param, $condition)
    {
        return $this->where($condition)->update($param);
    }

    public function artworkFetchByUserId($user_id, $fields = ['id', 'artwork_img'], $take = 4)
    {
        $where = [
            'user_id' => $user_id,
            'status' => 1,
            'is_delete' => 0
        ];
        $query = $this->select($fields);
        $query->where($where);
        $query->orderBy('updated_at', 'desc');
        $query->take($take);
        $result = $query->get();
        return $result ? $result->toArray() : $result;
    }


    public function artworkCountGet($user_id)
    {
        $where = [
            'user_id' => $user_id,
            'status' => 1,  //状态:0,未审核 1,通过 2,不通过
            'is_delete' => 0,
        ];
        return $this->where($where)->count();
    }

    public function getAuthorIdByArtworkId($artwork_id)
    {
        $where = [$this->primaryKey => $artwork_id];
        $artwork_info = $this->where($where)->first();
        if (empty($artwork_info)) {
            return false;
        }
        return $artwork_info['user_id'] ?? 0;
    }

    public function newArtistArtworkList($condition = [], $fields = '*', $order = 'id, desc')
    {
        $obj = $this::select($fields);
        $obj->where($condition);
        $obj->orderBy($order);
        if (!empty($groupBy)) {
            $obj->groupBy($groupBy);
        }
        $result = $obj->paginate($this->page);
        return $result ? $result->toArray() : $result;
    }


    public function editStateById($artworkId, $state)
    {
        if (empty($artworkId)) {
            return false;
        }
        return $this->where(['id' => $artworkId])->update(['is_sell' => $state]);
    }
}
