<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AppBroadcasts
 *
 * @property int $id 主键
 * @property string $title 标题
 * @property string $subtitle 副标题
 * @property string $description 描述
 * @property string $goto_type 跳转动作：(wap/native)
 * @property string $param 参数(wap链接，文章ID，展览ID，……）
 * @property string $msg_class 推广类型: 1资讯、2展览、3活动
 * @property string $state 发送状态: 0未发送、1已发送、2定时发送
 * @property string|null $send_time 发送时间
 * @property int $admin_id 管理员ID
 * @property string $admin_name 管理员账号
 * @property \Carbon\Carbon $updated_at 修改时间
 * @property \Carbon\Carbon $created_at 创建时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppBroadcasts whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppBroadcasts whereAdminName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppBroadcasts whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppBroadcasts whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppBroadcasts whereGotoType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppBroadcasts whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppBroadcasts whereMsgClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppBroadcasts whereParam($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppBroadcasts whereSendTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppBroadcasts whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppBroadcasts whereSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppBroadcasts whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppBroadcasts whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AppBroadcasts extends Model
{
    /**
     * 关联到模型的数据表
     *
     * @var string
     */
    protected $table = 'app_broadcasts';

    /**
     * 获取列表
     *
     * @param array $condition
     * @param array|string $field
     * @param $read_time
     * @return mixed
     */
    public function getList($condition, $field = '*')
    {
        $condition['state'] = 1;
        $query = $this->select($field);
        $query->where($condition);
        //if (!empty($read_time)) {
          //  $query->where('created_at', '>=', $read_time);
        //}
        $query->orderBy('updated_at', 'desc');
        $query->orderBy('created_at', 'desc');
        $result = $query->paginate();
        return $result ? $result->toArray() : $result;
    }

    public function notificationCountGet()
    {
        return $this->count();
    }

    public function notificationNoRead($read_time)
    {
        return $this->where('created_at', '>=', $read_time)->count();
    }
}
