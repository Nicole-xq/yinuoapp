<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ChatMsg
 *
 * @property int $id 记录ID
 * @property int $from_id 会员ID
 * @property string $from_name 会员名
 * @property string $from_ip 发自IP
 * @property int $to_id 接收会员ID
 * @property string $to_name 接收会员名
 * @property string|null $to_msg 消息内容
 * @property int|null $read_state 状态:1为已读,2为未读,默认为2
 * @property int|null $from_delete 发送者是否删除：1是、0否。默认0
 * @property int|null $to_delete 接收者是否删除：1是、0否。默认0
 * @property \Carbon\Carbon|null $updated_at 修改时间
 * @property \Carbon\Carbon|null $created_at 添加时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatMsg whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatMsg whereFromDelete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatMsg whereFromId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatMsg whereFromIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatMsg whereFromName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatMsg whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatMsg whereReadState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatMsg whereToDelete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatMsg whereToId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatMsg whereToMsg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatMsg whereToName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatMsg whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ChatMsg extends Model
{
    protected $table = 'chat_msg';

    public $user_id;
    public $self_id;

    /**
     * 添加对话
     *
     * @param $insert array
     * */
    public function add($insert)
    {
        return $this->insert($insert);
    }

    /**
     * 获取对话列表
     *
     * @param $condition array 条件
     * */
    public function getList($condition, $field = '*', $order = 'created_at desc')
    {
        return $this->where($condition)
            ->where(function ($query) {
                $where = [
                    'from_id' => $this->user_id,
                    'to_id' => $this->self_id,
                    'to_delete' => 0,
                ];
                $orWhere = [
                    'from_id' => $this->self_id,
                    'to_id' => $this->user_id,
                    'from_delete' => 0,
                ];
                $query->where($where)->orWhere($orWhere);
            })
            ->select($field)
            ->orderByRaw($order)
            ->paginate();
    }

    public function modify($condition, $update)
    {
        return $this->where($condition)->update($update);
    }

}
