<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ArticleVote
 *
 * @mixin \Eloquent
 */
class ArticleVote extends Model
{
    /**
     * 关联到模型的数据表
     *
     * @var string
     */
    protected $table = 'article_vote';

    /**
     * 获取投票详情
     *
     * @return mixed
     */
    public function getInfo($condition, $field = '*')
    {
        return self::select($field)->where($condition)->first();
    }
}
