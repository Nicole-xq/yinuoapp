<?php

namespace App\Models\Active;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Active\MiniUser
 *
 * @property int $id 主键ID
 * @property string|null $user_key 用户通用的user_key
 * @property string|null $key 商城用户key
 * @property string|null $invite_code 商城用户邀请码
 * @property string $openid 用户唯一标识
 * @property int $user_id 商城的用户id
 * @property string $union_id
 * @property string $nick_name 微信昵称
 * @property string $avatar_url 用户头像
 * @property int $gender 用户的性别，1:是男性，2:是女性，0:是未知
 * @property string $phone 用户手机号
 * @property string $city 用户所在城市
 * @property string $province 用户所在省份
 * @property string $country 用户所在国家
 * @property string $language 用户的语言
 * @property float $amount 活动金额
 * @property float|null $amount_count 该用户获得的总金额
 * @property int $lottery 剩余抽奖次数
 * @property int $remain_lottery 用户已进行抽奖次数
 * @property int|null $is_mini 0: 仅授权 1: 通过小程序注册用户 2: App注册用户
 * @property string|null $mini_code 小程序的用户标识
 * @property \Carbon\Carbon $created_at 创建时间
 * @property string|null $register_time 用户注册时间
 * @property string|null $last_login_time 最后一次登录时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniUser whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniUser whereAmountCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniUser whereAvatarUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniUser whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniUser whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniUser whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniUser whereInviteCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniUser whereIsMini($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniUser whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniUser whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniUser whereLastLoginTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniUser whereLottery($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniUser whereMiniCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniUser whereNickName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniUser whereOpenid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniUser wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniUser whereProvince($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniUser whereRegisterTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniUser whereRemainLottery($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniUser whereUnionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniUser whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniUser whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniUser whereUserKey($value)
 * @mixin \Eloquent
 */
class MiniUser extends Model
{
    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'mini_user';
}
