<?php

namespace App\Models\Active;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Active\MiniInvite
 *
 * @property int $id 主键ID
 * @property string $invite_id 邀请人小程序唯一标识
 * @property int $register_id 被邀请注册人ID
 * @property string $register_name 注册用户昵称
 * @property string|null $mini_code 小程序的用户标识 邀请人
 * @property int $is_act 0: 未进行操作 1: 进行抽奖加一操作
 * @property int $is_valid 默认有效,还没有参与统计 0,已添加至用户抽奖次数
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniInvite whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniInvite whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniInvite whereInviteId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniInvite whereIsAct($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniInvite whereIsValid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniInvite whereMiniCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniInvite whereRegisterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniInvite whereRegisterName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniInvite whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class MiniInvite extends Model
{
    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'mini_invite';
}
