<?php

namespace App\Models\Active;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Active\MiniLottery
 *
 * @property int $id 主键ID
 * @property string $mini_code 用户唯一标识
 * @property int $user_id 活动用户ID
 * @property string $nick_name
 * @property int $type 1: 红包 2: 提现
 * @property float $win_amount 中奖金额或者提现金额
 * @property \Carbon\Carbon $created_at 抽奖时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniLottery whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniLottery whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniLottery whereMiniCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniLottery whereNickName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniLottery whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniLottery whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniLottery whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniLottery whereWinAmount($value)
 * @mixin \Eloquent
 */
class MiniLottery extends Model
{
    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'mini_lottery';
}