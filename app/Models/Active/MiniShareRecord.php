<?php

namespace App\Models\Active;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Active\MiniShareRecord
 *
 * @property int $id 主键ID
 * @property string|null $mini_code 小程序用户标识
 * @property string|null $nick_name 用户昵称
 * @property int $is_mini 0: 仅授权 1: 通过小程序注册用户 2: App注册用户
 * @property string|null $register_time 分享用户注册时间
 * @property \Carbon\Carbon $created_at 分享创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniShareRecord whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniShareRecord whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniShareRecord whereIsMini($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniShareRecord whereMiniCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniShareRecord whereNickName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniShareRecord whereRegisterTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniShareRecord whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class MiniShareRecord extends Model
{
    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'mini_share_record';
}
