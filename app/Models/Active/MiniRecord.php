<?php

namespace App\Models\Active;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Active\MiniRecord
 *
 * @property int $id 主键ID
 * @property string|null $openid 小程序用户open_id
 * @property string $nick_name nick_name
 * @property int|null $is_invited 0 : 用户自主访问小程序 1: 用户受邀请访问小程序
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniRecord whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniRecord whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniRecord whereIsInvited($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniRecord whereNickName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniRecord whereOpenid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Active\MiniRecord whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class MiniRecord extends Model
{
    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'mini_record';
}
