<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Banner
 *
 * @mixin \Eloquent
 */
class Banner extends Model
{
    // 页面展示数量
    public $take = 8;

    /**
     * 关联到模型的数据表
     *
     * @var string
     */
    protected $table = 'banner';

    /**
     * 获取列表
     *
     * @param array $condition
     * @param array|string $field
     * @return mixed
     */
    public function getList($condition, $field = '*')
    {
        return $this->select($field)
            ->where($condition)
            ->orderBy('sort','asc')
            ->orderBy('created_at','desc')
            ->take($this->take)
            ->get();
    }
}
