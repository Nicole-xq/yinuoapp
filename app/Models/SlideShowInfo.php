<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SlideShowInfo
 *
 * @property int $id
 * @property int $slideshow_id
 * @property string|null $title 标题
 * @property string|null $img 图片
 * @property string|null $url 链接
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SlideShowInfo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SlideShowInfo whereImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SlideShowInfo whereSlideshowId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SlideShowInfo whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SlideShowInfo whereUrl($value)
 * @mixin \Eloquent
 */
class SlideShowInfo extends Model
{
    // 页面展示数量
    public $take = 8;

    /**
     * 关联到模型的数据表
     *
     * @var string
     */
    protected $table = 'slideshow_info';

    /**
     * 获取列表
     *
     * @param array $condition
     * @param array|string $field
     * @return mixed
     */
    public function getList($condition, $field = '*')
    {
        return $this->select($field)
            ->where($condition)
            ->take($this->take)
            ->get();
    }
}
