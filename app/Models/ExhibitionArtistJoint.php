<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ExhibitionArtistJoint
 *
 * @property int|null $artist_id 艺术家ID
 * @property int|null $exhibition_id 会展列表ID
 * @property string|null $created_at 添加时间
 * @property string|null $updated_at 修改时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtistJoint whereArtistId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtistJoint whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtistJoint whereExhibitionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExhibitionArtistJoint whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ExhibitionArtistJoint extends Model
{

    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'exhibition_artist_joint';

    public $timestamps = false;


    /*
     *  根据 会展ID 获取参展艺术家ids 
     */
    public function get_artist_ids_byExhibitionId( $exhibition_id)
    {
        $where = ['exhibition_id' => $exhibition_id];
        return $this->where($where)
            ->lists('artist_id');
    }
}
