<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Address
 *
 * @property int $area_id 索引ID
 * @property string $area_name 地区名称
 * @property int $area_parent_id 地区父ID
 * @property int $area_sort 排序
 * @property int $area_deep 地区深度，从1开始
 * @property string|null $area_region 大区名称
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereAreaDeep($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereAreaName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereAreaParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereAreaRegion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereAreaSort($value)
 * @mixin \Eloquent
 */
class Address extends Model
{

    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'area';


    public function getAddress($pid = 0 )
    {
        $where = [
            'area_parent_id' => $pid
        ];
        return $this::where($where)
            ->get()
            ->toarray();
    }

    public function getInfoById($id){
        return $this->where('area_id',$id)->first();
    }


    public function get_cityId_byCityName($city_name)
    {
        $where = ['area_name' => $city_name];
        return $this::where($where)->lists('area_id');
    }

}
