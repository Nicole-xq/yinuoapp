<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AppointmentSign
 *
 * @property int $id
 * @property int|null $exhibition_id 展览id
 * @property int|null $appointment_id 约展id
 * @property int|null $user_id 报名人id
 * @property int|null $status 状态:0,未审核 1,通过 2,不通过
 * @property int $is_delete 发起人是否删除：0未删，1已删除
 * @property int|null $is_read 是否阅读：1已阅读，0未阅读
 * @property string|null $address 地址信息
 * @property string|null $updated_at 修改时间
 * @property string|null $created_at 添加时间
 * @property-read \App\Models\UserIndex $appointUsers
 * @property-read \App\Models\AppointmentList $appointmentList
 * @property-read \App\Models\Interested $initiatorInterested
 * @property-read \App\Models\Interested $participantsInterested
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppointmentSign whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppointmentSign whereAppointmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppointmentSign whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppointmentSign whereExhibitionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppointmentSign whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppointmentSign whereIsDelete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppointmentSign whereIsRead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppointmentSign whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppointmentSign whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppointmentSign whereUserId($value)
 * @mixin \Eloquent
 */
class AppointmentSign extends Model
{
    //protected $perPage = 10;               //要求取出数据表所有的数据的时候 默认显示取十条


    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'appointment_sign';

    public $timestamps = false;
    public $apListField = '*';

    /**
     * 当前约展报中间表 数据表
     * 与 用户表 数据表关联关系
     */
    public function appointUsers()
    {
        return $this->hasOne('App\Models\UserIndex', 'user_id', 'user_id');
    }

    /**
     * 当前约展报中间表 数据表
     * 与 用户表 数据表关联关系
     */
    public function appointmentList()
    {
        return $this->hasOne(AppointmentList::class, 'id', 'appointment_id');
    }

    /**
     * 当前约展报名表 与 兴趣表
     * 关联参与人
     */
    public function participantsInterested()
    {
        return $this->hasOne(Interested::class, 'participants_user_id', 'user_id');
    }

    /**
     * 当前约展报名表 与 兴趣表
     * 关联发起人
     */
    public function initiatorInterested()
    {
        return $this->hasOne(Interested::class, 'initiator_user_id', 'user_id');
    }

    public function getOne($condition)
    {
        return $this->where($condition)->first();
    }


    /**
     * 和TA约 约展报名记录  查询所有的报名记录
     */

    public function showInfo($appointment_id)
    {
        $where = [
            'appointment_id' => $appointment_id,               //发起人id
            'status'         => 1,                             //约展状态  状态:0,未审核 1,通过 2,不通过
            'is_delete'      => 0                              //约展发起人删除该记录
        ];

        return $this::with([
            'appointUsers' => function ($query) {
                $query->select([
                    'user_id', 'user_sex', 'user_name', 'db', 'user_avatar'
                ]);
            }
        ])
            ->where($where)
            ->paginate();
    }

    /**
     * 和TA约 约展报名记录  查询所有的报名记录 （新接口）
     */
    public function showInfo2($appointment_id)
    {
        $where = [
            'appointment_id' => $appointment_id,               //发起人id
            'status'         => 1,                             //约展状态  状态:0,未审核 1,通过 2,不通过
            'is_delete'      => 0                              //约展发起人删除该记录
        ];

        return $this::with([
            'appointUsers' => function ($query) {
                $query->select([
                    'user_id', 'user_sex', 'user_name', 'user_nickname', 'db', 'user_avatar'
                ]);
            }
        ])
            ->with('initiatorInterested')
            ->where($where)
            ->paginate();
    }

    /**
     * 列表展示
     * */
    public function getList($condition, $field = '*')
    {
        return $this::select($field)
            ->with(['appointmentList' => function ($query) {
                $query->select($this->apListField);
            }])
            ->with('participantsInterested')
            ->where($condition)
            ->paginate();
    }


    public function appointmentNumGet($user_id)
    {
        $where = [
            'user_id' => $user_id,
            'status' => 1,
            'is_delete' => 0,
        ];
        return $this->where($where)->count();
    }

    public function appointmentUnreadGet($user_id)
    {
        $where = [
            'user_id' => $user_id,
            'status' => 1,
            'is_delete' => 0,
            'is_read' => 0,
        ];
        return $this->where($where)->count();
    }
}
