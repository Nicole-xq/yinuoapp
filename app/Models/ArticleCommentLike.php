<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ArticleCommentLike
 *
 * @mixin \Eloquent
 */
class ArticleCommentLike extends Model
{
    /**
     * 关联到模型的数据表
     *
     * @var string
     */
    protected $table = 'article_comment_like';


    /**
     * 获取点赞详情
     *
     * @return mixed
     */
    public function getInfo($condition, $field = '*')
    {
        return self::select($field)->where($condition)->first();
    }

    /**
     * 获取列表
     *
     * @return mixed
     * */
    public function getList($condition, $field = '*')
    {
        $obj = self::select($field);
        if ($condition['in']) {
            foreach ($condition['in'] as $item) {
                $obj->whereIn($item[0], $item[1]);
            }
            unset($condition['in']);
        }
        return $obj->where($condition)->get();
    }

}
