<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ExhibitionLikeLog
 *
 * @mixin \Eloquent
 */
class ExhibitionLikeLog extends Model
{
    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'exhibition_like_log';

    public $timestamps = false;

    public function get_like_status($db_name, $user_id, $exhibition_id)
    {
        $where = [
            'user_id' => $user_id,
            'is_delete' => 0
        ];
        
        return $this->setConnection($db_name)
            ->setTable($this->table)
            ->where($where)
            ->whereIn('exhibition_id', $exhibition_id)
            ->get()
            ->toarray();
    }

}
