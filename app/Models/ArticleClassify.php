<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ArticleClassify
 *
 * @property int $id 自增ID
 * @property int|null $parent_id 上一级ID
 * @property string|null $type_name 类型名称
 * @property int|null $sort 排序
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleClassify whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleClassify whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleClassify whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleClassify whereTypeName($value)
 * @mixin \Eloquent
 */
class ArticleClassify extends Model
{
    /**
     * 关联到模型的数据表
     *
     * @var string
     */
    protected $table = 'article_classify';

    public $timestamps = false;

    /**
     * 查询列表
     * @param array $condition
     * @param array|string $field
     * @return mixed
     */
    public function getList($condition, $field = '*')
    {
        return $this->select($field)->where($condition)->get();
    }

}
