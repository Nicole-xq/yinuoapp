<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ArtworkCategory
 *
 * @property int $id 自增ID
 * @property int|null $parent_id 上级ID
 * @property string|null $name 类型名称
 * @property string|null $type_info 属性名称
 * @property string|null $updated_at 修改时间
 * @property string|null $created_at 添加时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkCategory whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkCategory whereTypeInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArtworkCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ArtworkCategory extends Model
{

    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'artwork_category';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
