<?php
/**
 * Created by Test, 2018/08/30 18:35.
 * @author serpent.
 *
 * Copyright (c) 2018 serpent All rights reserved.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\registrationExhibition
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $mobile
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\registrationExhibition whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\registrationExhibition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\registrationExhibition whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\registrationExhibition whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\registrationExhibition whereUserId($value)
 * @mixin \Eloquent
 */
class registrationExhibition extends Model
{
    protected $table = 'registration_exhibition';
    protected $primaryKey = 'id';

    function getInfo($param){
        return $this->where($param)->first();
    }

    function addInfo($param){
        return $this::insert($param);
    }
}