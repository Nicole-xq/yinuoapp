<?php
namespace App\Rpc\Exception;

use Server\Rpc\Common\Shared\Constant;

/**
 * rpc 服务异常类 辅助
 * Class ServiceException
 * 所有响应 msg 是用户可视的
 * @package App\Rpc\Exception
 */
class ServiceExceptionTools{
    /**
     * @param $code
     * @param null $msg
     * @param array $data
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public static function throwError($code, $msg = NULL, $data = []){
        if($msg === NULL){
            $exceptionMsg = Constant::get('ExceptionMsg');
            if (!empty($exceptionMsg[$code])){
                $msg = $exceptionMsg[$code];
            }else{
                $msg = '调用错误，稍后重试';
            }
        }
        $service_exception = new \Server\Rpc\Common\Shared\ServiceException(['code'=>$code, 'message'=>$msg, 'data'=>$data]);
        throw $service_exception;
    }
}

