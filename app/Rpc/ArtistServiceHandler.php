<?php
/**
 * Created by PhpStorm.
 * User: xuhui
 * Date: 17/5/11
 * Time: 17:14
 */
namespace App\Rpc;

use App\Exceptions\ResponseException;
use App\Models\Artwork;
use Server\Rpc\Service\Artist\ArtistServiceIf;
use Server\Rpc\Common\Shared\ServiceExceptionCode;
use App\Rpc\Exception\ServiceExceptionTools;
use App\Lib\Helpers\LogHelper;

class ArtistServiceHandler extends BaseHandler implements ArtistServiceIf {
    /**
     * 同步作品状态
     *
     * @param string $aId
     * @param string $status
     * @param string $opMsg
     * @return string
     * @throws \Server\Rpc\Common\Shared\ServiceException
     */
    public function syncArtworkStatus($aId, $status, $opMsg = '')
    {
        try{
            return (new Artwork())->editStateById($aId, $status);
        }catch(ResponseException $e){
            //响应异常
            ServiceExceptionTools::throwError(ServiceExceptionCode::QUERY_FAIL, $e->getMessage());
        }catch(\Exception $e){
            $this->_rpcErrorLog(__FUNCTION__, [LogHelper::getEInfo($e), func_get_args()]);
            ServiceExceptionTools::throwError(ServiceExceptionCode::FATAL_ERROR);
        }
    }
}
