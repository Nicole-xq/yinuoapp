<?php
namespace App\Rpc;

class BaseHandler{
    protected function _rpcErrorLog($msg, $context = []){
        \Log::error(get_class($this).'error'.$msg, $context);
    }
}
