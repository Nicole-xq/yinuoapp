<?php 
namespace App\Entity\Constant;
/**
 * 对外的Api响应code 常量集合
 * 这里保留code > 1000 给实际api业务使用
 * @package App\Services\Constant
 */
class ApiResponseCode{
    const SUCCESS = 200;


    const NOT_PERMISSION= 403;
    const NOT_DATA= 406;
    const LIMIT_ACTION = 4104;
    const ARG_INVALID = 200001;
    const OPERATE_FAIL = 200005;
    const NOT_LOGIN = 300013;

    const FAIL = 501;
    //这里兼容原来的400 code
    const FAIL_ERROR = 400;
    //业务类-----------------
//    const APP_NOT_EXIST = 600;

    /**
     * 每个code都必须有msg
     * @var array
     */
    public static $codeMsg = [
        self::SUCCESS => 'success',
        self::NOT_LOGIN => '登录失效',
        self::NOT_PERMISSION => '没有权限',
        self::ARG_INVALID => '参数无效',
        self::NOT_DATA => '查找不到数据',
        self::OPERATE_FAIL => '操作失败',
        self::LIMIT_ACTION => '操作频繁',

        self::FAIL => '未知错误,您可以联系客服处理',//服务器错误
        self::FAIL_ERROR => '操作失败',
    ];

    /**
     * @param $code
     * @return mixed|null
     */
    public static function getMsg($code){
        return isset(self::$codeMsg[$code]) ? self::$codeMsg[$code] : null;
    }

}