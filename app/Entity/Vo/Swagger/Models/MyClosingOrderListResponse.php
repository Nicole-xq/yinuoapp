<?php
/**
 * User: hank
 */

namespace App\Services\Swagger\Models;





/**
 * @SWG\Definition(@SWG\Xml(name="MyClosingOrderListResponse"))
 */
class MyClosingOrderListResponse
{

    /**
     * @SWG\Property(@SWG\Items(ref="#/definitions/MyClosingOrderListResponseListResponse"))
     * @var array
     */
    public $list;
    /**
     * @SWG\Property(example="count|integer(134)")
     * @var integer
     */
    public $count;

    /**
     * @SWG\Property(example="maxPage|integer(134)")
     * @var integer
     */
    public $maxPage;

    /**
     * @SWG\Property(example="nowPage|integer(1)")
     * @var integer
     */
    public $nowPage;

}
/**
 * @SWG\Definition(@SWG\Xml(name="MyClosingOrderListResponseListResponseGoodsListResponse"))
 */
class MyClosingOrderListResponseListResponseGoodsListResponse
{

    /**
     * @SWG\Property(example="goods_id|integer(109937)")
     * @var integer
     */
    public $goods_id;

    /**
     * @SWG\Property(example="goods_name|string(陶瓷世家—青年陶艺家颜晋端手作柴烧单杯)")
     * @var string
     */
    public $goods_name;

    /**
     * @SWG\Property(example="单个商品价|string(68.00)")
     * @var string 单个商品价
     */
    public $goods_price;
    /**
     * @SWG\Property(example="商品图片|string(http://yinuoimg.oss-cn-shanghai.aliyuncs.com/shop/store/goods/58/58_05623545295465637.jpg?x-oss-process=style/360)")
     * @var string
     */
    public $goods_image;

    /**
     * @SWG\Property(example="商品支付总价|string(68.00)")
     * @var string 商品支付总价
     */
    public $goods_pay_price;

    /**
     * @SWG\Property(example="goods_num|integer(1)")
     * @var integer
     */
    public $goods_num;

    /**
     * @SWG\Property(example="艺术品类别id|NULL()")
     * @var integer 艺术品类别id
     */
    public $artwork_category_id;

}
/**
 * @SWG\Definition(@SWG\Xml(name="MyClosingOrderListResponseListResponseFeeSumInfoResponseCategoryInfoResponse"))
 */
class MyClosingOrderListResponseListResponseFeeSumInfoResponseCategoryInfoResponse
{

    /**
     * @SWG\Property(example="总费用|string(34)")
     * @var string 总费用
     */
    public $sum;

    /**
     * @SWG\Property(example="类别名|string(未知)")
     * @var string 类别名
     */
    public $cName;

    /**
     * @SWG\Property(example="费率 百分比|integer(50)")
     * @var integer 费率 百分比
     */
    public $rate;

    /**
     * @SWG\Property(example="类型id|integer(9999999)")
     * @var integer 类型id
     */
    public $artwork_category_id;

}
/**
 * @SWG\Definition(@SWG\Xml(name="MyClosingOrderListResponseListResponseFeeSumInfoResponse"))
 */
class MyClosingOrderListResponseListResponseFeeSumInfoResponse
{

    /**
     * @SWG\Property(example="总费用|string(34)")
     * @var string 总费用
     */
    public $sum;

    /**
     * @SWG\Property(@SWG\Items(ref="#/definitions/MyClosingOrderListResponseListResponseFeeSumInfoResponseCategoryInfoResponse"))
     * @var array 具体类别费用信息
     */
    public $categoryInfo;
}
/**
 * @SWG\Definition(@SWG\Xml(name="MyClosingOrderListResponseListResponse"))
 */
class MyClosingOrderListResponseListResponse
{

    /**
     * @SWG\Property(example="order_sn|integer(9000000003680001)")
     * @var integer
     */
    public $order_sn;

    /**
     * @SWG\Property(example="order_id|integer(532)")
     * @var integer
     */
    public $order_id;

    /**
     * @SWG\Property(example="pay_sn|integer(480564839746283900)")
     * @var integer
     */
    public $pay_sn;

    /**
     * @SWG\Property(example="store_id|integer(276)")
     * @var integer
     */
    public $store_id;

    /**
     * @SWG\Property(example="store_name|string(今日爆款)")
     * @var string
     */
    public $store_name;

    /**
     * @SWG\Property(example="buyer_id|integer(61932)")
     * @var integer
     */
    public $buyer_id;

    /**
     * @SWG\Property(example="buyer_name|string(苏殊)")
     * @var string
     */
    public $buyer_name;

    /**
     * @SWG\Property(example="订单原价 为null时取 order_amount|NULL()")
     * @var string 订单原价 为null时取 order_amount
     */
    public $order_amount_original;

    /**
     * @SWG\Property(example="add_time|integer(1511495746)")
     * @var integer
     */
    public $add_time;

    /**
     * @SWG\Property(example="payment_time|integer(1511495766)")
     * @var integer
     */
    public $payment_time;

    /**
     * @SWG\Property(example="finnshed_time|integer(1513105801)")
     * @var integer
     */
    public $finnshed_time;

    /**
     * @SWG\Property(example="结算时间戳|integer(1513105801)")
     * @var integer 结算时间戳
     */
    public $artist_closing_time;

    /**
     * @SWG\Property(example="goods_amount|string(68.00)")
     * @var string
     */
    public $goods_amount;

    /**
     * @SWG\Property(example="结算状态0.默认状态未结算  10.已结算|integer(0)")
     * @var integer 结算状态0.默认状态未结算  10.已结算
     */
    public $artist_closing_status;

    /**
     * @SWG\Property(example="订单实际支付价|string(68.00)")
     * @var string 订单实际支付价
     */
    public $order_amount;

    /**
     * @SWG\Property(example="order_state|integer(40)")
     * @var integer
     */
    public $order_state;

    /**
     * @SWG\Property(@SWG\Items(ref="#/definitions/MyClosingOrderListResponseListResponseGoodsListResponse"))
     * @var array
     */
    public $goodsList;
    /**
     * @SWG\Property(ref="#/definitions/MyClosingOrderListResponseListResponseFeeSumInfoResponse")
     * @var object
     */
    public $feeSumInfo;

    /**
     * @SWG\Property(example="订单最终结算金额|string(34)")
     * @var string 订单最终结算金额
     */
    public $order_amount_closing;

}