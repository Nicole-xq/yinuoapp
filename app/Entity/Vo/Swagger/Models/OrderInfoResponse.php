<?php
/**
 * User: Mr.Liu
 */

namespace App\Services\Swagger\Models;


/**
 * @SWG\Definition(@SWG\Xml(name="CartListResponse"))
 */
class OrderInfoResponse
{

    /**
     * @SWG\Property(@SWG\Items(ref="#/definitions/OrderInfoResponseInfoResponse"))
     * @var array
     */
    public $order_info;

}
/**
 * @SWG\Definition(@SWG\Xml(name="OrderInfoResponseInfoResponse"))
 */
class OrderInfoResponseInfoResponse
{

    /**
     * @SWG\Property(example="收货人|string(ls)")
     * @var string 收货人
     */
    public $reciver_name;

    /**
     * @SWG\Property(example="收货人手机号|string(17612157766)")
     * @var string 收货人手机号
     */
    public $reciver_phone;

    /**
     * @SWG\Property(example="收货人地址|string(天津 天津市 和平区 awefwaf)")
     * @var string 收货人地址
     */
    public $reciver_addr;

    /**
     * @SWG\Property(example="店铺ID|int(85)")
     * @var string 店铺ID
     */
    public $store_id;

    /**
     * @SWG\Property(example="店铺名称|string(zhidingzhuanqu)")
     * @var string 店铺名称
     */
    public $store_name;


    /**
     * @SWG\Property(example="买家留言|string(我是买家留言--早点发货)")
     * @var string 卖家留言
     */
    public $order_message;


    /**
     * @SWG\Property(example="订单编号|string(2000000000431201)")
     * @var string 订单编号
     */
    public $order_sn;

    /**
     * @SWG\Property(example="下单时间|string(2019-03-23 12:04:37)")
     * @var string 下单时间
     */
    public $add_time;


    /**
     * @SWG\Property(example="支付时间|string('')")
     * @var string 支付时间
     */
    public $payment_time;

    /**
     * @SWG\Property(example="红包金额|double(1.1)")
     * @var double 红包金额
     */
    public $rpt_amount;

    /**
     * @SWG\Property(example="使用的积分金额 先判断is_points|double(1.1)")
     * @var double 使用的积分金额 先判断is_points
     */
    public $points_amount;

    /**
     * @SWG\Property(example="是否使用积分 1.使用了, 2.未使用|integer(1)")
     * @var integer 是否使用积分 1.使用了, 2.未使用
     */
    public $is_points;


    /**
     * @SWG\Property(example="商品总额|string(9999999.00)")
     * @var string 商品总额
     */
    public $order_amount;


    /**
     * @SWG\Property(example="运费金额|string(0.00)")
     * @var string 运费金额
     */
    public $shipping_fee;

    /**
     * @SWG\Property(example="合计|string(9999999.00)")
     * @var string 合计
     */
    public $real_pay_amount;

    /**
     * @SWG\Property(example="是否卖家查看订单-待发货显示发货|string(9999999.00)")
     * @var string 是否卖家查看订单-待发货显示发货
     */
    public $if_seller;

    /**
     * @SWG\Property(@SWG\Items(ref="#/definitions/ShipmentResponseDataResponseInfoResponse"))
     * @var array
     */
    public $shipment;


    /**
     * @SWG\Property(@SWG\Items(ref="#/definitions/GoodsListResponseDataResponseInfoResponse"))
     * @var array
     */
    public $goods_list;

}

/**
 * @SWG\Definition(@SWG\Xml(name="ShipmentResponseDataResponseInfoResponse"))
 */
class ShipmentResponseDataResponseInfoResponse
{
    /**
     * @SWG\Property(example="顶部文字描述|integer(108562)")
     * @var string 顶部文字描述
     */
    public $desc;

    /**
     * @SWG\Property(example="操作时间|integer(108562)")
     * @var string 操作时间
     */
    public $time;

}

/**
 * @SWG\Definition(@SWG\Xml(name="GoodsListResponseDataResponseInfoResponse"))
 */
class GoodsListResponseDataResponseInfoResponse
{
    /**
     * @SWG\Property(example="商品ID|integer(108562)")
     * @var string 商品ID
     */
    public $goods_id;

    /**
     * @SWG\Property(example="商品名称|string(尊享升级测试1（后台最大金额）)")
     * @var string 商品名称
     */
    public $goods_name;

    /**
     * @SWG\Property(example="商品数量|integer(1)")
     * @var string 商品数量
     */
    public $goods_num;

    /**
     * @SWG\Property(example="商品价格|string(9999999.00)")
     * @var string 商品价格
     */
    public $goods_price;

    /**
     * @SWG\Property(example="商品图片url|string(http://yinuoimg.oss-cn-shanghai.aliyuncs.com/shop/store/goods/85/85_05930917287645342.jpeg?x-oss-process=style/240)")
     * @var string 商品图片url
     */
    public $image_url;
}