<?php
/**
 * User: hank
 */

namespace App\Services\Swagger\Models;


/**
 * @SWG\Definition(@SWG\Xml(name="MyOrderPayResponse"))
 */
class MyOrderPayResponse
{

    /**
     * @SWG\Property(example="微信支付参数|string(wxe351fb554403e968)")
     * @var string 微信支付参数
     */
    public $appid;

    /**
     * @SWG\Property(example="微信支付参数|string(8a1c0fb2c318d0fae0ff9dd398a4d3db)")
     * @var string 微信支付参数
     */
    public $noncestr;

    /**
     * @SWG\Property(example="微信支付参数|string(Sign=WXPay)")
     * @var string 微信支付参数
     */
    public $package;

    /**
     * @SWG\Property(example="微信支付参数|string(1487469362)")
     * @var string 微信支付参数
     */
    public $partnerid;

    /**
     * @SWG\Property(example="微信支付参数|string(wx231130285516619931782d553907851746)")
     * @var string 微信支付参数
     */
    public $prepayid;

    /**
     * @SWG\Property(example="微信支付参数|integer(1553311828)")
     * @var integer 微信支付参数
     */
    public $timestamp;

    /**
     * @SWG\Property(example="微信支付签名|string(54CF1794C49CEA154B42E3451DEA6D82)")
     * @var string 微信支付签名
     */
    public $sign;

    /**
     * @SWG\Property(example="支付宝支付签名串|string(alipay_sdk=alipay-sdk-php-xxx&app_id=xxxxxx&biz_content=%7B%22boD............)")
     * @var string 支付宝支付签名串
     */
    public $signStr;

    /**
     * @SWG\Property(example="支付宝code|integer(0)")
     * @var integer 支付宝code
     */
    public $code;

}