<?php
/**
 * User: hank
 */

namespace App\Services\Swagger\Models;


/**
 * @SWG\Definition(@SWG\Xml(name="MpLoginResponse"))
 */
class MpLoginResponse
{

    /**
     * @SWG\Property(example="openid|integer(165)")
     * @var integer total
     */
    public $openid;

    /**
     * @SWG\Property(example="unionid|integer(10)")
     * @var integer
     */
    public $unionid;

    /**
     * @SWG\Property(example="微信昵称|string(棍棍)")
     * @var integer 当前页
     */
    public $nick_name;

    /**
     * @SWG\Property(example="微信头像url|string(www.wbfwe.png)")
     * @var integer
     */
    public $avatar_url;

    /**
     * @SWG\Property(example="用户性别|integer(1)")
     * @var integer
     */
    public $gender;

    /**
     * @SWG\Property(example="城市|NULL()")
     * @var integer
     */
    public $city;

    /**
     * @SWG\Property(example="省份|integer(1)")
     * @var integer
     */
    public $province;

    /**
     * @SWG\Property(example="to|integer(2)")
     * @var integer
     */
    public $language;

    /**
     * @SWG\Property(example="是否需要注册 1不需要 0需要|integer(2)")
     * @var integer
     */
    public $status;

    /**
     * @SWG\Property(example="用户登录标识|integer(2)")
     * @var integer
     */
    public $api_token;

    /**
     * @SWG\Property(example="用户邀请码|integer(2)")
     * @var integer
     */
    public $invite_code;
}