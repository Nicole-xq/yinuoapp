<?php
/**
 * User: Mr.Liu
 */

namespace App\Services\Swagger\Models;


/**
 * @SWG\Definition(@SWG\Xml(name="ArtworkListResponse"))
 */
class ArtworkListResponse
{

    /**
     * @SWG\Property(example="total|integer(1)")
     * @var integer total
     */
    public $total;

    /**
     * @SWG\Property(example="per_page|integer(10)")
     * @var integer
     */
    public $per_page;

    /**
     * @SWG\Property(example="当前页|integer(1)")
     * @var integer 当前页
     */
    public $current_page;

    /**
     * @SWG\Property(example="last_page|integer(1)")
     * @var integer
     */
    public $last_page;

    /**
     * @SWG\Property(example="next_page_url|NULL()")
     * @var NULL
     */
    public $next_page_url;

    /**
     * @SWG\Property(example="prev_page_url|NULL()")
     * @var NULL
     */
    public $prev_page_url;

    /**
     * @SWG\Property(example="from|integer(1)")
     * @var integer
     */
    public $from;

    /**
     * @SWG\Property(example="to|integer(2)")
     * @var integer
     */
    public $to;

    /**
     * @SWG\Property(@SWG\Items(ref="#/definitions/ArtworkListResponseDataResponseInfoResponse"))
     * @var array
     */
    public $data;

}
/**
 * @SWG\Definition(@SWG\Xml(name="ArtworkListResponseDataResponseInfoResponse"))
 */
class ArtworkListResponseDataResponseInfoResponse
{

    /**
     * @SWG\Property(example="1|integer(11)")
     * @var string 作品ID
     */
    public $id;

    /**
     * @SWG\Property(example="117|integer(11)")
     * @var string 作者的用户ID
     */
    public $user_id;

    /**
     * @SWG\Property(example="68|integer(11)")
     * @var string 作者的艺术家ID
     */
    public $artist_id;

    /**
     * @SWG\Property(example="作品名称|string(作品名称)")
     * @var string 作品名称
     */
    public $artwork_name;

    /**
     * @SWG\Property(example="作品图片|string(https://testyinuo2.oss-cn-beijing.aliyuncs.com/head_img/avatar_default.jpg)")
     * @var string 作品图片
     */
    public $artwork_img;

    /**
     * @SWG\Property(example="分类ID|int(11)")
     * @var string 分类ID
     */
    public $category_id;

    /**
     * @SWG\Property(example="作品创建时间|string(2018-09-12 00:00:00)")
     * @var string 作品创建时间
     */
    public $artwork_create_time;

    /**
     * @SWG\Property(example="材质ID|int(11)")
     * @var string 材质ID
     */
    public $materials_id;

    /**
     * @SWG\Property(example="题材ID|int(11)")
     * @var string 题材ID
     */
    public $theme_id;

    /**
     * @SWG\Property(example="属性信息|string()")
     * @var string 属性信息
     */
    public $type_info;

    /**
     * @SWG\Property(example="作品简介|string()")
     * @var string 作品简介
     */
    public $content;

    /**
     * @SWG\Property(example="点赞量|int(11)")
     * @var string 点赞量
     */
    public $like_num;

    /**
     * @SWG\Property(example="阅读量|int(11)")
     * @var string 阅读量
     */
    public $click_num;

    /**
     * @SWG\Property(example="评论量|int(11)")
     * @var string 评论量
     */
    public $comment_num;


    /**
     * @SWG\Property(example="是否删除(1是,0否)|int(11)")
     * @var string 是否删除
     */
    public $is_del;


    /**
     * @SWG\Property(example="创建时间|string(11)")
     * @var string 创建时间
     */
    public $created_at;

    /**
     * @SWG\Property(example="作品状态0展示1售卖2已售出|integer(1)")
     * @var integer 作品状态0展示1售卖2已售出
     */
    public $is_sell;


    /**
     * @SWG\Property(example="更新时间|string(11)")
     * @var string 更新时间
     */
    public $updated_at;
}