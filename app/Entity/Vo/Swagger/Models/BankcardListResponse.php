<?php
/**
 * User: hank
 */

namespace App\Services\Swagger\Models;


/**
 * @SWG\Definition(@SWG\Xml(name="BankcardListResponse"))
 */
class BankcardListResponse
{

    /**
     * @SWG\Property(example="total|integer(1)")
     * @var integer total
     */
    public $total;

    /**
     * @SWG\Property(example="per_page|integer(10)")
     * @var integer
     */
    public $per_page;

    /**
     * @SWG\Property(example="当前页|integer(1)")
     * @var integer 当前页
     */
    public $current_page;

    /**
     * @SWG\Property(example="last_page|integer(1)")
     * @var integer
     */
    public $last_page;

    /**
     * @SWG\Property(example="next_page_url|NULL()")
     * @var NULL
     */
    public $next_page_url;

    /**
     * @SWG\Property(example="prev_page_url|NULL()")
     * @var NULL
     */
    public $prev_page_url;

    /**
     * @SWG\Property(example="from|integer(1)")
     * @var integer
     */
    public $from;

    /**
     * @SWG\Property(example="to|integer(2)")
     * @var integer
     */
    public $to;

    /**
     * @SWG\Property(@SWG\Items(ref="#/definitions/BankcardListResponseDataResponseInfoResponse"))
     * @var array
     */
    public $data;

}
/**
 * @SWG\Definition(@SWG\Xml(name="BankcardListResponseDataResponseInfoResponse"))
 */
class BankcardListResponseDataResponseInfoResponse
{

    /**
     * @SWG\Property(example="银行卡ID|string(55)")
     * @var string 银行卡ID
     */
    public $bankcard_id;

    /**
     * @SWG\Property(example="会员ID|integer(77)")
     * @var integer 会员ID
     */
    public $user_id;

    /**
     * @SWG\Property(example="会员名称|string(13681967371)")
     * @var string 会员名称
     */
    public $user_name;
    /**
     * @SWG\Property(example="真实姓名|string(黄飞鸿)")
     * @var string 真实姓名
     */
    public $true_name;

    /**
     * @SWG\Property(example="卡种|string(银联借记卡)")
     * @var string 卡种
     */
    public $card_type;

    /**
     * @SWG\Property(example="银行名称|string(招商银行)")
     * @var string 银行名称
     */
    public $bank_name;

    /**
     * @SWG\Property(example="银行logo|string(http://auth.apis.la/bank/4_CMB.png)")
     * @var string 银行logo
     */
    public $bank_image;
    /**
     * @SWG\Property(example="背景色|string(red)")
     * @var string 背景色
     */
    public $background;

    /**
     * @SWG\Property(example="默认优选|string(0)")
     * @var string 默认优选
     */
    public $is_default;

    /**
     * @SWG\Property(example="卡号|string(**** **** **** 6733)")
     * @var string 卡号
     */
    public $bank_card_show;

    /**
     * @SWG\Property(example="简称|string(cmbchina_bank)")
     * @var string 简称
     */
    public $bank_card_name;

}