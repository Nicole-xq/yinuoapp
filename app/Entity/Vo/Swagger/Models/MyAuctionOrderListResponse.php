<?php
/**
 * User: hank
 */

namespace App\Services\Swagger\Models;



/**
 * @SWG\Definition(@SWG\Xml(name="MyAuctionOrderListResponse"))
 */
class MyAuctionOrderListResponse
{

    /**
     * @SWG\Property(@SWG\Items(ref="#/definitions/MyAuctionOrderListResponseMargin_order_listResponse"))
     * @var array 列表
     */
    public $list;
    /**
     * @SWG\Property(example="个数|string(66)")
     * @var integer
     */
    public $count;

    /**
     * @SWG\Property(example="maxPage|integer(66)")
     * @var integer
     */
    public $maxPage;

    /**
     * @SWG\Property(example="nowPage|integer(1)")
     * @var integer
     */
    public $nowPage;

}
/**
 * @SWG\Definition(@SWG\Xml(name="MyAuctionOrderListResponseMargin_order_listResponse"))
 */
class MyAuctionOrderListResponseMargin_order_listResponse
{

    /**
     * @SWG\Property(example="保证金订单id|string(2224)")
     * @var string 保证金订单id
     */
    public $margin_id;

    /**
     * @SWG\Property(example="订单编号|string(2000000004643901)")
     * @var string 订单编号
     */
    public $order_sn;
    /**
     * @SWG\Property(example="商品个数|integer(1)")
     * @var integer 商品个数
     */
    public $goods_num;

    /**
     * @SWG\Property(example="拍品id|string(4383)")
     * @var string 拍品id
     */
    public $auction_id;

    /**
     * @SWG\Property(example="拍品图片|string(http://yinuoimg.oss-cn-shanghai.aliyuncs.com/shop/store/goods/0/0_06045129183698205.jpg?x-oss-process=style/360)")
     * @var string 拍品图片
     */
    public $auction_image;

    /**
     * @SWG\Property(example="拍品名|string(金刚菩提)")
     * @var string 拍品名
     */
    public $auction_name;

    /**
     * @SWG\Property(example="保证金金额|string(10.00)")
     * @var string 保证金金额
     */
    public $margin_amount;

    /**
     * @SWG\Property(example="是不是诺币专场|integer(0)")
     * @var integer 是不是诺币专场
     */
    public $is_nb;

    /**
     * @SWG\Property(example="  状态判断   1: 竞拍中  2 : 已拍中  3 : 未拍中|integer(2)")
     * @var integer  状态判断   1: 竞拍中  2 : 已拍中  3 : 未拍中
     */
    public $is_kai_pai;

    /**
     * @SWG\Property(example=" 状态判断   1: 冻结中  2 : 已退款  3 : 已抵扣  4 : 违约扣款|integer(3)")
     * @var integer 状态判断   1: 冻结中  2 : 已退款  3 : 已抵扣  4 : 违约扣款
     */
    public $status;

    /**
     * @SWG\Property(example="order_id|string(1332)")
     * @var string
     */
    public $order_id;

    /**
     * @SWG\Property(example="订单状态  0 : 默认不展示 1: 已取消 2: 未付款 3 : 待发货 4 : 已发货 5 已收货|integer(3)")
     * @var integer 订单状态  0 : 默认不展示 1: 已取消 2: 未付款 3 : 待发货 4 : 已发货 5 已收货
     */
    public $order_state;

    /**
     * @SWG\Property(example="创建时间|string(1551169141)")
     * @var string 创建时间
     */
    public $created_at;

    /**
     * @SWG\Property(example="拍品当前价|string(20.00)")
     * @var string 拍品当前价
     */
    public $current_price;

}