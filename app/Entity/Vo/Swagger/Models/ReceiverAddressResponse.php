<?php
/**
 * User: hank
 */

namespace App\Services\Swagger\Models;


/**
 * @SWG\Definition(@SWG\Xml(name="ReceiverAddressResponse"))
 */
class ReceiverAddressResponse
{

    /**
     * @SWG\Property(example="地址ID|string(531)")
     * @var string 地址ID
     */
    public $address_id;

    /**
     * @SWG\Property(example="会员ID|string(65323)")
     * @var string 会员ID
     */
    public $member_id;

    /**
     * @SWG\Property(example="会员姓名|string(测试)")
     * @var string 会员姓名
     */
    public $true_name;

    /**
     * @SWG\Property(example="地区ID|string(1657)")
     * @var string 地区ID
     */
    public $area_id;

    /**
     * @SWG\Property(example="市级ID|string(126)")
     * @var string 市级ID
     */
    public $city_id;

    /**
     * @SWG\Property(example="地区内容|string(吉林	白山市	抚松县)")
     * @var string 地区内容
     */
    public $area_info;

    /**
     * @SWG\Property(example="地址|string(测试)")
     * @var string 地址
     */
    public $address;

    /**
     * @SWG\Property(example="座机电话|NULL()")
     * @var string 座机电话
     */
    public $tel_phone;

    /**
     * @SWG\Property(example="手机电话|string(13916994003)")
     * @var string 手机电话
     */
    public $mob_phone;

    /**
     * @SWG\Property(example="1.默认收货地址|string(0)")
     * @var string 1.默认收货地址
     */
    public $is_default;

    /**
     * @SWG\Property(example="自提点ID|string(0)")
     * @var string 自提点ID
     */
    public $dlyp_id;

}