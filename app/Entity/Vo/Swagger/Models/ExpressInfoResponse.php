<?php
/**
 * User: Mr.Liu
 */

namespace App\Services\Swagger\Models;


/**
 * @SWG\Definition(@SWG\Xml(name="CartListResponse"))
 */
class ExpressInfoResponse
{

    /**
     * @SWG\Property(@SWG\Items(ref="#/definitions/ExpressInfoResponseInfoResponse"))
     * @var array
     */
    public $express;

}
/**
 * @SWG\Definition(@SWG\Xml(name="ExpressInfoResponseInfoResponse"))
 */
class ExpressInfoResponseInfoResponse
{

    /**
     * @SWG\Property(example="物流时间|string(ls)")
     * @var string 物流时间
     */
    public $time;

    /**
     * @SWG\Property(example="描述字段|string(17612157766)")
     * @var string 描述字段
     */
    public $desc;

    /**
     * @SWG\Property(example="物流单号|string(天津 天津市 和平区 awefwaf)")
     * @var string 物流单
     */
    public $express_no;

    /**
     * @SWG\Property(example="物流公司名称|int(85)")
     * @var string 物流公司名称
     */
    public $express;
}