<?php
/**
 * User: hank
 */

namespace App\Services\Swagger\Models;


/**
 * @SWG\Definition(@SWG\Xml(name="MyExpressResponse"))
 */
class MyExpressResponse
{

    /**
     * @SWG\Property(example="物流id|integer(2)")
     * @var integer 物流id
     */
    public $id;

    /**
     * @SWG\Property(example="物流名称|string(包裹平邮)")
     * @var string 物流名称
     */
    public $e_name;

}