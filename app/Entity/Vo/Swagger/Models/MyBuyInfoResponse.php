<?php
/**
 * User: hank
 */

namespace App\Services\Swagger\Models;


/**
 * @SWG\Definition(@SWG\Xml(name="MyBuyInfoResponse"))
 */
class MyBuyInfoResponse
{

    /**
     * @SWG\Property(@SWG\Items(ref="#/definitions/MyBuyInfoResponseStore_cart_listResponse"))
     * @var array 商店购物车列表
     */
    public $store_cart_list;
    /**
     * @SWG\Property(example="freight_hash|string(lrwPS-p7YcrCTTDxukadLsOVOk_GzbgwahyvFxUTx0d8NZCKjbss-TfznmliDF2e9pg_N_jcT8lCDCyR3k0uNUcuSPm-r9cEzhzFvDsOFjTBd7k8gYciqIsRnsm8RejiKYktr_aEvMzSbFpr5Acf9_dww6h0Xgp1Frzl-NFeSuSX-sfiVjn0FuUxPFJkfCNl_svZiPLvhrt3tBxTyyhWwo7fRTpzyaNqr1nXiJAjun7WuGY)")
     * @var string 将商品ID、运费模板、运费序列化，加密，输出到模板，选择地区AJAX计算运费时作为参数使用
     *
     * buy.logic.php 355 freight_list
     */
    public $freight_hash;

    /**
     * @SWG\Property(ref="#/definitions/MyBuyInfoResponseAddress_infoResponse")
     * @var object 地址信息
     */
    public $address_info;

    /**
     * @SWG\Property(example="是否货到付款|NULL()")
     * @var NULL 是否货到付款
     */
    public $ifshow_offpay;

    /**
     * @SWG\Property(example="增值税加密串|string(hq4eiNKIxn7HbvboBzesaush8iiPPu37Xkq)")
     * @var string 增值税加密串
     */
    public $vat_hash;

    /**
     * @SWG\Property(ref="#/definitions/MyBuyInfoResponseInv_infoResponse")
     * @var object 发票信息
     */
    public $inv_info;

    /**
     * @SWG\Property(example="预存款可用金额|number(71612.82)")
     * @var number 预存款可用金额
     */
    public $available_predeposit;

    /**
     * @SWG\Property(example="可用充值卡余额|number()")
     * @var number 可用充值卡余额
     */
    public $available_rc_balance;

    /**
     * @SWG\Property(example="可用诺币金额|double(10.6)")
     * @var double 可用诺币金额
     */
    public $points_amount;

    /**
     * @SWG\Property(@SWG\Items(ref="#/definitions/MyBuyInfoRptResponse"))
     * @var array 红包列表
     */
    public $rpt_list;
    /**
     * @SWG\Property(@SWG\Items(type="NULL"))
     * @var array 平台折扣信息 无用
     */
    public $zk_list;
    /**
     * @SWG\Property(example="订单总金额|string(150000)")
     * @var string 订单总金额
     */
    public $order_amount;

    /**
     * @SWG\Property(ref="#/definitions/MyBuyInfoRptResponse")
     * @var object 第一个红包的信息
     */
    public $rpt_info;
    /**
     * @SWG\Property(ref="#/definitions/MyBuyInfoResponseAddress_apiResponse")
     * @var object 地区信息
     * 选择不同地区时，异步处理并返回每个店铺总运费以及本地区是否能使用货到付款
     * 如果店铺统一设置了满免运费规则，则运费模板无效
     * 如果店铺未设置满免规则，且使用运费模板，按运费模板计算，如果其中有商品使用相同的运费模板,作为一种商品算运费
     * 如果未找到运费模板，按免运费处理
     * 如果没有使用运费模板，商品运费按快递价格计算，运费不随购买数量增加
     */
    public $address_api;

    /**
     * @SWG\Property(@SWG\Items(type="string"))
     * @var array
     */
    public $store_final_total_list;
    /**
     * @SWG\Property(ref="#/definitions/MyBuyInfoResponseYinuo_store_infoResponse")
     * @var object
     */
    public $yinuo_store_info;

    /**
     * @SWG\Property(example="underline|boolean(1)")
     * @var boolean
     */
    public $underline;

}
/**
 * @SWG\Definition(@SWG\Xml(name="MyBuyInfoResponseStore_cart_listResponseGoods_listResponse"))
 */
class MyBuyInfoResponseStore_cart_listResponseGoods_listResponse
{

    /**
     * @SWG\Property(example="cart_id|string(701)")
     * @var string
     */
    public $cart_id;

    /**
     * @SWG\Property(example="buyer_id|string(20434)")
     * @var string
     */
    public $buyer_id;

    /**
     * @SWG\Property(example="store_id|string(286)")
     * @var string
     */
    public $store_id;

    /**
     * @SWG\Property(example="store_name|string(王祎宁)")
     * @var string
     */
    public $store_name;

    /**
     * @SWG\Property(example="goods_id|string(110791)")
     * @var string
     */
    public $goods_id;

    /**
     * @SWG\Property(example="goods_name|string(无题1)")
     * @var string
     */
    public $goods_name;

    /**
     * @SWG\Property(example="goods_price|string(100000.00)")
     * @var string
     */
    public $goods_price;

    /**
     * @SWG\Property(example="goods_num|string(1)")
     * @var string
     */
    public $goods_num;

    /**
     * @SWG\Property(example="goods_image|string(286_05820297929874817.png)")
     * @var string
     */
    public $goods_image;

    /**
     * @SWG\Property(example="bl_id|string(0)")
     * @var string
     */
    public $bl_id;

    /**
     * @SWG\Property(example="gs_id|string(0)")
     * @var string
     */
    public $gs_id;

    /**
     * @SWG\Property(example="is_special|string(0)")
     * @var string
     */
    public $is_special;

    /**
     * @SWG\Property(example="brand_id|string(0)")
     * @var string
     */
    public $brand_id;

    /**
     * @SWG\Property(example="brand_name|NULL()")
     * @var NULL
     */
    public $brand_name;

    /**
     * @SWG\Property(example="state|boolean(1)")
     * @var boolean
     */
    public $state;

    /**
     * @SWG\Property(example="storage_state|boolean(1)")
     * @var boolean
     */
    public $storage_state;

    /**
     * @SWG\Property(example="goods_commonid|string(110668)")
     * @var string
     */
    public $goods_commonid;

    /**
     * @SWG\Property(example="gc_id|string(303)")
     * @var string
     */
    public $gc_id;

    /**
     * @SWG\Property(example="transport_id|string(0)")
     * @var string
     */
    public $transport_id;

    /**
     * @SWG\Property(example="goods_freight|string(0.00)")
     * @var string
     */
    public $goods_freight;

    /**
     * @SWG\Property(example="goods_trans_v|string(0.00)")
     * @var string
     */
    public $goods_trans_v;

    /**
     * @SWG\Property(example="goods_vat|string(0)")
     * @var string
     */
    public $goods_vat;

    /**
     * @SWG\Property(example="goods_storage|string(1)")
     * @var string
     */
    public $goods_storage;

    /**
     * @SWG\Property(example="goods_storage_alarm|string(0)")
     * @var string
     */
    public $goods_storage_alarm;

    /**
     * @SWG\Property(example="is_fcode|string(0)")
     * @var string
     */
    public $is_fcode;

    /**
     * @SWG\Property(example="have_gift|string(0)")
     * @var string
     */
    public $have_gift;

    /**
     * @SWG\Property(example="groupbuy_info|NULL()")
     * @var NULL
     */
    public $groupbuy_info;

    /**
     * @SWG\Property(example="xianshi_info|NULL()")
     * @var NULL
     */
    public $xianshi_info;

    /**
     * @SWG\Property(example="is_chain|string(0)")
     * @var string
     */
    public $is_chain;

    /**
     * @SWG\Property(example="is_dis|string(0)")
     * @var string
     */
    public $is_dis;

    /**
     * @SWG\Property(example="is_book|string(0)")
     * @var string
     */
    public $is_book;

    /**
     * @SWG\Property(example="book_down_payment|string(0.00)")
     * @var string
     */
    public $book_down_payment;

    /**
     * @SWG\Property(example="book_final_payment|string(0.00)")
     * @var string
     */
    public $book_final_payment;

    /**
     * @SWG\Property(example="book_down_time|string(0)")
     * @var string
     */
    public $book_down_time;

    /**
     * @SWG\Property(@SWG\Items(type="NULL"))
     * @var array
     */
    public $sole_info;
    /**
     * @SWG\Property(@SWG\Items(type="NULL"))
     * @var array
     */
    public $contractlist;
    /**
     * @SWG\Property(example="goods_total|string(100000)")
     * @var string
     */
    public $goods_total;

    /**
     * @SWG\Property(example="goods_image_url|string(http://yinuoimg.oss-cn-shanghai.aliyuncs.com/shop/store/goods/286/286_05820297929874817.png?x-oss-process=style/240)")
     * @var string
     */
    public $goods_image_url;

}
/**
 * @SWG\Definition(@SWG\Xml(name="MyBuyInfoResponseStore_cart_listResponse"))
 */
class MyBuyInfoResponseStore_cart_listResponse
{

    /**
     * @SWG\Property(@SWG\Items(ref="#/definitions/MyBuyInfoResponseStore_cart_listResponseGoods_listResponse"))
     * @var array 本店商品列表
     */
    public $goods_list;
    /**
     * @SWG\Property(example="本店总金额|string(100000)")
     * @var string 本店总金额
     */
    public $store_goods_total;

    /**
     * @SWG\Property(example="store_mansong_rule_list|NULL()")
     * @var NULL
     */
    public $store_mansong_rule_list;

    /**
     * @SWG\Property(@SWG\Items(type="NULL"))
     * @var array 代金券信息
     */
    public $store_voucher_info;
    /**
     * @SWG\Property(@SWG\Items(type="NULL"))
     * @var array 代金券列表
     */
    public $store_voucher_list;
    /**
     * @SWG\Property(example="freight|string(1)")
     * @var string 配送费
     */
    public $freight;

    /**
     * @SWG\Property(example="store_name|string(王祎宁)")
     * @var string
     */
    public $store_name;

}
/**
 * @SWG\Definition(@SWG\Xml(name="MyBuyInfoResponseAddress_infoResponse"))
 */
class MyBuyInfoResponseAddress_infoResponse
{

    /**
     * @SWG\Property(example="address_id|string(501)")
     * @var string
     */
    public $address_id;

    /**
     * @SWG\Property(example="member_id|string(20434)")
     * @var string
     */
    public $member_id;

    /**
     * @SWG\Property(example="true_name|string(测试)")
     * @var string
     */
    public $true_name;

    /**
     * @SWG\Property(example="area_id|string(12)")
     * @var string
     */
    public $area_id;

    /**
     * @SWG\Property(example="city_id|string(186)")
     * @var string
     */
    public $city_id;

    /**
     * @SWG\Property(example="area_info|string(安徽-合肥市-包河区)")
     * @var string
     */
    public $area_info;

    /**
     * @SWG\Property(example="address|string(123)")
     * @var string
     */
    public $address;

    /**
     * @SWG\Property(example="tel_phone|NULL()")
     * @var NULL
     */
    public $tel_phone;

    /**
     * @SWG\Property(example="mob_phone|string(18501703168)")
     * @var string
     */
    public $mob_phone;

    /**
     * @SWG\Property(example="is_default|string(0)")
     * @var string
     */
    public $is_default;

    /**
     * @SWG\Property(example="dlyp_id|string(0)")
     * @var string
     */
    public $dlyp_id;

}
/**
 * @SWG\Definition(@SWG\Xml(name="MyBuyInfoResponseInv_infoResponse"))
 */
class MyBuyInfoResponseInv_infoResponse
{

    /**
     * @SWG\Property(example="content|string(不需要发票)")
     * @var string
     */
    public $content;

}
/**
 * @SWG\Definition(@SWG\Xml(name="MyBuyInfoResponseAddress_apiResponse"))
 */
class MyBuyInfoResponseAddress_apiResponse
{

    /**
     * @SWG\Property(example="state|string(success)")
     * @var string
     */
    public $state;

    /**
     * @SWG\Property(@SWG\Items(type="string"))
     * @var array
     */
    public $content;
    /**
     * @SWG\Property(@SWG\Items(type="NULL"))
     * @var array
     */
    public $no_send_tpl_ids;
    /**
     * @SWG\Property(example="allow_offpay|string(0)")
     * @var string
     */
    public $allow_offpay;

    /**
     * @SWG\Property(@SWG\Items(type="NULL"))
     * @var array
     */
    public $allow_offpay_batch;
    /**
     * @SWG\Property(example="offpay_hash|string(cNhyEurCcjeFHE5qLAhku6pPl2Vw09k3XnRVo5N)")
     * @var string
     */
    public $offpay_hash;

    /**
     * @SWG\Property(example="offpay_hash_batch|string(H89wzMUtnFMMbGxl9xI-mbOfiv150l1)")
     * @var string
     */
    public $offpay_hash_batch;

}
/**
 * @SWG\Definition(@SWG\Xml(name="MyBuyInfoResponseYinuo_store_infoResponse"))
 */
class MyBuyInfoResponseYinuo_store_infoResponse
{

    /**
     * @SWG\Property(example="max_discount|integer(0)")
     * @var integer
     */
    public $max_discount;

    /**
     * @SWG\Property(example="max_discount_rate|string(80%)")
     * @var string
     */
    public $max_discount_rate;

}/**
 * @SWG\Definition(@SWG\Xml(name="MyBuyInfoRptResponse"))
 */
class MyBuyInfoRptResponse
{

    /**
     * @SWG\Property(example="红包id|string(68255)")
     * @var string 红包id
     */
    public $rpacket_id;

    /**
     * @SWG\Property(example="红包面额|string(188)")
     * @var string 红包面额
     */
    public $rpacket_price;

    /**
     * @SWG\Property(example="红包使用时的订单限额|string(189.00)")
     * @var string 红包使用时的订单限额
     */
    public $rpacket_limit;
    /**
     * @SWG\Property(example="红包结束日期|integer(1946271999)")
     * @var integer 红包结束日期
     */
    public $rpacket_end_date;
    /**
     * @SWG\Property(example="红包简介|string(仅供尊享专区使用)")
     * @var string 红包简介
     */
    public $rpacket_desc;

    /**
     * @SWG\Property(example="红包模版编号|string(17)")
     * @var string
     */
    public $rpacket_t_id;

    /**
     * @SWG\Property(example="desc|string(188元红包 有效期至 2019-12-31 消费满189.00可用)")
     * @var string
     */
    public $desc;

}