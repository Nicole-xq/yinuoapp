<?php
/**
 * Created by PhpStorm.
 * User: Hank
 * Date: 2019/3/20 0020
 * Time: 下午 15:21
 */

namespace App\Entity\Vo\Swagger\Models;

/**
 * @SWG\Definition(@SWG\Xml(name="MyBankCardListResponse"))
 */
class MyBankCardListResponse
{
    /**
     * @SWG\Property(example="简写|string(CMB)")
     * @var string 简写
     */
    public $abbr;

    /**
     * @SWG\Property(example="logo|string(http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/CMB.jpg)")
     * @var string
     */
    public $logo;

    /**
     * @SWG\Property(example="银行卡类型|string(银联借记卡)")
     * @var string 银行卡类型
     */
    public $card_type;

    /**
     * @SWG\Property(example="背景orange 橙 red 红 blue 蓝 green 绿 violet 紫|string(red)")
     * @var string 背景orange 橙 red 红 blue 蓝 green 绿 violet 紫
     * http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/bg/bg-blue@2x.png
     * http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/bg/bg-blue@3x.png
     */
    public $background;

    /**
     * @SWG\Property(example="银行名称|string(招商银行)")
     * @var string 银行名称
     */
    public $bank_name;
}