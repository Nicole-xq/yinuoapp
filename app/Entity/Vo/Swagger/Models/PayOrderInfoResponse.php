<?php
/**
 * User: hank
 */

namespace App\Services\Swagger\Models;



/**
 * @SWG\Definition(@SWG\Xml(name="PayOrderInfoResponse"))
 */
class PayOrderInfoResponse
{

    /**
     * @SWG\Property(ref="#/definitions/PayOrderInfoResponsePay_infoResponse")
     * @var object
     */
    public $pay_info;

}
/**
 * @SWG\Definition(@SWG\Xml(name="PayOrderInfoResponsePay_infoResponsePayment_listResponse"))
 */
class PayOrderInfoResponsePay_infoResponsePayment_listResponse
{

    /**
     * @SWG\Property(example="payment_code|string(alipay)")
     * @var string
     */
    public $payment_code;

    /**
     * @SWG\Property(example="payment_name|string(支付宝)")
     * @var string
     */
    public $payment_name;

}
/**
 * @SWG\Definition(@SWG\Xml(name="PayOrderInfoResponsePay_infoResponse"))
 */
class PayOrderInfoResponsePay_infoResponse
{

    /**
     * @SWG\Property(example="pay_amount|string(7200)")
     * @var string
     */
    public $pay_amount;
    /**
     * @SWG\Property(example="最后付款剩余时间秒|integer(1000)")
     * @var integer 最后付款剩余时间秒
     */
    public $order_cancel_day;



    /**
     * @SWG\Property(example="member_available_pd|string(71612.82)")
     * @var string
     */
    public $member_available_pd;

    /**
     * @SWG\Property(example="member_available_rcb|string(0.00)")
     * @var string
     */
    public $member_available_rcb;

    /**
     * @SWG\Property(example="order_type|string(1)")
     * @var string
     */
    public $order_type;

    /**
     * @SWG\Property(example="member_paypwd|boolean(1)")
     * @var boolean
     */
    public $member_paypwd;

    /**
     * @SWG\Property(example="member_points|string(1060)")
     * @var string
     */
    public $member_points;

    /**
     * @SWG\Property(example="points_money|string(10.6)")
     * @var string
     */
    public $points_money;

    /**
     * @SWG\Property(example="pay_sn|string(350606257193943434)")
     * @var string
     */
    public $pay_sn;

    /**
     * @SWG\Property(example="payed_amount|string(0)")
     * @var string
     */
    public $payed_amount;

    /**
     * @SWG\Property(example="member_points_money|string(10.6)")
     * @var string
     */
    public $member_points_money;

    /**
     * @SWG\Property(@SWG\Items(ref="#/definitions/PayOrderInfoResponsePay_infoResponsePayment_listResponse"))
     * @var array
     */
    public $payment_list;
}