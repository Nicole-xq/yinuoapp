<?php
/**
 * User: Mr.Liu
 */

namespace App\Services\Swagger\Models;


/**
 * @SWG\Definition(@SWG\Xml(name="CartListResponse"))
 */
class CartListResponse
{

    /**
     * @SWG\Property(example="购物车数量|integer(11)")
     * @var integer total
     */
    public $cart_count;

    /**
     * @SWG\Property(example="购物车总价|integer(11)")
     * @var integer
     */
    public $sum;

    /**
     * @SWG\Property(@SWG\Items(ref="#/definitions/CartListResponseDataResponseInfoResponse"))
     * @var array
     */
    public $cart_list;

}
/**
 * @SWG\Definition(@SWG\Xml(name="CartListResponseDataResponseInfoResponse"))
 */
class CartListResponseDataResponseInfoResponse
{

    /**
     * @SWG\Property(example="店铺ID|integer(11)")
     * @var string 店铺ID
     */
    public $store_id;

    /**
     * @SWG\Property(example="店铺名称|string(珏·美学艺术馆)")
     * @var string 店铺名称
     */
    public $store_name;

    /**
     * @SWG\Property(@SWG\Items(ref="#/definitions/ShopInfoResponseDataResponseInfoResponse"))
     * @var array
     */
    public $goods;

}

/**
 * @SWG\Definition(@SWG\Xml(name="ShopInfoResponseDataResponseInfoResponse"))
 */
class ShopInfoResponseDataResponseInfoResponse
{
    /**
     * @SWG\Property(example="购物车ID|integer(11)")
     * @var string 购物车ID
     */
    public $cart_id;

    /**
     * @SWG\Property(example="买家ID|integer(11)")
     * @var string 买家ID
     */
    public $buyer_id;

    /**
     * @SWG\Property(example="店铺ID|integer(11)")
     * @var string 店铺ID
     */
    public $store_id;

    /**
     * @SWG\Property(example="店铺名称|string(珏·美学艺术馆)")
     * @var string 店铺名称
     */
    public $store_name;

    /**
     * @SWG\Property(example="商品名称|string(引吭啸傲天地间)")
     * @var string 商品名称
     */
    public $goods_name;

    /**
     * @SWG\Property(example="商品价格|decimal(999999.00)")
     * @var string 购物车ID
     */
    public $goods_price;

    /**
     * @SWG\Property(example="购物车商品数量|integer(1)")
     * @var string 购物车ID
     */
    public $goods_num;

    /**
     * @SWG\Property(example="商品图片名称|string(335_06013402767557491.jpg)")
     * @var string 购物车ID
     */
    public $goods_image;

    /**
     * @SWG\Property(example="商品图片路径|string(http://yinuoimg.oss-cn-shanghai.aliyuncs.com/shop/store/goods/335/335_06013402767557491.jpg?x-oss-process=style/240)")
     * @var string 购物车ID
     */
    public $goods_image_url;

    /**
     * @SWG\Property(example="商品库存量|integer(1)")
     * @var string 商品库存量
     */
    public $goods_storage;

    /**
     * @SWG\Property(example="商品总价格|integer(999999)")
     * @var string 购物车ID
     */
    public $goods_total;

    /**
     * @SWG\Property(example="状态|bool(false)")
     * @var bool 状态
     */
    public $state;

    /**
     * @SWG\Property(example="库存状态|bool(false)")
     * @var bool 库存状态
     */
    public $storage_state;
}