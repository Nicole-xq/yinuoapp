<?php
/**
 * User: hank
 */

namespace App\Services\Swagger\Models;


/**
 * @SWG\Definition(@SWG\Xml(name="ApiMyCenterResponse"))
 */
class ApiMyCenterResponse
{
    /**
     * @SWG\Property(example="银行卡个数|integer(1)")
     * @var integer 银行卡个数
     */
    public $bankcardCount;

    /**
     * @SWG\Property(example="status|integer(3)")
     * @var integer
     */
    public $status;

    /**
     * @SWG\Property(ref="#/definitions/ApiMyCenterResponseUser_infoResponse")
     * @var object 用户信息
     */
    public $user_info;

    /**
     * @SWG\Property(example="关注数目|integer(0)")
     * @var integer 关注数目
     */
    public $follows_num;

    /**
     * @SWG\Property(ref="#/definitions/ApiMyCenterResponseFansResponse")
     * @var object 粉丝信息
     */
    public $fans;

    /**
     * @SWG\Property(ref="#/definitions/ApiMyCenterResponseCommentResponse")
     * @var object 评论信息
     */
    public $comment;

    /**
     * @SWG\Property(example="点赞数目|integer(1)")
     * @var integer 点赞数目
     */
    public $like_num;

    /**
     * @SWG\Property(ref="#/definitions/ApiMyCenterResponseAppointmentResponse")
     * @var object
     */
    public $appointment;

    /**
     * @SWG\Property(ref="#/definitions/ApiMyCenterResponseNotificationResponse")
     * @var object
     */
    public $notification;

}
/**
 * @SWG\Definition(@SWG\Xml(name="ApiMyCenterResponseUser_infoResponse"))
 */
class ApiMyCenterResponseUser_infoResponse
{

    /**
     * @SWG\Property(example="user_id|integer(408)")
     * @var integer
     */
    public $user_id;

    /**
     * @SWG\Property(example="认证类型0未认证1个人2机构|integer(0)")
     * @var integer 认证类型0未认证1个人2机构
     */
    public $authentication_type;

    /**
     * @SWG\Property(example="角色名称|string(申请认证)")
     * @var string 角色名称
     */
    public $artist_type;

    /**
     * @SWG\Property(example="昵称|string(yinuowang)")
     * @var string 昵称
     */
    public $user_nickname;

    /**
     * @SWG\Property(example="头像|string(http://testyinuo2.oss-cn-beijing.aliyuncs.com/head_img/avatar_408.jpg?1551850289)")
     * @var string 头像
     */
    public $user_avatar;

    /**
     * @SWG\Property(example="成员等级logo|string()")
     * @var string 成员等级logo
     */
    public $type_logo;

    /**
     * @SWG\Property(example="会员等级|string()")
     * @var string 会员等级
     */
    public $level;

    /**
     * @SWG\Property(example="会员等级名称|string()")
     * @var string 会员等级名称
     */
    public $level_name;

    /**
     * @SWG\Property(example="会员类型名称|string()")
     * @var string 会员类型名称
     */
    public $member_type_name;

    /**
     * @SWG\Property(example="count_partner|integer(0)")
     * @var integer
     */
    public $count_partner;

    /**
     * @SWG\Property(example="邀请码|string()")
     * @var string 邀请码
     */
    public $invite_code;

    /**
     * @SWG\Property(example="用户性别|integer(1)")
     * @var integer 用户性别
     */
    public $user_sex;

    /**
     * @SWG\Property(example="用户认证艺术家或机构ID|integer(0)")
     * @var integer 用户认证艺术家或机构ID
     */
    public $authentication_id;

}
/**
 * @SWG\Definition(@SWG\Xml(name="ApiMyCenterResponseFansResponse"))
 */
class ApiMyCenterResponseFansResponse
{

    /**
     * @SWG\Property(example="count|integer(0)")
     * @var integer
     */
    public $count;

    /**
     * @SWG\Property(example="is_show|integer(0)")
     * @var integer
     */
    public $is_show;

}
/**
 * @SWG\Definition(@SWG\Xml(name="ApiMyCenterResponseCommentResponse"))
 */
class ApiMyCenterResponseCommentResponse
{

    /**
     * @SWG\Property(example="is_show|integer(0)")
     * @var integer
     */
    public $is_show;

    /**
     * @SWG\Property(example="count|integer(3)")
     * @var integer
     */
    public $count;

}
/**
 * @SWG\Definition(@SWG\Xml(name="ApiMyCenterResponseAppointmentResponse"))
 */
class ApiMyCenterResponseAppointmentResponse
{

    /**
     * @SWG\Property(example="count|integer(0)")
     * @var integer
     */
    public $count;

    /**
     * @SWG\Property(example="is_show|integer(0)")
     * @var integer
     */
    public $is_show;

}
/**
 * @SWG\Definition(@SWG\Xml(name="ApiMyCenterResponseNotificationResponse"))
 */
class ApiMyCenterResponseNotificationResponse
{

    /**
     * @SWG\Property(example="count|integer(8)")
     * @var integer
     */
    public $count;

    /**
     * @SWG\Property(example="is_show|integer(1)")
     * @var integer
     */
    public $is_show;

}