<?php
/**
 * User: hank
 */

namespace App\Services\Swagger\Models;



/**
 * @SWG\Definition(@SWG\Xml(name="MyOrderListResponse"))
 */
class MyOrderListResponse
{

    /**
     * @SWG\Property(@SWG\Items(ref="#/definitions/MyOrderListResponseListResponse"))
     * @var array 列表
     */
    public $list;
    /**
     * @SWG\Property(example="总个数|integer(107)")
     * @var integer 总个数
     */
    public $count;

    /**
     * @SWG\Property(example="最大页码|integer(107)")
     * @var integer 最大页码
     */
    public $maxPage;

    /**
     * @SWG\Property(example="当前页码|integer(1)")
     * @var integer 当前页码
     */
    public $nowPage;

}
/**
 * @SWG\Definition(@SWG\Xml(name="MyOrderListResponseListResponse"))
 */
class MyOrderListResponseListResponse
{

    /**
     * @SWG\Property(example="订单id|integer(266)")
     * @var integer 订单id
     */
    public $order_id;

    /**
     * @SWG\Property(example="订单编号|string(2000000004663501)")
     * @var string 订单编号
     */
    public $order_sn;

    /**
     * @SWG\Property(example="支付单号|string(700607022457700130)")
     * @var string 支付单号
     */
    public $pay_sn;
    /**
     * @SWG\Property(example="订单类型|integer(1)")
     * @var double 订单类型 订单类型1普通订单(默认),2预定订单,3门店自提订单 4拍卖订单
     */
    public $order_type;
    /**
     * @SWG\Property(example="拍卖id|integer(1)")
     * @var double 拍卖id
     */
    public $auction_id;

    /**
     * @SWG\Property(example="支付单号2|NULL()")
     * @var NULL 支付单号2
     */
    public $pay_sn1;

    /**
     * @SWG\Property(example="店铺id|integer(84)")
     * @var integer 店铺id
     */
    public $store_id;

    /**
     * @SWG\Property(example="艺术家id 如果有跳转到艺术家主页|integer(84)")
     * @var integer 艺术家id 如果有跳转到艺术家主页
     */
    public $artist_id;

    /**
     * @SWG\Property(example="店铺名|string(尊享专区)")
     * @var string 店铺名
     */
    public $store_name;

    /**
     * @SWG\Property(example="买家id|integer(368)")
     * @var integer 买家id
     */
    public $buyer_id;

    /**
     * @SWG\Property(example="买家名|string(test_1)")
     * @var string 买家名
     */
    public $buyer_name;

    /**
     * @SWG\Property(example="添加时间|integer(1498718499)")
     * @var integer 添加时间
     */
    public $add_time;

    /**
     * @SWG\Property(example="payment_code|string(predeposit)")
     * @var string
     */
    public $payment_code;

    /**
     * @SWG\Property(example="支付时间|integer(1498718508)")
     * @var integer 支付时间
     */
    public $payment_time;

    /**
     * @SWG\Property(example="完成时间|integer(1498718868)")
     * @var integer 完成时间
     */
    public $finnshed_time;

    /**
     * @SWG\Property(example="商品金额|string(5000.00)")
     * @var string 商品金额
     */
    public $goods_amount;

    /**
     * @SWG\Property(example="订单金额|string(4812.00)")
     * @var string 订单金额
     */
    public $order_amount;
    /**
     * @SWG\Property(example="最后付款剩余时间秒(只有在订单订单状态为10时才出现)|integer(100)")
     * @var integer 最后付款剩余时间秒(只有在订单订单状态为10时才出现)
     */
    public $order_cancel_day;

    /**
     * @SWG\Property(example="订单状态 10.待付款 20.待发货 30.待收货(用户订单中 20.待发货 归到 待收货 ---- 加一个提示正在发货) 40.已完成 0.已取消|integer(40)")
     * @var integer 订单状态 10.待付款 20.待发货 30.待收货(用户订单中 20.待发货 归到 待收货 ---- 加一个提示正在发货) 40.已完成 0.已取消
     */
    public $order_state;

    /**
     * @SWG\Property(@SWG\Items(ref="#/definitions/MyOrderListResponseListResponseGoodsListResponse"))
     * @var array
     */
    public $goodsList;


}
/**
 * @SWG\Definition(@SWG\Xml(name="MyOrderListResponseListResponseGoodsListResponse"))
 */
class MyOrderListResponseListResponseGoodsListResponse
{
    /**
     * @SWG\Property(example="goods_id|integer(1)")
     * @var integer
     */
    public $goods_id;

    /**
     * @SWG\Property(example="goods_name|string(周莹书法系列-49)")
     * @var string
     */
    public $goods_name;

    /**
     * @SWG\Property(example="goods_price|string(1600.00)")
     * @var string
     */
    public $goods_price;
    /**
     * @SWG\Property(example="商品最终价|string(1600.00)")
     * @var string 商品最终价
     */
    public $goods_pay_price;

    /**
     * @SWG\Property(example="商品图片|string(http://yinuoimg.oss-cn-shanghai.aliyuncs.com/shop/store/goods/58/58_05623545295465637.jpg?x-oss-process=style/360)")
     * @var string
     */
    public $goods_image;
    /**
     * @SWG\Property(example="对应的艺术家作品id 如果有跳转到作品详情页|integer(1)")
     * @var integer 对应的艺术家作品id 如果有跳转到作品详情页
     */
    public $artwork_id;

    /**
     * @SWG\Property(example="goods_num|integer(1)")
     * @var integer
     */
    public $goods_num;

}