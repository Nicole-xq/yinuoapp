<?php
/**
 * User: hank
 */

namespace App\Services\Swagger\Models;



/**
 * @SWG\Definition(@SWG\Xml(name="MyArtistClosingLogResponse"))
 */
class MyArtistClosingLogResponse
{

    /**
     * @SWG\Property(@SWG\Items(ref="#/definitions/MyArtistClosingLogResponseListResponse"))
     * @var array
     */
    public $list;
    /**
     * @SWG\Property(example="count|integer(2)")
     * @var integer
     */
    public $count;

    /**
     * @SWG\Property(example="maxPage|integer(1)")
     * @var integer
     */
    public $maxPage;

    /**
     * @SWG\Property(example="nowPage|integer(1)")
     * @var integer
     */
    public $nowPage;

}
/**
 * @SWG\Definition(@SWG\Xml(name="MyArtistClosingLogResponseListResponse"))
 */
class MyArtistClosingLogResponseListResponse
{

    /**
     * @SWG\Property(example="日志id|integer(3574)")
     * @var integer 日志id
     */
    public $lg_id;

    /**
     * @SWG\Property(example="结算金额|string(50.00)")
     * @var string 结算金额
     */
    public $lg_av_amount;

    /**
     * @SWG\Property(example="剩余可用预存款|string(19137.00)")
     * @var string 剩余可用预存款
     */
    public $lg_available_amount;

    /**
     * @SWG\Property(example="结算时间|integer(1553424600)")
     * @var integer 结算时间
     */
    public $lg_add_time;

    /**
     * @SWG\Property(example="备注|string(艺术家订单结算)")
     * @var string 备注
     */
    public $lg_desc;
    /**
     * @SWG\Property(example="结算日志类型 1. 结算 -1.转余额|integer(-1)")
     * @var integer 结算日志类型 1. 结算 -1.转余额
     */
    public $io_type;

    /**
     * @SWG\Property(example="结算单号|string(20190324185000e291bcc33d)")
     * @var string 结算单号
     */
    public $lg_source;

}