<?php

//if (!function_exists("application")) {
//    /**
//     * @return \App\Application
//     */
//    function application() {
//        return \App\Application::one();
//    }
//}
//
///**
// * @return \App\Services\Auth\AdminTokenGuard
// */
//function adminGuard() {
//    return \Auth::guard('admin');
//}


if (!function_exists("application")) {
    /**
     * @return \App\Application
     */
    function application() {
        return \App\Application::one();
    }
}

if (!function_exists("yLog")) {
    /**
     * 日志工具 </br>
     * 使用方法:
     *  1. log($a); log($a, $b) --- 快速 debug
     *  2. log()->error("xxx", $array); ........
     * @param array $context
     * @return \Monolog\Logger
     */
    function yLog(...$context){
        if(empty($context)){
            return app("log");
        }
        //直接使用debug
        $debugB = debug_backtrace();
        $msg = "yLogDebug:". !empty($debugB[0]) ? ($debugB[0]['file']."(".$debugB[0]['line'].")"):"";
        \Log::debug($msg, $context);
    }
}
/**
 * 是否为生产环境
 * @return bool
 */
function isProduction(){
    return env('APP_ENV') == 'production';
}

//开发环境使用 调试工具:
if(!isProduction()){
    function myDump($valS, $isExit = true, $isPre = true){
        call_user_func_array(array('\App\Lib\Helpers\Debug', PHP_SAPI != 'cli' ? 'dump':'cliDump'), func_get_args());
    }
    function fileDump($val){
        call_user_func_array(array('\App\Lib\Helpers\Debug', 'fileDump'), func_get_args());
    }
    function showExceptionTrace(\Exception $e, $isExit = true, $is_return = false, $ispre = true){
        if($is_return){
            ob_start();
        }
        call_user_func_array(array('\App\Lib\Helpers\Debug', (PHP_SAPI != 'cli' && $ispre == true) ? 'showExceptionBacktrace':'cliShowExceptionBacktrace'), func_get_args());
        if($is_return){
            $outStr =  ob_get_contents();
            ob_end_clean();
            return $outStr;
        }
    }
}