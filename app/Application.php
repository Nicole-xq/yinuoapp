<?php namespace App;

use App\Lib\Redis\RedisLock;
use App\Logic\Api\Admin\PermissionApi;
use App\Logic\Api\Admin\PlatformApi;
use App\Logic\Api\ArtistApi;
use App\Logic\Api\FeedbackApi;
use App\Logic\Api\MemberApi;
use App\Logic\Api\ThirdPartyApi;
use App\Logic\Api\ThirdPartyApiApi;
use App\Logic\Api\UserApi;
use App\Logic\Api\UserSafeApi;
use Server\Rpc\Service\ShopNc\ShopServiceIf;
use Server\Rpc\Service\ShopNc\UserServiceIf;

//use Galaxy\Framework\MQ\Lib\MessageCenter;

/**
 *
 * Class Application
 * @package App
 *
 * @property ArtistApi artistApi
 * @property FeedbackApi feedbackApi
 * @property MemberApi memberApi
 *
 * @property ShopServiceIf shopService
 * @property UserServiceIf shopUserService
 * @property RedisLock redisLock
 */
class Application extends \Illuminate\Foundation\Application
{
    /**
     * //返回全局实例
     * @return Application
     */
    public static function one()
    {
        return self::getInstance();
    }
}
