### 用户认证申请记录表添加字段
# 2018-10-09
ALTER TABLE `ynh_authentication_apply` ADD COLUMN `nick_name` VARCHAR(50) DEFAULT NULL COMMENT '个人-认证昵称, 机构-公司简称'
ALTER TABLE `ynh_authentication_apply` ADD COLUMN `phone_num` VARCHAR(15) DEFAULT NULL COMMENT '个人-手机号, 机构-联系电话'
ALTER TABLE `ynh_authentication_apply` ADD COLUMN `user_sex` tinyint(1) DEFAULT 1 COMMENT '个人-用户性别 1为男 2为女, 默认 男'
ALTER TABLE `ynh_authentication_apply` ADD COLUMN `nation` VARCHAR(30) DEFAULT '中国' COMMENT '目前分中国,海外'
ALTER TABLE `ynh_authentication_apply` ADD COLUMN `artwork_pic` VARCHAR(200) DEFAULT NULL COMMENT '个人-作品图片'
ALTER TABLE `ynh_authentication_apply` ADD COLUMN `brand_pic` VARCHAR(50) DEFAULT NULL COMMENT '机构-商标专利图片'
ALTER TABLE `ynh_authentication_apply` ADD COLUMN `authentication_time` datetime DEFAULT NULL COMMENT '认证通过时间'
ALTER TABLE `ynh_authentication_apply` ADD COLUMN `is_recommend` tinyint(1) DEFAULT '0' COMMENT '推荐：1是，0否'


### 用户认证申请记录表修改字段
# 2018-10-09
ALTER TABLE `ynh_authentication_apply` MODIFY COLUMN `real_name` VARCHAR(50) DEFAULT NULL COMMENT '个人-真实姓名, 机构-公司名称'
ALTER TABLE `ynh_authentication_apply` MODIFY COLUMN `trinity_num` VARCHAR(255) DEFAULT NULL COMMENT '机构-三证合一营业执照号'
ALTER TABLE `ynh_authentication_apply` MODIFY COLUMN `document_pic` VARCHAR(50) DEFAULT NULL COMMENT '个人-奖项证书图片, 机构-营业执照图片'
ALTER TABLE `ynh_authentication_apply` MODIFY COLUMN `real_avatar` VARCHAR(50) DEFAULT NULL COMMENT '个人-个人照片'
ALTER TABLE `ynh_authentication_apply` MODIFY COLUMN `identification_num` VARCHAR(50) DEFAULT NULL COMMENT '个人-用户身份证号码, 机构-法人身份证号码'
ALTER TABLE `ynh_authentication_apply` MODIFY COLUMN `real_avatar` VARCHAR(50) DEFAULT NULL COMMENT '个人-用户身份证号码, 机构-法人身份证号码'
ALTER TABLE `ynh_authentication_apply` MODIFY COLUMN `name` VARCHAR(50) DEFAULT NULL COMMENT '机构-法人姓名'

### 创建作品评论表
# 2018-10-09
CREATE TABLE `artwork_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `artwork_id` int(10) unsigned DEFAULT NULL COMMENT '作品ID',
  `author_id` int(10) unsigned DEFAULT NULL COMMENT '作者ID',
  `parent_id` int(10) unsigned DEFAULT '0' COMMENT '上级id',
  `commentator_id` int(10) unsigned DEFAULT NULL COMMENT '评论人ID',
  `commentator_nickname` varchar(255) DEFAULT NULL COMMENT '评论人昵称',
  `accepter_id` int(10) unsigned DEFAULT NULL COMMENT '被评论人ID',
  `accepter_nickname` varchar(255) DEFAULT NULL COMMENT '被评论人昵称',
  `comment_msg` varchar(255) DEFAULT NULL COMMENT '留言信息',
  `state` tinyint(2) DEFAULT '0' COMMENT '状态：1已读，0未读',
  `like_num` int(10) unsigned DEFAULT '0' COMMENT '点赞数量',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `is_delete` tinyint(2) DEFAULT '0' COMMENT '删除：1是，0否',
  `is_examine` tinyint(2) DEFAULT '0' COMMENT '审核：0未审核，1通过，2未通过',
  `is_show` tinyint(2) DEFAULT '1' COMMENT '评论列表是否展示：1都显示，2仅评论人显示，3仅被评论人显示，3都不显示',
  `admin_id` int(11) DEFAULT NULL COMMENT '审核人',
  PRIMARY KEY (`id`),
  KEY `article_id` (`artwork_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='作品评论表';

### 作品表
# 2018-10-09
ALTER TABLE `artwork` ADD COLUMN `status` tinyint(1) DEFAULT 1 COMMENT '状态:0,未审核 1,通过 2,不通过'

#reading time
### 用户表  添加用户阅读系统推送消息时间
# 2018-10-16
ALTER TABLE `user_index` ADD COLUMN `read_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '用户阅读系统推送消息时间'

ALTER TABLE `user_index` ADD COLUMN `fans_num` int(11) DEFAULT 0 COMMENT '用户未阅读粉丝消息条数'

ALTER TABLE `user_index` ADD COLUMN `comment_num` int(11) DEFAULT 0 COMMENT '用户未阅读评论相关消息条数'

ALTER TABLE `user_index` ADD COLUMN `appointment_num` int(11) DEFAULT 0 COMMENT '用户未阅读约展消息条数'

### 艺术家表
# 2018-10-30
ALTER TABLE `artist_personal` ADD COLUMN `artwork_time` timestamp NULL COMMENT '艺术家作品更新时间'

# 2018-11-1
ALTER TABLE `artist_personal` ADD COLUMN `is_recommend` tinyint(1) DEFAULT '0' COMMENT '推荐：1是，0否'
ALTER TABLE `artist_personal` ADD COLUMN `authentication_time` datetime DEFAULT NULL COMMENT '认证通过时间'


### 机构表
# 2018-11-6
ALTER TABLE `artist_organization` ADD COLUMN `phone_num` varchar(15) DEFAULT NULL COMMENT '机构-联系电话'
ALTER TABLE `artist_organization` ADD COLUMN `brand_pic` varchar(255) DEFAULT NULL COMMENT '机构-商标专利图片'
ALTER TABLE `artist_organization` ADD COLUMN `nick_name` varchar(50) DEFAULT NULL COMMENT '机构-公司简称' after `name`


###  文章表修改文章类型
# 2018-11-6
ALTER TABLE `article` MODIFY COLUMN `content` mediumtext DEFAULT NULL COMMENT '正文'



