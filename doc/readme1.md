# `deprecated`

# GMall 服务端开发指南 
(声明：以下抄别人的，有改动)

## 环境搭建

#### 框架需求

- Laravel 5.6
- PHP 7.1.7+
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension

修改 `php.ini` 找到 `disable_functions`，解除以下几个方法的禁用：

- passthru
- exec
- system
- shell_exec
- proc_open
- proc_get_status
- symlink

## 开发准备

#### 安装 Composer 和 NPM/Yarn 依赖包管理工具：

- [Composer](http://docs.phpcomposer.com) 并切换 [中国镜像源](https://pkg.phpcomposer.com)
- 通过 [`NVM`](https://github.com/creationix/nvm) 来安装 `NPM`，或者使用 [Yarn](https://yarnpkg.com)

#### 安装 PECL FileInfo 扩展（如果需要）

```bash
cd ~/soft/lnmp1.4-full/src/php-7.1.7/ext/fileinfo
/usr/local/php/bin/phpize
./configure --with-php-config=/usr/local/php/bin/php-config
make && make install
echo 'extension=fileinfo.so' >> /usr/local/php/etc/php.ini
/etc/init.d/php-fpm reload
```

#### WebServer 配置

本地开发建议配置域名 `local.test.yinuovip.com`，并修改 `/etc/hosts` 增加域名指向 127.0.0.1

Apache Rewrite Rules:

```
Options +FollowSymLinks
RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^ index.php [L]
```

Nginx Rewrite Rules:

```
location / {
    try_files $uri $uri/ /index.php?$query_string;
}
```

#### Git 检出、初始化代码

检出代码后请在项目根目录执行：

```
composer install

chmod -R 777 bootstrap/cache
chmod -R 777 storage
mkdir -m 777 storage/logs/sql
php artisan storage:link
```

## 编码规范

严格遵守 PSR 代码规范 <http://www.php-fig.org/psr/>

- PSR-1 Basic Coding Standard
- PSR-2 Code Style Guide
- PSR-4 Autoloading Standard

## 代码规范辅助检查

拉取代码后，请把 `documents/pre-commit` 复制到 `.git/hooks/` 文件夹里。

项目根目录有两个 `shell` 脚本，大家可以随时执行检查自己代码：

- 代码规范检查 ./phpcs (咱们没有这个)
- 代码规范修正 ./phpcbf (咱们没有这个)

## 开发步骤

我们采用 [GitFlow 工作流](http://blog.jobbole.com/76867/GitFlow)，分支约定：

- master 生产环境
- develop 开发主干
- feature-* 功能分支（主要）
- hot-fix 热修复
- release 测试发布

平时在 feature 分支上开发，开发结束到 gitlab 向 develop 发起合并请求，代码复审后，接受 MR 合并。
develop 分支对应的是开发环境，访问域名是 `develop.test.yinuovip.com`

请 SSH 登录到服务器执行：`(PATH)/pull.sh` 拉取代码到最新。

## 提测步骤

到 gitlab 发起 develop 到 release 的合并请求，代码复审后，接受 MR 合并。
release 分支对应的是测试环境，访问域名是 `git.yinuovip.com`

请 SSH 登录到服务器执行：`(PATH)/pull.sh` 拉取代码到最新。

## 上线步骤

到 gitlab 发起 release 到 master 的合并请求，代码复审后，接受 MR 合并。
master 分支对应的生产环境，访问域名是 `yinuovip.com`

上线脚本登录到 `(PATH)` 执行 `(PATH)/pull.sh`。