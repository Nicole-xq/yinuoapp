# `deprecated`
## 环境搭建
(声明：以下抄别人的，无改动)

#### 框架需求

本项目基于 Laravel 5.5 [文档](https://laravel.com/docs/5.5) + laravel Admin[文档](http://laravel-admin.org/docs/#/)，要求：

- MySQL 5.7
- PHP 7+
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension

### 开发准备

#### 安装 Composer 依赖管理工具：

- [Composer](http://docs.phpcomposer.com) 并切换中国镜像

#### 安装 Supervisord

```bash
easy_install supervisor
echo_supervisord_conf >  /etc/supervisord.conf
```

在文件夹`/etc/supervisord.d`中创建新文件`ai_kaoqin_server.ini`, 内容：

```ini
[program:AI_KaoQin_Server_Horizon]
process_name=%(program_name)s
autostart=true
autorestart=true
redirect_stderr=true
command=/usr/bin/php /home/wwwroot/ai_kaoqin_server/artisan horizon
stdout_logfile=/home/wwwlogs/supervisord_ai_kaoqin_server_horizon.out
```

常用管理命令：

```
/etc/init.d/supervisord start|restart|stop|reload|status
```

#### 计划任务 Crontab

确保 crond 服务启动：

```bash
service crond start
```

增加计划任务 `crontab -e`：

```
# ========== Laravel AI_KaoQin_Prod 所需计划任务 ==========

* * * * * php /home/wwwroot/ai_kaoqin_server/artisan schedule:run >> /dev/null 2>&1

# ========== 全局其它计划任务 ==========

# 定期重启所有队列
0 5 * * * /etc/init.d/supervisord reload
```

#### 检出代码

Git 检出代码后请在项目根目录执行：

```
chmod -R 777 bootstrap/cache
chmod -R 777 storage
mkdir -m 777 storage/app/public/avatar
mkdir -m 777 storage/app/public/sign
php artisan storage:link
```

### 配置 Nginx

将https证书及私钥上传至 '/usr/local/nginx/conf/ssl' 文件夹中

在目标机器新建 Nginx VirtualHost。

DocumentRoot 设为 `/home/wwwroot/ai_kaoqin/public` 目录。

Nginx RewriteRules 为：

```
location / {
    try_files $uri $uri/ /index.php?$query_string;
}
```

Apache `.htaccess` RewriteRules 为：

```
Options +FollowSymLinks
RewriteEngine On

RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^ index.php [L]
```

我用的配置文件 `/usr/local/nginx/conf/vhost/ai_kaoqin.conf` 内容为：

```
server
    {
        listen 443 ssl http2;

        ssl on;
        ssl_certificate /usr/local/nginx/conf/ssl/af.ai.gplqdb.com.crt;
        ssl_certificate_key /usr/local/nginx/conf/ssl/af.ai.gplqdb.com.key;
        ssl_session_timeout 5m;
        ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
        ssl_ciphers AESGCM:ALL:!DH:!EXPORT:!RC4:+HIGH:!MEDIUM:!LOW:!aNULL:!eNULL;
        ssl_prefer_server_ciphers on;

        add_header Strict-Transport-Security max-age=15768000;

        server_name af.ai.gplqdb.com;
        root  /home/wwwroot/ai_kaoqin_server/public;
        include php-laravel.conf;

        access_log  /home/wwwlogs/access_ai_kaoqin_server.log  big_api;
    }

server
    {
        listen 80;
        server_name af.ai.gplqdb.com;
        rewrite ^(.*)$  https://$host$1 permanent;
    }
```

其中包含的新增文件 `/usr/local/nginx/conf/php-laravel.conf` 内容为：

```
index index.html index.htm index.php default.html default.htm default.php;

location /
{
    try_files $uri $uri/ /index.php?$query_string;
}

include none.conf;

location ~ [^/]\.php(/|$)
{
    fastcgi_pass  unix:/tmp/php-cgi.sock;
    fastcgi_index index.php;
    include fastcgi.conf;
    include pathinfo.conf;
}

location ~ .*\.(gif|jpg|jpeg|png|bmp|swf)$
{
    expires      30d;
}

location ~ .*\.(js|css)?$
{
    expires      12h;
}

location = /favicon.ico {
    log_not_found off;
    access_log off;
}
```

修改 `/usr/local/nginx/conf/nginx.conf`，在 ` access_log off;` 这一行后面增加以下内容：

```
        log_format  access  '$remote_addr - $remote_user [$time_local] "$request" '
             '$status $body_bytes_sent "$http_referer" '
             '"$http_user_agent" $http_x_forwarded_for';
```

配置完成，最后重启 Nginx `/etc/init.d/nginx restart` 即可。

另：如果需要Nginx 访问日志记录 RespBody 响应内容，请参考文章：
<http://silverd.cn/2017/01/05/nginx-resp-body.html>

### 安装 OhMyZsh

```
yum install zsh
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```