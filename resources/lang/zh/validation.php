<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => ' :attribute 不是一个合法的链接',
    'after'                => ' :attribute 必须是 :date 之后的时间',
    'alpha'                => ' :attribute 只能是字母.',
    'alpha_dash'           => ' :attribute 只能是字母,数字和分隔符.',
    'alpha_num'            => ' :attribute 只能是字母和数字.',
    'array'                => ' :attribute 必须是数组.',
    'before'               => 'The :attribute must be a date before :date.',
    'between'              => [
        'numeric' => ' :attribute 必须在 :min 和 :max 之间',
        'file'    => ' :attribute 大小必须在 :min 和 :max k 之间.',
        'string'  => ' :attribute 长度必须在 :min 和 :max 之间.',
        'array'   => ' :attribute 数目必须在 :min 和 :max 之间.',
    ],
    'boolean'              => ' :attribute 必须是布尔值.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => ' :attribute 不是合法的时间.',
    'date_format'          => ' :attribute 和 :format 格式不匹配.',
    'different'            => ' :attribute 和 :other 必须不同.',
    'digits'               => ' :attribute 必须是 :digits 个数字.',
    'digits_between'       => ' :attribute  数字个数必须在 :min 和 :max 之间.',
    'email'                => ' :attribute  必须是合法的 email 地址.',
    'filled'               => ' :attribute  字段是必填的.',
    'exists'               => ' :attribute  选择是不合法的.',
    'image'                => ' :attribute 必须是图片.',
    'in'                   => ' :attribute  选择超出了允许的范围.',
    'integer'              => ' :attribute 必须是整数.',
    'ip'                   => ' :attribute 必须是合法的ip地址.',
    'max'                  => [
        'numeric' => ' :attribute 不能大于 :max.',
        'file'    => ' :attribute 大小不能大于 :max k.',
        'string'  => ' :attribute 大小不能多于 :max 个字符.',
        'array'   => ' :attribute 数组大小不能大于 :max .',
    ],
    'mimes'                => ' :attribute 文件类型必须为: :values.',
    'min'                  => [
        'numeric' => ' :attribute  不能小于 :min.',
        'file'    => ' :attribute 大小不能小于 :min k.',
        'string'  => ' :attribute  长度不能小于 :min 字母.',
        'array'   => ' :attribute  至少有 :min 个元素.',
    ],
    'not_in'               => ' :attribute 不在范围之内.',
    'numeric'              => ' :attribute  必须为整数.',
    'regex'                => ' :attribute 格式不符合.',
    'required'             => ':attribute必填',
    'required_if'          => ' 当 :other 是 :value时, :attribute 是必填的',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => '当 :values 未全部选择时 :attribute 字段是必须的.',
    'required_with_all'    => '当 :values 全部选择时 :attribute 字段是必须的.',
    'required_without'     => '当 :values  未选择 :attribute 字段是必须的.',
    'required_without_all' => '  当 :values 全部未选择时 :attribute 字段是必须的.',
    'same'                 => ':attribute 和 :other 必须匹配.',
    'size'                 => [
        'numeric' => ' :attribute 的大小必须为 :size.',
        'file'    => ' :attribute 的大小必须为 :size k.',
        'string'  => ' :attribute 长度必须为 :size .',
        'array'   => ' :attribute 必须包含 :size 个元素.',
    ],
    'string'               => ' :attribute 必须是字符串类型.',
    'timezone'             => ' :attribute 必须是合理的时间区.',
    'unique'               => '该:attribute已被使用',
    'url'                  => ' :attribute 格式不正确.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'phone' => '手机号码',
        'mobile' => '手机号码',
        'code'  => '验证码',
        'password' => '密码',
        'old_password' => '旧密码',
    ],

];
