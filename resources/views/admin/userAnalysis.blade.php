<head>
    <link rel="stylesheet" href="{{ URL::asset('css/double-date.css') }}">
</head>
<form action="{{ url('/admin/mini_user_analysis') }}" method="get">
    <div class="outer clearfix">
        <div class="date date1 fl" id="from">
            <input type="text" class="date-check" name="begin">
        </div>
        <div class="date fr" id="to">
            <input type="text" class="date-check" name="end">
            <input type="submit" value="提交">
        </div>
    </div>
</form>
<?php foreach ($userData as $key => $value) {?>
<div id="container{{$key}}"></div>
<script language="JavaScript">
    $(document).ready(function () {
        var chart = {
            type: 'column'
        };
        var title = {
            text: '{{$value['title']}}'
        };
        var subtitle = {
            text: ''
        };
        var xAxis = {
            categories: [
                <?php foreach ($value['categories'] as $val) {?>
                    '{{$val}}',
                <?php }?>
            ],
            crosshair: true
        };
        var yAxis = {
            min: 0,
            title: {
                text: '{{$value['text']}}'
            }
        };
        var tooltip = {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.0f} {{$value['unit']}}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        };
        var plotOptions = {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        };
        var credits = {
            enabled: false
        };

        var series = [
                <?php foreach($value['series'] as $val){?>
            {
                data: {{json_encode($val['data'])}},
                name: '{{$val['name']}}',
            },
            <?php }?>
        ];

        var json = {};
        json.chart = chart;
        json.title = title;
        json.subtitle = subtitle;
        json.tooltip = tooltip;
        json.xAxis = xAxis;
        json.yAxis = yAxis;
        json.series = series;
        json.plotOptions = plotOptions;
        json.credits = credits;
        $('#container{{$key}}').highcharts(json);
    });
</script>
<?php }?>