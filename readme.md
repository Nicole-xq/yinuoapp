# 项目说明:
- 需要 `php >= 7.1`
- laravel版本为 5.2.45

## l5-swagger 说明 版本 3.x
- ~~Run l5-swagger:publish to publish everything~~
- *Run l5-swagger:publish-config to publish configs (config/l5-swagger.php)*
- *Run l5-swagger:publish-assets to publish swagger-ui to your public folder (public/vendor/l5-swagger)*
- ~~Run l5-swagger:publish-views to publish views (resources/views/vendor/l5-swagger)~~
- Run l5-swagger:generate to generate docs or set generate_always param to true in your config or .env file


## ide-helper使用 2.4
- php artisan ide-helper:generate - phpDoc generation for Laravel Facades
- php artisan ide-helper:models - phpDocs for models (*php artisan ide-helper:models Post*)
- php artisan ide-helper:models "\App\Models\ShopNcOrder"
- php artisan ide-helper:meta - PhpStorm Meta file

## 创建model[https://github.com/ignasbernotas/laravel-model-generator]

    php artisan help make:models
    
    Usage:
      make:models [options]
    
    Options:
          --tables[=TABLES]          Comma separated table names to generate
          --prefix[=PREFIX]          Table Prefix [default: DB::getTablePrefix()]
          --dir[=DIR]                Model directory [default: "Models/"]
          --extends[=EXTENDS]        Parent class [default: "Illuminate\Database\Eloquent\Model"]
          --fillable[=FILLABLE]      Rules for $fillable array columns [default: ""]
          --guarded[=GUARDED]        Rules for $guarded array columns [default: "ends:_guarded"]
          --timestamps[=TIMESTAMPS]  Rules for $timestamps columns [default: "ends:_at"]
      -i, --ignore[=IGNORE]          Ignores the tables you define, separated with ,
      -f, --force[=FORCE]            Force override [default: false]
      -s, --ignoresystem             If you want to ignore system tables.
                                                  Just type --ignoresystem or -s
      -m, --getset                   Defines if you want to generate set and get methods
      -h, --help                     Display this help message
      -q, --quiet                    Do not output any message
      -V, --version                  Display this application version
          --ansi                     Force ANSI output
          --no-ansi                  Disable ANSI output
      -n, --no-interaction           Do not ask any interactive question
          --env[=ENV]                The environment the command should run under.
      -v|vv|vvv, --verbose           Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug
    
    Help:
      Build models from existing schema.

## `问题待解决`
- "mockery/mockery": "0.9.*" 应该放在 require-dev 里 但是项目组Mockery\Exception被引用
- "symfony/css-selector" , "symfony/dom-crawler" 似乎没有使用

------------------

# 上线说明
composer install --no-dev




-----------------------------------------------------------------------

# Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

## Official Documentation
官方文件5.2.45

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

框架的文档可以在LaaFLE网站上找到。

----------

## 初次克隆项目到本地
##### 1、基础类库，使用 `composer` 自动安装。

```
composer install
composer require encore/laravel-admin "1.2.*"
```
##### 2、创建配置文件`.env`
###### 复制项目根目录下`.env.example`文件，或者直接拉去现有的`.env`

```
cp .env.example .env
```

##### 3、生成应用 Key，执行 `Artisan` 即可。
```
php artisan key:generate 

```

##### 4、目录赋权，`storage` 和 `bootstrap/cache` 目录应该是可写的。
```
chmod -R 777 bootstrap/cache
chmod -R 777 storage
```

----------
## 更新代码
##### 5、每次代码发布后，及时更新类库，获取最新的类库。

```angular2html
composer update

##config/app.php 增加配置
Encore\Admin\Providers\AdminServiceProvider::class,
Stevenyangecho\UEditor\UEditorServiceProvider::class,
Jacobcyl\AliOSS\AliOssServiceProvider::class
```