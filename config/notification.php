<?php

return [
    /**
     * 友盟消息推送配置
     * */
    'umeng' => [
        'ios' => [
            'appkey' => env('UMENG_IOS_APP_KEY',''),
            'app_master_secret' => env('UMENG_IOS_APP_MASTER_SECRET',''),
            'production_mode' => env('UMENG_IOS_PRODUCTION_MODE',true),
        ],
        'android' => [
            'appkey' => env('UMENG_ANDROID_APP_KEY',''),
            'app_master_secret' => env('UMENG_ANDROID_MESSAGE_SECRET',''),
            'umeng_message_secret' => env('UMENG_ANDROID_APP_MASTER_SECRET',''),
            'production_mode' => env('UMENG_ANDROID_PRODUCTION_MODE',true),
        ],
    ],
];