<?php


return [

    /*
    |--------------------------------------------------------------------------
    | customized http code
    |--------------------------------------------------------------------------
    |
    | The first number is error type, the second and third number is
    | product type, and it is a specific error code from fourth to
    | sixth.But the success is different.
    |
    */

    'code' => [
        200 => '成功',
        200001 => '缺少必要的参数',
        200002 => '参数格式不正确',
        200004 => '没有找到内容，或已被删除',
        200005 => '操作失败',
        200009 => '没有这篇文章',
        200033 => '该文章没有所属作者 !',
        200034 => '被评论人信息查询出错 !',
        200032 => '没有这篇作品',
        200031 => '该文章不属于你,无权操作',
        200010 => '没有这个评论了',
        200011 => '您已经投过票了',
        200012 => '评论失败了',
        200013 => '评论成功，审核中',
        200014 => '未通过审核的评论，暂不能操作',
        200020 => '服务器内部错误，操作失败！',
        200021 => '您输入的手机号格式错误，请重新输入！',
        200022 => '请输入账号！',
        200023 => '请输入手机号！',
        200024 => '您输入的验证码！',
        200025 => '您输入的验证码错误，请重新输入！',
        200026 => '您输入的验证码已经失效，请重新输入！',
        200027 => '间隔时间倒计时期间不可以重复获取验证码！',
        200028 => '请输入密码！',
        200029 => '密码格式错误，请重新出入！',
        200030 => '您输入的手机号或密码错误，请重新登录！',


        //登录注册
        200050 => '请选择地区',
        200051 => '密码长度6-50',
        200052 => '登录失败',
        200053 => '修改密码不能与最近密码相同',
        300001 => '数据格式错误，操作失败！',
        300002 => '会员信息保存失败！',
        300003 => '用户名或密码错误,请重新输入！',
        300004 => '用户名或手机号错误,请重新输入！',
        300005 => '验证码错误或已过期，请重新输入！',
        300006 => '当前手机号已被注册，请更换其他号码。',
        300007 => '请填写正确手机号',
        300008 => '获取token失败',
        300010 => '登录失败',
        300011 => '帐号被限制',
        300012 => '手机号尚未注册，无需操作！',
        300013 => '请先登录！',
        300014 => '用户信息已过期，请重新登录！',
        300015 => '没有此账号！',
        300016 => '头像上传失败！',
        300017 => '当前手机号已被绑定，请更换其他号码。',
        300018 => '没有找到此用户！',
        300019 => '头像保存失败！',
        300020 => '信息保存失败！',

        //验证码
        200100 => '验证码格式错误',
        200101 => '验证码已失效，请重新获取',
        200102 => '验证码发送失败',
        200103 => '60秒内不可频繁发送',
        200104 => '图片上传失败 !',
        200105 => '该文章审核已通过,不能修改 !',
        200106 => '信息提交状态修改失败 !',
        200107 => '信息提交保存失败 !',
        200108 => '文章修改保存失败 !',
        200109 => '文章删除失败 !',
        200110 => '您不能关注自己 !',
        200111 => '评论删除失败 !',
        200112 => '该评论已不存在 !',

        //艺诺号
        200200 => '您还没有认证艺术家 !',
        200201 => '您的提交正在审核中 !',
        200202 => '您已认证成功,请勿重复认证 !',
        200203 => '艺术家信息查询出错 !',
        200204 => '图片地址错误 !',
        //文章
        503001 => '上传文件的格式不正确',
        503006 => '上传文件的超出大小限制',
        503002 => '同步成功-记录保存失败',
        503003 => '权限错误',
        503004 => '文章保存失败',
        403017 => '临近定时时间不能取消发送任务',
        403018 => '临近定时时间不能修改发送任务',
        403019 => '超过发送时间不能发送',
        403020 => '缺少发表记录ID参数',
        403021 => '该文章已删除, 无法查看 !',
        //SMS
        416001 => '添加成功,审核中,请耐心等待',
        416002 => '签名添加失败',


        503005 => '查询出错了，请联系管理员',

        //约展
        57001 => '约展添加失败 !',
        57002 => '查询出错 !',
        57003 => '你已经发布过该约展了 !',
        57004 => '你已经报名该约展了 !',
        57010 => '没有这个参约啊 ！',
        57011 => '您不能再次操作了 ！',

        //会展
        205001 => '没有内容 !',

		//小程序
		200237 => '本次活动已结束 !',
		200221 => '转余额不能少于18 !',
		200222 => 'json格式,解析错误 !',
		200223 => '服务器出错 !',
        200224 => '请补全用户信息 !',
        200225 => '用户记录保存失败 !',
        200226 => '用户抽奖次数新增失败 !',
        200227 => '请求地址有误 !',
        200228 => 'session_key获取失败 !',
        200229 => '抽奖次数已用完 !',
        200230 => 'access_token获取失败 !',
        200231 => '获取二维码失败 !',
        200232 => '二维码保存失败 !',
        200233 => '验证码发送失败 !',
        200234 => '商城用户注册失败 !',
        200235 => '用户信息获取失败 !',
        200236 => '您的账号还未进行注册 !',
        200238 => '您的微信已绑定账号 !',
        200239 => '微信信息查询出错 !',
        200240 => '提现金额不能大于账户余额 !',

        //认证
        220001 => '用户性别填写错误 !',

        //作品
        221001 => '该作品已删除,无法查看 !',
        221003 => '该作品不存在 !',
        221004 => '该作品不属于你 !',
        221002 => '该评论已被删除 !',
        221006 => '二级评论删除失败 !',
        221005 => '您发的内容包含敏感文字，请修改后再发布 !',

        //图片上传
        223001 => '文件扩展名获取失败 !',
        223002 => '表单请勿重复提交 !',

        //shop 商城购物车
        224001 => '该作品暂未出售 !',
        224002 => '不能购买自己发布的商品 !',
        224003 => '库存不足 !',
        224004 => '该订单不存在 !',
        224005 => '订单价格和初始价格一样 !',
        224006 => '订单价格修改失败 !',
        224007 => '艺术家不存在 !',
        224008 => '该作品未进行出售 !',

        /*
        // 友盟错误码
        1000 => '请求参数没有appkey或为空值',
        1001 => '请求参数没有payload或为非法json',
        1002 => '请求参数payload中, 没有body或为非法json',
        1003 => 'payload.display_type为message时, 请求参数payload.body中, 没有custom字段',
        1004 => '请求参数payload中, 没有display_type或为空值',
        1005 => '请求参数payload.body中, img格式有误, 需以http或https开始',
        1007 => 'payload.body.after_open为go_url时, 请求参数payload.body中, url格式有误, 需以http或https开始',
        1008 => 'payload.display_type为notification时, 请求参数payload.body中, 没有ticker参数',
        1009 => 'payload.display_type为notification时, 请求参数payload.body中, 没有title参数',
        1010 => 'payload.display_type为notification时, 请求参数payload.body中, 没有text参数',
        1014 => 'task_id对应任务没有找到',
        1015 => 'type为unicast或listcast时, 请求参数没有device_tokens或为空值',
        1016 => '请求参数没有type或为空值',
        1019 => '请求参数payload中, display_type值非法',
        1020 => '应用组中尚未添加应用',
        1022 => 'payload.body.after_open为go_url时, 请求参数payload.body中, 没有url参数或为空',
        1024 => 'payload.body.after_open为go_activity时, 请求参数payload.body中, 没有activity或为空值',
        1026 => '请求参数payload中, extra为非法json',
        1027 => '请请求参数payload中, policy为非法json',
        1028 => 'task_id对应任务无法撤销',
        2000 => '该应用已被禁用',
        2002 => '请求参数policy中, start_time必须大于当前时间',
        2003 => '请求参数policy中, expire_time必须大于start_time和当前时间',
        2004 => 'IP白名单尚未添加, 请到网站后台添加您的服务器IP或关闭IP白名单功能',
        2006 => 'Validation token不一致(PS: 此校验方法已废弃, 请采用sign进行校验)',
        2007 => '未对请求进行签名',
        2008 => 'json解析错误',
        2009 => 'type为customizedcast时, 请求参数没有alias、file_id或皆为空值',
        2016 => 'type为groupcast时, 请求参数没有filter或为非法json',
        2017 => '添加tag失败',
        2018 => 'type为filecast时, 请求参数没有file_id或为空值',
        2019 => 'type为filecast时, file_id对应的文件不存在',
        2021 => 'appkey不存在',
        2022 => 'payload长度过长',
        2023 => '文件上传失败, 请稍后重试',
        2025 => '请求参数没有aps或为非法json',
        2027 => '签名不正确',
        2028 => '时间戳已过期',
        2029 => '请求参数没有content或为空值',
        2031 => 'filter格式不正确',
        2032 => '未上传生产证书, 请到Web后台上传',
        2033 => '未上传开发证书, 请到Web后台上传',
        2034 => '证书已过期',
        2035 => '定时任务发送时, 证书已过期',
        2036 => '时间戳格式错误',
        2039 => '请求参数policy中, 时间格式必须是yyyy-MM-dd HH:mm:ss',
        2040 => '请求参数policy中, expire_time不能超过发送时间+7天',
        2046 => '请求参数policy中, start_time不能超过当前时间+7天',
        2047 => 'type为customizedcast时, 请求参数没有alias_type或为空值',
        2048 => 'type值须为unicast、listcast、filecast、broadcast、groupcast、groupcast中的一种',
        2049 => 'type为customizedcast时, 请求参数alias、file_id只可二选一',
        2050 => '发送频率超出限制',
        2052 => '请求参数没有timestamp或为空值',
        2053 => '请求参数没有task_id或为空值',
        2054 => 'IP不在白名单中, 请到网站后台添加您的服务器IP或关闭IP白名单功能',
        5001 => '证书解析bundle id失败, 请重新上传',
        5002 => '请求参数payload中p、d为友盟保留字段',
        5007 => 'certificate_revoked错误',
        5008 => 'certificate_unkown错误',
        5009 => 'handshake_failure错误',
        5010 => '配置使用Token Auth, 但未上传p8证书',
        6001 => '该app未开通服务端tag接口',
        6002 => '内部错误(iOS证书)',
        6003 => '内部错误(数据库)',
        6004 => '内部错误(TFS)',
        */
    ],

];