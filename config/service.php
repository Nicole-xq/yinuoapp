<?php


return [
    //服务监控
//    "zookeeper" => env('ZOOKEEPER', "zoo1.rwh.com:2181,zoo2.rwh.com:2181,zoo3.rwh.com:2181"),

    //服务使用的协议
    "protocol" => 'Thrift\Protocol\TBinaryProtocol',

    //客户端配置
    "clients" => [
        'Server.Rpc.Service.ShopNc.ShopService' => [
            'endpoint' => env('SHOPNC_RPC_URL', 'http://dev.huang.yinuovip.com/src/index.php/rpc'),
        ],
        'Server.Rpc.Service.ShopNc.UserService' => [
            'endpoint' => env('SHOPNC_RPC_URL', 'http://dev.huang.yinuovip.com/src/index.php/rpc'),
        ]

//        'Server.Rpc.Service.Oauth.OauthService' => [
//            'endpoint' => env('I_RPC_URL', 'http://192.168.100.77/rpc'),
//        ],
    ],

    //服务提供者配置
    "providers" => [
        'Server.Rpc.Service.Artist.ArtistService' => [
            'handler' => 'App\Rpc\ArtistServiceHandler',
        ],
//        'Server.Rpc.Service.Oauth.OauthService' => [
//            'handler' => 'App\Rpc\OauthServiceHandler',
//        ],
    ],
];