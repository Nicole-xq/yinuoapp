<?php
/**
 * orange 橙
    red 红
    blue 蓝
    green 绿
    violet 紫
 * http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/bg/bg-blue@2x.png
 * http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/bg/bg-blue@3x.png
 *
 * 银联借记卡
 * 银联贷记卡
 */

return [
    '招商银行'=>[
        'abbr'=> 'CMB',
        'logo'=>'http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/CMB.jpg',
        'card_type'=>'银联借记卡',
        'background'=>'red'
    ],
    '中国建设银行'=>[
        'abbr'=> 'CCB',
        'logo'=>'http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/CCB.jpg',
        'card_type'=>'银联借记卡',
        'background'=>'blue'
    ],
    '交通银行'=>[
        'abbr'=> 'COM',
        'logo'=>'http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/COM.jpg',
        'card_type'=>'银联借记卡',
        'background'=>'blue'
    ],
    '邮政储蓄银行'=>[
        'abbr'=> 'CPG',
        'logo'=>'http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/CPG.jpg',
        'card_type'=>'银联借记卡',
        'background'=>'green'
    ],
    '工商银行'=>[
        'abbr'=> 'ICBC',
        'logo'=>'http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/ICBC.jpg',
        'card_type'=>'银联借记卡',
        'background'=>'red'
    ],
    '农业银行'=>[
        'abbr'=> 'ABC',
        'logo'=>'http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/ABC.jpg',
        'card_type'=>'银联借记卡',
        'background'=>'green'
    ],
    '中国银行'=>[
        'abbr'=> 'BOC',
        'logo'=>'http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/BOC.jpg',
        'card_type'=>'银联借记卡',
        'background'=>'red'
    ],
    '中信银行'=>[
        'abbr'=> 'CITIC',
        'logo'=>'http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/CITIC.jpg',
        'card_type'=>'银联借记卡',
        'background'=>'red'
    ],
    '光大银行'=>[
        'abbr'=> 'CEB',
        'logo'=>'http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/CEB.jpg',
        'card_type'=>'银联借记卡',
        'background'=>'violet'
    ],
    '华夏银行'=>[
        'abbr'=> 'HXB',
        'logo'=>'http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/HXB.jpg',
        'card_type'=>'银联借记卡',
        'background'=>'red'
    ],
    '民生银行'=>[
        'abbr'=> 'CMSB',
        'logo'=>'http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/CMSB.jpg',
        'card_type'=>'银联借记卡',
        'background'=>'green'
    ],
    '广发银行'=>[
        'abbr'=> 'CGB',
        'logo'=>'http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/CGB.jpg',
        'card_type'=>'银联借记卡',
        'background'=>'red'
    ],
    '平安银行'=>[
        'abbr'=> 'SZD',
        'logo'=>'http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/SZD.jpg',
        'card_type'=>'银联借记卡',
        'background'=>'orange'
    ],
    '浦东发展银行'=>[
        'abbr'=> 'SPDB',
        'logo'=>'http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/SPDB.jpg',
        'card_type'=>'银联借记卡',
        'background'=>'blue'
    ],
    '兴业银行'=>[
        'abbr'=> 'CIB',
        'logo'=>'http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/CIB.jpg',
        'card_type'=>'银联借记卡',
        'background'=>'blue'
    ],
    '渤海银行'=>[
        'abbr'=> 'BHYH',
        'logo'=>'http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/BHYH.jpg',
        'card_type'=>'银联借记卡',
        'background'=>'blue'
    ],
    '上海银行'=>[
        'abbr'=> 'BOS',
        'logo'=>'http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/BOS.jpg',
        'card_type'=>'银联借记卡',
        'background'=>'blue'
    ],
    '厦门银行'=>[
        'abbr'=> 'XMYH',
        'logo'=>'http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/XMYH.jpg',
        'card_type'=>'银联借记卡',
        'background'=>'blue'
    ],
    '北京银行'=>[
        'abbr'=> 'BCCB',
        'logo'=>'http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/BCCB.jpg',
        'card_type'=>'银联借记卡',
        'background'=>'red'
    ],
    '南京银行'=>[
        'abbr'=> 'NJYH',
        'logo'=>'http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/NJYH.jpg',
        'card_type'=>'银联借记卡',
        'background'=>'red'
    ],
    '安徽省农村信用社'=>[
        'abbr'=> 'AHNSYH',
        'logo'=>'http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/AHNSYH.jpg',
        'card_type'=>'银联借记卡',
        'background'=>'red'
    ],
    '安徽省农村信用社联合社'=>[
        'abbr'=> 'AHNSYHB',
        'logo'=>'http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/AHNSYHB.jpg',
        'card_type'=>'银联借记卡',
        'background'=>'red'
    ],
    '徽商银行'=>[
        'abbr'=> 'HSYH',
        'logo'=>'http://ynysimg.oss-cn-beijing.aliyuncs.com/manual/bankCardlogo/HSYH.jpg',
        'card_type'=>'银联借记卡',
        'background'=>'red'
    ]
];