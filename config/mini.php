<?php
/**
 * Created by PhpStorm.
 * User: Mr.Liu
 * Date: 2018/8/27 0027
 * Time: 17:04
 */
return [
    //id  prize 奖品 红包金额  v 中奖概率 占总概率的百分比
    //第一次抽奖金额
    'first' => [
        '0' => array('id' => 1, 'prize' => 100, 'v' => 1),
        '1' => array('id' => 2, 'prize' => 50, 'v' => 2),
        '2' => array('id' => 3, 'prize' => 20, 'v' => 3),
        '3' => array('id' => 4, 'prize' => 10, 'v' => 5),
        '4' => array('id' => 5, 'prize' => 5, 'v' => 20),
        '5' => array('id' => 6, 'prize' => 3, 'v' => 50),
        '6' => array('id' => 7, 'prize' => 2, 'v' => 100),
        '7' => array('id' => 8, 'prize' => 1, 'v' => 320),
        '8' => array('id' => 9, 'prize' => 0.5, 'v' => 490),
    ],
    //超过15的金额
    'section' => [
        '0' => array('id' => 1, 'prize' => 5, 'v' => 1),
        '1' => array('id' => 2, 'prize' => 3, 'v' => 2),
        '2' => array('id' => 3, 'prize' => 2, 'v' => 3),
        '3' => array('id' => 4, 'prize' => 1, 'v' => 15),
        '4' => array('id' => 5, 'prize' => 0.5, 'v' => 39),
        '5' => array('id' => 6, 'prize' => 0, 'v' => 40),
    ],
    //第一次之后抽奖金额
    'many' => [
        '0' => array('id' => 1, 'prize' => 100, 'v' => 1),
        '1' => array('id' => 2, 'prize' => 50, 'v' => 2),
        '2' => array('id' => 3, 'prize' => 20, 'v' => 3),
        '3' => array('id' => 4, 'prize' => 10, 'v' => 5),
        '4' => array('id' => 5, 'prize' => 5, 'v' => 10),
        '5' => array('id' => 6, 'prize' => 3, 'v' => 30),
        '6' => array('id' => 7, 'prize' => 2, 'v' => 80),
        '7' => array('id' => 8, 'prize' => 1, 'v' => 220),
        '8' => array('id' => 9, 'prize' => 0.5, 'v' => 450),
        '9' => array('id' => 10, 'prize' => 0, 'v' => 200),
    ],
    'inviteNum' => 10,
    'names' => "曾 经 跟 闺 蜜 不 醒 们 说 高 考 后 去 疯 去 通 宵 三 长 王 万 青 山 半 城 一 陌 宁 星 相 倒 不 想 动 梦",
];