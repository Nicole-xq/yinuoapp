<?php
/**
 * Created by PhpStorm.
 * User: fanxi
 * Date: 2017/4/19
 * Time: 下午7:52
 */

return [
    /**
     * log 记录类型
     */
    'level' => env("LOG_LEVEL", Galaxy\Framework\Contracts\Service\Log::INFO),


    'path' => env("LOG_FILE_PATH", "/tmp/galaxy/"),

    /**
     * 日志文件名称
     */
    'name' => env("APP_NAME", "app"),

    /**
     * 监控目录
     */
    "monitor" => env("LOG_MONITOR_PATH", "/tmp/galaxy"),
];