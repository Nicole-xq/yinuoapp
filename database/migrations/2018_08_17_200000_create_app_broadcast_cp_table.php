<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppBroadcastCpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_broadcasts', function (Blueprint $table) {
            $table->increments('id')
                ->comment("主键");
            $table->string('title', 255)
                ->comment('标题');
            $table->string('subtitle', 255)
                ->comment('副标题');
            $table->text('description')
                ->comment('描述');
            $table->string('goto_type', 20)
                ->comment('跳转动作：(wap/native)');
            $table->string('param', 255)
                ->comment('参数(wap链接，文章ID，展览ID，……）');
            $table->string('msg_class')
                ->comment('推广类型: 1资讯、2展览、3活动');
            $table->string('state')
                ->comment('发送状态: 0未发送、1已发送、2定时发送');
            $table->timestamp('send_time')->nullable()
                ->comment('发送时间');
            $table->integer('admin_id')
                ->comment('管理员ID');
            $table->string('admin_name',255)
                ->comment('管理员账号');
            $table->timestamp('updated_at')
                ->comment('修改时间');
            $table->timestamp('created_at')
                ->comment('创建时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('app_broadcasts');
    }
}
